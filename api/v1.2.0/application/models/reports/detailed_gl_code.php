<?php
require_once("report.php");
class Detailed_gl_code extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array(
			'department' => array(
				array('data'=>lang('reports_dept').'/'.lang('reports_cat').'/'.lang('reports_subcat'), 'align'=>'left'),
				array('data'=>lang('reports_quantity'), 'align'=> 'right'), 
				array('data'=>lang('reports_subtotal'), 'align'=> 'right'), 
				array('data'=>lang('reports_tax'), 'align'=> 'right'), 
				array('data'=>lang('reports_total'), 'align'=> 'right'), 
				array('data'=>lang('reports_cost'), 'align'=> 'right'), 
				array('data'=>lang('reports_profit'), 'align'=> 'right')
            ),
            'category' => array(
                array('data'=>lang('reports_category'), 'align'=> 'left'), 
				array('data'=>lang('reports_subtotal'), 'align'=> 'right'), 
				array('data'=>lang('reports_tax'), 'align'=> 'right'), 
				array('data'=>lang('reports_total'), 'align'=> 'right'), 
				array('data'=>lang('reports_profit'), 'align'=> 'right')
            ),
            'subcategory' => array(
                array('data'=>lang('reports_subcategory'), 'align'=> 'left'), 
				array('data'=>lang('reports_subtotal'), 'align'=> 'right'), 
				array('data'=>lang('reports_tax'), 'align'=> 'right'), 
				array('data'=>lang('reports_total'), 'align'=> 'right'), 
				array('data'=>lang('reports_profit'), 'align'=> 'right')
            )
        );		
	}
	
	public function getData_2()
	{
		$this->db->select('department, sum(subtotal) as subtotal, sum(quantity_purchased) as quantity, sum(total) as total, sum(item_cost_price * quantity_purchased) as cost, sum(tax) as tax, sum(profit) as profit');
		//$this->db->select('sale_id, sale_date, sum(quantity_purchased) as items_purchased, CONCAT(employee.first_name," ",employee.last_name) as employee_name, CONCAT(customer.first_name," ",customer.last_name) as customer_name, sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax, sum(profit) as profit, payment_type, comment', false);
		$this->db->from('sales_items_temp');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		if ($this->params['department'] != 'all')
			$this->db->where('department = "'.$this->params['department'].'"');
		if ($this->params['terminal'] != 'all')
			$this->db->where('terminal_id = "'.$this->params['terminal'].'"');
		$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);
		$this->db->group_by('department');
		$this->db->order_by('department');
		$data = array();
		$data['dept_summary'] = $this->db->get()->result_array();
		$data['details'] = array();
		foreach($data['dept_summary'] as $dept_key=>$dept_value)
		{
	        $this->db->select('department, category, subcategory, sum(subtotal) as subtotal, sum(item_cost_price * quantity_purchased) as cost, sum(quantity_purchased) as quantity, sum(total) as total, sum(tax) as tax, sum(profit) as profit');
			//$this->db->select('sale_id, sale_date, sum(quantity_purchased) as items_purchased, CONCAT(employee.first_name," ",employee.last_name) as employee_name, CONCAT(customer.first_name," ",customer.last_name) as customer_name, sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax, sum(profit) as profit, payment_type, comment', false);
			$this->db->from('sales_items_temp');
			if ($this->params['sale_type'] == 'sales')
			{
				$this->db->where('quantity_purchased > 0');
			}
			elseif ($this->params['sale_type'] == 'returns')
			{
				$this->db->where('quantity_purchased < 0');
			}
			$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);
			$this->db->where('department', $dept_value['department']);
			$this->db->group_by('department, category');
			$this->db->order_by('department, category');
	
			$data['summary'][$dept_key] = $this->db->get()->result_array();
			$data['details'][$dept_key] = array();
		
			foreach($data['summary'][$dept_key] as $key=>$value)
			{
				//if (!$this->permissions->is_super_admin())
	                          //  $this->db->where('course_id', $this->session->userdata('course_id'));
	            $this->db->select('subcategory, sum(subtotal) as subtotal, sum(total) as total, sum(item_cost_price * quantity_purchased) as cost, sum(quantity_purchased) as quantity, sum(tax) as tax, sum(profit) as profit');
				//$this->db->select('item_number, item_kit_number, items.name as item_name, item_kits.name as item_kit_name, sales_items_temp.category, quantity_purchased, serialnumber, sales_items_temp.description, subtotal,total, tax, profit, discount_percent');
				$this->db->from('sales_items_temp');
				if ($this->params['sale_type'] == 'sales')
				{
					$this->db->where('quantity_purchased > 0');
				}
				elseif ($this->params['sale_type'] == 'returns')
				{
					$this->db->where('quantity_purchased < 0');
				}
				$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);
				$this->db->where('department', $dept_value['department']);
				$this->db->where('category', $value['category']);
				$this->db->group_by('department, category, subcategory');
				$this->db->order_by('department, category, subcategory');
				$data['details'][$dept_key][$key] = $this->db->get()->result_array();
				$data['items'][$dept_key][$key] = array();
				
				foreach($data['details'][$dept_key][$key] as $dkey=>$dvalue) 
				{
					$this->db->select("name, description, item_number, sum(subtotal) as subtotal, sum(total) as total, sum(item_cost_price * quantity_purchased) as cost, sum(quantity_purchased) as quantity, sum(tax) as tax, sum(profit) as profit");
					//$this->db->select('item_number, item_kit_number, items.name as item_name, item_kits.name as item_kit_name, sales_items_temp.category, quantity_purchased, serialnumber, sales_items_temp.description, subtotal,total, tax, profit, discount_percent');
					$this->db->from('sales_items_temp');
					if ($this->params['sale_type'] == 'sales')
					{
						$this->db->where('quantity_purchased > 0');
					}
					elseif ($this->params['sale_type'] == 'returns')
					{
						$this->db->where('quantity_purchased < 0');
					}
					$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);
					$this->db->where('department', $dept_value['department']);
					$this->db->where('category', $value['category']);
					$this->db->where('subcategory', $dvalue['subcategory']);
					$this->db->group_by('department, category, subcategory, item_id');
					$this->db->order_by('department, category, subcategory');
				
					$data['items'][$dept_key][$key][$dkey] = $this->db->get()->result_array();
				}
			}
		}

		return $data;
	}

	public function getData()
	{
		//$this->db->from('sales_items_temp');
		//print_r($this->db->get()->result_array());
		//echo $this->db->last_query();
		$sale_type = '';
		if ($this->params['sale_type'] != 'all')
		{
			if ($this->params['sale_type'] == 'returns')
				$sale_type = ' AND quantity_purchased < 0 ';
			else
				$sale_type = ' AND quantity_purchased > 0 ';
		}
		$department = $terminal = '';
		if ($this->params['department'] != 'all')
			$deparment = ' AND department = "'.$this->params['department'].'" ';
		if ($this->params['terminal'] != 'all')
			$terminal = ' AND terminal_id = "'.$this->params['terminal'].'" ';
		//echo $this->db->last_query();
		$dq = $this->db->query("SELECT department, sum(subtotal) as subtotal, sum(quantity_purchased) as quantity, sum(total) as total, sum(item_cost_price * quantity_purchased) as cost, sum(tax) as tax, sum(profit) as profit, sum(CASE WHEN payment_type LIKE '%Cash%' THEN total ELSE 0 END) AS cash, sum(CASE WHEN payment_type LIKE '%Credit Card%' OR payment_type LIKE '%AMEX%' OR payment_type LIKE '%VISA%' OR payment_type LIKE '%M/C%' OR payment_type LIKE '%DCVR%' THEN total ELSE 0 END) AS credit
							FROM ".$this->db->dbprefix('sales_items_temp')."
							WHERE ".$this->db->dbprefix('sales_items_temp').".deleted = 0
							$sale_type
							$deparment
							$terminal
							GROUP BY department
							ORDER BY department",
							FALSE,
							TRUE,
							TRUE);
		$data = array();
		//echo $this->db->last_query();
		$data['dept_summary'] = $dq->result_array();
		$data['details'] = array();
		//echo 'get data';
		foreach($data['dept_summary'] as $dept_key=>$dept_value)
		{
			$cq = $this->db->query("SELECT department, category, subcategory, sum(subtotal) as subtotal, sum(item_cost_price * quantity_purchased) as cost, sum(quantity_purchased) as quantity, sum(total) as total, sum(tax) as tax, sum(profit) as profit, sum(CASE WHEN payment_type LIKE '%Cash%' THEN total ELSE 0 END) AS cash, sum(CASE WHEN payment_type LIKE '%Credit Card%' OR payment_type LIKE '%AMEX%' OR payment_type LIKE '%VISA%' OR payment_type LIKE '%M/C%' OR payment_type LIKE '%DCVR%' THEN total ELSE 0 END) AS credit
							FROM ".$this->db->dbprefix('sales_items_temp')."
							WHERE ".$this->db->dbprefix('sales_items_temp').".deleted = 0
							AND department = '".addslashes($dept_value['department'])."'
							$sale_type
							$deparment
							$terminal
							GROUP BY department, category
							ORDER BY department, category",
							FALSE,
							TRUE,
							TRUE);
			$data['summary'][$dept_key] = $cq->result_array();
			$data['details'][$dept_key] = array();
			foreach($data['summary'][$dept_key] as $key=>$value)
			{
				$scq = $this->db->query("SELECT subcategory, sum(subtotal) as subtotal, sum(total) as total, sum(item_cost_price * quantity_purchased) as cost, sum(quantity_purchased) as quantity, sum(tax) as tax, sum(profit) as profit, sum(CASE WHEN payment_type LIKE '%Cash%' THEN total ELSE 0 END) AS cash, sum(CASE WHEN payment_type LIKE '%Credit Card%' OR payment_type LIKE '%AMEX%' OR payment_type LIKE '%VISA%' OR payment_type LIKE '%M/C%' OR payment_type LIKE '%DCVR%' THEN total ELSE 0 END) AS credit
							FROM ".$this->db->dbprefix('sales_items_temp')."
							WHERE ".$this->db->dbprefix('sales_items_temp').".deleted = 0
							AND department = '".addslashes($dept_value['department'])."'
							AND category = '".addslashes($value['category'])."'
							$sale_type
							$deparment
							$terminal
							GROUP BY department, category, subcategory
							ORDER BY department, category, subcategory",
							FALSE,
							TRUE,
							TRUE);
				$data['details'][$dept_key][$key] = $scq->result_array();
				$data['items'][$dept_key][$key] = array();
				
				foreach($data['details'][$dept_key][$key] as $dkey=>$dvalue) 
				{
					$iq = $this->db->query("SELECT name, description, item_number, sum(subtotal) as subtotal, sum(total) as total, sum(item_cost_price * quantity_purchased) as cost, sum(quantity_purchased) as quantity, sum(tax) as tax, sum(profit) as profit
							FROM ".$this->db->dbprefix('sales_items_temp')."
							WHERE ".$this->db->dbprefix('sales_items_temp').".deleted = 0
							AND department = '".addslashes($dept_value['department'])."'
							AND category = '".addslashes($value['category'])."'
							AND subcategory = '".addslashes($dvalue['subcategory'])."'
							$sale_type
							$deparment
							$terminal
							GROUP BY department, category, subcategory, item_id
							ORDER BY department, category, subcategory",
							FALSE,
							TRUE,
							TRUE);
									
		 			$data['items'][$dept_key][$key][$dkey] = $iq->result_array();
				}
			}
		}
		
		return $data;
	}
	// public function getPaymentData()
	// {
		// $sales_totals = array();
// 		
		// $this->db->select('sale_id, SUM(total) as total', false);
		// $this->db->from('sales_items_temp');
		// $this->db->group_by('sale_id');
		// $result = $this->db->get();
		// //echo $this->db->last_query();
		// foreach($result->result_array() as $sale_total_row)
		// {
			// $sales_totals[$sale_total_row['sale_id']] = $sale_total_row['total'];
		// }
// //echo 'about to load view';
// 		
		// $this->db->select('sales_payments.sale_id, sales_payments.payment_type, payment_amount', false);
		// $this->db->from('sales_payments');
		// $this->db->join('sales', 'sales.sale_id=sales_payments.sale_id and foreup_sales.course_id = "'.$this->session->userdata('course_id').'"');
		// $this->db->where('date(sale_time) BETWEEN "'. $this->params['start_date']. '" and "'. $this->params['end_date'].'"');
		// if ($this->params['sale_type'] == 'sales')
		// {
			// $this->db->where('payment_amount > 0');
		// }
		// elseif ($this->params['sale_type'] == 'returns')
		// {
			// $this->db->where('payment_amount < 0');
		// }
		// if ($this->params['terminal'] != 'all')
			// $this->db->where('terminal_id = "'.$this->params['terminal'].'"');
// 		
		// $this->db->where($this->db->dbprefix('sales').'.deleted', 0);
		// $this->db->order_by('sale_id, payment_type');
		// $sales_payments = $this->db->get()->result_array();
// 		
		// foreach($sales_payments as $row)
		// {
        	// $payments_by_sale[$row['sale_id']][] = $row;
		// }
// 		
		// $payment_data = array();
// 		
		// foreach($payments_by_sale as $sale_id => $payment_rows)
		// {
			// $total_sale_balance = $sales_totals[$sale_id];
// 			
			// foreach($payment_rows as $row)
			// {
				// $payment_amount = $row['payment_amount'];// <= $total_sale_balance ? $row['payment_amount'] : $total_sale_balance;
				// $cdn = $this->config->item('customer_credit_nickname')!=''?$this->config->item('customer_credit_nickname'):lang('customers_account_balance');
				// $mbn = $this->config->item('member_balance_nickname')!=''?$this->config->item('member_balance_nickname'):lang('customers_member_account_balance');
				// //echo $row['payment_type'].' - '.$row['payment_amount'].'<br/>';
				// //Consolidating Credit Cards,  Account Payments, and Giftcards
				// if (strpos($row['payment_type'], 'Tip') !== false)
				// {
					// $row['payment_type'] = 'Tips';
				// }
				// else if (strpos($row['payment_type'], 'M/C') !== false || strpos($row['payment_type'], 'VISA') !== false || strpos($row['payment_type'], 'AMEX') !== false || strpos($row['payment_type'], 'DCVR') !== false)
				// {
					// $row['payment_type'] = 'Credit Card';
				// }
				// else if (strpos($row['payment_type'], $cdn) !== false)
				// {
						// $row['payment_type'] = $cdn;
						// //echo $payment_amount.'<br/>';
				// }
				// else if (strpos($row['payment_type'], $mbn) !== false)
				// {
						// $row['payment_type'] = $mbn;
						// //echo $payment_amount.'<br/>';
				// }
				// else if (strpos($row['payment_type'], 'Gift Card') !== false)
					// $row['payment_type'] = 'Gift Card';
				// else if (strpos($row['payment_type'], 'Raincheck') !== false)
					// $row['payment_type'] = 'Raincheck';
				// else if (strpos($row['payment_type'], 'Change') !== false)
					// $row['payment_type'] = 'Cash';
// 				
				// //if($row['payment_type'] == 'Credit Card')
					// //echo 'Sale '.$row['sale_id'].' - '.$payment_amount."<br/>";
// 				
				// if (!isset($payment_data[$row['payment_type']]))
					// $payment_data[$row['payment_type']] = array('payment_type' => $row['payment_type'], 'payment_amount' => 0 );
				// if ($total_sale_balance != 0 || $row['payment_type'] == 'Tips')
					// $payment_data[$row['payment_type']]['payment_amount'] += $payment_amount;
// 				
				// if (!isset($payment_data['Total']))
					// $payment_data['Total'] = array('payment_type' => 'Total', 'payment_amount' => 0 );
				// if ($total_sale_balance != 0)
					// $payment_data['Total']['payment_amount'] += $payment_amount;
// 				
				// $total_sale_balance-=$payment_amount;
			// }
// //echo $row['payment_amount'].' '.$total_sale_balance.'<br/>';
		// }
		// //echo $this->db->last_query();
		// return $payment_data;
	// }
	// public function getSummaryData()
	// {
// 		
		// //if (!$this->permissions->is_super_admin())
                  // //  $this->db->where('course_id', $this->session->userdata('course_id'));
		// /*$this->db->select('payment_type');
		// $this->db->from('sales_items_temp');
		// $this->db->group_by('sale_id');
		// $results = $this->db->get()->result_array();
		// $account_charges = 0;
		// foreach($results as $result) {
			// //TODO:Account for multiple payments on account in one transaction
			// if (preg_match('/Account- [^$]+: \$(.+)</',$result['payment_type'], $match)) {
				// $account_charges += (float)$match[1];
			// }
		// }*/
// 		
		// $totals = $this->getPaymentData();
		// //echo 'Account total: '.$account_charges;
		// //print_r($results);
		// //echo $this->db->last_query();
		// $cdn = $this->config->item('customer_credit_nickname')!=''?$this->config->item('customer_credit_nickname'):lang('customers_account_balance');
		// $mbn = $this->config->item('member_balance_nickname')!=''?$this->config->item('member_balance_nickname'):lang('customers_member_account_balance');
		// $account_charges = (isset($totals[$cdn]['payment_amount']))?$totals[$cdn]['payment_amount']:0;
		// $member_balance = (isset($totals[$mbn]['payment_amount']))?$totals[$mbn]['payment_amount']:0;
		// $giftcards = (isset($totals['Gift Card']['payment_amount']))?$totals['Gift Card']['payment_amount']:0;
		// $rainchecks = (isset($totals['Raincheck']['payment_amount']))?$totals['Raincheck']['payment_amount']:0;
		// $tips = (isset($totals['Tips']['payment_amount']))?$totals['Tips']['payment_amount']:0;
		// $total_payments = (isset($totals['Total']['payment_amount']))?$totals['Total']['payment_amount']-$account_charges-$member_balance-$giftcards:0;		//echo 'Account total: '.$account_charges;
		// $credit_card = (isset($totals['Credit Card']['payment_amount']))?$totals['Credit Card']['payment_amount']:0;
		// $check = (isset($totals['Check']['payment_amount']))?$totals['Check']['payment_amount']:0;
		// $cash = (isset($totals['Cash']['payment_amount']))?$totals['Cash']['payment_amount']:0;
		// $total_payments = $credit_card + $check + $cash;//(isset($totals['Total']['payment_amount']))?$totals['Total']['payment_amount']-$account_charges-$member_balance-$giftcards:0;		//echo 'Account total: '.$account_charges;
// 		
		// //This section was implemented for East Bay, it is a temporary fix, and must be updated
		// //TODO: Remove this and fix the problem of tracking where account balances are spent.	
		// $this->db->select('sum(total) as ps_total');
		// $this->db->from('sales_items_temp');
		// if ($this->params['sale_type'] == 'sales')
		// {
			// $this->db->where('quantity_purchased > 0');
		// }
		// elseif ($this->params['sale_type'] == 'returns')
		// {
			// $this->db->where('quantity_purchased < 0');
		// }
		// $this->db->where('department = "Pro Shop"');
// 		
		// $this->db->where('deleted', 0);
		// $summary_data = array();
		// foreach ($this->db->get()->row_array() as $key => $value)
			// if($value > 0 && $this->session->userdata('course_id') == 6279) 	
				// $summary_data[$key] = $value - $account_charges - $member_balance;
		// //END EAST BAY CUSTOM FIX
		// //$summary_data['profit'] = 
		// $summary_data['total'] = 
		// //$summary_data['rounding_error'] = 
		// $summary_data['tax'] = 
		// $summary_data['tips'] = 
		// $summary_data['rainchecks'] = 
		// $summary_data['giftcards'] = 
		// $summary_data['member_balance'] = 
		// $summary_data['account_charges'] = 
		// $summary_data['subtotal'] = 0;
		// //Tax totaling fix
        // $this->db->select('sum(subtotal) as subtotal, percent');
		// $this->db->from('sales_items_temp');
		// $this->db->where("percent != ''");
		// if ($this->params['sale_type'] == 'sales')
		// {
			// $this->db->where('quantity_purchased > 0');
		// }
		// elseif ($this->params['sale_type'] == 'returns')
		// {
			// $this->db->where('quantity_purchased < 0');
		// }
		// if ($this->params['department'] != 'all')
			// $this->db->where('department = "'.$this->params['department'].'"');
		// if ($this->params['terminal'] != 'all')
			// $this->db->where('terminal_id = "'.$this->params['terminal'].'"');
// 		
		// $this->db->where('deleted', 0);
		// $this->db->group_by('percent');
		// foreach ($this->db->get()->result_array() as $key => $row)
		// {
			// //print_r($row);
			// $percent = ($row['percent'] != '')?$row['percent']:0;
			// //$summary_data['tax'] += $row['subtotal']*($percent/100);
		// }
		// //$summary_data['tax'] = round($summary_data['tax'], 2);
		// //End tax totaling fix
// 		
        // $this->db->select('sum(subtotal) as subtotal, sum(tax) as collected_tax, sum(item_cost_price) as cost, sum(total) as total, sum(profit) as profit');
		// $this->db->from('sales_items_temp');
		// if ($this->params['sale_type'] == 'sales')
		// {
			// $this->db->where('quantity_purchased > 0');
		// }
		// elseif ($this->params['sale_type'] == 'returns')
		// {
			// $this->db->where('quantity_purchased < 0');
		// }
		// if ($this->params['department'] != 'all')
			// $this->db->where('department = "'.$this->params['department'].'"');
		// if ($this->params['terminal'] != 'all')
			// $this->db->where('terminal_id = "'.$this->params['terminal'].'"');
// 		
		// $this->db->where('deleted', 0);
		// //$summary_data = array();
		// $totals_array = $this->db->get()->row_array();
// //		foreach ($this->db->get()->row_array() as $key => $value) 
	// //		if ($key != 'tax' && $key!='subtotal')
		// //		$summary_data[$key] = $value - $account_charges;
			// //else if ($key == 'subtotal') {
				// //$summary_data[$key] = $value;
				// //$summary_data['account_charges'] = -$account_charges;
			// //}
			// //else 
				// //$summary_data[$key] = $value;
		// if ($this->params['department'] != 'all')
		// {
			// $summary_data['total'] = $totals_array['total'];
			// unset($summary_data['account_charges']);
			// unset($summary_data['member_balance']);
			// unset($summary_data['giftcards']);
			// unset($summary_data['tips']);
		// }
		// else {
			// $summary_data['total'] = $total_payments;
			// $summary_data['account_charges'] = -$account_charges;
			// $summary_data['member_balance'] = -$member_balance;
			// $summary_data['giftcards'] = -$giftcards;
			// $summary_data['rainchecks'] = -$rainchecks;
			// $summary_data['tips'] = $tips;
		// }
// 
// 		
		// $summary_data['subtotal'] = $totals_array['subtotal'];
		// //$summary_data['rounding_error'] = $totals_array['collected_tax'] - $summary_data['tax'];
		// //$summary_data['profit'] = $summary_data['subtotal'] - $totals_array['cost'] + $summary_data['rounding_error'] - $account_charges;
		// $summary_data['tax'] = $totals_array['collected_tax'];		
		// return $summary_data;
	// }
}
?>