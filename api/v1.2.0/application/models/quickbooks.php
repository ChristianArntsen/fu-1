<?php
require_once(APPPATH.'libraries/QuickBooks.php');

class QuickBooks extends CI_Model
{
	const DB_BATCH_SIZE = 2500;

	private static $dsn;
	private static $redemptionMethods = array(
		'Member Credit',
		'Customer Credit',
		'Loyalty',
		'Raincheck',
		'Comp',
		'Gift Card'
	);

	private static $paymentMethods = array(
		'Cash',
		'Check',
		'Credit Card',
		'Visa',
		'Discover',
		'American Express',
		'MasterCard',
		'Debit Card'
	);

	private static $salesTaxCodes = array(
		'Non',
		'Sales Tax'
	);

	public function __construct()
	{
		parent::__construct();

		ini_set('memory_limit','128M');

		// MySQL connection information
		self::$dsn = $this->db->dbdriver.'://'.$this->db->username.':'.$this->db->password.'@'.$this->db->hostname.'/'.$this->db->database;
	}

	// Generates a unique ID (Sha1 hash) for QuickBooks queue records
	public function generate_uid($array, $prepend = ''){

		if(!is_array($array)){
			$array = array($array);
		}

		$recordStr = trim($prepend.implode(':', $array));
		$uid = sha1($recordStr);

		return $uid;
	}

	// Cleans text of weird characters before inserting into XML
	function clean_text($text){

		// Reject overly long 2 byte sequences, as well as characters above U+10000 and replace with blank space
		$text = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]'.
			'|[\x00-\x7F][\x80-\xBF]+'.
			'|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*'.
			'|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})'.
			'|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/S',
			'', $text);

		// Reject overly long 3 byte sequences and UTF-16 surrogates and replace with blank space
		$text = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]'.
			'|\xED[\xA0-\xBF][\x80-\xBF]/S', '', $text);

		$return = '';
		$current;
		if(empty($text)){
			return $return;
		}

		// Loop through each character and make sure it is in proper range
		$length = strlen($text);
		for ($i=0; $i < $length; $i++)
		{
			$current = ord($text{$i});
			if (($current == 0x9) ||
				($current == 0xA) ||
				($current == 0xD) ||
				(($current >= 0x20) && ($current <= 0xD7FF)) ||
				(($current >= 0xE000) && ($current <= 0xFFFD)) ||
				(($current >= 0x10000) && ($current <= 0x10FFFF)))
			{
				$return .= chr($current);
			}
			else
			{
				$return .= " ";
			}
		}

		return trim($return);
	}

	// Adds necessary items/charges into QuickBooks for basic functionality
	function init_quickbooks($courseId, $force = true){

		$queue = new QuickBooks_Queue(self::$dsn);

		$salesAccount = $this->get_sales_account($courseId);
		$redemptionAccount = $this->get_redemption_account($courseId);
		$tipsAccount = $this->get_tips_account($courseId);

		if(empty($salesAccount['name']) || empty($redemptionAccount['name']) || empty($tipsAccount['name'])){
			return false;
		}

		// Insert default line item for Invoices
		$queue->enqueue(QUICKBOOKS_ADD_NONINVENTORYITEM, sha1('Invoice'), 2, $courseId, $courseId,
			'<ItemNonInventoryAddRq><ItemNonInventoryAdd>
				<Name>Invoice</Name>
				<SalesTaxCodeRef>
					<FullName>Non</FullName>
				</SalesTaxCodeRef>
				<SalesOrPurchase>
					<Price>0.00</Price>
					<AccountRef>
						<FullName>'.$salesAccount['name'].'</FullName>
					</AccountRef>
				</SalesOrPurchase>
			</ItemNonInventoryAdd></ItemNonInventoryAddRq>',
		$force);

		// Load redemption charges
		$redemptionMethods = self::$redemptionMethods;
		foreach(self::$redemptionMethods as $redemption){
			$qbxml =
				'<ItemOtherChargeAddRq><ItemOtherChargeAdd>'.
					'<Name>'.$redemption.'</Name>'.
					'<IsActive>1</IsActive>'.
					'<SalesOrPurchase>'.
						'<Price>0.00</Price>'.
						'<AccountRef><FullName>'.$redemptionAccount['name'].'</FullName></AccountRef>'.
					'</SalesOrPurchase>'.
				'</ItemOtherChargeAdd></ItemOtherChargeAddRq>';
			$queue->enqueue(QUICKBOOKS_ADD_OTHERCHARGEITEM, sha1($redemption), 2, $courseId, $courseId, $qbxml, $force);
		}

		// Load tip item
		$qbxml =
			'<ItemOtherChargeAddRq><ItemOtherChargeAdd>'.
				'<Name>Tip</Name>'.
				'<IsActive>1</IsActive>'.
				'<SalesOrPurchase>'.
					'<Price>0.00</Price>'.
					'<AccountRef><FullName>'.$tipsAccount['name'].'</FullName></AccountRef>'.
				'</SalesOrPurchase>'.
			'</ItemOtherChargeAdd></ItemOtherChargeAddRq>';
		$queue->enqueue(QUICKBOOKS_ADD_OTHERCHARGEITEM, sha1('Tip'), 2, $courseId, $courseId, $qbxml, $force);

		// Load payment methods
		foreach(self::$paymentMethods as $payment){
			$qbxml =
				'<ItemPaymentAddRq><ItemPaymentAdd>'.
					'<Name>'.$payment.'</Name>'.
					'<IsActive>1</IsActive>'.
				'</ItemPaymentAdd></ItemPaymentAddRq>';
			$queue->enqueue(QUICKBOOKS_ADD_PAYMENTITEM, sha1($payment), 2, $courseId, $courseId, $qbxml, $force);
		}

		// Create "blank" customer (for records with no customer attached)
		$qbxml = '<CustomerAddRq><CustomerAdd>
			<Name>NO CUSTOMER</Name>
			<IsActive>1</IsActive>
		</CustomerAdd></CustomerAddRq>';
		$queue->enqueue(QUICKBOOKS_ADD_CUSTOMER, sha1('NO CUSTOMER'), 2, $courseId, $courseId, $qbxml, $force);

		return true;
	}

	function set_payment_line($type, $amount, $details){

		$itemName = $type;
		if(in_array($type, self::$redemptionMethods)){
			$neg = '-';
			$isRedemption = true;
		}else{
			$neg = '';
			$isRedemption = false;
		}

		// Build QuickBooks line item object
		$qb_sale_line = new QuickBooks_Object_SalesReceipt_SalesReceiptLine();

		$qb_sale_line->setItemName($itemName);
		$qb_sale_line->setDesc($details);
		$qb_sale_line->setRate($neg.$amount);

		return $qb_sale_line;
	}

	function get_quickbooks_payment_method($paymentDetails){

		if(stripos($paymentDetails, 'tip') !== false || stripos($paymentDetails, 'gratuity') !== false){
			return 'Tip';
		}else if(stripos($paymentDetails, 'cash') !== false){
			return 'Cash';
		}else if(stripos($paymentDetails, 'Credit Card') !== false){
			return 'Credit Card';
		}else if(stripos($paymentDetails, 'M/C') !== false){
			return 'MasterCard';
		}else if(stripos($paymentDetails, 'DCVR') !== false){
			return 'Discover';
		}else if(stripos($paymentDetails, 'VISA') !== false){
			return 'Visa';
		}else if(stripos($paymentDetails, 'Amex') !== false){
			return 'American Express';
		}else if(stripos($paymentDetails, 'Debit') !== false){
			return 'Debit Card';
		}else if(stripos($paymentDetails, 'Member') !== false || stripos($paymentDetails, 'Account') !== false){
			return 'Member Credit';
		}else if(stripos($paymentDetails, 'Customer') !== false){
			return 'Customer Credit';
		}else if(stripos($paymentDetails, 'Raincheck') !== false){
			return 'Raincheck';
		}else if(stripos($paymentDetails, 'check') !== false){
			return 'Check';
		}else if(stripos($paymentDetails, 'Gift Card') !== false){
			return 'Gift Card';
		}else if(stripos($paymentDetails, 'loyalty') !== false){
			return 'Loyalty';
		}else if(stripos($paymentDetails, 'Comp') !== false){
			return 'Comp';
		}else{
			return false;
		}
	}

	/* Pulls all returns from database and prepares it to be sent
	 * to QuickBooks as credit memos
	 */
	function queue_returns($courseId, $start_date, $end_date, $batch = 1){

		$type = 'returns';
		//$department = urldecode($department);
		$this->load->model('reports/Detailed_sales');
		$modelDetailedSales = $this->Detailed_sales;

		// Initialize QuickBooks command queue
		$queue = new QuickBooks_Queue(self::$dsn);

		// Set parameters for sales query
		$modelDetailedSales->setParams(array(
			'start_date'=>$start_date,
			'end_date'=>$end_date,
			'sale_type' => $type,
			'department'=> null)
		);

		// Drop temporary table, before creating
		$this->db->query("DROP TEMPORARY TABLE IF EXISTS ".$this->db->dbprefix('sales_items_temp'));

		// Create temporary table
		$this->Sale->create_sales_items_temp_table(array(
			'start_date'=>$start_date,
			'end_date'=>$end_date,
			'sale_type' => $type,
			'department'=> null,
			'course_id'=> $courseId)
		);

		// First get count of all returns
		$this->db->select('COUNT(sale_id) AS count');
		$this->db->from('sales_items_temp');
		$this->db->join('quickbooks_sync AS sync', "sync.record_id = sale_id AND sync.record_type = 'return' AND sync.course_id = ".(int) $courseId, 'left');
		$this->db->where('sync.record_id IS NULL');
		$this->db->where('quantity_purchased < 0');
		$countResult = $this->db->get();
		$returnsCount = $countResult->row_array();

		if($countResult->num_rows() <= 0 || $returnsCount['count'] == 0){
			return false;
		}

		$returnsCount = (int) $returnsCount['count'];
		$batchSize = self::DB_BATCH_SIZE;

		// Figure out how many requests we need to make
		$numRequests = ceil( $returnsCount / $batchSize );

		// Loop through number of requests that need to be made
		for($x = 0; $x <= $numRequests; $x++){

			// Retrieve sales data from temporary table
			$returns = $modelDetailedSales->get_quickbooks_data($courseId, $x * $batchSize, $batchSize);

			if(empty($returns)){
				continue;
			}

			$qbExtra = array(
				'course_id' => $courseId,
				'record_type' => 'return',
				'record_id' => array()
			);

			// Loop through each return and convert to QBXML
			$count = 0;
			$qbxml = '';
			foreach($returns as $key => $return){

				if(empty($return['details']) && count($returns) <= 0){
					continue;
				}
				if($return['total'] > 0){
					continue;
				}

				if(empty($return['customer_id'])){
					$customerName = 'NO CUSTOMER';
				}else{
					$customerName = $return['customer_id'].' '.$return['customer_name'];
				}
				$returnId = 'Ret'.$return['sale_id'];

				// Properly format date of transaction
				$date = new DateTime($return['sale_time']);
				$txnDate = $date->format('Y-m-d');

				$returnXml = "<CreditMemoAddRq><CreditMemoAdd>
					<CustomerRef>
						<FullName>{$customerName}</FullName>
					</CustomerRef>
					<TxnDate>{$txnDate}</TxnDate>
					<RefNumber>{$returnId}</RefNumber>";

				$validLines = false;
				// Loop through items part of return
				foreach($return['details'] as $lineItem){

					if(empty($lineItem['item_id']) && empty($lineItem['item_kit_id']) && empty($lineItem['invoice_id'])){
						continue;
					}

					if(!empty($lineItem['item_id'])){
						$itemName = 'Item '.$lineItem['item_id'];
					}else if(!empty($lineItem['item_kit_id'])){
						$itemName = 'Item Kit '.$lineItem['item_kit_id'];
					}else if(!empty($lineItem['invoice_id'])){
						$itemName = 'Invoice';
					}
					$qty = abs($lineItem['quantity_purchased']);
					$amount = money_format('%i', abs($lineItem['subtotal']));

					$lineXml = "<CreditMemoLineAdd>
						<ItemRef>
							<FullName>{$itemName}</FullName>
						</ItemRef>
						<Desc>{$lineItem['description']}</Desc>
						<Quantity>{$qty}</Quantity>
						<Rate>0.00</Rate>
						<Amount>{$amount}</Amount>
					</CreditMemoLineAdd>";

					$returnXml .= $lineXml;

					if(!empty($lineItem['tax'])){
						$taxAmount = money_format('%i', abs($lineItem['tax']));

						$taxXml = "<CreditMemoLineAdd>
							<ItemRef>
								<FullName>Sales Tax</FullName>
							</ItemRef>
							<Amount>{$taxAmount}</Amount>
						</CreditMemoLineAdd>";

						$returnXml .= $taxXml;
					}
					$validLines = true;
				}

				if(!$validLines){
					unset($returnXml);
					continue;
				}

				// End QBXML
				$qbxml .= $returnXml . '</CreditMemoAdd></CreditMemoAddRq>';
				$qbExtra['record_id'][] = $return['sale_id'];

				// Insert commands into QB queue
				if($count != 0 && $count % $batch == 0){
					$queue->enqueue(QUICKBOOKS_ADD_CREDITMEMO, $this->generate_uid($qbExtra['record_id'], $qbExtra['record_type']), 0, $qbExtra, $courseId, $qbxml, true);
					unset($qbxml);
					$qbxml = '';
					$qbExtra['record_id'] = array();
				}

				$returnXml = '';
				$count++;
			}

			// Insert any left over commands into queue
			if(!empty($qbxml)){
				$queue->enqueue(QUICKBOOKS_ADD_CREDITMEMO, $this->generate_uid($qbExtra['record_id'], $qbExtra['record_type']), 0, $qbExtra, $courseId, $qbxml, true);
			}

			unset($returns);
			unset($qbxml);
		}

		unset($returns);
		unset($qbxml);

		return $count;
	}

	// Pulls all tips/gratuity and queues them up to be sent to QuickBooks
	function queue_tips($courseId, $batch = 1){

		$result = $this->db->query("SELECT payment.sale_id, payment.payment_amount, payment.payment_type,
				sale.customer_id, CONCAT(customer.first_name,' ',customer.last_name) AS customer_name, sale.sale_time
			FROM ".$this->db->dbprefix('sales_payments')." AS payment
			INNER JOIN ".$this->db->dbprefix('sales')." AS sale
				ON sale.sale_id = payment.sale_id
			LEFT JOIN ".$this->db->dbprefix('people')." AS customer
				ON customer.person_id = sale.customer_id
			LEFT JOIN ".$this->db->dbprefix('quickbooks_sync')." AS sync
				ON sync.record_id = sale.sale_id
				AND sync.record_type = 'tip'
				AND sync.course_id = sale.course_id
			WHERE sale.course_id = ".(int) $courseId."
				AND sync.record_id IS NULL
				AND (payment.payment_type LIKE '%tip%' || payment.payment_type LIKE '%gratuity%')");
		$tips = $result->result_array();

		if(empty($tips)){
			return false;
		}

		// Initialize QuickBooks command queue
		$queue = new QuickBooks_Queue(self::$dsn);

		// Organize tips by Sale
		$sales = array();
		foreach($tips as $tip){

			// If sale hasn't been stored yet, store it in array
			if(empty($sales[$tip['sale_id']])){
				$sales[$tip['sale_id']] = array(
					'customer_id' => $tip['customer_id'],
					'customer_name' => $tip['customer_name'],
					'sale_id' => $tip['sale_id'],
					'sale_time' => $tip['sale_time'],
					'tips' => array()
				);
			}

			// Store each tip under appropriate sale
			$sales[$tip['sale_id']]['tips'][] = array(
				'amount' => $tip['payment_amount'],
				'details' => $tip['payment_type']
			);
		}

		$qbExtra = array(
			'course_id' => $courseId,
			'record_type' => 'tip',
			'record_id' => array()
		);

		// Loop through each sale containing tips and convert to QBXML
		$count = 1;
		$qbxml = '';
		foreach($sales as $key => $sale){

			$qb_sale = new QUICKBOOKS_OBJECT_SALESRECEIPT();

			if(empty($sale['customer_id'])){
				$customerName = 'NO CUSTOMER';
			}else{
				$customerName = $sale['customer_id'].' '.$sale['customer_name'];
			}

			$qb_sale->setCustomerName($customerName);
			$qb_sale->setTxnDate($sale['sale_time']);
			$qb_sale->setRefNumber($sale['sale_id'].' Tips');

			$validLines = false;

			// Loop through tips part of sale
			foreach($sale['tips'] as $tip){
				if($tip['amount'] < 0){
					continue;
				}
				$validLines = true;
				$qb_sale_line = new QuickBooks_Object_SalesReceipt_SalesReceiptLine();

				$qb_sale_line->setItemName('Tip');
				$qb_sale_line->setDesc($tip['details']);
				$qb_sale_line->setQuantity(1);
				$qb_sale_line->setRate(0.00);
				$qb_sale_line->setAmount(abs($tip['amount']));

				$qb_sale->addSalesReceiptLine($qb_sale_line);
			}

			if(!$validLines){
				continue;
			}

			// Generate QBXML
			$qbxml .= $qb_sale->asQBXML(QUICKBOOKS_ADD_SALESRECEIPT);
			$qbExtra['record_id'][] = $sale['sale_id'];

			// Insert commands into QB queue 10 at a time
			if($count != 0 && $count % $batch == 0){
				$queue->enqueue(QUICKBOOKS_ADD_SALESRECEIPT, $this->generate_uid($qbExtra['record_id'], $qbExtra['record_type']), 0, $qbExtra, $courseId, $qbxml, true);
				$qbxml = '';
				$qbExtra['record_id'] = array();
			}

			$count++;
			unset($qb_sale);
		}

		// Insert any left over commands into queue
		if(!empty($qbxml)){
			$queue->enqueue(QUICKBOOKS_ADD_SALESRECEIPT, $this->generate_uid($qbExtra['record_id'], $qbExtra['record_type']), 0, $qbExtra, $courseId, $qbxml, true);
		}

		return $count;
	}

	/* Pulls all sales data from database and prepares it to be sent
	 * to QuickBooks
	 */
	function queue_sales($courseId, $start_date, $end_date, $batch = 1){

		$type = 'sales';

		//$department = urldecode($department);
		$this->load->model('reports/Detailed_sales');
		$modelDetailedSales = $this->Detailed_sales;

		// Initialize QuickBooks command queue
		$queue = new QuickBooks_Queue(self::$dsn);

		// Set parameters for sales query
		$modelDetailedSales->setParams(array(
			'start_date'=>$start_date,
			'end_date'=>$end_date,
			'sale_type' => $type,
			'department'=> null)
		);

		// Drop temporary table, before creating
		$this->db->query("DROP TEMPORARY TABLE IF EXISTS ".$this->db->dbprefix('sales_items_temp'));

		// Create temporary table
		$this->Sale->create_sales_items_temp_table(array(
			'start_date'=>$start_date,
			'end_date'=>$end_date,
			'sale_type' => $type,
			'department'=> null,
			'course_id'=>$courseId)
		);

		// First get count of all sales records available
		$this->db->select('COUNT(sale_id) AS count');
		$this->db->from('sales_items_temp');
		$this->db->join('quickbooks_sync AS sync', "sync.record_id = sale_id AND sync.record_type = 'sale' AND sync.course_id = ".(int) $courseId, 'left');
		$this->db->where('sync.record_id IS NULL');
		$this->db->where('quantity_purchased > 0');
		$countResult = $this->db->get();
		$salesCount = $countResult->row_array();

		if($countResult->num_rows() <= 0 || $salesCount['count'] == 0){
			return false;
		}

		$salesCount = (int) $salesCount['count'];
		$batchSize = self::DB_BATCH_SIZE;

		// Figure out how many requests we need to make (in batches of 1000)
		$numRequests = ceil( $salesCount / $batchSize );

		// Loop through number of requests that need to be made
		for($x = 0; $x <= $numRequests; $x++){

			// Retrieve sales data from temporary table
			$sales = $modelDetailedSales->get_quickbooks_data($courseId, $x * $batchSize, $batchSize);

			if(empty($sales)){
				continue;
			}

			$qbExtra = array(
				'course_id' => $courseId,
				'record_type' => 'sale',
				'record_id' => array()
			);

			// Loop through each sale and convert to QBXML
			$count = 1;
			$qbxml = '';
			foreach($sales as $key => $sale){

				if(empty($sale['details']) && count($sales) <= 0){
					continue;
				}
				if($sale['total'] < 0){
					continue;
				}

				if(empty($sale['customer_id'])){
					$customerName = 'NO CUSTOMER';
				}else{
					$customerName = $sale['customer_id'].' '.$sale['customer_name'];
				}

				$qb_sale = new QUICKBOOKS_OBJECT_SALESRECEIPT();

				$qb_sale->setCustomerName($customerName);
				$qb_sale->setTxnDate($sale['sale_time']);
				$qb_sale->setRefNumber($sale['sale_id']);

				$validLines = false;

				// Loop through line items part of sale
				foreach($sale['details'] as $lineItem){
					if(empty($lineItem['item_id']) && empty($lineItem['item_kit_id']) && empty($lineItem['invoice_id'])){
						continue;
					}

					$qb_sale_line = new QuickBooks_Object_SalesReceipt_SalesReceiptLine();

					if(!empty($lineItem['item_id'])){
						$itemName = 'Item '.$lineItem['item_id'];
					}else if(!empty($lineItem['item_kit_id'])){
						$itemName = 'Item Kit '.$lineItem['item_kit_id'];
					}else if(!empty($lineItem['invoice_id'])){
						$itemName = 'Invoice';
					}else{
						continue;
					}
					$validLines = true;

					$qb_sale_line->setItemName($itemName);
					$qb_sale_line->setDesc($lineItem['description']);
					$qb_sale_line->setQuantity(abs($lineItem['quantity_purchased']));
					$qb_sale_line->setRate(0.00);
					$qb_sale_line->setAmount(abs($lineItem['subtotal']));

					$qb_sale->addSalesReceiptLine($qb_sale_line);

					if($lineItem['tax'] > 0){
						$qb_tax_line = new QuickBooks_Object_SalesReceipt_SalesReceiptLine();
						$qb_tax_line->setItemName('Sales Tax');
						$qb_tax_line->setAmount($lineItem['tax']);

						$qb_sale->addSalesReceiptLine($qb_tax_line);
					}
				}

				if(!$validLines){
					continue;
				}

				// Add any member credit/giftcard redemptions as negative line items
				if(!empty($sale['payments'])){

					if(count($sale['payments']) == 1){

						$paymentMethod = $this->get_quickbooks_payment_method($sale['payments'][0]['payment_type']);
						if($paymentMethod != 'Tip' && $paymentMethod){

							// If only 1 payment, and it is a standard payment, add it as property of sales receipt
							if($paymentMethod == 'Cash' || $paymentMethod == 'Credit Card' || $paymentMethod == 'Check'
								|| $paymentMethod == 'Visa' || $paymentMethod == 'MasterCard' || $paymentMethod == 'Debit Card'
								|| $paymentMethod == 'Discover' || $paymentMethod == 'American Express'){

								// By default QuickBooks does not have a general credit card payment type
								// change it to Visa
								if($paymentMethod == 'Credit Card'){
									$paymentMethod = 'Visa';
								}
								$qb_sale->setPaymentMethodName($paymentMethod);

							// If payment is Giftcard, member credit, etc. add as separate line item charge
							}else{
								$amount = abs($sale['payments'][0]['payment_amount']);

								// If credit being redeemed is more than total of sales receipt, force it to be equal
								// QuickBooks does not allow negative sales receipts
								if($amount > $sale['total']){
									$amount = $sale['total'];
								}

								$qb_sale->addSalesReceiptLine($this->set_payment_line($paymentMethod, $amount, $sale['payments'][0]['payment_type']));
							}
						}

					// If multiple payments, add each as its own line in the sales receipt
					}else{
						$totalPayments = 0;
						$changeIssued = 0;
						foreach($sale['payments'] as $payment){

							$qbPaymentMethod = $this->get_quickbooks_payment_method($payment['payment_type']);

							// Do not add tips to sales receipt
							if($qbPaymentMethod == 'Tip' || !$qbPaymentMethod){
								continue;
							}

							// If payment is change issued, store it to subtract it from Cash payment later
							if(stripos($payment['payment_type'], 'Change issued') !== false){
								$changeIssued = abs($payment['payment_amount']);

							}else{
								// Total up change issued, subtract it from cash payment
								// to get actual payment made
								if($qbPaymentMethod == 'Cash'){
									$payment['payment_amount'] -= $changeIssued;
								}
								$totalPayments += abs($payment['payment_amount']);

								// If payments add up to more than total of sales receipt, force it to be equal
								// QuickBooks does not allow negative sales receipts
								if($totalPayments > $sale['total']){
									$diff = $totalPayments - $sale['total'];
									$qb_sale->addSalesReceiptLine($this->set_payment_line($qbPaymentMethod, abs($payment['payment_amount'] - $diff), $payment['payment_type']));
									break;
								}else{
									$qb_sale->addSalesReceiptLine($this->set_payment_line($qbPaymentMethod, abs($payment['payment_amount']), $payment['payment_type']));
								}
							}
						}
					}
				}

				// Generate QBXML
				$qbxml .= $qb_sale->asQBXML(QUICKBOOKS_ADD_SALESRECEIPT);
				$qbExtra['record_id'][] = $sale['sale_id'];

				// Insert commands into QB queue 10 at a time
				if($count != 0 && $count % $batch == 0){
					$queue->enqueue(QUICKBOOKS_ADD_SALESRECEIPT, $this->generate_uid($qbExtra['record_id'], $qbExtra['record_type']), 0, $qbExtra, $courseId, $qbxml, true);
					unset($qbxml);
					$qbxml = '';
					$qbExtra['record_id'] = array();
				}

				$count++;
				unset($qb_sale);
			}

			// Insert any left over commands into queue
			if(!empty($qbxml)){
				$queue->enqueue(QUICKBOOKS_ADD_SALESRECEIPT, $this->generate_uid($qbExtra['record_id'], $qbExtra['record_type']), 0, $qbExtra, $courseId, $qbxml, true);
			}

			unset($sales);
		}

		unset($sales);
		unset($qbxml);

		return $count;
	}

	public function queue_customers($courseId, $batch = 1){

		$this->load->model('item');

		// Initialize QuickBooks command queue
		$queue = new QuickBooks_Queue(self::$dsn);

		// First get count of all customer records available
		$this->db->select('COUNT(person_id) AS count');
		$this->db->from('customers');
		$this->db->join('quickbooks_sync', "record_id = ".$this->db->dbprefix('customers').".person_id AND record_type = 'customer' AND ".$this->db->dbprefix('quickbooks_sync').".course_id = ".(int) $courseId, 'left');
		$this->db->where('customers.course_id', $courseId);
		$this->db->where('quickbooks_sync.record_id IS NULL');
		$this->db->group_by('customers.person_id');
		$countResult = $this->db->get();
		$personCount = $countResult->row_array();

		if($countResult->num_rows() <= 0 || $personCount['count'] == 0){
			return false;
		}

		$personCount = (int) $personCount['count'];
		$batchSize = self::DB_BATCH_SIZE;

		// Figure out how many requests we need to make
		$numRequests = ceil($personCount / $batchSize);

		for($x = 0; $x <= $numRequests; $x++){

			// Get list of customers
			$this->db->select('customers.person_id, customers.deleted, customers.company_name,
				people.first_name, people.last_name, people.phone_number,
				people.cell_phone_number, people.email, people.address_1,
				people.address_2, people.city, people.state,
				people.zip, people.country, people.comments');
			$this->db->from('customers');
			$this->db->join('people', 'people.person_id = customers.person_id', 'left');
			$this->db->join('quickbooks_sync', "record_id = ".$this->db->dbprefix('customers').".person_id AND record_type = 'customer' AND ".$this->db->dbprefix('quickbooks_sync').".course_id = ".(int) $courseId, 'left');
			$this->db->where('customers.course_id', $courseId);
			$this->db->where('quickbooks_sync.record_id IS NULL');
			$this->db->group_by('customers.person_id');
			$this->db->limit($batchSize, $x * $batchSize);

			$peopleResult = $this->db->get();
			$people = $peopleResult->result_array();

			if(empty($people)){
				return true;
			}

			// Extra data that will be passed with QB request to correlate QB
			// records with ForeUp records
			$qbExtra = array(
				'course_id' => $courseId,
				'record_type' => 'customer',
				'record_id' => array()
			);

			// Loop through each item and build QBXML
			$count = 0;
			$qbxml = '';
			foreach($people as $key => $customer){

				$name = $customer['person_id'].' ';
				if(empty($customer['first_name']) && empty($customer['last_name'])){
					$name .= $customer['company_name'];
				}else{
					$name .= $customer['first_name'].' '.$customer['last_name'];
				}
				$name = substr($name, 0, 40);

				$qb_customer = new QUICKBOOKS_OBJECT_CUSTOMER();
				$qb_customer->setName($this->clean_text($name));
				$qb_customer->setFirstName($this->clean_text(substr($customer['first_name'], 0, 25)));
				$qb_customer->setLastName($this->clean_text(substr($customer['last_name'], 0, 25)));

				if($customer['deleted'] == 1){
					$qb_customer->setIsActive(0);
				}

				// If address is set for customer
				if(!empty($customer['address_1']) || !empty($customer['address_2']) || !empty($customer['city'])
					|| !empty($customer['state']) || !empty($customer['zip']) || !empty($customer['country'])){

					$qb_customer->setBillAddress(
						$this->clean_text($customer['address_1']),
						$this->clean_text($customer['address_2']),
						null, // Line 3
						null, // Line 4
						null, // Line 5
						$this->clean_text($customer['city']),
						$this->clean_text($customer['state']),
						'',
						$this->clean_text($customer['zip']),
						$this->clean_text($customer['country']),
						'' // Note
					);
				}

				$qb_customer->setPhone($this->clean_text(substr($customer['phone_number'], 0, 21)));
				$qb_customer->setAltPhone($this->clean_text(substr($customer['cell_phone_number'], 0, 21)));
				$qb_customer->setEmail($this->clean_text(substr($customer['email'], 0, 1023)));
				$qb_customer->setNotes($this->clean_text($customer['comments']));

				// Generate QBXML
				$qbxml .= $qb_customer->asQBXML(QUICKBOOKS_ADD_CUSTOMER);
				$qbExtra['record_id'][] = $customer['person_id'];

				if($count != 0 && $count % $batch == 0){
					// Insert command into queue
					$queue->enqueue(QUICKBOOKS_ADD_CUSTOMER, $this->generate_uid($qbExtra['record_id'], $qbExtra['record_type']), 1, $qbExtra, $courseId, $qbxml, true);
					unset($qbxml);
					$qbxml = '';
					$qbExtra['record_id'] = array();
				}
				$count++;
			}

			if(!empty($qbxml)){
				// Insert command into queue
				$queue->enqueue(QUICKBOOKS_ADD_CUSTOMER, $this->generate_uid($qbExtra['record_id'], $qbExtra['record_type']), 1, $qbExtra, $courseId, $qbxml, true);
			}

			unset($people);
			unset($qbxml);
		}

		unset($people);
		unset($qbxml);

		return $count;
	}

	/*
	 * Retrieves items created and queues up QuickBooks QBXML commands
	 * to insert the new items into QuickBooks
	 */
	public function queue_items($courseId, $batch = 1){

		$this->load->model('item');

		// Initialize QuickBooks command queue
		$queue = new QuickBooks_Queue(self::$dsn);

		// Get sales account number to attach item(s) to
		$salesAccount = $this->get_sales_account($courseId);
		if(empty($salesAccount)){
			return false;
		}

		// Extra data that will be passed with QB request to correlate QB
		// records with ForeUp records
		$qbExtra = array(
			'course_id' => $courseId,
			'record_type' => 'item',
			'record_id' => array()
		);

		// First get count of all sales records available
		$this->db->select('COUNT(item_id) AS count');
		$this->db->from('items');
		$this->db->join('quickbooks_sync AS sync', "sync.record_id = item_id AND sync.record_type = 'item' AND sync.course_id = ".(int) $courseId, 'left');
		$this->db->where('sync.record_id IS NULL');
		$countResult = $this->db->get();
		$itemsCount = $countResult->row_array();

		if($countResult->num_rows() <= 0 || $itemsCount['count'] == 0){
			return false;
		}

		$itemsCount = (int) $itemsCount['count'];
		$batchSize = self::DB_BATCH_SIZE;

		// Figure out how many requests we need to make
		$numRequests = ceil( $itemsCount / $batchSize );

		for($x = 0; $x <= $numRequests; $x++){

			// Get list of items from database
			$this->db->select('items.name, items.item_id, items.unit_price, items.description, items.item_number, items.deleted');
			$this->db->from('items');
			$this->db->join('quickbooks_sync', "record_id = item_id AND record_type = 'item' AND ".$this->db->dbprefix('quickbooks_sync').".course_id = ".(int) $courseId, 'left');
			$this->db->where('items.course_id', $courseId);
			$this->db->where('quickbooks_sync.record_id IS NULL');
			$this->db->group_by('items.item_id');
			$this->db->limit($batchSize, $x * $batchSize);

			$item_result = $this->db->get();
			$items = $item_result->result_array();

			if(empty($items)){
				continue;
			}

			// Loop through each item and build QBXML
			$count = 0;
			$qbxml = '';
			foreach($items as $key => $item){
				$qb_item = new QUICKBOOKS_OBJECT_NONINVENTORYITEM();
				$qb_item->setName( substr('Item '.$item['item_id'], 0, 31) );
				$qb_item->setPrice($item['unit_price']);

				if(!empty($item['description'])){
					$desc = $item['name'].' - '.$item['description'];
				}else{
					$desc = $item['name'];
				}

				if($item['item_number']){
					$desc .= ' UPC:'.$item['item_number'];
				}

				if($item['deleted'] == 1){
					$qb_item->setIsActive(0);
				}

				$qb_item->setDescription($this->clean_text($desc));
				$qb_item->setAccountName($salesAccount['name']);

				// Generate QBXML
				$qbxml .= $qb_item->asQBXML(QUICKBOOKS_ADD_NONINVENTORYITEM);
				$qbExtra['record_id'][] = $item['item_id'];

				if($count != 0 && $count % $batch == 0){
					// Insert command into queue
					$queue->enqueue(QUICKBOOKS_ADD_NONINVENTORYITEM, $this->generate_uid($qbExtra['record_id'], $qbExtra['record_type']), 1, $qbExtra, $courseId, $qbxml, true);
					unset($qbxml);
					$qbxml = null;
					$qbExtra['record_id'] = array();
				}
				$count++;
			}

			if(!empty($qbxml)){
				// Insert command into queue
				$queue->enqueue(QUICKBOOKS_ADD_NONINVENTORYITEM, $this->generate_uid($qbExtra['record_id'], $qbExtra['record_type']), 1, $qbExtra, $courseId, $qbxml, true);
			}

			unset($qbxml);
			unset($items);
		}

		unset($qbxml);
		unset($items);

		return $count;
	}

	/*
	 * Retrieves item kits in a specific date range
	 * and queues up QuickBooks QBXML commands to insert the
	 * new items into QuickBooks
	 */
	public function queue_item_kits($courseId, $batch = 1){

		$this->load->model('item_kit');

		// Initialize QuickBooks command queue
		$queue = new QuickBooks_Queue(self::$dsn);

		// Get sales account number to attach item(s) to
		$salesAccount = $this->get_sales_account($courseId);
		if(empty($salesAccount)){
			return false;
		}

		// Extra data that will be passed with QB request to correlate QB
		// records with ForeUp records
		$qbExtra = array(
			'course_id' => $courseId,
			'record_type' => 'item_kit',
			'record_id' => array()
		);

		// Get list of item kits from database
		$this->db->select('item_kits.item_kit_id, item_kits.name, item_kits.description,
			item_kits.unit_price, item_kits.item_kit_number, item_kits.deleted');
		$this->db->from('item_kits');
		$this->db->join('quickbooks_sync', "record_id = item_kit_id AND record_type = 'item_kit' AND ".$this->db->dbprefix('quickbooks_sync').".course_id = ".(int) $courseId, 'left');
		$this->db->where('item_kits.course_id', $courseId);
		$this->db->where('quickbooks_sync.record_id IS NULL');
		$item_result = $this->db->get();

		$item_kits = $item_result->result_array();

		if(empty($item_kits)){
			return false;
		}

		// Loop through each item and build QBXML
		$count = 0;
		$qbxml = '';
		foreach($item_kits as $key => $item_kit){
			$qb_item = new QUICKBOOKS_OBJECT_NONINVENTORYITEM();
			$qb_item->setName( substr('Item Kit '.$item_kit['item_kit_id'], 0, 31) );
			$qb_item->setPrice($item_kit['unit_price']);

			if(!empty($item_kit['description'])){
				$desc = $item_kit['name'].' - '.$item_kit['description'];
			}else{
				$desc = $item_kit['name'];
			}

			if($item_kit['item_kit_number']){
				$desc .= ' #:'.$item_kit['item_kit_number'];
			}

			if($item_kit['deleted'] == 1){
				$qb_item->setIsActive(0);
			}

			$qb_item->setDescription($this->clean_text($desc));
			$qb_item->setAccountName($salesAccount['name']);

			// Generate QBXML
			$qbxml .= $qb_item->asQBXML(QUICKBOOKS_ADD_NONINVENTORYITEM);
			$qbExtra['record_id'][] = $item_kit['item_kit_id'];

			if($count != 0 && $count % $batch == 0){
				// Insert command into queue
				$queue->enqueue(QUICKBOOKS_ADD_NONINVENTORYITEM, $this->generate_uid($qbExtra['record_id'], $qbExtra['record_type']), 1, $qbExtra, $courseId, $qbxml, true);
				$qbxml = '';
				$qbExtra['record_id'] = array();
			}

			$count++;
		}

		if(!empty($qbxml)){
			// Insert command into queue
			$queue->enqueue(QUICKBOOKS_ADD_NONINVENTORYITEM, $this->generate_uid($qbExtra['record_id'], $qbExtra['record_type']), 1, $qbExtra, $courseId, $qbxml, true);
		}

		return $count;
	}

	public function insert_sync_log($courseId, $recordId, $recordType, $quickbooksId = null, $date = null){

		if($date == null){
			$date = date('Y-m-d H:i:s');
		}

		return $this->db->query("INSERT INTO `".$this->db->dbprefix."quickbooks_sync`
			(`course_id`, `record_id`, `record_type`, `quickbooks_id`, `date_synced`) VALUES ("
				.$this->db->escape($courseId).","
				.$this->db->escape($recordId).","
				.$this->db->escape($recordType).","
				.$this->db->escape($quickbooksId).","
				.$this->db->escape($date)
			.") ON DUPLICATE KEY UPDATE `quickbooks_id` = VALUES(`quickbooks_id`), `date_synced` = VALUES(`date_synced`)");
	}

	/*
	 * Save QuickBooks accounts for a course
	 */
	public function save_accounts($accounts = null){

		if(empty($accounts)){
			return false;
		}

		// Begin batch SQL query
		$query = "INSERT IGNORE INTO ".$this->db->dbprefix('quickbooks_accounts').
			" (`course_id`, `quickbooks_id`, `name`, `balance`, `type`) VALUES ";

		foreach($accounts as $key => $account){
			$query .= "(".
				$this->db->escape($account['course_id']).",".
				$this->db->escape($account['quickbooks_id']).",".
				$this->db->escape($account['name']).",".
				$this->db->escape($account['balance']).",".
				$this->db->escape($account['type']);
			$query .= "),";
		}
		// Remove last comma from SQL query
		$query = rtrim($query, ",");

		$result = $this->db->query($query);
		return $result;
	}

	/*
	 * Get sales account for course
	 */
	public function get_sales_account($courseId){
		$response = $this->db->get_where('quickbooks_accounts', array('course_id'=>$courseId, 'is_sales'=>1));
		return $response->row_array();
	}

	public function get_redemption_account($courseId){
		$response = $this->db->get_where('quickbooks_accounts', array('course_id'=>$courseId, 'is_redemption'=>1));
		return $response->row_array();
	}

	public function get_tips_account($courseId){
		$response = $this->db->get_where('quickbooks_accounts', array('course_id'=>$courseId, 'is_tips'=>1));
		return $response->row_array();
	}

	public function set_sales_account($courseId, $accountId){

		$this->db->where(array('course_id'=>$courseId));
		$this->db->update('quickbooks_accounts', array('is_sales'=>0));

		$this->db->where(array('course_id'=>$courseId, 'quickbooks_id'=>$accountId));
		$result = $this->db->update('quickbooks_accounts', array('is_sales'=>1));

		return $result;
	}

	public function set_redemption_account($courseId, $accountId){

		$this->db->where(array('course_id'=>$courseId));
		$this->db->update('quickbooks_accounts', array('is_redemption'=>0));

		$this->db->where(array('course_id'=>$courseId, 'quickbooks_id'=>$accountId));
		$result = $this->db->update('quickbooks_accounts', array('is_redemption'=>1));

		return $result;
	}

	public function set_tips_account($courseId, $accountId){

		$this->db->where(array('course_id'=>$courseId));
		$this->db->update('quickbooks_accounts', array('is_tips'=>0));

		$this->db->where(array('course_id'=>$courseId, 'quickbooks_id'=>$accountId));
		$result = $this->db->update('quickbooks_accounts', array('is_tips'=>1));

		return $result;
	}

	public function set_user_initialized($courseId){
		$defaultPrefix = $this->db->dbprefix;
		$this->db->set_dbprefix('quickbooks_');

		$this->db->where(array('qb_username'=>$courseId));
		$result = $this->db->update('user', array('is_initialized'=>1));

		$this->db->set_dbprefix($defaultPrefix);

		return $result;
	}

	public function get_user($username){

		$defaultPrefix = $this->db->dbprefix;
		$this->db->set_dbprefix('quickbooks_');

		$this->db->select('qb_username, qb_password, qb_company_file, is_initialized');
		$result = $this->db->get_where('user', array('qb_username'=>$username, 'status'=>'e'));

		$this->db->set_dbprefix($defaultPrefix);

		$row = $result->row_array();

		if(!empty($row['qb_password'])){
			$row['qb_password'] = substr( md5($username), 0, 6);
		}

		return $row;
	}

	// Get all quickbooks users
	public function get_users(){

		$defaultPrefix = $this->db->dbprefix;
		$this->db->set_dbprefix('quickbooks_');

		$this->db->select('qb_username, qb_password, qb_company_file');
		$result = $this->db->get_where('user', array('status'=>'e'));

		$this->db->set_dbprefix($defaultPrefix);

		$rows = $result->result_array();
		return $rows;
	}

	// Delete QuickBooks user
	public function delete_user($username){

		if(empty($username)){
			return false;
		}

		$defaultPrefix = $this->db->dbprefix;
		$this->db->set_dbprefix('quickbooks_');

		$result = $this->db->delete('user', array('qb_username'=>$username));

		$this->db->set_dbprefix($defaultPrefix);
		return $result;
	}

	// Delete all QuickBooks accounts for a course
	public function delete_accounts($courseId){
		$response = $this->db->delete('quickbooks_accounts', array('course_id'=>$courseId));
		return $response;
	}

	// Delete all pending/past QuickBooks queue items for a course
	public function clear_queue($courseId){
		$defaultPrefix = $this->db->dbprefix;
		$this->db->set_dbprefix('quickbooks_');

		$result = $this->db->delete('queue', array('qb_username'=>$courseId));

		$this->db->set_dbprefix($defaultPrefix);
		return $result;
	}

	// Delete all pending/past QuickBooks queue items for a course
	public function clear_synced_records($courseId){

		$result = $this->db->delete('quickbooks_sync', array('course_id'=>$courseId));
		return $result;
	}

	/*
	 * Get all QuickBooks accounts for a specific course
	 */
	public function get_accounts(){
		$courseId = $this->session->userdata('course_id');
		$response = $this->db->get_where('quickbooks_accounts', array('course_id'=>$courseId));
		return $response->result_array();
	}

	/*
	 * Get all QuickBooks accounts for a specific course
	 */
	public function update_company_file($username, $companyFile){
		$defaultPrefix = $this->db->dbprefix;
		$this->db->set_dbprefix('quickbooks_');
		$result = $this->db->update('user', array('qb_company_file'=>$companyFile), array('qb_username'=>$username));

		$this->db->set_dbprefix($defaultPrefix);
		return $result;
	}
}