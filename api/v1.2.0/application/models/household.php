<?php
class Household extends CI_Model
{
	/*
	Determines if a given person_id is a household head
	*/
	function get_id($person_id)
	{
		$this->db->from('households');
		//$this->db->join('household_members', 'household_members.household_id = households.household_id', 'left');
		//$this->db->where("(household_head_id = {$person_id} OR household_member_id = {$person_id})");
		$this->db->where("(household_head_id = {$person_id})");
		$this->db->limit(1);
        $result = $this->db->get()->row_array();
	//echo $this->db->last_query();
		return $result['household_id'];
	}
	
	/*
	Determines if a given person_id is a household head
	*/
	function is_head($person_id)
	{
		$this->db->from('households');
		$this->db->where('household_head_id',$person_id);
		$this->db->limit(1);
        $query = $this->db->get();
	
		return ($query->num_rows()==1);
	}
	
    /*
	Determines if a given person_id is a household member
	*/
	function is_member($person_id)
	{
		$this->db->from('household_members');
		$this->db->where('household_member_id',$person_id);
		$this->db->limit(1);
        $query = $this->db->get();
	
		return ($query->num_rows()==1);
	}
	
	/*
	Gets info for the household head
	*/
	function get_head($person_id)
	{
		//return $this->db->query("SELECT * FROM foreup_people WHERE person_id = (SELECT household_head_id FROM foreup_households WHERE household_id = (SELECT household_id FROM foreup_household_members WHERE household_member_id = $person_id LIMIT 1) LIMIT 1) LIMIT 1")->row_array();
		
		
		$this->db->from('households');
		$this->db->join('household_members', 'household_members.household_id = households.household_id');
		$this->db->join('people', 'people.person_id = households.household_head_id');
		$this->db->where('household_member_id',$person_id);
		$this->db->limit(1);
        $query = $this->db->get();
	
		return $query->row_array();
	}

	/*
	Gets info for the household members
	*/
	function get_members($person_id)
	{
		//return $this->db->query("SELECT * FROM foreup_people WHERE person_id IN (SELECT household_member_id FROM foreup_household_members WHERE household_id = (SELECT household_id FROM foreup_households WHERE household_head_id = $person_id LIMIT 1))")->result_array();
		
		$this->db->from('household_members');
		$this->db->join('households', 'household_members.household_id = households.household_id');
		$this->db->join('people', 'people.person_id = household_members.household_member_id');
		$this->db->where('household_head_id',$person_id);
		//$this->db->limit(1);
        $query = $this->db->get();
	
		return $query->result_array();
	}

	/*
	Save household
	*/
	function save($members, $head_id)
	{
		$household_id = $this->get_id($head_id);
		if ($household_id)
		{
			$this->delete_members($household_id);
		}
		else 
		{
			$this->db->insert('households', array('household_head_id'=>$head_id));
			$household_id = $this->db->insert_id();
		}
		$member_array = array();
		foreach($members as $member)
		{
			$member_array[] = array('household_id'=>$household_id, 'household_member_id'=>$member);
		}

		return $this->db->insert_batch('household_members', $member_array);
	}
	
	/*
	Delete household members
	*/
	function delete_members($household_id)
	{
		$this->db->where('household_id', $household_id);
		return $this->db->delete('household_members');
	}
	/*
	Delete household
	*/
	function delete($household_id)
	{
		$this->db->where('household_id', $household_id);
		$this->db->limit(1);
		$this->db->delete('households');
		
		return $this->delete_members($household_id);
	}
	
}
?>
