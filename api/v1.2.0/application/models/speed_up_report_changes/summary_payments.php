<?php
require_once("report.php");
class Summary_payments extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array(array('data'=>lang('reports_payment_type'), 'align'=> 'left'), array('data'=>lang('reports_total'), 'align'=> 'right'));
	}
	
	public function getData($include_average_sales = false)
	{
		$sales_totals = array();
		
		$this->db->select('sale_id, SUM(total) as total', false);
		$this->db->from('sales_items_temp');
		$this->db->group_by('sale_id');
		if ($this->params['department'] != 'all')
			$this->db->where('department = "'.$this->params['department'].'"');
		if ($this->params['terminal'] != 'all')
			$this->db->where('terminal_id = "'.$this->params['terminal'].'"');
			
		$data = $this->db->get();
	//echo $this->db->last_query();
		foreach($data->result_array() as $sale_total_row)
		{
			$sales_totals[$sale_total_row['sale_id']] = $sale_total_row['total'];
		}
		$this->db->select('sales_payments.sale_id, sales_payments.payment_type, payment_amount', false);
		$this->db->from('sales_payments');
		$this->db->join('sales', 'sales.sale_id=sales_payments.sale_id and foreup_sales.course_id = "'.$this->session->userdata('course_id').'"');
		$this->db->where('date(sale_time) BETWEEN "'. $this->params['start_date']. '" and "'. $this->params['end_date'].'"');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('payment_amount > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('payment_amount < 0');
		}
		if ($this->params['terminal'] != 'all')
			$this->db->where('terminal_id = "'.$this->params['terminal'].'"');
		
		$this->db->where($this->db->dbprefix('sales').'.deleted', 0);
		$this->db->order_by('sale_id, payment_type');
		$sales_payments = $this->db->get()->result_array();		
		//print_r($sales_payments);
		//added by James in order to calculate the average sale price. 
		$average_sales = array(
			'total'=>0,
			'count'=>0
		);
		foreach($sales_payments as $row)
		{
			if ($row['payment_amount'] > 0) {				
				$average_sales['total'] += $row['payment_amount'];
				$average_sales['count'] += 1;
			}		

        	$payments_by_sale[$row['sale_id']][] = $row;
			$payment_totals_by_sale[$row['sale_id']] += $row['payment_amount'];
		}
		
		if ($this->params['department'] == 'all')
		{
			$payment_data = array();
			
			foreach($payments_by_sale as $sale_id => $payment_rows)
			{
				$total_sale_balance = $sales_totals[$sale_id];
				$cdn = $this->config->item('customer_credit_nickname')!=''?$this->config->item('customer_credit_nickname'):lang('customers_account_balance');
				$mbn = $this->config->item('member_balance_nickname')!=''?$this->config->item('member_balance_nickname'):lang('customers_member_account_balance');
				//echo $cdn.' - '.$mbn;	
				foreach($payment_rows as $row)
				{
					$payment_amount = $row['payment_amount'];// <= $total_sale_balance ? $row['payment_amount'] : $total_sale_balance;
					//echo $row['payment_type'].' - '.$row['payment_amount'].'<br/>';
					//Consolidating Credit Cards,  Account Payments, and Giftcards
					if (strpos($row['payment_type'], 'Tip') !== false)
					{
						$row['payment_type'] = 'Tips';
					}
					else if (strpos($row['payment_type'], 'M/C') !== false || strpos($row['payment_type'], 'VISA') !== false || strpos($row['payment_type'], 'AMEX') !== false || strpos($row['payment_type'], 'DCVR') !== false)
					{
						$row['payment_type'] = 'Credit Card';
					}
					else if (strpos($row['payment_type'], $cdn) !== false)
					{
							$row['payment_type'] = $cdn;
							//echo $payment_amount.'<br/>';
					}
					else if (strpos($row['payment_type'], $mbn) !== false)
					{
							$row['payment_type'] = $mbn;
							//echo $payment_amount.'<br/>';
					}
					else if (strpos($row['payment_type'], 'Change') !== false)
					{
						$row['payment_type'] = 'Cash';
					}
					else if (strpos($row['payment_type'], 'Gift Card') !== false)
						$row['payment_type'] = 'Gift Card';
					
					//if($row['payment_type'] == 'Credit Card')
						//echo 'Sale '.$row['sale_id'].' - '.$payment_amount."<br/>";
					
					if (!isset($payment_data[$row['payment_type']]))
						$payment_data[$row['payment_type']] = array('payment_type' => $row['payment_type'], 'payment_amount' => 0 );
					if ($total_sale_balance != 0 || $row['payment_type'] == 'Tips')
						$payment_data[$row['payment_type']]['payment_amount'] += $payment_amount;
					
					if (!isset($payment_data['Total']))
						$payment_data['Total'] = array('payment_type' => 'Total', 'payment_amount' => 0 );
					if ($total_sale_balance != 0 || $row['payment_type'] == 'Tips')
						$payment_data['Total']['payment_amount'] += $payment_amount;
					
					
					
					$total_sale_balance-=$payment_amount;
				}
			}
			
			if ($include_average_sales) {			
				$payment_data['average_sale'] = number_format($average_sales['total'] / $average_sales['count'], 2);
			}
			return $payment_data;
		}
		else {
			$payment_data = array();
			
			foreach($payments_by_sale as $sale_id => $payment_rows)
			{
				$total_sale_balance = $sales_totals[$sale_id];
				$cdn = $this->config->item('customer_credit_nickname')!=''?$this->config->item('customer_credit_nickname'):lang('customers_account_balance');
				$mbn = $this->config->item('member_balance_nickname')!=''?$this->config->item('member_balance_nickname'):lang('customers_member_account_balance');
				//echo $cdn.' - '.$mbn;	
				foreach($payment_rows as $row)
				{
					$payment_amount = $row['payment_amount'];// <= $total_sale_balance ? $row['payment_amount'] : $total_sale_balance;
					//echo $row['payment_type'].' - '.$row['payment_amount'].'<br/>';
					//Consolidating Credit Cards,  Account Payments, and Giftcards
					if (strpos($row['payment_type'], 'Tip') !== false)
					{
						$row['payment_type'] = 'Tips';
					}
					else if (strpos($row['payment_type'], 'M/C') !== false || strpos($row['payment_type'], 'VISA') !== false || strpos($row['payment_type'], 'AMEX') !== false || strpos($row['payment_type'], 'DCVR') !== false)
					{
						$row['payment_type'] = 'Credit Card';
					}
					else if (strpos($row['payment_type'], $cdn) !== false)
					{
							$row['payment_type'] = $cdn;
							//echo $payment_amount.'<br/>';
					}
					else if (strpos($row['payment_type'], $mbn) !== false)
					{
							$row['payment_type'] = $mbn;
							//echo $payment_amount.'<br/>';
					}
					else if (strpos($row['payment_type'], 'Change') !== false)
					{
						$row['payment_type'] = 'Cash';
					}
					else if (strpos($row['payment_type'], 'Gift Card') !== false)
						$row['payment_type'] = 'Gift Card';
					
					//if($row['payment_type'] == 'Credit Card')
						//echo 'Sale '.$row['sale_id'].' - '.$payment_amount."<br/>";
					
					if (!isset($payment_data[$row['payment_type']]))
						$payment_data[$row['payment_type']] = array('payment_type' => $row['payment_type'], 'payment_amount' => 0 );
					if ($total_sale_balance != 0)
						$payment_data[$row['payment_type']]['payment_amount'] += $payment_amount;
					
					if (!isset($payment_data['Total']))
						$payment_data['Total'] = array('payment_type' => 'Total', 'payment_amount' => 0 );
					if ($total_sale_balance != 0)
						$payment_data['Total']['payment_amount'] += $payment_amount;
					
					
					
					$total_sale_balance-=$payment_amount;
				}
			}
			
			if ($include_average_sales) {			
				$payment_data['average_sale'] = number_format($average_sales['total'] / $average_sales['count'], 2);
			}
			return $payment_data;
		}
	}
	
	/*public function getSummaryData($on_account = 0)
	{
		$this->db->select('sum(subtotal) as subtotal, sum(tax) as tax, sum(total) as total, sum(profit) as profit');
		$this->db->from('sales_items_temp');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		
		$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);
		$summary_data = $this->db->get()->row_array();
		$final_summary_data = array();
		foreach ($summary_data as $name => $value)
		{
			if ($name == 'subtotal' && $on_account != 0)
			{
				$final_summary_data[$name] = $value;
				//$final_summary_data['account'] = -$on_account;
			}
			else if ($name == 'total' || $name == 'profit')
				$final_summary_data[$name] = $value - $on_account;
			else
				$final_summary_data[$name] = $value;
		}
		return $final_summary_data;
	}*/
	public function getSummaryData($totals = array())
	{
		//if (!$this->permissions->is_super_admin())
                  //  $this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->select('payment_type');
		$this->db->from('sales_items_temp');
		if ($this->params['department'] != 'all')
			$this->db->where('department = "'.$this->params['department'].'"');
		$this->db->group_by('sale_id');
		$results = $this->db->get()->result_array();
		$account_charges = 0;
		foreach($results as $result) {
			//TODO:Account for multiple payments on account in one transaction
			if (preg_match('/Account- [^$]+: \$(.+)</',$result['payment_type'], $match)) {
				$account_charges += (float)$match[1];
			}
		}
		$cdn = $this->config->item('customer_credit_nickname')!=''?$this->config->item('customer_credit_nickname'):lang('customers_account_balance');
		$mbn = $this->config->item('member_balance_nickname')!=''?$this->config->item('member_balance_nickname'):lang('customers_member_account_balance');
		$account_charges = (isset($totals[$cdn]['payment_amount']))?$totals[$cdn]['payment_amount']:0;
		$member_balance = (isset($totals[$mbn]['payment_amount']))?$totals[$mbn]['payment_amount']:0;
		$giftcards = (isset($totals['Gift Card']['payment_amount']))?$totals['Gift Card']['payment_amount']:0;
		$rainchecks = (isset($totals['Raincheck']['payment_amount']))?$totals['Raincheck']['payment_amount']:0;
		$tips = (isset($totals['Tips']['payment_amount']))?$totals['Tips']['payment_amount']:0;
		$credit_cards = (isset($totals['Credit Card']['payment_amount']))?$totals['Credit Card']['payment_amount']:0;
		$check = (isset($totals['Check']['payment_amount']))?$totals['Check']['payment_amount']:0;
		$cash = (isset($totals['Cash']['payment_amount']))?$totals['Cash']['payment_amount']:0;
		$total_payments = $credit_cards + $check + $cash;//(isset($totals['Total']['payment_amount']))?$totals['Total']['payment_amount']-$account_charges-$member_balance-$giftcards:0;		//echo 'Account total: '.$account_charges;
		//$total_payments = (isset($totals['Total']['payment_amount']))?$totals['Total']['payment_amount']-$account_charges-$member_balance-$giftcards:0;		//echo 'Account total: '.$account_charges;
		//print_r($totals);
		//echo $account_charges.' - '.$giftcards.' - '.$total_payments;
		
		//This section was implemented for East Bay, it is a temporary fix, and must be updated
		//TODO: Remove this and fix the problem of tracking where account balances are spent.	
		//$summary_data['profit'] = 
		$summary_data['total'] = 
		//$summary_data['rounding_error'] = 
		$summary_data['tax'] = 
		$summary_data['tips'] = 
		$summary_data['rainchecks'] = 
		$summary_data['giftcards'] = 
		$summary_data['member_balance'] = 
		$summary_data['account_charges'] = 
		$summary_data['subtotal'] = 0;
		//Tax totaling fix
        // $this->db->select('sum(subtotal) as subtotal, percent');
		// $this->db->from('sales_items_temp');
		// if ($this->params['department'] != 'all')
			// $this->db->where('department = "'.$this->params['department'].'"');
		// if ($this->params['terminal'] != 'all')
			// $this->db->where('terminal_id = "'.$this->params['terminal'].'"');
		// if ($this->params['sale_type'] == 'sales')
		// {
			// $this->db->where('quantity_purchased > 0');
		// }
		// elseif ($this->params['sale_type'] == 'returns')
		// {
			// $this->db->where('quantity_purchased < 0');
		// }
// 		
		// $this->db->where('deleted', 0);
		// $this->db->group_by('percent');
		// foreach ($this->db->get()->result_array() as $key => $row)
		// {
			// //print_r($row);
			// $percent = ($row['percent'] != '')?$row['percent']:0;
		// //	$summary_data['tax'] += $row['subtotal']*($percent/100);
		// }
		//$summary_data['tax'] = round($summary_data['tax'], 2);
		//End tax totaling fix
		
        $this->db->select('sum(subtotal) as subtotal, sum(tax) as collected_tax, sum(item_cost_price) as cost, sum(total) as total, sum(profit) as profit');
		$this->db->from('sales_items_temp');
		if ($this->params['department'] != 'all')
			$this->db->where('department = "'.$this->params['department'].'"');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		
		$this->db->where('deleted', 0);
		//$summary_data = array();
		$totals_array = $this->db->get()->row_array();
//		foreach ($this->db->get()->row_array() as $key => $value) 
	//		if ($key != 'tax' && $key!='subtotal')
		//		$summary_data[$key] = $value - $account_charges;
			//else if ($key == 'subtotal') {
				//$summary_data[$key] = $value;
				//$summary_data['account_charges'] = -$account_charges;
			//}
			//else 
				//$summary_data[$key] = $value;
		$summary_data['total'] = $total_payments;
		$summary_data['account_charges'] = -$account_charges;
		$summary_data['member_balance'] = -$member_balance;
 		$summary_data['giftcards'] = -$giftcards;
		$summary_data['rainchecks'] = -$rainchecks;
		$summary_data['tips'] = -$tips;
		$summary_data['subtotal'] = $totals_array['subtotal'];
		$summary_data['tax'] = $totals_array['collected_tax'];		
		//$summary_data['rounding_error'] = $totals_array['collected_tax'] - $summary_data['tax'];
		//$summary_data['profit'] = $summary_data['total'] - $totals_array['cost'] - $summary_data['tax'];
				
		return $summary_data;
	}
	public function getAllData() {
		$this->db->select('*');
		$this->db->from('sales_items_temp');
		//$this->db->limit(1000);
		$result = $this->db->get();
		foreach ($result->result() as $r)
		{
			echo "<br/>";
		}
		echo '<br/>'.$this->db->last_query();
		echo '<br/>';
		print_r($result);
		echo '<br/>';
		print_r($result->result());
		echo '<br/>';
		print_r($result->result_array());
		return ($result->result_array());
		//return $result->row_array();
	}
}
?>