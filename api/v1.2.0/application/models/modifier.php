<?php
class Modifier extends CI_Model {
	public function __construct(){
		$this->load->helper('array');
	}

	public function structure_options($row){
		$options =& $row['options'];
		$default =& $row['default'];
		$selected =& $row['selected_option'];

		$structuredOptions = array();
		if(!empty($options)){
			$options = json_decode($row['options'], true);
		}

		// If no options set, just default to 'yes' or 'no'
		if(empty($options) || empty($options[0])){
			$options = array(array('label'=>'yes', 'price'=>0.00), array('label'=>'no', 'price'=>0.00));
		}

		// Set default selected option (if set)
		if(empty($selected)){
			$selected = $default;
		}

		$row['selected_option'] = $selected;
		$price = 0.00;
		foreach($options as $option){
			if(strtolower($selected) == strtolower($option['label'])){
				$price = (float) round($option['price'], 2);
			}
		}
		$row['selected_price'] = $price;

		return $row;
	}

	public function search($query){

		$this->db->select('modifier_id AS value, name AS label');
		$this->db->from('modifiers');
		$this->db->where("name LIKE '%".$this->db->escape_like_str($query)."%'");
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('deleted', 0);
		$query = $this->db->get();

		$rows = $query->result_array();
		return $rows;
	}

	public function get_by_item($itemId){

		if(empty($itemId)){
			return false;
		}

		$this->db->select('m.modifier_id, m.name, IF(override_price IS NULL, default_price, override_price) AS price,
			m.options, im.item_id, im.default', false);
		$this->db->from('modifiers AS m');
		$this->db->join('item_modifiers AS im', 'im.modifier_id = m.modifier_id', 'inner');
		$this->db->where(array('im.item_id' => $itemId));
		$this->db->where('m.course_id', $this->session->userdata('course_id'));
		$this->db->where('m.deleted', 0);

		$query = $this->db->get();
		$modifiers = $query->result_array();
		$rows = array();

		// Loop through rows and decode JSON options into array
		foreach($modifiers as $key => $row){
			$rows[$row['modifier_id']] = $this->structure_options($row);
			unset($rows[$row['modifier_id']]['auto_select']);
		}

		return $rows;
	}

	public function get($modifierId = null, $itemId = null){

		$this->db->select('*');
		$this->db->from('modifiers');

		if(!empty($itemId)){
			$this->db->join('item_modifiers', 'item_modifiers.modifier_id = modifiers.modifier_id', 'inner');
			$this->db->where(array('item_modifiers.item_id' => $itemId));
		}

		if(!empty($modifierId)){
			$this->db->where(array('modifiers.modifier_id' => $modifierId));
		}

		$this->db->where('modifiers.course_id', $this->session->userdata('course_id'));
		$this->db->where('modifiers.deleted', 0);

		$query = $this->db->get();
		$rows = $query->result_array();
		$data = array();

		foreach($rows as $key => $row){
			$data[] = $this->structure_options($row);
		}

		return $data;
	}

	public function save($data, $modifierId = null){

		if(empty($data)){
			return false;
		}

		// Filter fields passed
		$data = elements(array('name', 'category_id', 'options', 'date_created', 'item', 'required'), $data, null);
		$itemData = elements(array('item_id', 'override_price', 'default'), $data['item'], null);
		unset($data['item']);
		$data['course_id'] = $this->session->userdata('course_id');

		if(!empty($data['options']) && is_array($data['options'])){
			$data['options'] = json_encode($data['options']);
		}else{
			$data['options'] = null;
		}

		foreach($data as $field => $val){
			if($val == null){
				unset($data[$field]);
			}
		}

		$itemId = null;
		if(!empty($itemData['item_id'])){
			$itemId = $itemData['item_id'];
		}

		if(!empty($data)){

			// If creating a new record
			if(empty($modifierId)){
				$success = $this->db->insert('modifiers', $data);
				$modifierId = $this->db->insert_id();

			// If updating an existing record
			}else{
				$success = $this->db->update('modifiers', $data, array('modifier_id' => $modifierId));
			}
		}

		if(!empty($itemId)){
			// Check if item modifier already exists
			$query = $this->db->get_where('item_modifiers', array('modifier_id' => $modifierId, 'item_id' => $itemId));

			if($query->num_rows() > 0){
				foreach($itemData as $field => $val){
					if($val === null){
						unset($data[$field]);
					}
				}

				if(!empty($itemData)){
					unset($itemData['item_id']);
					$itemSuccess = $this->db->update('item_modifiers', $itemData, array('item_id' => $itemId, 'modifier_id' => $modifierId));
				}

			// If item modifier link doesn't exist, insert it
			}else{

				if(!empty($itemData)){
					$itemData['modifier_id'] = $modifierId;
					$itemSuccess = $this->db->insert('item_modifiers', $itemData);
				}
			}
		}

		return $modifierId;
	}

	public function delete($modifierId){
		if(empty($modifierId)){
			return false;
		}

		return $this->db->update('modifiers', array('deleted' => 1), array('modifier_id'=>$modifierId));
	}

	public function delete_from_item($itemId, $modifierId){
		if(empty($modifierId) || empty($itemId)){
			return false;
		}

		return $this->db->delete('item_modifiers', array('modifier_id'=>$modifierId, 'item_id'=>$itemId));
	}
}
?>