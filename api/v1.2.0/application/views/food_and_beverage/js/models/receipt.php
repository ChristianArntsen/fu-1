var Receipt = Backbone.Model.extend({
	idAttribute: "receipt_id",
	defaults: {
		"receipt_id": null,
		"items": [],
		"date_paid": null,
		"status": "pending",
		"total": 0.00,
		"subtotal": 0.00,
		"tax": 0.00,
		"amount_due": 0.00
	},

	set: function(attributes, options){
		if(!attributes.receipt_id && !this.get('receipt_id')){
			attributes.receipt_id = App.receipts.getNextNumber();
			attributes.id = attributes.receipt_id;
		}
		Backbone.Model.prototype.set.call(this, attributes, options);
	},

	initialize: function(){
		var receipt_id = this.get('receipt_id');

		// Initialize item collection
		var itemCollection = new ReceiptCart(this.get('items'));
		itemCollection.url = App.api_table + 'receipts/' + receipt_id + '/items/';

		// Initialize payment collection
		var paymentCollection = new PaymentCollection(this.get('payments'));
		paymentCollection.url = App.api_table + 'receipts/' + receipt_id + '/payments/';

		this.set({
			"items": itemCollection,
			"payments": paymentCollection
		});

		this.listenTo(this.get('payments'), 'add remove reset', this.markPaid, this);
	},

	calculateTotals: function(){
		var splitItems = this.collection.getSplitItems();

		// Init receipt variables
		data = {};
		data.total = 0.00;
		data.subtotal = 0.00;
		data.tax = 0.00;

		// Add up all the items in receipt
		_.each(this.get('items').models, function(item){
			var line = item.get('line');
			var divisor = splitItems[line];

			item.calculatePrice(divisor);
			data.total += item.get('split_total') + item.get('split_sides_total');
			data.subtotal += item.get('split_subtotal') + item.get('split_sides_subtotal');
			data.tax += item.get('split_tax') + item.get('split_sides_tax');
		});

		// Make sure receipt totals are rounded nicely
		data.total = _.round(data.total, 2);
		data.subtotal = _.round(data.subtotal, 2);
		data.tax = _.round(data.tax, 2);

		// Update the receipt with totals
		this.set(data);
		return data;
	},

	getTotalDue: function(){
		// Figure out how much is due on the receipt
		this.calculateTotals();
		var totalPaid = _.round(this.get('payments').getTotal(), 2);
		totalDue = _.round(this.get('total') - totalPaid, 2);
		return totalDue;
	},

	isPaid: function(){
		if(this.getTotalDue() > 0){
			return false;
		}
		return true;
	},

	markPaid: function(){
		var isPaid = 1;
		if(this.get('payments').getTotal() == 0){
			var isPaid = 0;
		}

		// Mark each item in cart as paid or unpaid
		_.each(this.get('items').models, function(item){
			var line = item.get('line');
			App.cart.get(line).set({'is_paid': isPaid});
		});

		// Mark receipt as paid (if entire receipt has been paid)
		if(this.isPaid()){
			this.set({'status':'complete'});
		}
	},

	printReceipt: function(event){
		console.log('------------- printing receipt models/receipt.php ----------------');

		this.calculateTotals();

		var receiptText = '';
		var EOL = '\r\n';
		var receipt = this;

		receiptText += 'Receipt #' + receipt.get('receipt_id') + EOL +
			'================================' + EOL +
			'ITEMS' + EOL;
		var cart = [];
		// Loop through items on receipt
		_.each(this.get('items').models, function(item){
			var sides = [];
			receiptText += item.get('name') + '........................' + accounting.formatMoney(item.get('split_subtotal')) + EOL;

			// Loop through sides, soups, and salads (if any of each)
			if(item.get('sides')){
				_.each(item.get('sides').models, function(side){
					receiptText += side.get('name') + '........................' + accounting.formatMoney(side.get('split_subtotal')) + EOL;
					if (side.get('split_subtotal') > 0)
						sides.push({name:side.get('name'), price:side.get('split_subtotal')});
				});
			}
			if(item.get('soups')){
				_.each(item.get('soups').models, function(soup){
					receiptText += soup.get('name') + '........................' + accounting.formatMoney(soup.get('split_subtotal')) + EOL;
					if (soup.get('split_subtotal') > 0)
						sides.push({name:soup.get('name'), price:soup.get('split_subtotal')});
				});
			}
			if(item.get('salads')){
				_.each(item.get('salads').models, function(salad){
					receiptText += salad.get('name') + '........................' + accounting.formatMoney(salad.get('split_subtotal')) + EOL;
					if (salad.get('split_subtotal') > 0)
						sides.push({name:salad.get('name'), price:salad.get('split_subtotal')});
				});
			}
			cart.push({name:item.get('name'), quantity:item.get('quantity'), discount:_.round(item.get('discount'), 2), price:item.get('split_subtotal'), sides:sides});
		});

		var payments = [];
		// Loop through payments
		_.each(this.get('payments').models, function(payment){
			console.log('payment');
			console.dir(payment);
			payments.push({type:payment.attributes.type+' '+payment.attributes.card_number,amount:payment.attributes.amount});
		});


		receiptText += '================================' + EOL +
		'Subtotal ........................' + accounting.formatMoney(receipt.get('subtotal')) + EOL +
		'Tax ........................' + accounting.formatMoney(receipt.get('tax')) + EOL +
		'TOTAL ........................' + accounting.formatMoney(receipt.get('total')) + EOL;

		//alert(receiptText);

		var data = {
			cart:cart,// Each object needs to include name, quantity, discount, and price
			payments:payments,
			subtotal:receipt.get('subtotal'),
			tax:receipt.get('tax'),
			total:receipt.get('total'),
			//change_due:''
		};

		console.debug(data); //JBDEBUG

		var receipt_data = '';
		receipt_data = webprnt.build_itemized_receipt(receipt_data, data);
		//if (App.receipt.print_tip_line)
		//{
		//	receipt_data = webprnt.add_tip_line(receipt_data);
		//	if (webprnt.credit_card_payments)
		//		receipt_data = webprnt.add_signature_line(receipt_data);
		//}
		if (App.receipt.return_policy != '')
		{
	    	receipt_data = webprnt.add_return_policy(receipt_data, App.receipt.return_policy);
	    }
	    //receipt_data = webprnt.add_barcode(receipt_data, 'Sale ID', data.sale_id);
	    receipt_data = webprnt.add_itemized_header(receipt_data, App.receipt.header, receipt.get('receipt_id'))
	    receipt_data = webprnt.add_receipt_header(receipt_data, App.receipt.header)
	    receipt_data = webprnt.add_paper_cut(receipt_data);
	    //if (App.receipt. != undefined && double_print == true)
	    //	receipt_data += receipt_data;
	    //receipt_data = webprnt.add_cash_drawer_open(receipt_data);
	    webprnt.add_space(receipt_data);
	    webprnt.print(receipt_data, "http://"+App.receipt_ip+"/StarWebPrint/SendMessage");

		return false;
	}
});