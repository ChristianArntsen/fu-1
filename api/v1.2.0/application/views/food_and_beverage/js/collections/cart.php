var Cart = Backbone.Collection.extend({
	url: App.api_table + 'cart',
	model: function(attrs, options){
		return new CartItem(attrs, options);
	},

	getNextSeat: function(){
		var seat = 0;
		_.each(this.models, function(cartItem){
			var curSeat = parseInt(cartItem.get('seat'));

			if(curSeat && curSeat > seat){
				seat = curSeat;
			}
		});

		return seat + 1;
	},

	getNextLine: function(){
		var line = 0;
		_.each(this.models, function(cartItem){
			var curLine = parseInt(cartItem.get('line'));

			if(curLine && curLine > line){
				line = curLine;
			}
		});

		return line + 1;
	},

	getTotals: function(){
		var total = 0.00;
		var subtotal = 0.00;
		var tax = 0.00;
		var num_items = 0;

		_.each(this.models, function(cartItem){
			total += cartItem.get('total') + cartItem.get('sides_total');
			subtotal += cartItem.get('subtotal') + cartItem.get('sides_subtotal');
			tax += cartItem.get('tax') + cartItem.get('sides_tax');
			num_items++;
		});

		return {
			"total": _.round(total, 2),
			"subtotal": _.round(subtotal, 2),
			"tax": _.round(tax, 2),
			"num_items": _.round(num_items, 2)
		};
	},

	unSelectItems: function(){
		var selectedItems = this.where({"selected":true});
		_.each(selectedItems, function(item){
			item.unset("selected");
		});

		return this;
	}
});
