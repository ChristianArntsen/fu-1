<script type="text/html" id="template_add_tip">
<div class="row" style="padding: 20px; overflow: hidden;">
	<div class="form_field">
		<label style="width: 80px; float: left; font-weight: bold;">Amount</label>
		<input type="text" name="amount" id="tip_amount" style="width: 100px;" value="<%=accounting.formatMoney(amount, '')%>" />
	</div>
	<div class="form_field">
		<label style="width: 80px; float: left; font-weight: bold;">Type</label>
		<input type="hidden" name="type" id="tip_type" value="<%-type%>" />
		<span style="font-size: 16px; float: left; line-height: 35px;"><%-type%></span>
	</div>
	<input type="hidden" name="invoice_id" id="tip_invoice_id" value="<%-invoice_id%>" />
</div>
<div class="row" style="padding: 20px; overflow: hidden;">
	<a href="#" class="save fnb_button" style="display: block; width: 100px;" href="#">Save Tip</a>
</div>
</script>