<script type="text/html" id="template_open_register">
<div class="row">Enter the starting amount for your register</div>
<div class="row">
	<div class="form_field">
		<label style="width: 80px; float: left; font-weight: bold;">Amount</label>
		<input type="text" name="amount" id="register_open_amount" style="width: 100px;" value="<%=accounting.formatMoney(open_amount, '')%>" />
	</div>
</div>
<div class="row" style="padding: 20px; overflow: hidden;">
	<a href="#" class="skip fnb_button" style="display: block; float: left; width: 100px; font-size: 14px;" href="#">Skip</a>
	<a href="#" class="save fnb_button" style="display: block; float: right; width: 100px; font-size: 14px; font-weight: bold" href="#">Save</a>
</div>
</script>