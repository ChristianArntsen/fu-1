<div id='user_menu_contents'>
	<div id='user_menu_header'>
	</div>
	<div id='user_actions'>
		<?php
			$allowed_modules = ($this->Module->get_allowed_modules($this->session->userdata('person_id')));
			foreach($allowed_modules->result() as $module)
			{
				$module_list[] = $module->module_id;
			}
			?>
		<div id="home_module_list">
			<div class="module_item">
				<a id='timeclock_button' href="<?php echo site_url("timeclock");?>">
					<span class="module_icon timeclock_icon"></span>
					<div>
						<div><?php echo lang("module_timeclock") ?></div>
						<div class='module_desc'><?php echo lang('module_timeclock_desc');?></div>
					</div>
				</a>
			</div>
			<script>
				$('#timeclock_button').colorbox({width:500,title:'Time Clock'});
			</script>
			<?php
		        if ($this->permissions->is_super_admin() || ($this->permissions->course_has_module('employees') && in_array('config',$module_list)))
		        {
		
			?>
			<div class="module_item">
				<a href="<?php echo site_url("employees");?>">
					<span class="module_icon employees_icon"></span>
					<div>
						<div><?php echo lang("module_employees") ?></div>
						<div class='module_desc'><?php echo lang('module_employees_desc');?></div>
					</div>
				</a>
			</div>
			<?php
			}
			?>
			<?php
		        if ($this->permissions->is_super_admin() || ($this->permissions->course_has_module('config') && in_array('config',$module_list)))
		        {
		
			?>
			<div class="module_item">
				<a href="<?php echo site_url("config");?>">
					<span class="module_icon config_icon"></span>
					<div>
						<div><?php echo lang("module_config") ?></div>
						<div class='module_desc'><?php echo lang('module_config_desc');?></div>
					</div>
				</a>
			</div>
			<?php
			}
			?>
			<div class="module_item">
				<a href="<?php echo site_url("support");?>" target='_blank'>
					<span class="module_icon support_icon"></span>
					<div>
						<div>Support</div>
						<div class='module_desc'>Help section</div>
					</div>
				</a>
			</div>
			<div class="module_item">
				<a href="index.php/home/logout">
					<span class="module_icon logout_icon"></span>
					<div>
						<div>Logout</div>
						<div class='module_desc'>Sign out of ForeUP</div>
					</div>
				</a>
			</div>
			<div class='clear'></div>
		</div>
	</div>
</div>