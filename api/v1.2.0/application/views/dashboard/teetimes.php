<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php $this->load->view("partial/header"); ?>
<style type="text/css">
  table.dashboard tbody td{
    font-weight: bold;
  }
  #placeholder{float:left;}
  #legend{
    float:left;
    margin-top: 50px;
    margin-left: 15px;
  }
  #legend td.legendLabel{
    padding-left: 5px;
  }
  #table_holder {
  	width:100%;
  }
  .money {
  	text-align:right;
  }
</style>
<table>
	<tbody>
	  <tr>
	    <td id="item_table">
	    <div id="table_holder">
	      <br/>
	    <h4>Teetime Totals</h4>
	    <table class="dashboard">
	      <thead>
	        <tr>
	          <th class="leftmost">Reserved</th>
	          <th>Online</th>
	          <th>Courses</th>
	          <th>Sold</th>
	          <th>Unsold</th>
	          <th class="rightmost">Revenue</th>
	        </tr>
	      </thead>
	      <tbody>
	        <tr>
	          <td width="15%"><?= $teetime_stats['total']; ?></td>
	          <td width="15%"><?= $teetime_stats['online']; ?></td>
	          <td width="15%"><?= $teetime_stats['booking_courses'].'/'.$teetime_stats['courses']; ?></td>
	          <td width="15%"><?= $teetime_stats['sold_teetimes'] ?></td>
	          <td width="15%"><?= $teetime_stats['potential_teetimes'] ?></td>
	          <td width="25%"><?= $teetime_stats['revenue'] ?></td>
	        </tr>
	      </tbody>
	    </table>
	    </div>
	    </td>
	  </tr>
	  <tr>
	    <td id="item_table">
	    <div id="table_holder">
	      <br/>
	    <h4>Individual Courses</h4>
	    <table class="dashboard">
	      <thead>
	        <tr>
	          <th class="leftmost"></th>
	          <th colspan="3">Last Month</th>
	          <th colspan="3">This Month</th>
	          <th colspan='3' class="rightmost">Total</th>
	        </tr>
	        <tr>
	          <th>Course Name</th>
	          <th>Online/Res.</th>
	          <th>Sold/Unsold</th>
	          <th>Revenue</th>
	          <th>Online/Res.</th>
	          <th>Sold/Unsold</th>
	          <th>Revenue</th>
	          <th>Online/Res.</th>
	          <th>Sold/Unsold</th>
	          <th>Revenue</th>
	        </tr>
	      </thead>
	      <tbody>
	      	<?php foreach($tcs as $cs) { ?>
	        <tr>
	          <td width="37%"><?= $cs['name']; ?></td>
	          <td class='money' width="7%"><?= number_format($cs['online_last_month']).'/'.number_format($cs['reserved_last_month']); ?></td>
	          <td class='money' width="7%"><?= number_format($cs['sold_last_month']).'/'.number_format($cs['potential_last_month']-$cs['sold_last_month']); ?></td>
	          <td class='money' width="7%"><?= to_currency($cs['revenue_last_month']); ?></td>
	          <td class='money' width="7%"><?= number_format($cs['online_this_month']).'/'.number_format($cs['reserved_this_month']); ?></td>
	          <td class='money' width="7%"><?= number_format($cs['sold_this_month']).'/'.number_format($cs['potential_this_month']-$cs['sold_this_month']); ?></td>
	          <td class='money' width="7%"><?= to_currency($cs['revenue_this_month']); ?></td>
	          <td class='money' width="7%"><?= number_format($cs['online']).'/'.number_format($cs['total']); ?></td>
	          <td class='money' width="7%"><?= number_format($cs['sold']).'/'.number_format($cs['potential_teetimes']-$cs['sold']); ?></td>
			  <td class='money' width="7%"><?= to_currency($cs['teetime_revenue']); ?></td>
	        </tr>
	        <?php } ?>
	      </tbody>
	    </table>
	    </div>
	    </td>
	  </tr>
	</tbody>
</table>


<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>