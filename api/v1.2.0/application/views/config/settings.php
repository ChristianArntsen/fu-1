<?php $this->load->view("partial/header"); ?>
<style>
    .ui-widget {
        font-size:.8em;
    }
    .ui-buttonset .ui-button {
        margin-right:-0.5em;
    }
    #slider { 
        margin: 10px 10px 10px 20px; 
        width:250px;
    }
    .ui-slider-handle {
        font-size:.8em;
        padding:1px 0 0 2px;
    }
    .tab_container {
	border: 1px solid #999;
	border-top: none;
	overflow: hidden;
	clear: both;
	float: left; 
        width: 900px;
	background: #fff;
        margin:0 26px
}
.tab_content {
	padding: 20px;
	font-size: 1.2em;
        overflow: auto;
}
    ul.tabs {
	margin: 0 26px;
	padding: 0;
	float: left;
	list-style: none;
	height: 46px; /*--Set height of tabs--*/
	border-bottom: 1px solid #999;
	border-left: 1px solid #999;
	width: 900px;
}
ul.tabs li {
	float: left;
	margin: 0;
	padding: 0;
	height: 45px; /*--Subtract 1px from the height of the unordered list--*/
	line-height: 45px; /*--Vertically aligns the text within the tab--*/
	border: 1px solid #999;
	border-left: none;
	margin-bottom: -1px; /*--Pull the list item down 1px--*/
	overflow: hidden;
	position: relative;
	background: #e0e0e0;
}
ul.tabs li a {
	text-decoration: none;
	color: #000;
	display: block;
	font-size: .9em;
	padding: 0 10px;
	border: 1px solid #fff; /*--Gives the bevel look with a 1px white border inside the list item--*/
	outline: none;
        width:114px;
        text-align: center;
}
ul.tabs li a div {
    height:12px;
}
ul.tabs li a span {
    font-size:.7em
}
ul.tabs li a:hover {
	background: #ccc;
}
html ul.tabs li.active, html ul.tabs li.active a:hover  { /*--Makes sure that the active tab does not listen to the hover properties--*/
	background: #fff;
	border-bottom: 1px solid #fff; /*--Makes the active tab look like it's connected with its content--*/
}
#time_button_set {
    float:left;
    margin:10px 0 0 16px;
}
#player_button_set {
    float:right;
    margin:10px 21px 0 0;
}
.search_controls {
    padding:5px;
}
.teetime {
    border:1px solid #dedede;
    padding:8px 15px;
    margin-bottom:5px;
}
.teetime .left {
    float:left;
    width:10%;
}
.teetime .center {
    float:left;
    width:75%;
    text-align: center;
}
.teetime .right {
    float:right;
    width:10%;
}
.teetime .left .players span {
    float: left;
}
.teetime .left .players span.ui-button-text {
    font-size: .6em;
    margin: 1px 0 0 2px;
}
.centered_submit {
    text-align:center;
}
.settings_note {
    color:#666;
    font-size:.8em;
    margin-left:10px;
}
.tee_time_label {
    font-size: 12px;
    text-align: right;
    width:15%;
    padding-right: 10px;
}
.price_cell {
    width:10%;
}
.price_label {
    width:60px;
    height:20px;
    margin-right:5px;
}
fieldset tr.field_row {
    height:35px;
}
.small_note {
    font-size:.6em;
}
.first_cell {
    width:25%;
}
.day_cell {
    width:13%;
}
#green_fee_prices {
	scroll:auto;
}
#teetime_tax_rate, #cart_tax_rate {
	width:60px;
}
fieldset div.field_row label {
	width:250px;
}
.default_price_class_row .price_cell {
	text-align:center;
}
</style>
<div id='settings_box'>
<fieldset id="config_info">
	<ul id="error_message_box"></ul>
<ul class="tabs">
        <li class='settings_title'>Course Settings</li>
        <li id='tab_1'><a href="#tab1">Details</a></li>
        <li id='tab_2'><a href="#tab2">Hours/Booking</a></li>
        <?php if (!$this->permissions->course_has_module('reservations')) { ?>
        <li id='tab_3'><a href="#tab3">Green Fees</a></li>
        <?php } else { ?>
        <li id='tab_3'><a href="#tab3">Fees</a></li>
        <?php } ?>
        <?php if (!$this->permissions->course_has_module('reservations')) { ?>
        <li id='tab_4'><a href="#tab4">Teesheet</a></li>
        <?php } ?>
        <li id='tab_5'><a href="#tab5">POS</a></li>
        <li id='tab_6'><a href="#tab6">Misc</a></li>
        <li id='tab_7'><a href="#tab7">Terminals</a></li>
        <li id='tab_8'><a href="#tab8">Loyalty</a></li>
    </ul>

    <div class="tab_container">
        <div id="tab1" class="tab_content" style='<?= ($page == 1 ? '' : 'display:none')?>'>
        <?php
			echo form_open('config/save/details',array('id'=>'config_details_form'));
		?>

        	<?php $this->load->view("config/details"); ?>
        	<div class="centered_submit">
				<?php 
				echo form_submit(array(
					'name'=>'submit_details',
					'id'=>'submit_details',
					'value'=>lang('common_save'),
					'class'=>'submit_button float_right')
				);
				?>
        	</div>
        </form>
        </div>    
	    <div id="tab2" class="tab_content" style='<?= ($page == 2 ? '' : 'display:none')?>'>
	    	<?php
				echo form_open('config/save/hours',array('id'=>'config_hours_form'));
			?>

	        <?php $this->load->view("config/hours"); ?>
	    	<div class="centered_submit">
				<?php 
				echo form_submit(array(
					'name'=>'submit_hours',
					'id'=>'submit_hours',
					'value'=>lang('common_save'),
					'class'=>'submit_button float_right')
				);
				?>
        	</div>
        </form>
        </div>
	    <div id="tab3" class="tab_content" style='<?= ($page == 3 ? '' : 'display:none')?>'>
	    	<?php
	    	if ($this->permissions->course_has_module('reservations'))
			{
				echo form_open('config/save/fees',array('id'=>'config_green_fees_form'));
				
					if ($this->config->item('simulator'))
						$this->load->view("config/fees_simulator");
					else
		        		$this->load->view("config/fees"); 
			}
			else {
				echo form_open('config/save/green_fees',array('id'=>'config_green_fees_form'));
			?>
		        <?php 
		        	if ($this->config->item('simulator'))
						$this->load->view("config/green_fees_simulator");
					else
		        		$this->load->view("config/green_fees"); 
	        }
		    ?>
	    	<div class="centered_submit">
				<?php 
				echo form_submit(array(
					'name'=>'submit_green_fees',
					'id'=>'submit_green_fees',
					'value'=>lang('common_save'),
					'class'=>'submit_button float_right')
				);
				?>
        	</div>
        </form>
        </div>    
	    <div id="tab4" class="tab_content" style='<?= ($page == 4 ? '' : 'display:none')?>'>
	    	<?php $this->load->view("config/teesheets"); ?>
	    </div>    
	    <div id="tab5" class="tab_content" style='<?= ($page == 5 ? '' : 'display:none')?>'>
	        <?php
				echo form_open('config/save/pos',array('id'=>'config_pos_form'));
			?>
			<?php $this->load->view("config/pos"); ?>
	    	<div class="centered_submit">
				<?php 
				echo form_submit(array(
					'name'=>'submit_pos',
					'id'=>'submit_pos',
					'value'=>lang('common_save'),
					'class'=>'submit_button float_right')
				);
				?>
        	</div>
        </form>
        </div>
	    <div id="tab6" class="tab_content" style='<?= ($page == 6 ? '' : 'display:none')?>'>
	        <?php $this->load->view("config/misc"); ?>
        </div>
	    <div id="tab7" class="tab_content" style='<?= ($page == 7 ? '' : 'display:none')?>'>
	    	<?php
				echo form_open('config/save/terminals',array('id'=>'config_terminals_form'));
			?>

	        <?php $this->load->view("config/terminals"); ?>
	    	<div class="centered_submit">
				<?php 
				echo form_submit(array(
					'name'=>'submit_terminals',
					'id'=>'submit_terminals',
					'value'=>lang('common_save'),
					'class'=>'submit_button float_right')
				);
				?>
        	</div>
        </form>
        </div>
        <div id="tab8" class="tab_content" style='<?= ($page == 8 ? '' : 'display:none')?>'>
	    	<?php
				echo form_open('config/save/loyalty',array('id'=>'config_loyalty_form'));
			?>

	        <?php $this->load->view("config/loyalty"); ?>
	    	<div class="centered_submit">
				<?php 
				echo form_submit(array(
					'name'=>'submit_loyalty',
					'id'=>'submit_loyalty',
					'value'=>lang('common_save'),
					'class'=>'submit_button float_right')
				);
				?>
			</div>
		 </form>
        </div>
	</div>
        <div class="centered_submit" style='display:none'>
<?php 
echo form_hidden('settings_section', '');
echo form_submit(array(
	'name'=>'submitf',
	'id'=>'submitf',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
        </div>
</fieldset>
</div>
</div>
<?php
//echo form_close();
?>
<div id="feedback_bar"></div>
<script type='text/javascript'>
var price_table = {
    initialize:function(){
    	for (var i = 2; i < 52; i ++) {
    		console.log('hiding col '+i);
	        this.hide_column(i);
	        this.set_header_callback('price_header_'+i, i);
    	}
        
        // Check if twilight or super twilight is active, and hide rows if not.
        if ($('select:[name=early_bird_hours_begin]').val() == '2399')
            this.deactivate_early_bird_prices();
        if ($('select:[name=morning_hours_begin]').val() == '2399')
            this.deactivate_morning_prices();
        if ($('select:[name=afternoon_hours_begin]').val() == '2399')
            this.deactivate_afternoon_prices();
        if ($('select:[name=twilight_hour]').val() == '2399')
            this.deactivate_twilight_prices();
        if ($('select:[name=super_twilight_hour]').val() == '2399')
            this.deactivate_super_twilight_prices();
        if ($('select:[name=holidays]').val() == '0')
            this.deactivate_holiday_prices();
    },
    set_header_callback:function(header_input, index){
    	$('.'+header_input).each(function(){
    		console.log($(this).val() != '');
    		var header_id = $(this)[0].id;
    		var teesheet_id = header_id.substr(header_id.lastIndexOf('_')+1);
    		//console.log('tsid'+teesheet_id);
	        $(this).keyup(function(){
	            if ($(this).val() != '')
	                price_table.show_column(index, teesheet_id);
	        });
	        $(this).blur(function(){
	            if ($(this).val() == '')
	                price_table.hide_column(index, teesheet_id);
	        });
    		
    	})
    },
    show_column:function(index, teesheet_id){
        /*$('.price_col_'+index).show();
        if ($('.price_header_'+(index+1)))
            $('.price_header_'+(index+1)).show();
            
        $('.price_category_'+index+'_'+teesheet_id).each(function(){
        	$(this).removeAttr('disabled');
        })*/
        $('.price_header_'+index).each(function(){
    		var header_id = $(this)[0].id;
    		var teesheet_id = header_id.substr(header_id.lastIndexOf('_')+1);
    		$('.'+header_id).show();
	        $('#default_price_class_'+(index+1)+'_'+teesheet_id).show();
			$('.'+header_id).removeAttr('disabled');
	        if ($('#price_category_'+(index+1)+'_'+teesheet_id))
	        	$('#price_category_'+(index+1)+'_'+teesheet_id).show();        
	    })
    },
    hide_column:function(index){
    	$('.price_header_'+index).each(function(){
    		var header_id = $(this)[0].id;
    		var teesheet_id = header_id.substr(header_id.lastIndexOf('_')+1);
    		if ($(this).val() == '') {
	            //$('.price_cateogry_'+index+'_'+teesheet_id).hide();
	            if ($('#price_category_'+(index+1)+'_'+teesheet_id) && $('#price_category_'+(index+1)+'_'+teesheet_id).val() == '')
	                $('#price_category_'+(index+1)+'_'+teesheet_id).hide();
	            $('#default_price_class_'+(index+1)+'_'+teesheet_id).hide();
	          //console.log('#price_header_'+(index+1)+'_'+teesheet_id);
	          //console.log($('#price_header_'+(index+1)+'_'+teesheet_id));
	          //console.log("Val: "+$('#price_header_'+(index+1)+'_'+teesheet_id).val());
	            //console.dir($('#price_cateogry_'+index+'_'+teesheet_id));    
	            $('.price_category_'+index+'_'+teesheet_id).each(function(){
	            	$(this).attr('disabled','disabled');
	            	$(this).hide();
	            })
	        }
    	})
    },
    deactivate_early_bird_prices:function(){
        $('.early_bird_col').hide();
    },
    activate_early_bird_prices:function(){
        $('.early_bird_col').show();
    },
    deactivate_morning_prices:function(){
        $('.morning_col').hide();
    },
    activate_morning_prices:function(){
        $('.morning_col').show();
    },
    deactivate_afternoon_prices:function(){
        $('.afternoon_col').hide();
    },
    activate_afternoon_prices:function(){
        $('.afternoon_col').show();
    },
    deactivate_twilight_prices:function(){
        $('.twilight_col').hide();
    },
    activate_twilight_prices:function(){
        $('.twilight_col').show();
    },
    deactivate_super_twilight_prices:function(){
        $('.super_twilight_col').hide();
    },
    activate_super_twilight_prices:function(){
        $('.super_twilight_col').show();
    },
    deactivate_holiday_prices:function(){
        $('.holiday_col').hide();
    },
    activate_holiday_prices:function(){
        $('.holiday_col').show();
    }
}
//validation and submit handling
$(document).ready(function()
{
	$('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
	$( "#teetime_department" ).autocomplete({
		source: "<?php echo site_url('items/suggest_department');?>",
		delay: 10,
		autoFocus: false,
		minLength: 0
	});
	$( "#cart_department" ).autocomplete({
		source: "<?php echo site_url('items/suggest_department');?>",
		delay: 10,
		autoFocus: false,
		minLength: 0
	});
	$('.colbox').colorbox({width:'550'});
    price_table.initialize();
    init_table_sorting();
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    //enable_search('<?php echo site_url("teesheets/suggest")?>','<?php echo lang("common_confirm_search")?>');
    enable_delete('<?php echo lang("teesheets_confirm_delete")?>','<?php echo lang("teesheets_none_selected")?>');
    

	//$('.price_box').decimalMask({separator: ".",
    //  decSize: 2,
    //  intSize: 8});
    //$('#submit_details').click(function(e) {e.preventDefault();$('input[name=settings_section]').val('details');$('#config_form').submit();});
    //$('#submit_hours').click(function(e) {e.preventDefault();$('input[name=settings_section]').val('hours');$('#config_form').submit();});
    //$('#submit_green_fees').click(function(e) {e.preventDefault();$('input[name=settings_section]').val('green_fees');$('#config_form').submit();});
    //$('#submit_pos').click(function(e) {e.preventDefault();$('#config_pos_form').submit();});
    //$('#submit_misc').click(function(e) {e.preventDefault();$('input[name=settings_section]').val('misc');$('#config_form').submit();});
	//$('#submit_green_fees').click(function(event){event.preventDefault();});
	var rules = {};
	var messages = {};
	var submitting = false;
	$('#config_details_form, #config_hours_form, #config_green_fees_form, #config_pos_form, #config_misc_form, #config_terminals_form, #config_loyalty_form, #config_quickbooks_form').each(function() {
		if ($(this).attr('id') == 'config_details_form')
		{
			rules = {
	     		//company: "<?php echo lang('config_company_required'); ?>",
	     		//address: "<?php echo lang('config_address_required'); ?>",
	     		//phone: "<?php echo lang('config_phone_required'); ?>",
	     		email: {
	     			required:true,
	     			email:true
	     		}
			};
			messages = {
	     		//company: "<?php echo lang('config_company_required'); ?>",
	     		//address: "<?php echo lang('config_address_required'); ?>",
	     		//phone: "<?php echo lang('config_phone_required'); ?>",
	     		email:{
	     			required:"<?php echo lang('common_email_required'); ?>",
	     			email:"<?php echo lang('common_email_invalid_format'); ?>"
	     		}
			};
		}
		$(this).validate({
		submitHandler:function(form)
		{
            trace('about to ajax submit ');
            trace(submitting?'true':'false');
			if (submitting) { trace('already submitting');return;}
			submitting = true;
			
			//<input type="text" class="price_label price_class price_header_10 valid" id="price_category_10_27" tabindex="10" value="saf" name="price_category_10_27">
			//<input type="text" class="price_label price_box price_col_10" id="27_10_1" tabindex="10" value="0.00" name="27_10_1">
			//<input type="text" class="price_label price_box price_col_10" id="27_10_2" tabindex="10" value="0.00" name="27_10_2">
			//$('.price_class').each(function(){
				//var header_id = $(this).val();
				//if ($(this).val() == '')
			//	{
			//		$(this).attr('disabled','disabled');
			//		$('.'+header_id).each(function(){
		//				$(this).attr('disabled','disabled');
		//			})
		//			console.log(header_id);
		//		}	
		//	});
			
		//	return;

            $(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
				success:function(response)
				{
					trace('success!');
					console.dir(response);
					if(response.success)
	                {
                        set_feedback(response.message,'success_message',false);		
	                }
	                else
	                {
                        set_feedback(response.message,'error_message',true);		
	                }
	                submitting = false;
	                $(form).unmask();
				},
                failure:function(response)
                {
                    trace('failed');
                },
				dataType:'json'
            });

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: rules,
		messages: messages
	});
	});
	
    //$(".tab_content").hide(); //Hide all content
	$("#tab_<?=$page?>").addClass("active").show(); //Activate first tab
	//$("#tab<?=$page?>").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});
	//$('#tab_<?=$page?>').click();


	$('#enable_quickbooks').on('click', function(e){
		$.post('<?php echo site_url('qb/generate_user');?>', null, function(response){
			if(response){
				$('#quickbooks_account').html(response);
				$('#enable_quickbooks').hide();
			}
		},'html');
	});
	
});
function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(
		{
			sortList: [[1,0]],
			headers:
			{
				0: { sorter: false},
				3: { sorter: false}
			}
		});
	}
}

function post_teesheet_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.teesheet_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.teesheet_id,'<?php echo site_url("teesheets/get_row")?>');
			set_feedback(response.message,'success_message',false);

		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				highlight_row(response.teesheet_id);
				set_feedback(response.message,'success_message',false);
			});
		}
	}
}
</script>
<?php $this->load->view("partial/footer"); ?>


