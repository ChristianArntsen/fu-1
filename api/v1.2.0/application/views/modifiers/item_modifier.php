<tr>
	<td>
		<input type="hidden" name="modifiers[<?php echo $modifier['modifier_id']; ?>][modifier_id]" value="<?php echo $modifier['modifier_id']; ?>" class="modifier_id" />
		<div class='form_field'>
			<?php echo form_input(array('name' => "modifiers[{$modifier['modifier_id']}][name]", 'value' => $modifier['name'], 'style' => 'width: 300px;', 'class'=>'modifier_name', 'disabled'=>false));?>
		</div>
	</td>
	<td>
		<div class='form_field'>
			<?php echo form_input(array('name' => "modifiers[{$modifier['modifier_id']}][options]", 'value' => implode(',', $modifier['options']), 'style' => 'width: 380px;', 'class'=>'modifier_options', 'disabled'=>false));?>
		</div>
	</td>				
	<td>
		<div class='form_field'>
			<?php echo form_input(array('name' => "modifiers[{$modifier['modifier_id']}][override_price]", 'value' => $modifier['override_price'], 'style' => 'width: 75px;', 'class'=>'modifier_default_price'));?>
		</div>
	</td>
	<td>
		<div class='form_field'>
			<?php 
			if($modifier['auto_select'] == 1){
				$checked = true;
			}else{
				$checked = false;
			}
			echo form_checkbox(array('name' => "modifiers[{$modifier['modifier_id']}][auto_select]", 'value' => 1, 'class'=>'modifier_auto_select', 'checked'=>$checked)); ?>
		</div>
	</td>				
	<td>
		<a href="#" class="delete_modifier" style="color: white;">X</a>
	</td>
</tr>