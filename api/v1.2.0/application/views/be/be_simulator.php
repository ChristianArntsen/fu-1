<?php $this->load->view("partial/course_header");
//print_r($this->booking_lib->get_basket());
?>

<style>
    .ui-widget {
        font-size:.8em;
    }
    .ui-buttonset .ui-button {
        margin-right:-0.5em;
    }
    #slider {
        margin: 10px 10px 10px 20px;
        width:250px;
    }
    .ui-slider-handle {
        font-size:.8em;
        padding:1px 0 0 2px;
    }
    .tab_container {
	border: 1px solid #999;
	border-top: none;
	overflow: hidden;
	clear: both;
	float: left;
        width: 863px;
	background: #efefef;
        height:416px;
}
.tab_content {
	padding:0px 20px;
	font-size: 1.2em;
        height:370px;
        overflow: auto;
}
    ul.tabs {
	margin: 0;
	padding: 0px 0px 0px 4px;
	float: left;
	list-style: none;
	height: 36px; /*--Set height of tabs--*/
	border-bottom: 1px solid #999;
	border-left: 1px solid #999;
	width: 860px;
	background:url(<?php echo base_url(); ?>images/backgrounds/top_bar_bg_black.png) repeat-x;
}
ul.tabs li {
	float: left;
	margin: 0px 2px;
	padding: 0;
	height: 30px; /*--Subtract 1px from the height of the unordered list--*/
	line-height: 30px; /*--Vertically aligns the text within the tab--*/
	border: 1px solid #999;
	border-left: none;
	margin-bottom: -1px; /*--Pull the list item down 1px--*/
	margin-top:5px;
	overflow: hidden;
	position: relative;
	background: #d5d5d5;
	border-radius:5px 5px 0px 0px;
}
ul.tabs li a {
	text-decoration: none;
	color: #000;
	display: block;
	font-size: .9em;
	padding: 0 0px;
	border: 1px solid #fff; /*--Gives the bevel look with a 1px white border inside the list item--*/
	outline: none;
        width:85px;
        text-align: center;
        line-height: 18px;
	border-radius:5px 5px 0px 0px;
}
ul.tabs li a div {
    height:12px;
    font-size:12px;
    font-weight:bold;
}
ul.tabs li a span {
    font-size:.7em
}
ul.tabs li a:hover {
	background: #ccc;
}
html ul.tabs li.active, html ul.tabs li.active a:hover  { /*--Makes sure that the active tab does not listen to the hover properties--*/
	background: #fff;
	border-bottom: 1px solid #fff; /*--Makes the active tab look like it's connected with its content--*/
}
#time_button_set, #hole_button_set {
    float:left;
    margin:10px 0 0 16px;
}
#player_button_set {
    float:right;
    margin:10px 21px 0 0;
}
.search_controls {
    padding:0px 5px 10px;
    border-bottom: 1px solid #ddd;
    overflow:hidden;
    height:34px;
}
.teetime {
    border:1px solid #dedede;
    padding:4px 15px;
    margin-bottom:5px;
    height:32px;
}
.teetime .left {
    float:left;
    width:47%;
}
.teetime .center {
    float:left;
    width:52%;
    text-align: center;
}
.teetime .right {
    float:right;
    width:11%;
    margin-top:5px;
}
.teetime .left .players span {
    float: left;
}
.teetime .left .time {
	line-height:44px;
}
.teetime .left .players span.ui-button-text {
    font-size: .6em;
    margin: 1px 0 0 2px;
}
.loading_golf_ball {
    font-size:14px;
    padding-top:94px;
    text-align:center;
}
.be_header {
    color:#336699;
    margin:15px auto 5px;
    text-align: center;
}
.be_form {
    width:65%;
    margin:auto;

}
#employee_basic_info.login_info {
    margin-right:5px;
}
.register_form {
    width:85%;
}
.be_form td input {
    margin-top:3px;
    margin-left:5px;
}
.be_fine_print {
    color:#999999;
    font-size:11px;
    text-align:center;
}
.be_form td hr {
    color:#336699;
    margin:11px 0px 7px;
}
.be_reserve_buttonset {
    float:left;
    margin-right:22px;
}
.be_buttonset_label {
    line-height: 30px;
    color:#336699;
    margin-right:10px;
}
.reserve_button .ui-button-text-icon-primary .ui-button-text {
	padding: 10px;
	margin-left:20px;
}
#cboxLoadedContent .teetime .course_name {
    font-size:1.4em;
    margin-top:6px;
}
#cboxLoadedContent .teetime .info {
    padding-left:10px;
    margin-top:7px;
}
#cboxLoadedContent .teetime .time, #cboxLoadedContent .teetime .holes, #cboxLoadedContent .teetime .date {
    color:#336699;
    font-size:1em;
    padding:2px 6px;
    float:left;
    line-height:17px;
    margin-right:4px;
}
#content_area .teetime .holes, #content_area .teetime .price, #content_area .teetime .price_label, #content_area .teetime .spots {
    padding:2px 6px;
    float:left;
    line-height:17px;
    margin-right:4px;
}
#content_area .teetime .holes, #content_area .teetime .price, #content_area .teetime .price_label {
    font-size:.6em;
}
#content_area .teetime .price_label {
    margin-right:2px;
    padding-right:0px;
    line-height:19px;
}
#cboxLoadedContent .teetime .info .ui-icon {
    float:left;
    margin-right:5px;
}
#content_area .teetime .ui-icon {
    float:left;
    margin-right:5px;
}
#cboxLoadedContent .teetime .reservation_info {
    float:left;
    width:70%;
}
#cboxLoadedContent .teetime .price_box {
    float:right;
    text-align: right;
    width:27%;
    font-size:2.5em;
    line-height: 50px;
}
#content_area .teetime .price_details {
    padding-left: 135px;
}
#course_name {
    font-size:2em;
    text-align:center;
}
#teesheet_name {
	text-align: center;
}
.column {
	width:355px;
	margin:0 10px 0 0;
}
.booking_rules {
	padding:5px;
	font-size:11px;
}
.booking_rules p {
	margin-bottom:3px;
}
#home_module_list {
	clear:none;
	float:left;
	width:590px;
	margin-top:0px;
}
.wrapper {
	margin-top:20px;
}
.menu_item_home {
	height:62px;
	padding:10px 10px 0 0;
}
#menubar_background, #menubar_full{
	background:url('../images/header/header_piece.png') repeat-x 0 -2px transparent;
}
.ui-datepicker {
	width:27em;
	margin-bottom:5px;
}
#cart_content {
	min-height:140px;
	font-size:16px;
}
.align_right {
	text-align:right;
}
#cart_content a.delete_item {
	text-align:center;
}
#cart_footer {

}
.ui-button-text-only .ui-button-text {
	padding:0px;
}
.ui-button-text-icon-primary .ui-button-text, .ui-button-text-icons .ui-button-text {
	padding: 0px;
}
label.available_button {
	padding:10px 20px 10px 27px;
}
#content_area .teetime .ui-icon {
	margin-right: 10px;
}
span.spacer, span.simulator_title {
	width:190px;
	display:block;
	height:20px;
	float:left;
}
span.spacer {
	width:32px;
}
span.simulator_title {
	line-height:32px;
}
label.ui-button, .reserve_button {
	background: -webkit-linear-gradient(top, #349AC5, #4173B3);
	color: white;
	display: block;
	font-size: 14px;
	font-weight: lighter;
	text-align: center;
	text-shadow: 0px -1px 0px black;
	border-radius: 4px;
	box-shadow: inset 0px 1px 1px 0px rgba(255, 255, 255, 0.5), 0px 3px 1px -2px rgba(255, 255, 255, .2);
	border: 1px solid #232323;
	padding: 5px 20px;
	margin:0px 10px 0px 0px;
}
label.ui-button, .reserve_button {
	background: #349ac5; /* for non-css3 browsers */
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#349ac5', endColorstr='#4173b3'); /* for IE */
	background: -webkit-linear-gradient(top, #349ac5, #4173b3);
	background: -moz-linear-gradient(top,  #349ac5,  #4173b3); /* for firefox 3.6+ */
	color: white;
	display: block;
	font-size: 14px;
	font-weight: normal;
	height: 30px;
	padding: 5px 0 0 10px;
	text-align: center;
	text-shadow: 0px -1px 0px black;
	border-radius: 4px;
	margin-bottom: 8px;
	box-shadow: inset 0px 1px 1px 0px rgba(255, 255, 255, 0.5), 0px 3px 1px -2px rgba(255, 255, 255, .2);
	border: 1px solid #232323;
}
.reserve_button {
	padding:0px;
	width:200px;
}
.reserve_button a.ui-state-default {
	border:none;
	color:white;
}
.right {
	float:right
}




/* COPIED FROM be_updated */
html, table, div, .ui-widget {
		font-family:Quicksand, Helvetica,Arial, sans-serif;
	}

    html {
		background: url(<?php echo base_url(); ?>images/backgrounds/course.png)no-repeat center center fixed;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
		font-family: Quicksand, Helvetica, Arial, sans-serif;
	}
   .ui-widget {
        font-size:.8em;
    }
    .ui-buttonset .ui-button {
        margin-right:-0.5em;
    }
    #slider {
        margin: 10px 10px 10px 20px;
        width:250px;
    }
    .ui-slider-handle {
        font-size:.8em;
        padding:1px 0 0 2px;
    }

label.ui-button {
	padding:0px 5px 0px;
}
.ui-button .ui-button-text {
	line-height: 24px;
}
.ui-state-active .ui-button .ui-state-active .ui-button-text {
	/*line-height: 30px;*/
}
.ui-button-text-only .ui-button-text {
	padding:5px 10px;
}
div label.ui-state-active {
	border-right: none;
	box-shadow: inset -2px 2px 30px 3px black;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#222222', endColorstr='#333333'); /* for IE */
}

.flag_icon, .time_icon, .player_icon {
	display: block;
	height: 32px;
	width: 43px;
	background: url(<?php echo base_url(); ?>images/icons/shadow_icons.png) no-repeat 0px -208px;
	float: left;
	margin: 6px 0px 0px 5px;
}
.player_icon {
	float:right;
	background-position:-4px -9px;
	margin-right:25px;
}
.flag_icon {
	background-position: 0px -162px;
}
html ul.tabs li.active, html ul.tabs li.active a:hover  { /*--Makes sure that the active tab does not listen to the hover properties--*/
	background: #efefef;
	border-bottom: 1px solid #efefef; /*--Makes the active tab look like it's connected with its content--*/
}
#time_button_set, #hole_button_set {
    float:left;
    margin:10px 60px 0 15px;
}
#player_button_set {
    float:right;
    margin:10px 21px 0 0;
}
.search_controls {
    padding: 5px;
    border-bottom: 1px solid #ccc;
    box-shadow:0px 1px 0px 0px white;
}
.teetime {
    border:none;
    padding:8px 15px;
    background:transparent;
    width:96%;
    float:left;
    border-radius:0px;
    border-bottom:1px solid #ccc;
    border-right:1px solid none;
    box-shadow:0px 1px 0px 0px white;
    margin-bottom:0px;
}
div.even {
	border-right:none 0px;
	box-shadow:0px 1px 0px 0px white;
	padding-right:0px;
}
div.odd {
	padding-left:0px;
}
.teetime .left {
    float:left;
    width:200px;
}
.teetime .center {
    float:left;
    width:auto;
    text-align: center;
}
.teetime .right {
    float:right;
    width:15%;
    margin-top:5px;
}
.teetime .left .players span {
    float: left;
}
.teetime .left .players span.ui-button-text {
    font-size: .6em;
    margin: 1px 0 0 2px;
}
.loading_golf_ball {
    font-size:14px;
    padding-top:94px;
    text-align:center;
}
.be_header {
    color:#336699;
    margin:15px auto 5px;
    text-align: center;
}
.be_form {
    width:65%;
    margin:auto;

}
.login_button {
	height:32px;
}
#employee_basic_info.login_info {
    margin-right:5px;
}
.register_form {
    width:85%;
}
.be_form td input {
    margin-top:3px;
    margin-left:5px;
}
.be_fine_print {
    color:#999999;
    font-size:11px;
    text-align:center;
}
.be_form td hr {
    color:#336699;
    margin:11px 0px 7px;
}
.be_reserve_buttonset {
    float:left;
    margin-right:22px;
}
.be_buttonset_label {
    line-height: 30px;
    color:#336699;
    margin-right:10px;
}
#cboxLoadedContent .teetime .course_name {
    font-size:1.4em;
    margin-top:6px;
}
#cboxLoadedContent .teetime .info {
    padding-left:10px;
    margin-top:7px;
}
#cboxLoadedContent .teetime .time, #cboxLoadedContent .teetime .holes, #cboxLoadedContent .teetime .date {
    color:#336699;
    font-size:1em;
    padding:2px 6px;
    float:left;
    line-height:17px;
    margin-right:4px;
}
#content_area .teetime .holes, #content_area .teetime .price, #content_area .teetime .price_label, #content_area .teetime .spots {
    padding:2px 6px 2px 4px;
    float:left;
    line-height:17px;
    margin-right:14px;
    font-size:14px;
    font-weight:lighter;
    border:none;
   	background:transparent;
}
#content_area .teetime .price_label {
    margin-right:2px;
    padding-right:0px;
    line-height:19px;
}
#cboxLoadedContent .teetime .info .ui-icon {
    float:left;
    margin-right:5px;
}
#content_area .teetime .ui-icon {
    float:left;
    margin-right:5px;
}
#cboxLoadedContent .teetime .reservation_info {
    float:left;
    width:70%;
}
#cboxLoadedContent .teetime .price_box {
    float:right;
    text-align: right;
    width:27%;
    font-size:2.5em;
    line-height: 50px;
}
#content_area .teetime .price_details {
    padding-left: 90px;
}
#course_name {
    font-size:2em;
    text-align:center;
}
#teesheet_name {
	text-align: center;
}
.column {
	width:186px;
	margin:0 10px 0 0;
}
.booking_rules {
	width: 176px;
	padding:5px;
	font-size:11px;
}
.booking_rules p {
	margin-bottom:3px;
}
#home_module_list {
	clear:none;
	float:left;
	width:860px;
	margin-top:0px;
	margin-left:15px;
}
.wrapper {
	margin:20px auto;
	width:1100px;
}
.menu_item_home {
	height:62px;
	padding:10px 10px 0 0;
}
#menubar_background, #menubar_full{
	background:none;
	width: 100%;
	box-shadow: 0px 1px 0px 0px #B9B4AB, 0px 2px 0px 0px #ECE9E4;
	padding: 3px 3px 0px 3px;
}
.dailyweather {
	width: 41px;
	float: left;
	text-align:center;
	margin-right:1px;
}
#weather {
padding: 5px;
}
div.ui-datepicker {
	width:14.4em;
	margin-bottom:10px;
}
div#booking_rules_content, div#available_deals_content {
	padding:5px;
}
.curtemp {
	font-size: 25px;
	float: right;
}
.wcity {
	font-size: 10px;
	margin-left: 20px;
	float: right;
	margin-top: -4px;
}
#hole_button_set {
	width:100px;
}
#player_button_set {
	width:140px;
}
.teetime .time {
	float:left;
	color:#303030;
}
.teetime a {
	color:#303030;
	float:right;
}
.teetime .bottom span {
	color:#303030;
}
.flag_icon_sm, .person_icon_sm, .cart_icon_sm, .no_cart_icon_sm {
	width:20px;
	height:16px;
	display:block;
	background:transparent url(<?php echo base_url(); ?>images/icons/icons.png) no-repeat -173px -10px;
	float:left;
}
.flag_icon_sm {
	width:16px;
}
.person_icon_sm {
	background-position:-173px -50px
}
.cart_icon_sm {
	background-position:-173px -92px;
}
.no_cart_icon_sm {
	background-position:-172px -131px;
}
.ui-state-default a, .ui-state-default a:link {
	background: #349ac5; /* for non-css3 browsers */
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#349ac5', endColorstr='#4173b3'); /* for IE */
	background: -webkit-linear-gradient(top, #349AC5, #4173B3);
	background: -moz-linear-gradient(top,  #349AC5,  #4173B3); /* for firefox 3.6+ */
	color: white;
	display: block;
	font-size: 14px;
	font-weight: lighter;
	text-align: center;
	text-shadow: 0px -1px 0px black;
	border-radius: 4px;
	box-shadow: inset 0px 1px 1px 0px rgba(255, 255, 255, 0.5), 0px 3px 1px -2px rgba(255, 255, 255, .2);
	border: 1px solid #232323;
	padding:5px 20px;
}
#content_area {
	padding:10px;
}
#booking_rules, #cart, #course_selector {
	background:url(<?php echo base_url(); ?>images/backgrounds/bg3.png) repeat;
}
#booking_rules div.ui-datepicker-header, #cart div.ui-datepicker-header, #course_selector div.ui-datepicker-header {
	background:none;
	border:none;
	font-weight:lighter;
	font-size:14px;
	padding:6px 0px 8px;
}
#booking_rules_content, #cart_content, #cart_footer {
	background:white;
	box-shadow:0px 1px 10px 0px #777, 0px 6px 0px -3px white, 0px 7px 10px -3px #777, 0px 11px 0px -6px white;
	margin-bottom:12px;
	padding:10px;
}
</style>
<div id="thank_you_box"></div>
<div class="wrapper">
    <div class="column">
        <?php if ($teesheet_menu != '' && false) {?>
            <form action="" method="post">
                <div id="course_selector" class="ui-datepicker-inline ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" style="display: block;">
                    <div class="ui-datepicker-header ui-widget-header ui-helper-clearfix ui-corner-all">
                         Select Course
                    </div>
                    <?php echo $teesheet_menu; ?>
                    <input type="submit" value="changets" name="changets" id="changets" style="display:none"/>
                </div>
            </form>
            <script>
            function changeTeeSheet() {
			    var teesheet_id = $('#teesheetMenu').val();
			    $('#changets').click();
			}
			</script>
        <?php } ?>
        <div id="cart" class="ui-datepicker-inline ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" style="display: block;">
            <div class="ui-datepicker-header ui-widget-header ui-helper-clearfix ui-corner-all">
                 Reservation
            </div>
            <div id="cart_content">
            	<?php
            	echo $basket_html;
            	//print_r($basket_items)
            	?>
            </div>
            <div id='cart_footer'>
            	<div>Total Time: <span class='right'><?php echo $time_amount; ?></span></div>
            	<?php if ($basket_info['total']-$basket_info['price'] > 0) { ?>
            	<div>Discount: <span class="right">$<?php echo number_format($basket_info['total']-$basket_info['price'],2);?></span></div>
            	<?php } ?>
            	<div>Reservation Total: <span class='right'>$<?php echo ($basket_info['price'] > 0)?$basket_info['price']:'0.00';?></span></div>
            </div>
        </div>
        <div>
        	<div class='reserve_button'>
            	<?php echo anchor("be/reserve_simulator/", 'Reserve Now',array('class'=>'colbox','title'=>'Reserve Your Times'));?>
            </div>
        </div>
        <div id="booking_rules" class="ui-datepicker-inline ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" style="display: block;">
            <div class="ui-datepicker-header ui-widget-header ui-helper-clearfix ui-corner-all">
                 Booking Rules
            </div>
            <div id='booking_rules_content'>
        		<?php echo html_entity_decode($course_info->booking_rules); ?>
        	</div>
        </div>
    </div>
	<div id="home_module_list">
	    <ul class="tabs">
	        <li><a href="#tab1" id="tab1_label">
	                <?php echo $teetimes[0]['day_label'];?>
	            </a></li>
	        <li><a href="#tab2" id="tab2_label">
	                <?php echo $teetimes[1]['day_label'];?>
	            </a></li>
	        <li><a href="#tab3" id="tab3_label">
	                <?php echo $teetimes[2]['day_label'];?>
	            </a></li>
	        <li><a href="#tab4" id="tab4_label">
	                <?php echo $teetimes[3]['day_label'];?>
	            </a></li>
	        <li><a href="#tab5" id="tab5_label">
	                <?php echo $teetimes[4]['day_label'];?>
	            </a></li>
	        <li><a href="#tab6" id="tab6_label">
	                <?php echo $teetimes[5]['day_label'];?>
	            </a></li>
	        <li><a href="#tab7" id="tab7_label">
	                <?php echo $teetimes[6]['day_label'];?>
	            </a></li>
	    </ul>

	    <div class="tab_container">
	        <div class="search_controls scroll_sync">
	        	<table>
	        		<tbody>
	        			<tr>
	        				<td class='spacer_cell'><span class='spacer'></span></td>
	        				<?php foreach($tracks as $track)
							{
								if ($track['online_booking'])
								echo "<td class='teetime_cell'><span class='simulator_title'>{$track['title']}</span></td>";
							}
							?>
	        			</tr>
	        		</tbody>
	        	</table>


	        	<div class='clear'></div>
	        </div>
	        <div id="tab1" class="tab_content scroll_sync">
	        	<table>
	        		<tbody>
	            <!--Content-->
	            <?php
	                foreach ($teetimes[0]['teetimes'] as $teetime)
	                    echo $teetime;
	            ?>
	        		</tbody>
	        	</table>
	        </div>
	        <div id="tab2" class="tab_content scroll_sync">
	        	<table>
	        		<tbody>
	           <!--Content-->
	            <?php
	                foreach ($teetimes[1]['teetimes'] as $teetime)
	                    echo $teetime;
	            ?>
	        		</tbody>
	        	</table>
	        </div>
	        <div id="tab3" class="tab_content scroll_sync">
	        	<table>
	        		<tbody>
	           <!--Content-->
	            <?php
	                foreach ($teetimes[2]['teetimes'] as $teetime)
	                    echo $teetime;
	            ?>
	        		</tbody>
	        	</table>
	        </div>
	        <div id="tab4" class="tab_content scroll_sync">
	        	<table>
	        		<tbody>
	           <!--Content-->
	            <?php
	                foreach ($teetimes[3]['teetimes'] as $teetime)
	                    echo $teetime;
	            ?>
	        		</tbody>
	        	</table>
	        </div>
	        <div id="tab5" class="tab_content scroll_sync">
	        	<table>
	        		<tbody>
	           <!--Content-->
	            <?php
	                foreach ($teetimes[4]['teetimes'] as $teetime)
	                    echo $teetime;
	            ?>
	        		</tbody>
	        	</table>
	        </div>
	        <div id="tab6" class="tab_content scroll_sync">
	        	<table>
	        		<tbody>
	           <!--Content-->
	            <?php
	                foreach ($teetimes[5]['teetimes'] as $teetime)
	                    echo $teetime;
	            ?>
	        		</tbody>
	        	</table>
	        </div>
	        <div id="tab7" class="tab_content scroll_sync">
	        	<table>
	        		<tbody>
	           <!--Content-->
	            <?php
	                foreach ($teetimes[6]['teetimes'] as $teetime)
	                    echo $teetime;
	            ?>
	        		</tbody>
	        	</table>
	        </div>
	    </div>
	</div>
</div>
<?php //$this->load->view("partial/course_footer"); ?>
<script>
	$(document).ready(function(){
		$('.colbox').colorbox(
			{
    			'width':650,
    			'height':500,
                'onComplete':function() {
               		$.colorbox.resize();
               	}
            });
        <?php if ($course_info->reservations) {?>
        booking.get_available_reservations();
        <?php } ?>
	})
    var booking = {
        teesheet_id:'<?php echo $teesheet_id ?>',
        tab_index:'1',
        mark_as_selected:function(checkbox) {
    	 	$(checkbox).attr('checked', true);
    		//$(checkbox).button('option', {'icons':{primary: 'ui-icon-check'}});
    	},
    	mark_as_available:function(checkbox) {
    		$(checkbox).attr('checked', false);
    		//$(checkbox).button('option', {'icons':{primary: 'ui-icon-radio-on'}});
    		$(checkbox).next().removeClass('ui-state-active');
    	},
    	change_simulator_selection:function(checkbox) {
    		var checked = false;
    		//console.log('dying here i think '+typeof(checkbox)+' '+checkbox);
			if (typeof(checkbox) == 'string')
    			checkbox = $('#'+checkbox);
    		else
    			checked = checkbox.attr('checked');

    		var val = checkbox.val();
		    // if checked, add to cart, otherwise, remove it
	    	//$.ajax(

	    	//);
	    	if (checked) {
	    		$.ajax({
	               type: "POST",
	               url: "index.php/be/add_simulator_time",
	               data: "time_info="+val,
	               success: function(response){
	               		if (response.success)
	               		{
		                    booking.update_cart(response);

		        		    booking.mark_as_selected(checkbox);		        		}
		        		else
		        		{
		        			alert(response.message);
		        		    booking.mark_as_available(checkbox);
		        		    //.removeClass('ui-state-active');
		        		}
	    	       },
	    	       failure: function(response){
	    	       	 console.log('failed');
	    	       },
	    	       dataType:'json'
	            });
	    	}
	    	else{
	    		$.ajax({
	               type: "POST",
	               url: "index.php/be/remove_simulator_time",
	               data: "time_info="+val,
	               success: function(response){
	                   booking.update_cart(response);
	        		   booking.mark_as_available(checkbox);
	    	       },
	    	       dataType:'json'
	            });
	    	}
	    	//alert($(this).val());

    	},
    	empty_basket:function()
    	{
    		$.ajax({
               type: "POST",
               url: "index.php/be/empty_basket",
               data: "",
               success: function(response){
                   booking.update_cart(response);
                   booking.mark_as_available($('input:checked'));
        	   },
    	       dataType:'json'
            });
    	},
    	update_cart:function(data)
    	{
    		//console.dir(data.prices);
    		var cart_items = '';
    		var basket_items = data.basket_items;
    		var basket_html = data.basket_html;
    		var item_count = data.basket_info.items_in_basket;
    		var total = data.basket_info.total;
    		var price = (data.basket_info.price)?data.basket_info.price:'0.00';
    		var discount = (total - price).toFixed(2);
    		var total_time = data.total_time;
//    		if (item_count == 1) {total_time = '30 min'}
  //  		else if (item_count == 2) {total_time = '1 hour'}
    //		else if (item_count == 3) {total_time = '1 hour 30 min'}
    	//	else if (item_count == 4) {total_time = '2 hours'}

    		//for (var i in basket_items)
    		//{
    		//	cart_items += '<tr><td></td><td>'+name+'</td><td></td></tr>';
    		//}
    		$('#cart_content').html(basket_html);
    		var cart_footer_html = '<div>Total Time: <span class="right">'+(isNaN(total_time)?'0':(total_time/60).toFixed(2))+' Hour(s)</span></div>';
    		if (discount > 0)
    			cart_footer_html += '<div>Discount: <span class="right">$'+discount+'</span></div>';
    		cart_footer_html +=	'<div>Reservation Total: <span class="right">$'+price+'</span></div>';
    		$('#cart_footer').html(cart_footer_html);

    	},
        teesheet_id:'<?php echo $teesheet_id?$teesheet_id:$schedule_id ?>',
        time_range:'morning',
        holes:'18',
        available_spots:1,
        update_slider_hours:function(){
            var values = $( "#slider" ).slider( "option", "values" );
            var am_pm0 = 'a';
            var am_pm1 = 'a';
            if (values[0] > 11)
                am_pm0 = 'p';
            if (values[1] > 11)
                am_pm1 = 'p';
            if (values[0] > 12)
                values[0] = values[0]-12;
            if (values[1] > 12)
                values[1] = values[1]-12;

            $($('.ui-slider-handle')[0]).html(values[0]+am_pm0);
            $($('.ui-slider-handle')[1]).html(values[1]+am_pm1);

        },
        get_available_reservations:function(){
            $('.tab_content').html("<div class='loading_golf_ball'><img src='<?php echo base_url(); ?>images/loading_golfball2.gif'/><div>Finding available teetimes...</div></div>");
            $.ajax({
               type: "POST",
               url: "index.php/be/get_available_reservations",
               data: "&teesheet_id="+this.teesheet_id+"&tab_index="+this.tab_index,
               success: function(response){
                   booking.update_available_teetimes(response);
               }
            });
        },
        update_available_teetimes:function(ajax_response) {
            var data = eval('('+ajax_response+')');
            for (var i in data) {
                $('#tab'+(parseInt(i)+1)+'_label').html(data[i].day_label);
                var teetime_html = '';
                for (var j in data[i].teetimes) {
                    teetime_html += data[i].teetimes[j];
                }
                //console.log('#tab'+(parseInt(i)+1));
                $('#tab'+(parseInt(i)+1)).html("<table><tbody>"+teetime_html+"</tbody></table>");
            }
            this.initialize_buttons();
            this.add_reserve_button_styles();
            var selected = data.selected;
            for (var sim_num in selected)
            {
            	//console.log('sim_num '+sim_num);
            	$('#'+selected[sim_num]).next().addClass('ui-state-active');
            }
            $('.colbox').colorbox(
        		{
        			'width':650,
        			'height':500,
	                'onComplete':function() {
	               		$.colorbox.resize();
	               	}
	            });
        },
        open_reservation_popup:function() {

        },
        initialize_buttons:function() {
        	$('input:[type=checkbox]').button();
		    //$('input:[type=checkbox]:checked').button('option', {'icons':{primary: 'ui-icon-check'}, 'label':'Selected'});
		    $('input:[type=checkbox]').change(function(){
		    	booking.change_simulator_selection($(this));
		    });

        },
        activate_filter_buttons:function() {
            $("#"+this.time_range+'_range').click();
            $("input:[name=time_button]").click(function(e){
                //console.dir($(e.target));
                var time_range = $(e.target)[0].id;
                time_range = time_range.slice(0, time_range.indexOf('_'));
                //console.log(time_range);
                booking.time_range = time_range;
                booking.get_available_reservations();
            });
            $("#holes_"+this.holes).click();
            $("input:[name=holes_button]").click(function(e){
                //console.dir($(e.target));
                var holes = $(e.target)[0].id;
                holes = holes.slice(holes.indexOf('_')+1);
                booking.holes = holes;
                booking.get_available_reservations();
            });
            $("#player_"+this.available_spots).click();
            $("input:[name=player_button]").click(function(e){
                //console.dir($(e.target));
                var players = $(e.target)[0].id;
                players = players.slice(players.indexOf('_')+1);
                //console.log(players);
                booking.available_spots = players;
                booking.get_available_reservations();
            });
        },
        add_reserve_button_styles:function() {
            $('.reserve_button a').button();
        }
    };
    $(document).ready(function() {
    	$('.scroll_sync').synchronizeScroll('h');
        booking.activate_filter_buttons();
        booking.add_reserve_button_styles();
        //$('#reserve_button').button();
	    $(".tab_content").hide(); //Hide all content
		$("ul.tabs li:first").addClass("active").show(); //Activate first tab
		$(".tab_content:first").show(); //Show first tab content


	    booking.initialize_buttons();

	    //On Click Event
		$("ul.tabs li").click(function() {
			$("ul.tabs li").removeClass("active"); //Remove any "active" class
			$(this).addClass("active"); //Add "active" class to selected tab
			$(".tab_content").hide(); //Hide all tab content

			var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
			$(activeTab).fadeIn(); //Fade in the active ID content
			booking.tab_index = activeTab.substring(4);
			booking.get_available_reservations();
			return false;
		});
	        //$('#slider').slider({min:5, max:19, range:true, values:[5,10], change:function(){booking.update_slider_hours()}});
	        //booking.update_slider_hours();
	});
function post_person_form_submit(response)
{
        if(!response.success)
        {
                set_feedback(response.message,'error_message',true);
        }
        else
        {
                //This is an update, just update one row
                if(jQuery.inArray(response.person_id,get_visible_checkbox_ids()) != -1)
                {
                        //update_row(response.person_id,'<?php echo site_url("$controller_name/get_row")?>');
                        set_feedback(response.message,'success_message',false);

                }
                else //refresh entire table
                {
                        //do_search(true,function()
                        //{
                                //highlight new row
                        //        highlight_row(response.person_id);
                                set_feedback(response.message,'success_message',false);
                        //});
                }
        }
}
</script>