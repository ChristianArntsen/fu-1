<style>
	<?php if ($tee_time_card) { ?>
		#cbox2Wrapper .wrapper { 
			margin: 30px 20px 0px;
			width:515px; 
		}
	<?php } else { ?>
		.wrapper { margin: 30px 100px 0px; }
	<?php } ?>
	.etsFormGroup { margin: 10px 0; }
	#ets_img { margin:40px auto;}

	.etsButton {
		background: #3D80B9; /* for non-css3 browsers */
		color: white !important;
		border: 1px solid #232323 !important;
		display: block;
		font-size: 14px;
		font-weight: normal;
		height: 22px;
		padding: 5px 0 0 10px;
		text-align: center;
		text-shadow: 0px -1px 0px black !important;
		border-radius: 4px;
		margin-bottom: 8px;
		box-shadow: inset 0px 1px 1px 0px rgba(255, 255, 255, 0.5), 0px 3px 1px -2px rgba(255, 255, 255, .2);
		border: 1px solid #232323;
	}
</style>
<script>
	var interval_id = '';
	var count = 0;
	$(document).ready(function(){
		interval_id = setInterval(add_ets_handlers, 200);
		setTimeout(function(){$.colorbox2.resize()},3000);
	});
	function add_ets_handlers()
	{
		if ($('#ETSIFrame').length > 0)
		{
			ETSPayment.addResponseHandler("success", function(e){
				console.log('ets success');
				console.dir(e);
				$.ajax({
		           type: "POST",
		           url: "<?php echo $url; ?>",
		           data: 'response='+JSON.stringify(e),
		           success: function(response){
						$('#ets_payment_window').html(response);
					},
		            dataType:'html'
		         });
			});
			/*
			ETSPayment.addResponseHandler("error", function(e){
				console.log('ets error');
				console.dir(e);
				$.ajax({
		           type: "POST",
		           url: "<?php echo site_url('sales/process_cancelled'); ?>",
		           data: JSON.stringify({'response':e}),
		           success: function(response){
		           		console.dir(response);
				    },
		            dataType:'json'
		         });
			});
			ETSPayment.addResponseHandler("validation", function(e){
				console.log('ets validation error');
				console.debug(e);
				$.ajax({
		           type: "POST",
		           url: "fu/index.php/sales/ets_payment_made/",
		           data: JSON.stringify({'response':e}),
		           success: function(response){
		           		console.dir(response);
				    },
		            dataType:'json'
		         });
			});
			*/
			clearInterval(interval_id);
		}
		else if (count > 50)
		{
			clearInterval(interval_id);
		}
		count ++;
	}
</script>
<div class="wrapper" id="ets_payment_window">
	<?php if (!$tee_time_card) { ?>
	<h1>Total: $<?=$amount?></h1>
	<?php } ?>
	<!--h1>My Payment Page <?php echo trim($session->id); ?></h1-->

	<!-- HTML5 Magic -->
	<div id='ets_session_id' data-ets-key="<?php echo trim($session->id); ?>">
		<img id='ets_img' src="http://www.loadinfo.net/main/download?spinner=3875&disposition=inline" alt="">
	</div>

</div>
<div id='ets_script_box'>
</div>
<script>
	if (typeof ETSPayment == 'undefined')
	{
		var e = document.createElement('script');
		e.src = "<?php echo ECOM_ROOT_URL ?>/init";
		//document.getElementById("ets_script_box").appendChild(e);
		$('head')[0].appendChild(e);
	}
	else
	{
		ETSPayment.createIFrame();
	}
</script>