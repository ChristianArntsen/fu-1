var session = {
	data:{},
	set_userdata:function(key, value) {
		this.data[key] = value;
	},
	unset_userdata:function(key) {
		unset(this.data[key]);
	},
	userdata:function(key) {
		return this.data[key];
	}
}
