<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
CREATE TABLE IF NOT EXISTS `weather` (
  `id` int(11) NOT NULL auto_increment,
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `recorded_at` datetime NOT NULL,
  `temp` float NOT NULL,
  `conditions` varchar(150) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;
*/
class Weather extends CI_Model {

	var $zip_code = '87525';

	var $recorded_at;
	var $timestamp;
	var $temp;
	var $conditions;

	//function __construct(){
	//	parent::Model();
	//	$this->getLatest();
//	}

	function getLatest(){
		$woeid = $this->get_woeid();

		if ($woeid)
		{
			# load from db first..
			//$this->loadFromDatabase();
			# this will fetch and store weather..
			if($this->timeToGetNewWeather() == true && $woeid):
				return $this->fetchAndStoreWeather($woeid);
			endif;
		}
		else 
			return false;

	}
	
	function get_woeid () {
		//echo '<br/> get_woeid <br/>';
		$woeid = $this->session->userdata('woeid');
		if (!$woeid)
			$woeid = $this->fetch_woeid();
		return $woeid;
	}

	function fetch_woeid () {
		//echo '<br/> fetch_woeid <br/>';
		$zip_code = $this->session->userdata('zip');
		//echo $zip_code;
		if ($zip_code)
		{
			$url = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20geo.places%20where%20text%3D%22$zip_code%20United%20States%22&format=json";	
			$json = json_decode($this->stream_remote_file($url));
			//$weatherData = new SimplexmlElement($xml);
			//print_r($json);
			$woeid = $json->query->results->place->woeid;
			//echo "<br/>WOEID ".$woeid.'<br/>';

			if ($this->save_woeid($woeid))
			{
				$this->session->set_userdata('woeid', $woeid);
				return $woeid;
			}
			else 
				return false;
		}
		else 
			return false;
	}
	
	function save_woeid($woeid) {
		//echo '<br/> save_woeid <br/>';
		
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->limit(1);
		return $this->db->update('courses', array('woeid'=>$woeid));
	}
	
	private function timeToGetNewWeather(){
		//echo 'here'.strtotime($this->timestamp." +5 minutes")." ".time().'<br/>';
		if(strtotime(($this->timestamp != '' ? $this->timestamp : '1/1/1970') ." +5 minutes") < time()):
			//echo 'it is time';
			return true;
		endif;
		echo 'not time';
		return false;
	}

	private function loadFromDatabase($date = '', $limit = 5){
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->order_by('timestamp','desc');
		$this->db->where('recorded_at >=', date('Y-m-d 00:00:00'));
		$this->db->limit(5);
		$stats = $this->db->get('weather',1)->row();
		$this->temp = $stats->temp;
		$this->conditions = $stats->conditions;
		$this->recorded_at = $stats->recorded_at;
		$this->timestamp = $stats->timestamp;
	}

	private function fetchAndStoreWeather($woeid){
		//$url = 'http://weather.yahooapis.com/forecastrss?w='.$woeid.'&d=5';
		//$url = 'http://xml.weather.yahoo.com/forecastrss/_f.xml';
		$url = 'http://xml.weather.yahoo.com/forecastrss/'.$this->session->userdata('zip').'&d=5_f.xml';
		$xml = $this->stream_remote_file($url);
		$weather_data = simplexml_load_string($xml);
		$json = json_decode(json_encode($weather_data),1);
		
		$channel_yweather = $weather_data->channel->children("http://xml.weather.yahoo.com/ns/rss/1.0");
		$item_yweather = $weather_data->channel->item->children("http://xml.weather.yahoo.com/ns/rss/1.0");
		
		foreach($channel_yweather as $x => $channel_item) 
			foreach($channel_item->attributes() as $k => $attr) 
				$yw_channel[$x][$k] = $attr;

		foreach($item_yweather as $x => $yw_item) {
			foreach($yw_item->attributes() as $k => $attr) {
				if($k == 'day') $day = $attr;
				if($x == 'forecast') { $yw_forecast[$x][$day . ''][$k] = $attr;	} 
				else { $yw_forecast[$x][$k] = $attr; }
			}
		}
		$weather_array = array(
			'today' => array(
				'image'=>(string)$yw_forecast['condition']['code'][0],
				'temp'=>(string)$yw_forecast['condition']['temp'][0],
				'city'=>(string)$yw_channel['location']['city'][0]),
			'forecast'=> array(
				
			)	
		);
		foreach ($yw_forecast['forecast'] as $fc)
		{
			$weather_array['forecast'][] = array(
				'day'=>(string)$fc['day'][0],
				'low_temp'=>(string)$fc['low'][0],
				'high_temp'=>(string)$fc['high'][0],
				'image'=>(string)$fc['code'][0]
			);
		}
		//$weather_data->weather->forecast_conditions[0]->low['data']
		return ($weather_array);
		//print_r($json);
		//$this->load->library('rss_parser', array('url'=>$url, 'life'=>2));
		//$this->rss->set_url($url);
		//$weather_data = $this->rss->parse();
		//$xml = file_get_contents($url);
		//$xml = $this->rss_parser->getFeed(2);
		//echo '<br/>rss parser<br/>';
		//$weatherData = new SimplexmlElement($xml);
            
		//print_r($xml);
		//echo "<br/>".$xml->
		//$weatherData = $this->simplexml->xml_parse($xml);
		//print_r($weatherData);
//echo '<br/> Got yahoo info<br/>';
//print_r($weatherData->channel->item->yweather);
		$forecast = $json->forecast;
		//echo "<br/><br/>";
		//print_r($forecast);
		foreach($forecast as $day)
		{
			$fields = array();
			$fields['low'] = $day->low_temperature;
			$fields['high'] = $day->high_temperature;
			$fields['conditions'] = $day->condition;
			$fields['recorded_at'] = date('Y-m-d H:i:s', strtotime($day->day));
			$fields['course_id'] = $this->session->userdata('course_id');
			//echo "<br/>About to insert";
			//print_r($fields);
			$this->db->insert('weather', $fields);
		}

		$this->loadFromDatabase();

	}

	private function stream_remote_file($url){
		$ch = curl_init();
        $timeout = 0;

        //Set CURL options
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

        $xml_str=curl_exec($ch);

        //close CURL cause we dont need it anymore
        curl_close($ch);
		//echo '<br/>'.$url.'<br/>';
		//print_r($xml_str);
        //$xml = new SimplexmlElement($xml_str);
        //if ($xml->weather && $xml->weather->forecast_conditions) {
           // $weatherHTML .= "<div>
           
		
		return $xml_str;
		
		
		/*
		echo '<br/>Streaming remote file '.$url;
		$handle = fopen($url, "rb");
		$contents = stream_get_contents($handle);
		fclose($handle);
		print_r($contents);
		return $contents;*/
 	}

}
?>
