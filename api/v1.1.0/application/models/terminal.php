<?php
class Terminal extends CI_Model
{
	function get_info($terminal_id)
	{
		$this->db->from('terminals');
		$this->db->where('terminal_id', $terminal_id);
		$this->db->limit(1);
		
		return $this->db->get()->result_array();
	}
	function get_all() {
		$this->db->select('terminal_id, label, quickbutton_tab, auto_print_receipts, receipt_ip, hot_webprnt_ip, cold_webprnt_ip, 
			use_register_log, cash_register, print_tip_line, signature_slip_count, credit_card_receipt_count, non_credit_card_receipt_count');
		$this->db->from('terminals');
		$this->db->where('course_id', $this->session->userdata('course_id'));
		return $this->db->get();
	}
	function save(&$terminal_info, $terminal_id = false) {
		if (!$terminal_id)
		{
			return $this->db->insert('terminals', $terminal_info);
		}
		else 
		{
			$this->db->where('terminal_id', $terminal_id);
			return $this->db->update('terminals', $terminal_info);
		}
	}
	function delete($terminal_id) {
		$this->db->where('terminal_id', $terminal_id);
		return $this->db->delete('terminals');
	}
}
?>