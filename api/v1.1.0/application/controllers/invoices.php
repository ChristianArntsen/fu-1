<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Invoices extends Secure_area implements iData_controller
{
	function __construct()
	{
		parent::__construct('invoices');
		$this->load->model('Customer_billing');
		$this->load->model('Invoice');
		$this->load->model('Customer');
		$this->load->model('Customer_credit_card');
	}

	function index()
	{
        $config['base_url'] = site_url('invoices/index');
		$config['total_rows'] = $this->Invoice->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
		$this->pagination->initialize($config);
		//print_r($this->Invoice->get_all(false, $config['per_page'], $this->uri->segment(3))->result_array());
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$invoices = $this->Invoice->get_all(false, $config['per_page'], $this->uri->segment(3), true);
		$billings = $this->Customer_billing->get_all(false, $config['per_page'], $this->uri->segment(3), true);
		//print_r($billings->result_array());
		$data['invoice_manage_table']=get_invoices_manage_table($invoices,$this);
		$data['billing_manage_table']=get_customer_billings_manage_table($billings,$this);
		$this->load->view('invoices/manage',$data);
	}
	
	function find_item_info()
	{
		$item_number=$this->input->post('scan_item_number');
		echo json_encode($this->Billing->find_item_info($item_number));
	}

	function search($offset = 0, $month = false, $paid_status = 'all')
	{
		$data = array();
		$search=$this->input->post('search');
		$data_rows=get_invoices_manage_table_data_rows($this->Invoice->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20, $month, $paid_status, $offset),$this);
		//echo $this->db->last_query();
		$config['base_url'] = site_url('invoices/index');
		$config['total_rows'] = $this->Invoice->search($search, 0, $month, $paid_status);
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['data_rows'] = $data_rows;
		echo json_encode($data);
	
		
		
		// $search=$this->input->post('search');
		// $data_rows=get_billings_manage_table_data_rows($this->Billing->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20),$this);
		// echo $data_rows;
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Customer->get_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}
	
	function billing_search()
	{
        $suggestions = $this->Billing->get_billing_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}
	
	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest_category()
	{
		$suggestions = $this->Billing->get_category_suggestions($this->input->get('term'));
		echo json_encode($suggestions);
	}

	function get_row()
	{
		$item_id = $this->input->post('row_id');
		$data_row=get_billing_data_row($this->Billing->get_info($item_id),$this);
		echo $data_row;
	}
	
	function get_info($item_id=-1)
	{
		echo json_encode($this->Billing->get_info($item_id));
	}
	
	function view($invoice_id=-1)
	{
		$person_id = $this->input->get('person_id');
		$course_id = $this->session->userdata('course_id');
		$course_info = $this->Course->get_info($course_id);
		$invoice_data = $this->Invoice->get_info($invoice_id);
		$data = $invoice_data[0];		
		$data['items']=$this->Invoice->get_items($invoice_id);
		//echo '<br/>Person_id '.$person_id.'<br/>';
		//echo '<br/>Data["person_id"] '.$data['person_id'].'<br/>';
//print_r($data['items']);
		$data['person_info']= $person_id ? $this->Customer->get_info($person_id, $course_id) : $this->Customer->get_info($data['person_id'], $course_id);	
		//echo '<br/>'.$this->db->last_query().'<br/>';	
		$data['course_info']=$course_info;
		$data['credit_cards']= $person_id ? $this->Customer_credit_card->get($person_id) : $this->Customer_credit_card->get($data['person_id']);
		$data['popup'] = 1;// = array(
		$data['is_invoice'] = true;
		$data['sent'] = $invoice_id != -1 ? true : false;		
		if ($pdf)
		{
			$data['pdf'] = true;
			// $data['emailing_invoice'] = false;
			$invoice_html = $this->load->view('customers/invoice', $data, true);
			
			$this->load->library('Html2pdf');
			$html2pdf = new Html2pdf('P','A4','fr');
			//$html2pdf->setModeDebug();
			$html2pdf->pdf->SetDisplayMode('fullpage');
			$html2pdf->setDefaultFont('Arial');
			$html2pdf->writeHTML("<page style'width:600px;'>".$invoice_html.'</page>');
			$html2pdf->Output('invoice.pdf');			
		}
		else
		{
			if ($data['person_info']->person_id)
				$this->load->view('invoices/controls.php', $data);										
			$this->load->view('customers/invoice', $data);			
				
		}
	}
	function view_batch_pdfs()
	{
		$data = array();
		$course_id = $this->session->userdata('course_id');
		$url = 'archives/invoices/'.$course_id;
		$data['files'] = array_reverse(get_filenames($url));
		$data['course_id'] = $course_id;
		$data['no_batch_files_message'] = lang('invoices_no_batch_files');
		//echo 'here<br/>';
		//echo $this->session->userdata('course_id');
		//print_r($data['files']);
		$this->load->view('invoices/batch_pdfs', $data);	
	}
	function view_settings()
	{
		$data = array();//$this->invoice->get_settings();
		$data['generate_on_message'] = lang('invoices_generate_on_message');
		$this->load->view('invoices/settings', $data);
	}
	function history($course_id = -1)
	{
		if ($course_id == -1)
			return;
		else
		{
			$data['course_info'] = $this->Course->get_info($course_id);
			$data['course_payments'] = $this->Billing->get_course_payments($course_id);
			//print_r($course_payments);
			$this->load->view('invoices/history', $data);
		}
	}

	function save($billing_id=-1)
	{
		$billing_data = array(
			'course_id'=>$this->input->post('course_id'),
			'contact_email'=>$this->input->post('contact_email'),
			'tax_name'=>$this->input->post('tax_name'),
			'tax_rate'=>$this->input->post('tax_rate'),
			'product'=>$this->input->post('product'),
			'start_date'=>$this->input->post('start_date'),
			'period_start'=>$this->input->post('period_start'),
			'period_end'=>$this->input->post('period_end'),
			'credit_card_id'=>$this->input->post('credit_card_id'),
			'annual'=>$this->input->post('annual'),
			'email_limit'=>$this->input->post('email_limit'),
			'text_limit'=>$this->input->post('text_limit'),
			'annual_amount'=>$this->input->post('annual_amount'),
			'annual_month'=>$this->input->post('annual_month'),
			'annual_day'=>$this->input->post('annual_day'),
			'monthly'=>$this->input->post('monthly'),
			'monthly_amount'=>$this->input->post('monthly_amount'),
			'monthly_day'=>$this->input->post('monthly_day'),
			'teetimes'=>$this->input->post('teetimes'),
			'teetimes_daily'=>$this->input->post('teetimes_daily'),
			'teetimes_weekly'=>$this->input->post('teetimes_weekly'),
			'teesheet_id'=>$this->input->post('teesheet_id'),
			'free'=>$this->input->post('free')
		);
		if($this->Billing->save($billing_data,$billing_id))
		{
			//New item
			if($billing_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>lang('items_successful_adding').' '.
				$billing_data['name'],'course_id'=>$billing_data['billing_id']));
				$billing_id = $billing_data['billinge_id'];
			}
			else //previous item
			{
				echo json_encode(array('success'=>true,'message'=>lang('items_successful_updating').' '.
				$billing_data['name'],'billing_id'=>$billing_id));
			}
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('items_error_adding_updating').$this->db->last_query().' '.
			$billing_data['name'],'billing_id'=>-1));
		}

	}
	
	
	function delete()
	{
		$items_to_delete=$this->input->post('ids');

		if($this->Billing->delete_list($items_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('items_successful_deleted').' '.
			count($items_to_delete).' '.lang('items_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('items_cannot_be_deleted')));
		}
	}
	
	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 650;
	}
}
?>