<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require('ellipsis/DOMLettersIterator.php');
require('ellipsis/DOMWordsIterator.php');
require('ellipsis/TruncateHTML.php');

class Support extends CI_Controller
{
  private $phone_number = '(801) 215-9487';
  //private $support_email = 'yannix.pogay@gmail.com';
  private $support_email = 'support@foreUP.com';
  function __construct()
  {
    parent::__construct();

    $this->load->model('Help_topic');
  }

  public function index()
  {
  	/*
	 * NEW ZENDESK HANDLING
	 */
	//include_once "Authentication/JWT.php";
	$employee_data = $this->Employee->get_info($this->session->userdata('person_id'));
	$this->load->library('JWT');
  	// Log your user in.
	$now       = time();
	$token = array(
	  "jti"   => md5($now . rand()),
	  "iat"   => $now,
	  "name"  => $employee_data->first_name.' '.$employee_data->last_name,
	  "email" => $employee_data->email,
	  "external_id" => $this->session->userdata('person_id'),
	  "organization" => $this->session->userdata('course_name'),
	  "tags" => json_encode(array("phone_number"=>$employee_data->phone_number,"position"=>$employee_data->position))
	);
	$jwt = JWT::encode($token, $this->config->item('zendesk_shared_key'));
	// Redirect
	header("Location: https://" . $this->config->item('zendesk_subdomain') . ".zendesk.com/access/jwt?jwt=" . $jwt);
	
	/*
	 * OLD FUNCTIONALITY
	 */
    $data['phone_number'] = $this->phone_number;
    $data['email'] = $this->support_email;
    $data['common_questions'] = $this->get_common_questions();
    $commq = $this->Help_topic->get_common_questions();
    $data['common_questions_count'] = $commq->num_rows();
    
    $data['tutorials'] = $this->get_tutorials();
    $tut = $this->Help_topic->get_tutorials();
    $data['tutorials_count'] = $tut->num_rows();
    
    $mods = $this->Help_topic->get_all();

    if($mods->num_rows() == 0)
    {
      // initialize topics for the first time
      $this->initialize_topics();
      // then requery the topics
      $mods = $this->Help_topic->get_all();
    }

    $mod_third_count =  $mods->num_rows() / 3;

    $modules = array();

    $count = 0;

    $mod_opts = array();

    foreach($mods->result_array() as $mod)
    {
      $idx = ($count < $mod_third_count) ? 'col1' : ($count < $mod_third_count * 2?'col2':'col3');
      
      switch($mod['topic'])
      {
        case 'Tee Sheets': $module_id = 'teesheets'; break;
        case 'Inventory': $module_id = 'items'; break;
        case 'Settings': $module_id = 'config'; break;
        case 'Marketing': $module_id = 'marketing_campaigns'; break;
        default:
          $module_id = strtolower(str_replace(' ', '_', $mod['topic']));
        break;
      }

      $modules[$idx][] = array(
          'img'     => base_url("images/menubar/{$module_id}.png"),
          'link'    => site_url("support/get_topics/{$mod['id']}"),
          'module'  => $mod['topic']
      );

      $mod_opts[$mod['id']] = $mod['topic'];
      $count++;
    }

    $data['modules'] = $modules;
    $data['superadmin'] = '';
    // check what pages/options are to be enabled based on the user permission
    if ($this->permissions->is_super_admin())
    {
      $data['module_options'] = $mod_opts;

      $data['superadmin'] = $this->load->view('help/superadmin', $data, true);
    }

    $data['side_bar'] = $this->load->view('help/sidebar',$data, true);
    $data['search_form'] = $this->load->view('help/search_form',$data, true);
    $contents = $this->load->view('help/dashboard', $data, true);

    $this->load->view('help/template', array('contents'=>$contents));
  }

  private function initialize_topics()
  {
    $mods = $this->Module->get_all_modules();

    if($mods->num_rows() > 0)
    {
      foreach($mods->result_array() as $mod)
      {
        $data['topic'] = ucwords(str_replace('_', ' ', $mod['module_id']));
        $this->Help_topic->save($data);
      }
    }
  }

  public function auto_complete()
  {
    $term = $this->input->get('term');
    $term = trim($term);
    $term = preg_replace('/how do i/i', '', $term);
    $term = preg_replace('/how do/i', '', $term);
    $term = preg_replace('/how/i', '', $term);
    $terms = explode('+', $term);
    $terms = explode(' ', $term);
    $data = array();
    
    $posts = $this->Help_topic->search_post($terms);

    if($posts->num_rows > 0)
    {
      foreach($posts->result_array() as $post)
      {
        $content = TruncateHTML::truncateWords($post['contents'], 30, '...');
        $data[] = array(
            'id'=> site_url("support/get_topic/{$post['id']}"),
            'label'=> '<span class="KbArticle"><span class="article-autocomplete-subject" id="article-autocomplete-subject-402777">'.$post['title'].'</span><span class="article-autocomplete-body" id="article-autocomplete-body-402777">'.$content.'</span></span>',
            'value'=> $post['title'],
            'topic_id' => $post['id']
        );
      }
    }
    echo json_encode($data);
  }

  private function get_common_questions()
  {
    $data = array();

    $com_questions = $this->Help_topic->get_common_questions(5);

    if($com_questions->num_rows() > 0)
    {
      foreach($com_questions->result_array() as $comq)
      {
        $data[] = array(
            'link'  => site_url("support/get_topic/{$comq['id']}?bread_crumb=Common Questions"),
            'title' => $comq['title']
        );
      }
    }

    return $data;
  }

  private function get_tutorials()
  {
    $data = array();

    $com_tuts = $this->Help_topic->get_tutorials(5);

    if($com_tuts->num_rows() > 0)
    {
      foreach($com_tuts->result_array() as $comq)
      {
        $data[] = array(
            'link'  => site_url("support/get_topic/{$comq['id']}?bread_crumb=Tutorials"),
            'title' => $comq['title']
        );
      }
    }

    return $data;
  }


  public function get_all_common_questions()
  {
    $data['topics'] = array();

    $data['phone_number'] = $this->phone_number;
    $data['email'] = $this->support_email;
    $data['topic_bc'] = 'Common Questions';
    $data['topic_type'] = 'Common Questions';

    $posts = $this->Help_topic->get_common_questions();

    if($posts->num_rows() > 0)
    {
      foreach($posts->result_array() as $post)
      {
        $admin_opts = '';

        if($this->permissions->is_super_admin())
        {
          $admin_opts = "<a class='article-edit' id='{$post['id']}-edit-article'>Edit</a> | <a class='article-delete' id='{$post['id']}-delete-article'>Delete</a>";
        }

        $contents = str_replace('&lt;', '<', $post['contents']);
        $contents = str_replace('&gt;', '>', $contents);
        $post['contents'] = $contents;
        
        $data['topics'][] = array(
            'id' => $post['id'],
            'topic_id' => $post['topic_id'],
            'bc'   => ($post['type'] == 'question') ? 'Common Questions' : 'Tutorials',
            'type' => $post['type'],
            'title' => $post['title'],
            'content_el' => TruncateHTML::truncateWords( $post['contents'], 35, '...'),
            'content' => $post['contents'],
            'date_modified' => date('M d, Y', strtotime($post['date_modified'])),
            'keywords' => $post['keywords'],
            'admin_opts' => $admin_opts
        );
      }
    }


    $data['superadmin'] = '';
    // check what pages/options are to be enabled based on the user permission
    if ($this->permissions->is_super_admin())
    {
      $mods = $this->Help_topic->get_all();

      $mod_opts = array();

      foreach($mods->result_array() as $mod)
      {
        $mod_opts[$mod['id']] = $mod['topic'];
      }

      $data['module_options'] = $mod_opts;

      $data['superadmin'] = $this->load->view('help/superadmin', $data, true);
    }

    $data['side_bar'] = $this->load->view('help/sidebar',$data, true);
    $data['search_form'] = $this->load->view('help/search_form',$data, true);
    $contents = $this->load->view('help/tcq', $data, true);

    $this->load->view('help/template', array('contents'=>$contents));
  }
  
  public function get_all_tutorials()
  {
    $data['topics'] = array();

    $data['phone_number'] = $this->phone_number;
    $data['email'] = $this->support_email;
    $data['topic_bc'] = 'Tutorials';
    $data['topic_type'] = 'Tutorials';
    
    $posts = $this->Help_topic->get_tutorials();

    if($posts->num_rows() > 0)
    {
      foreach($posts->result_array() as $post)
      {
        $admin_opts = '';

        if($this->permissions->is_super_admin())
        {
          $admin_opts = "<a class='article-edit' id='{$post['id']}-edit-article'>Edit</a> | <a class='article-delete' id='{$post['id']}-delete-article'>Delete</a>";
        }

        $contents = str_replace('&lt;', '<', $post['contents']);
        $contents = str_replace('&gt;', '>', $contents);
        $post['contents'] = $contents;
        
        $data['topics'][] = array(
            'id' => $post['id'],
            'topic_id' => $post['topic_id'],
            'bc'   => ($post['type'] == 'question') ? 'Common Questions' : 'Tutorials',
            'type' => $post['type'],
            'title' => $post['title'],
            'content_el' => TruncateHTML::truncateWords( $post['contents'], 35, '...'),
            'content' => $post['contents'],
            'date_modified' => date('M d, Y', strtotime($post['date_modified'])),
            'keywords' => $post['keywords'],
            'admin_opts' => $admin_opts
        );
      }
    }
    

    $data['superadmin'] = '';
    // check what pages/options are to be enabled based on the user permission
    if ($this->permissions->is_super_admin())
    {
      $mods = $this->Help_topic->get_all();

      $mod_opts = array();

      foreach($mods->result_array() as $mod)
      {
        $mod_opts[$mod['id']] = $mod['topic'];
      }
      
      $data['module_options'] = $mod_opts;

      $data['superadmin'] = $this->load->view('help/superadmin', $data, true);
    }

    $data['side_bar'] = $this->load->view('help/sidebar',$data, true);
    $data['search_form'] = $this->load->view('help/search_form',$data, true);
    $contents = $this->load->view('help/tcq', $data, true);

    $this->load->view('help/template', array('contents'=>$contents));
  }

  public function rate_post()
  {
    $user_id = $this->session->userdata('person_id');

    $post_id = $this->input->get('post_id');
    $rating = $this->input->get('rating');

    $this->Help_topic->rate_post($user_id, $post_id, $rating);

    echo '<div class="success">Thank you</div>';
  }

  public function get_topic($id)
  {
    $breadcrumb = $this->input->get('bread_crumb');
    $bc_link = '';
    

    $user_id = $this->session->userdata('person_id');
    $info = $this->Help_topic->get_topic($id);

    $topic = $this->Help_topic->get_info($info->topic_id);    

    if(empty($breadcrumb)){
      $breadcrumb = $topic->topic;
      $bc_link = site_url("support/get_topics/{$info->topic_id}");
    }else{
      $bc = strtolower($breadcrumb);
      if($bc == 'tutorials')
      {
        $bc_link = site_url('support/get_all_tutorials');
      }
      else
      {
        $bc_link = site_url('support/get_all_common_questions');
      }
    }
    
    $data['topic'] = $topic->topic;
    $data['topic_id'] = $info->topic_id;
    $data['keywords'] = $info->keywords;
    $data['post_id'] = $id;
    $data['type'] = $info->type;
    $data['date_modified'] = date('M d, Y', strtotime($info->date_modified));
    $data['title'] = $info->title;
    $contents = str_replace('&lt;', '<', $info->contents);
    $contents = str_replace('&gt;', '>', $contents);
    $data['contents'] = $contents;
    $data['side_bar'] = $this->load->view('help/sidebar',$data, true);
    $data['from'] = '<a href="'. $bc_link .'">'. $breadcrumb .'</a>';
    $data['superadmin'] = '';
    $data['options'] = '';
    
    $related_articles_dis = array();   
    

    if(empty($info->related_articles))
    {
      $related_articles = $this->Help_topic->get_related_articles($id, $info->title);
      if($related_articles->num_rows() > 0)
      {
        foreach($related_articles->result_array() as $ra)
        {
          $related_articles_dis[] = array(
              'id'   => $ra['id'],
              'link' => site_url('support/get_topic/' . $ra['id']),
              'title' => $ra['title']
          );
        }
      }
      $info->related_articles = utf8_encode(serialize($related_articles_dis));

      $post_data = (array) $info;

      $this->Help_topic->save_post($post_data, $id);
    }
    else
    {
      $related_articles_dis = unserialize($info->related_articles);      
    }
    $data['related_articles'] = $related_articles_dis;
    
    if ($this->permissions->is_super_admin())
    {
      $mods = $this->Help_topic->get_all();

      $mod_opts = array();

      foreach($mods->result_array() as $mod)
      {
        $mod_opts[$mod['id']] = $mod['topic'];
      }
      $data['module_options'] = $mod_opts;

      $data['superadmin'] = $this->load->view('help/superadmin', $data, true);
      $data['options'] = "<a class='article-edit' id='{$id}-edit-article'>Edit</a> | <a class='article-delete' id='{$id}-delete-article'>Delete</a>";
    }
    elseif($this->Employee->is_logged_in() && !$this->Help_topic->post_has_rated($user_id, $id))
    {
      // check if user rating is available and if the article is not rated yet.

      $data['options'] = '<br/><div style="" id="rate_article_container">
          <div id="rate_article">
            <div>
              <a id="rate-yes" rel="nofollow" data-remote="true" data-method="post" class="rate_link" href="'. site_url("support/rate_post?post_id={$id}&rating=1") .'"> Yes </a>
              <span>
                I found this article helpful
              </span>
            </div>
            <div class="rate-link-down">
              <a id="rate-no" rel="nofollow" data-remote="true" data-method="post" class="rate_link" href="'. site_url("support/rate_post?post_id={$id}&rating=0") .'"> No</a>
              <span>
                I did not find this article helpful
              </span>
            </div>
          </div>
        </div>';
    }

    $data['ratings'] = '';

    $data['phone_number'] = $this->phone_number;
    $data['email'] = $this->support_email;
    
    $data['side_bar'] = $this->load->view('help/sidebar',$data, true);
    $data['search_form'] = $this->load->view('help/search_form',$data, true);
    $contents = $this->load->view('help/topic', $data, true);

    $this->load->view('help/template', array('contents'=>$contents));
  }

  public function get_topics($topic_id)
  {
    $info = $this->Help_topic->get_info($topic_id);

    $data['phone_number'] = $this->phone_number;
    $data['email'] = $this->support_email;
    
    $data['topic'] = $info->topic;
    $data['topic_id'] = $topic_id;
    $data['date_modified'] = date('M d, Y', strtotime($info->date_modified));
    
    $data['topics'] = array();

    $posts = $this->Help_topic->get_all_topics($topic_id);

    if($posts->num_rows() > 0)
    {
      foreach($posts->result_array() as $post)
      {
        $admin_opts = '';
        
        if($this->permissions->is_super_admin())
        {
          $admin_opts = "<a class='article-edit' id='{$post['id']}-edit-article'>Edit</a> | <a class='article-delete' id='{$post['id']}-delete-article'>Delete</a>";
        }

        $contents = str_replace('&lt;', '<', $post['contents']);
        $contents = str_replace('&gt;', '>', $contents);
        $post['contents'] = $contents;
    
        $data['topics'][] = array(
            'id' => $post['id'],
            'topic_id' => $post['topic_id'],
            'type' => $post['type'],
            'title' => $post['title'],
            'content' => TruncateHTML::truncateWords( $post['contents'], 35, '...'),
            'date_modified' => date('M d, Y', strtotime($post['date_modified'])),
            'keywords' => $post['keywords'],
            'admin_opts' => $admin_opts
        );
      }
    }

    $mods = $this->Help_topic->get_all();

    $mod_opts = array();

    foreach($mods->result_array() as $mod)
    {
      $mod_opts[$mod['id']] = $mod['topic'];
    }

    $data['superadmin'] = '';
    // check what pages/options are to be enabled based on the user permission
    if ($this->permissions->is_super_admin())
    {
      $data['module_options'] = $mod_opts;

      $data['superadmin'] = $this->load->view('help/superadmin', $data, true);
    }
    
    $data['side_bar'] = $this->load->view('help/sidebar',$data, true);
    $data['search_form'] = $this->load->view('help/search_form',$data, true);
    $contents = $this->load->view('help/topics', $data, true);

    $this->load->view('help/template', array('contents'=>$contents));
  }

  public function delete_post()
  {
    $id = $this->input->post('id');

    if($this->Help_topic->delete_post($id))
    {
      echo 'Article was successfully deleted.';
    }
    else
    {
      echo 'An error occurred during the transaction. Please try again';
    }
  }

  public function save()
  {
    $related_articles = $this->input->post('related_articles');
    $post_id = $this->input->post('id');
    $type = $this->input->post('type');
    $title = $this->input->post('title');
    $content = $this->input->post('content');
    $topic_id = $this->input->post('topic_id');
    $keywords = $this->input->post('keywords');
    $title = trim($title);
    $content = trim($content);
    if(empty($title) || empty($content))
    {
      return print 'Error: Title or Content must not be empty.';
    }

    $related_articles = json_decode($related_articles);

    $ra = array();
    foreach($related_articles->list as $r)
    {
      $ra[] = array(
            'id'   => $r->id,
            'link' => site_url('support/get_topic/' . $r->id),
            'title' => $r->title
        );
    }
    $post_data = array(
        'type' => $type,
        'title' => $title,
        'contents' => $content,
        'topic_id' => $topic_id,
        'keywords' => $keywords,
        'date_modified' => date('Y-m-d h:i:s'),
        'related_articles' => utf8_encode(serialize($ra))
    );

    $post_id = empty($post_id) ? false : $post_id;

    if($this->Help_topic->save_post($post_data, $post_id))
    {
      if(!$post_id) return print 'A new article has been added successfully.';

      echo 'Article was modified successfully.';
    }
    else
    {
      echo 'An error occurred during the transaction. Please try again';
    }
  }

  public function search()
  {
    $term = $this->input->get('q');
    $term = trim($term);
    $term = preg_replace('/how do i/i', '', $term);
    $term = preg_replace('/how do/i', '', $term);
    $term = preg_replace('/how/i', '', $term);
    $terms = explode('+', $term);
    $terms = explode(' ', $term);

    $data = array();    

    $data['topics'] = array();

    $data['phone_number'] = $this->phone_number;
    $data['email'] = $this->support_email;    
    $data['topic_bc'] = 'Search';

    $posts = $this->Help_topic->search_post($terms);
    $trms = implode(' ', $terms);
    
    $data['topic_type'] = $posts->num_rows() . " results found for \"<span id='search_term'>{$trms}</span>\"";
    
    if($posts->num_rows() > 0)
    {
      foreach($posts->result_array() as $post)
      {
        $admin_opts = '';

        if($this->permissions->is_super_admin())
        {
          $admin_opts = "<a class='article-edit' id='{$post['id']}-edit-article'>Edit</a> | <a class='article-delete' id='{$post['id']}-delete-article'>Delete</a>";
        }

        $data['topics'][] = array(
            'id' => $post['id'],
            'topic_id' => $post['topic_id'],
            'bc'   => ($post['type'] == 'question') ? 'Common Questions' : 'Tutorials',
            'type' => $post['type'],
            'title' => $post['title'],
            'content_el' => TruncateHTML::truncateWords( $post['contents'], 35, '...'),
            'content' => $post['contents'],
            'date_modified' => date('M d, Y h:iA T', strtotime($post['date_modified'])),
            'keywords' => $post['keywords'],
            'admin_opts' => $admin_opts
        );
      }
    }


    $data['superadmin'] = '';
    // check what pages/options are to be enabled based on the user permission
    if ($this->permissions->is_super_admin())
    {
      $mods = $this->Help_topic->get_all();

      $mod_opts = array();

      foreach($mods->result_array() as $mod)
      {
        $mod_opts[$mod['id']] = $mod['topic'];
      }

      $data['module_options'] = $mod_opts;

      $data['superadmin'] = $this->load->view('help/superadmin', $data, true);
    }

    $data['side_bar'] = $this->load->view('help/sidebar',$data, true);
    $data['search_form'] = $this->load->view('help/search_form',$data, true);
    $contents = $this->load->view('help/tcq', $data, true);

    $this->load->view('help/template', array('contents'=>$contents));
  }

  public function get_related_articles()
  {
    $title = $this->input->post('title');
    
    $related_articles = $this->Help_topic->get_related_articles(0, $title);

    $data = array();

    if($related_articles->num_rows() > 0)
    {
      foreach($related_articles->result_array() as $ra)
      {
        $data[] = array(
            'id'   => $ra['id'],
            'title' => $ra['title']
        );
      }
    }

    echo json_encode($data);
  }

  public function send_email()
  {
    $course  = $this->Course->get_info($this->session->userdata('course_id'));
    $user_id = $this->session->userdata('person_id');
    $info = $this->Employee->get_info($user_id);
    $fromName = $info->first_name . ' ' . $info->last_name . ' | ' . $course->name;
    $question = $this->input->post('content');
    $sender = $info->first_name . ' ' . $info->last_name;
    $contents = "
      Sender: {$sender} \n
      Golf Course: {$course->name} \n
      Question: {$question} \n
    ";

    $contents = nl2br($contents);
      
    send_sendgrid($this->support_email, 'FAQ/Help Question', $contents, $info->email, $fromName);

    echo 'Email sent. Thank you.';
  }

  public function request_answer()
  {
    $course  = $this->Course->get_info($this->session->userdata('course_id'));
    $user_id = $this->session->userdata('person_id');
    $topic = $this->input->post('topic');
    $details = $this->input->post('details');
    $sender = $info->first_name . ' ' . $info->last_name;
    $contents = "
      Sender: {$sender} \n
      Search: {$topic} \n
      Message: {$details}\n
      Golf Course: {$course->name}
    ";
    $info = $this->Employee->get_info($user_id);
    $fromName = $info->first_name . ' ' . $info->last_name . ' | ' . $course->name;

    $contents = nl2br($contents);

    send_sendgrid($this->support_email, 'FAQ/Help Answer Request', $contents, $info->email, $fromName);

    echo 'Email sent. Thank you.';
  }

  public function improvement_details()
  {
    $course  = $this->Course->get_info($this->session->userdata('course_id'));
    $user_id = $this->session->userdata('person_id');
    $info = $this->Employee->get_info($user_id);
    $fromName = $info->first_name . ' ' . $info->last_name;

    $topic = $this->input->post('topic');
    $details = $this->input->post('details');

    $contents = "
      Topic: {$topic} \n
      Comments: {$details}
    ";

    $info = $this->Employee->get_info($user_id);
    $fromName = $info->first_name . ' ' . $info->last_name . ' | ' . $course->name;
    $contents = nl2br($contents);
    send_sendgrid($this->support_email, 'FAQ/Help Improvement Help', $contents, $info->email, $fromName);

    echo 'Email sent. Thank you.';
  }
}