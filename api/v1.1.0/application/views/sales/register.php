<?php $this->load->view("partial/header");
$printer_name = $this->config->item('updated_printing')?'foreup':'foreup';
//print_r($cart);
?>
<style>
	#content_area_wrapper {
		width:100%;
	}
	#content_area {
		width:100%;
		background-color:transparent;
	}
	#new_quickbuttons {
		width:170px;
		position:relative;
		padding:10px;
	}
	#register_container {
		margin:0px auto;
	}
	.new_quickbutton {
		background:url(../images/pieces/buttons.png) -10px -490px transparent;
		height:30;
		text-align:center;
		display:block;
		color:white;
		font-size:12px;
		cursor:pointer;
	}
	a.selected_menu {
		background-position-x:-191px;
	}
	.edit_quickbuttons, .add_quickbutton, .settings_button {
		width: 184px;
		margin-top: 10px;
		color: #5D5D5D;
		text-transform: uppercase;
		font-weight: bold;
		text-shadow: 1px 1px 0px black;
		padding: 10px 3px;
		line-height: 1;
		text-align: left;
		box-shadow: inset 0px 0px 0px 1px rgba(0, 0, 0, 0.4), 0px 2px -1px 0px rgba(255, 255, 255, 0.2);
		border: none;
		background: rgba(0, 0, 0, 0.2);
	}
	.quickbutton_menu {
		position:relative;
		background-position:-10px -530px;
	}
	#column_transactions {
		background-position:-10px -610px;
	}
	.quickbutton_menu_box {
		display:none;
		margin-left:-999em;
		position:absolute;
		border-radius:5px 4px; -moz-border-radius:5px; -webkit-border-radius:5px;
		box-shadow:5px 5px 5px rgba (0,0,0,0.1); -webkit-box-shadow:5px 5px rgba (0,0,0 0.1); -moz-box-shadow:5px 5px rgba (0,0,0,0.1);
		font-family:Calibri, Tahoma, Geneva, sans-serif;
		position:absolute; left:11em; top:0em; z-index:99;
		margin-left:0; width:170px;
		background:white;
		border:1px solid #369;
		padding:5px;
	}
	a.show {
		display:block;
	}
	#sortable {
		list-style:none;
		/*margin-top:38px;*/
	}
	#suspend_sale_button {
		margin-top:0px;
	}
	#sortable .ui-state-default, #sortable .ui-widget-content .ui-state-default, #sortable .ui-widget-header .ui-state-default, #sortable #cboxTitle {
		border:none;
		background:none;
	}
	#sortable li.ui-state-default a {
		color:white;
	}
	.purchase_payments_box {
		background:#ff8;
		border-top:1px solid #ccc;
		border-bottom:1px solid #ccc;
		padding:2px 10px 5px;
		font-size:0.9em;
		display:none;
	}
	.purchase_payments_box div {
		height:20px;
		line-height:20px;
	}
	.purchase_payments_box .header {
		font-size:1.2em;
		margin:10px 0px;
	}
	.table_number {
		float: left;
		color: #333;
		width: 95%;
		border: 1px solid #369;
		background: white;
		margin: 5px;
		text-align: center;
		line-height: 30px;
		padding-bottom:5px;
	}
	.table_number .ss_customer_name {
		line-height:20px;
	}
	a.already_used {
		border:1px solid #ccc;
		background:#eee;
		cursor:default;
	}
	#cboxContent .table_number {
		width:50px;
		height:50px;
		line-height:50px;
		margin:0px;
		font-size:16px;
		font-weight:bold;
		border:1px solid #7c7c7c;
		color:#484848;
		background:#e7e7e7;
	}
	#cboxContent a.already_used {
		color:white;
		cursor:default;
		background:#7c7c7c;
	}
	#suspended_sales .selected {
		background:#CBE5FF;
	}
	+/* Sales page */
#feedback_bar {
       z-index:1000;
}
.hover_mask {
    width:224px;
    height:33px;
    background:url(../images/pieces/reg_mode3.png) black;
    position:relative;
    margin-top:-34px;
    opacity:.6;
}
.recent_transaction:hover .hover_mask {
    display:block;
}
#column_transactions, #suspended_sales_header {
	background:url(../images/backgrounds/top_bar_bg.png) no-repeat;
	height:37px;
	width:97%;
	text-align: left;
	line-height: 37px;
	text-indent: 15px;
	color: white;
	font-size: 16px;
	text-shadow: 0px -1px 1px black;
}
#suspended_sales_header {
	margin-top:10px;
}
#recent_transactions, #suspended_sales {
	box-shadow: inset 0px 0px 0px 1px rgba(0, 0, 0, 0.4), 0px 2px -1px 0px rgba(255, 255, 255, 0.2);
	border: none;
	background: rgba(0, 0, 0, 0.2);
	padding:6px 6px;
	width:190px;
    overflow:auto;
}
#recent_transactions {
    max-height:300px;
}
.recent_transaction {
    background:transparent url(../images/backgrounds/receipt.png) no-repeat bottom left;
    padding-bottom:20px;
    /*box-shadow:1px 1px 1px 1px #000;*/
}
.recent_transaction a {
    color:#336699;
}
.recent_transaction:hover {
}
.recent_transaction:hover .transaction_id {
}
.recent_transaction .transaction_time {
    font-size:14px;
    color:#242424;
    padding-left:6px;
}
.recent_transaction .transaction_name {
	font-size:16px;
	color:#242424;
	margin-top:5px;
	padding-left:10px;
}
.recent_transaction .transaction_total {
    float:right;
    font-size:16px;
    color:#242424;
    padding-right:10px;
}
.transaction_info {
    padding:5px;
}
.transaction_id {
    font-size:14px;
    text-align:center;
    color:#2073A9;
    float:right;
    padding-right:6px;
}
</style>
<script>
	$(document).ready(function(){
		// THIS KILLS THE ABILITY TO OPEN DOWNLOADS FROM THE SALES PAGE... BARCODE SCANNERS WERE DOING IT ACCIDENTALLY, SO WE SUPPRESSED IT
		$(document).keydown(function(e){if (e.which == 74 && e.ctrlKey){e.preventDefault();}});
		$('.edit_quickbuttons').click(function(event) {
			event.preventDefault();
			if (!quickbuttons.editable)
				quickbuttons.edit();
			else
				quickbuttons.end_edit();
		});
		$('.add_quickbutton').click(function(event){
			event.preventDefault();
			quickbuttons.add();
		});
		$('.issue_raincheck').click(function(event){
			event.preventDefault();
			$.colorbox({
        		href:'index.php/sales/view_raincheck/',
        		title:'Issue Raincheck',
        		width:600
        	});
		});
		quickbuttons.initialize();
		$('html').click(function() {
		 //Hide the menus if visible
		 	 quickbuttons.close_menu(function(){});
//			 $('#new_quickbuttons .quickbutton_menu_box').hide();
		     $('.selected_menu').removeClass('selected_menu');
		});

		 $('.quickbutton_menu_box').click(function(event){
		     event.stopPropagation();
		 });
	});
	var quickbuttons = {
		item_ids:{},
		editable:false,
		edit_link:'',
		click_link:'',
		current_menu:'',
		initialize:function () {
			console.log('initializing');
			$('a.quickbutton').click(function(event){
				event.preventDefault();
				quickbuttons.click(event);
			});
			$('a.quickbutton_menu').each (function() {
				if (!$(this).data('events'))
					$(this).click(function(event){
						event.preventDefault();
						quickbuttons.click(event, true);
					});
			});
		},
		add:function () {
			$.colorbox({
				href:'index.php/items/view_quickbutton/-1',
				width:550,
				onClosed:function() {
					quickbuttons.clear_item_ids();
				}
			})
		},
		clear_item_ids:function(){
			this.item_ids = {};
		},
		item_id_count:function () {
			var count = 0;
			var item_ids = this.item_ids
			for (i in item_ids)
				if (item_ids.hasOwnProperty(i)) {count++};
			return count;
		},
		edit:function () {
			//$('.quickbutton_menu_box').hide();
			this.close_menu(function() {});
			$('.selected_menu').removeClass('selected_menu');
			this.editable = true;
			this.show_add_button();
			this.show_outlines();
			$('.edit_quickbuttons').html('<span class="save_icon"></span>Save Order');
			$('#sortable_1, #sortable_2, #sortable_3').sortable({disabled:false});
		},
		end_edit:function () {
			this.editable = false;
			this.save_order();
			this.hide_add_button();
			this.hide_outlines();
			$('.edit_quickbuttons').html('<span class="edit_icon"></span>Edit Quickbuttons');
			$('#sortable_1, #sortable_2, #sortable_3').sortable("option", "disabled", true);
		},
		save:function () {

		},
		save_order:function () {
			var position_array_1 = $('#sortable_1').sortable('toArray');
			var position_array_2 = $('#sortable_2').sortable('toArray');
			var position_array_3 = $('#sortable_3').sortable('toArray');
			$.ajax({
               type: "POST",
               url: "index.php/items/save_quickbutton_positions",
               data: {
               		positions_1:position_array_1.length > 0 ? position_array_1.join('|') : '',
               		positions_2:position_array_2.length > 0 ? position_array_2.join('|') : '',
               		positions_3:position_array_3.length > 0 ? position_array_3.join('|') : ''
               },
               success: function(response){
               		console.dir(response);
			   }
            });
		},
		click:function (event, menu) {
			var type = (menu)?'menu':'not-menu';
			console.log('clicked '+type);
			var item_id = $(event.target).attr('item_id');
			var quickbutton_id = $(event.target).attr('quickbutton_id');
			if (this.editable)
				$.colorbox({href:'index.php/items/view_quickbutton/'+quickbutton_id, width:550});
			else if (menu === true)
				this.open_menu(event);
			else {
				this.close_menu( function() {
					sales.add_item(item_id); //window.location = 'index.php/sales/add/'+item_id;
				});
			}
		},
		open_menu:function (event) {
			event.stopPropagation();
			//$('.quickbutton_menu_box').hide();

			this.close_menu(
				function(){
					console.log('in the oncomplete');
					$('.selected_menu').removeClass('selected_menu');
					if (quickbuttons.current_menu == quickbutton_id)
					{
						quickbuttons.current_menu = '';
					}
					else
					{
						var quickbutton_id = $(event.target).attr('quickbutton_id');
						$('#menu_'+quickbutton_id).css('top',$(event.target).position().top);
						//$('#menu_'+quickbutton_id).show();
						$('#menu_'+quickbutton_id).slideDown(200);
						$('#menu_'+quickbutton_id).addClass('open_menu');
						$(event.target).addClass('selected_menu');
						quickbuttons.current_menu = quickbutton_id;
					}

				}
			);
		},
		close_menu:function (onComplete) {
			if ($('.open_menu')[0])
			{
				$('.open_menu').slideUp(200, onComplete);
				$('.open_menu').removeClass('open_menu');
			}
			else
				onComplete();
		},
		move:function () {

		},
		show_add_button:function () {
			$('.add_quickbutton').fadeIn(400);
		},
		hide_add_button:function () {
			$('.add_quickbutton').fadeOut(400);
		},
		show_outlines:function() {
			$('#new_quickbuttons').animate({
				border:'3px dashed #ccc'
			}, 400);
		},
		hide_outlines:function() {
			$('#new_quickbuttons').animate({
				border:'0px none white'
			}, 400);
		},
		delete:function(id) {
			$('#quickbutton_holder_'+id).remove();
		},
		remove:function(id) {
			console.log('inside remove');
			delete this.item_ids[id];
			$('#demo_button_'+id).remove();
			$.colorbox.resize();
		},
		update:function(id, html) {
			console.log('id '+id);
			if ($('#quickbutton_holder_'+id).length != 0)
			{
				console.log('replacing '+id);
				$('#quickbutton_holder_'+id).replaceWith($(html));
			}
			else
			{
				var tab = $('.quickbutton_tab.selected').attr('id').replace('qb_tab_','');
				if ($('#sortable_'+tab).length == 0)
				{
					$('#new_quickbuttons').append('<ul id="sortable_'+tab+'" class="quickbutton_group"></ul>');
					$('#sortable_'+tab).sortable({disabled:false});
				}

				$('#sortable_'+tab).prepend($(html));
			}
			this.initialize();
		}
	}
</script>
<div id='sales_register_holder'>
	<!-- Quick buttons column start -->
	<div class='left_column'>
		<div id='new_quickbuttons'>
			<a class='issue_raincheck new_quickbutton' href='index.php/sales/view_raincheck'>Issue Raincheck</a>
			<div class='quickbutton_tabs'>
				<span id='qb_tab_1' class='quickbutton_tab <?php echo (!$quickbutton_tab || $quickbutton_tab == 1 ? "selected":'')?>'>1</span>
				<span id='qb_tab_2' class='quickbutton_tab <?php echo ($quickbutton_tab == 2 ? "selected":'')?>'>2</span>
				<span id='qb_tab_3' class='quickbutton_tab <?php echo ($quickbutton_tab == 3 ? "selected":'')?>'>3</span>
				<div class='clear'></div>
			</div>
			<?php //print_r($quickbutton_info);?>
			<ul id='sortable_1' class='quickbutton_group' <?php echo ($quickbutton_tab && $quickbutton_tab != 1 ? "style='display:none;'":'')?>>
				<?php
				foreach ($quickbutton_info as $quickbutton_id => $quickbutton) {
					if ($quickbutton['tab'] == 2 && !$tab_2_started)
					{
						echo "</ul><ul id='sortable_2' class='quickbutton_group' ".(!$quickbutton_tab || $quickbutton_tab != 2 ? "style='display:none;'":'').">";
						$tab_2_started = true;
					}
					if ($quickbutton['tab'] == 3 && !$tab_3_started)
					{
						echo "</ul><ul id='sortable_3' class='quickbutton_group' ".(!$quickbutton_tab || $quickbutton_tab != 3 ? "style='display:none;'":'').">";
						$tab_3_started = true;
					}
					echo $this->Quickbutton->build($quickbutton_id, $quickbutton);
				}
				?>
			</ul>
		</div>
		<a class='add_quickbutton new_quickbutton' href='index.php/sales' style='display:none'><span class="add_icon"></span>Add Quickbutton</a>
		<a class='edit_quickbuttons new_quickbutton' href='index.php/sales'><span class="edit_icon"></span>Edit Quickbuttons</a>
	    <a href='index.php/config/view/sales/width~700' class='settings_button colbox new_quickbutton' title='Sales Page Settings'><span class="settings_icon"></span>Sales Settings</a>
	</div>
	<!-- Quick buttons column end -->
	<!-- Start primary column -->
	<div class='main_column'>
		<div id="register_container" class="sales">
			<table>
				<!-- sales/return hidden controls -->
               <tr>
                       <td>
	                       <div style='display:none'>
	                               <?php echo form_open("sales/change_mode",array('id'=>'mode_form')); ?>
	                               <span><?php echo lang('sales_mode') ?></span>
	                               <?php
	                                       echo form_dropdown('mode',$modes,$mode,'onchange="$(\'#mode_form\').submit();"  id="mode"');
	                                       echo form_close();                                               ?>
	                               </div>
               			</td>
               </tr>

		    	<tr>
		    		<td id="register_items_container" class='<?=$mode?>'>
		    			<!-- Actual sales/return controls and search box -->
						<div id="table_top" class="table_top_sale">
						<?php
							$this->load->view("sales/table_top", array("mode" => $mode));
						?>
						</div>
						<!-- Register area -->
						<div class='fixed_top_table'>
							<div class='header-background'></div>

						<div id="register_holder">
							<div id="register_box">
								<?php
									$register_box_info = array("cart"					=> $cart,
															   "giftcard_error_line"	=> $giftcard_error_line,
															   "items_module_allowed"	=> $items_module_allowedd,
															   "basket"					=> $basket);
									$this->load->view("sales/register_box", $register_box_info);
								?>
							</div>
						    <div id='sale_details'>
				            	<table>
				            		<tr>
				            			<td>
							            	<div class='subtotal_box'>
					                        	<div class="left float"><?php echo lang('sales_sub_total'); ?>:</div>
						                        <div id="basket_total" class="right float"><?php echo to_currency($basket_subtotal); ?></div>
						                        <div class='clear'></div>
					                        </div>
					                        <div id="items_in_basket" class="left"><?php echo $items_in_basket; ?> Items</div>
							            </td>
						                <td>
							                <div id='taxes_holder'>
						                        <?php foreach($basket_taxes as $name=>$value) { ?>
						                        <div>
						                            <div class="right register_taxes"><?php echo to_currency($value); ?></div>
						                            <div class="left register_taxes"><?php echo $name; ?></div>
						                            <div class='clear'></div>
						                        </div>
						                        <?php }; ?>
							                </div>
							                <div id='taxable_box'>
							                	<div class='right'><?php echo form_checkbox(array('name'=>'is_taxable', 'id'=>'is_taxable', 'checked'=>$taxable_checked, 'onclick'=>"sales.toggle_taxable(this)", "$taxable_disabled"=>"$taxable_disabled")); ?></div>
							                	<div class="left"><?php echo lang('sales_taxable'); ?></div>
							                    <div class='clear'></div>
						                    </div>
							                <div id="register_total">
					                            <div id="basket_final_total" class="right"><?php echo to_currency($basket_total); ?></div>
						                        <div class="left"><?php echo lang('sales_total'); ?>:</div>
					                        </div>
						                </td>
			          				</tr>
			          			</table>
				            </div>
						</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div id="overall_sale">
			<table id="customer_info_filled">
        		<?php
					$customer_info = array("customer_id"				=> $customer_id,
										   "customer"					=> $customer,
										   "customer_quickbuttons"		=> $customer_quickbuttons,
										   "customer_account_number"	=> $customer_account_number,
										   "customer_email"				=> $customer_email,
										   "customer_phone"				=> $customer_phone,
										   "cab_name"					=> $cab_name,
										   "customer_account_balance"	=> $customer_account_balance,
										   "is_member"					=> $is_member,
										   "cmb_name"					=> $cmb_name,
										   "customer_member_balance"	=> $customer_member_balance,
										   "giftcard"					=> $giftcard,
										   "giftcard_balance"			=> $giftcard_balance);
					$this->load->view("sales/customer_info_filled", $customer_info);
				?>
			</table>
			<div id="Payment_Types" >
				<table id="sales_items">
	                <?php foreach($payments as $payment) {?>
	                    <?php if (strpos($payment['payment_type'], lang('sales_giftcard'))!== FALSE) {?>
	                <tr>
                        <td class="left"><?php echo $payment['payment_type']. ' '.lang('sales_balance') ?>:</td>
                        <td class="right"><?php echo to_currency($this->Giftcard->get_giftcard_value(end(explode(':', $payment['payment_type']))) - $payment['payment_amount']);?></td>
	                </tr>
	                    <?php }?>
	                <?php }?>
                </table>
             	<div class='pay_box'>
					<div id='payments_button'>
						<?php echo ($mode == 'sale' ? 'Pay Now' : 'Return Now')?>
					</div>
					<?php
					// Only show this part if there is at least one payment entered.
					//if(count($payments) > 0)
					{
							?>
						<div id="finish_sale">
							<?php echo form_open("sales/complete",array('id'=>'finish_sale_form')); ?>
							<?php
							$process_sale = '';
							if ($payments_cover_total && $amount_due == '0.00')
							{
								//echo '<h1>Change: '.$amount_due.'</h1>';
								echo '<script>$("body").mask("'.lang("sales_completing_sale").'");</script>';
								$process_sale = '$(document).ready(function(){	mercury.complete_sale();})';
							}
							else if ($payments_cover_total && $mode == 'sales')
							{
								echo '<script>$(document).ready(function(){mercury.complete_sale_window();})</script>';
							}
							{
							/*
							echo '<script>$(document).ready(function(){$.colorbox({href:"index.php/sales/complete_sale_window",width:650,height:170})});</script>';
							echo '<label id="comment_label" for="comment">';
							echo lang('common_comments');
							echo ':</label>';
							echo form_textarea(array('name'=>'comment', 'id' => 'comment', 'value'=>$comment,'rows'=>'1',  'accesskey' => 'o'));
							echo form_input(array('name'=>'no_receipt','id'=>'no_receipt','value'=>0,  'type' => 'hidden'));
							//echo "<div class='small_button' id='finish_sale_button' style='float:left;margin-top:5px;'><span>".lang('sales_complete_sale')."</span></div>";
							//echo "<div class='small_button' id='finish_sale_no_receipt_button' style='float:left;margin-top:5px;'><span>".lang('sales_complete_sale_without_receipt')."</span></div>";
							*/
							}
							?>
							</form>
						</div>
					<?php }	?>
						<a class='small_button new_quickbutton' id='suspend_sale_button' href='javascript:void'>
							<span id='payment_suspend_sale' class='payment_button payment_button_medium'>Suspend Sale</span>
						</a>
					<!--span id='payment_suspend_sale' class='payment_button payment_button_wide'>Suspend Sale</span-->
					<span id='open_cash_drawer' class='payment_button payment_button_medium'>Cash Drawer</span>
					<?php if ($this->session->userdata('user_level') < 2) { ?>
					<span id='override_sale' class='payment_button payment_button_medium'>Override</span>
					<?php } ?>
					<?php if ($this->config->item('track_cash')) { ?>
						<?php echo anchor(site_url('sales/closeregister?continue=home'),
							"<span id='payment_suspend_sale' class='payment_button payment_button_medium'>".lang('sales_close_register')."</span>"); ?>
					<?php } ?>

					<div class='clear'></div>
				</div>
				<?php $this->load->view('sales/payments_list');?>
				<div class='clear'></div>
			</div>
	</div><!-- END OVERALL-->
		<?php if ($this->config->item('print_after_sale') && !$this->config->item('webprnt')) { ?>
		<div style='height:1px; width:1px; overflow: hidden'>
		       <applet id='qz' name="qz" code="qz.PrintApplet.class" archive="<?php echo base_url()?>/qz-print.jar" width="100" height="100">
		           <param name="printer" value="<?=$printer_name?>">
		           <!-- <param name="sleep" value="200"> -->
		       </applet>
		</div>
		<?php } ?>
		<div class='contextMenu' id='myTransactionMenu' style='display:none'>
			<ul>
			    <li id="print_transaction_receipt">Print Receipt</li>
			    <?php for ($i = 0; $i <5; $i++) {
			   		//$rec_tran = $recent_transactions[$i];
			    ?>
			    <li id="add_tip_<?=$i?>">Add Tip -</li>
			    <?php } ?>
			    <li id="add_tip">Add Tip</li>
			</ul>
		</div>
		<div id="feedback_bar"></div>
	</div>
	<div class='right_column'>
		<a class='new_quickbutton expanded' id='column_transactions'>Recent Transactions<span class='icon minimize_icon'></span></a>
		<div id='recent_transactions'>
		<?php
		    $recent_transactions_info = array("recent_transactions" =>	$recent_transactions);
			$this->load->view("sales/recent_transactions", $recent_transactions_info);
		?>
		</div>
		<div id='suspended_sales_header' class='expanded'>Suspended Sales<span class='icon minimize_icon'></span></div>
		<div id='suspended_sales'>
		<?php
			$suspended_sales_info = array("suspended_sales" =>	$suspended_sales, "selected_suspended_sale_id" => $selected_suspended_sale_id);
			$this->load->view("sales/suspended_sales", $suspended_sales_info);
		?>
		</div>
	</div>
	<div class='clear'></div>
</div>

<div id='payments_html' style='display:none'>
	<div id='payment_html_contents'>
	<?php $this->load->view('sales/payments_list');?>
	<?php echo form_open("sales/add_payment",array('id'=>'add_payment_form')); ?>
	<ul id="error_message_box"></ul>
	<fieldset id="giftcard_payment_info">
	<!-- <legend><?php echo lang("giftcards_charge_giftcard"); ?></legend> -->
	<div id='member_account_info' class="field_row clearfix">
	<?php echo form_label('Amount tendered:', 'amount_tendered',array('class'=>'required')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'amount_tendered',
			'id'=>'amount_tendered',
			'size'=>'20',
			'autocomplete'=>'off',
			'value'=>to_currency_no_money($basket_amount_due),
			'style'=>'text-align:right;'
			)
		);?>
		</div>
	</div>
	<!--div id='member_account_info' class="field_row clearfix">
	<?php echo form_label('Amount:', 'giftcard_amount',array('class'=>'required')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'giftcard_amount',
			'id'=>'giftcard_amount',
			'size'=>'20',
			'value'=>"$amount",
			'style'=>'text-align:right;'
			)
		);?>
		</div>
	</div-->
	<div id='giftcard_data' style='display:none'>
		<div id='member_account_info_3' class="field_row clearfix">
		<?php echo form_label('Giftcard #:', 'giftcard_number',array('class'=>'required')); ?>
			<div class='form_field'>
			<?php echo form_input(array(
				'name'=>'giftcard_number',
				'id'=>'giftcard_number',
				'size'=>'20',
				'value'=>'',
				'autocomplete'=>'off',
				'maxlength'=>'16')
			);?>
			</div>
		</div>
		<div class='clear' style='text-align:center'>
		<?php
		echo form_button(array(
			'name'=>'back',
			'id'=>'back',
			'value'=>'Back',
			'content'=> 'Back',
			'class'=>'back_button float_right')
		);
		?>
		<?php
		echo form_submit(array(
			'name'=>'submit',
			'id'=>'submit',
			'value'=>lang('common_submit'),
			'class'=>'submit_button float_right')
		);
		?>
		</div>
	</div>
	<div id='punch_card_data' style='display:none'>
		<div id='member_account_info_2' class="field_row clearfix">
		<?php echo form_label('Punch Card #:', 'punch_card_number',array('class'=>'required')); ?>
			<div class='form_field'>
			<?php echo form_input(array(
				'name'=>'punch_card_number',
				'id'=>'punch_card_number',
				'size'=>'20',
				'value'=>'',
				'autocomplete'=>'off',
				'maxlength'=>'16')
			);?>
			</div>
		</div>
		<div class='clear' style='text-align:center'>
		<?php
		echo form_button(array(
			'name'=>'back',
			'id'=>'back2',
			'value'=>'Back',
			'content'=> 'Back',
			'class'=>'back_button float_right')
		);
		?>
		<?php
		echo form_submit(array(
			'name'=>'submit',
			'id'=>'submit2',
			'value'=>lang('common_submit'),
			'class'=>'submit_button float_right')
		);
		?>
		</div>
	</div>
	<div id='raincheck_data' style='display:none'>
		<div id='member_account_info_2' class="field_row clearfix">
		<?php echo form_label('Raincheck #:', 'raincheck_number',array('class'=>'required')); ?>
			<div class='form_field'>
			<?php echo form_input(array(
				'name'=>'raincheck_number',
				'id'=>'raincheck_number',
				'size'=>'20',
				'value'=>'',
				'autocomplete'=>'off',
				'maxlength'=>'16')
			);?>
			</div>
		</div>
		<div class='clear' style='text-align:center'>
		<?php
		echo form_button(array(
			'name'=>'back',
			'id'=>'back3',
			'value'=>'Back',
			'content'=> 'Back',
			'class'=>'back_button float_right')
		);
		?>
		<?php
		echo form_submit(array(
			'name'=>'submit',
			'id'=>'submit3',
			'value'=>lang('common_submit'),
			'class'=>'submit_button float_right')
		);
		?>
		</div>
	</div>
	</fieldset>

	<style>
		html[xmlns] div.hidden {
			display:none;
		}
	</style>

	<div id=''>

	</div>
	<div id="make_payment">
	<?php
		$payment_window_info = array("mode" 	 => $mode,
	                           		 "cab_name"  => $cab_name,
	                           		 "cmb_name"  => $cmb_name);
        $this->load->view("sales/payment_window", $payment_window_info);
    ?>
	</div>
</div>
</div>
<script type='text/javascript'>

	//validation and submit handling
	$(document).ready(function()
	{
		$('#qb_tab_1, #qb_tab_2, #qb_tab_3').click(function(){
			quickbuttons.close_menu(function(){});
			var me = $(this);
			$('.quickbutton_tab').removeClass('selected');
			me.addClass('selected');
			var tab = me.attr('id').replace('qb_tab_', '');
			$('.quickbutton_group').hide();
			$('#sortable_'+tab).show();
			$.ajax({
               type: "POST",
               url: "index.php/sales/set_quickbutton_tab/"+tab,
               data: {},
               success: function(response){

			   },
			   dataType:'json'
            });
		});
//		$("#amount_tendered").val($('#giftcard_amount').val());
		var at = $('#amount_tendered');
			at.live('keypress',function(event){
				// Submit on enter
				console.log('at.k '+event.keyCode);
		        if ( event.keyCode == 13) {
		        	$('#payment_cash').click();
		        }
			});
		var submitting = false;
	    $('#add_payment_form2').validate({
			submitHandler:function(form)
			{
				giftcard_swipe = false;
				if (submitting) return;
				submitting = true;
				$(form).mask("<?php echo lang('common_wait'); ?>");
				submitting_payment = true;
				//$("input:[name=payment_type]").val('Gift Card');
				//$("input:[name=payment_gc_number]").val($('#giftcard_number').val());
				$("#amount_tendered").val($('#giftcard_amount').val());

				//$("#amount_tendered").focus();
				//mercury.add_payment();
			},
			errorLabelContainer: "#error_message_box",
	 		wrapper: "li",
			rules:
			{
				amount_tendered: "required",
				giftcard_number: function(element) {
		        	return $("input:[name=payment_type]").val() == 'Gift Card';
		      	}
	   		},
			messages:
			{
	     		amount_tendered: "<?php echo lang('giftcards_value_required'); ?>",
	     		giftcard_number: "<?php echo lang('giftcards_number_required'); ?>"
			}
		});

		<?php
			if ($selected_suspended_sale_id != '') {
				echo "sales.setup_suspend_button('".$selected_suspended_sale_id."');";
			} else {
				echo "sales.setup_suspend_button('', '". lang('sales_suspend_sale') ."');";
			}
		?>
	});
	</script>

	<script>
	var giftcard_swipe = false;
		$(document).ready(function(){
			var gcn = $('#giftcard_number');
			gcn.keydown(function(event){
				// Allow: backspace, delete, tab, escape, and enter
		        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
		             // Allow: Ctrl+A
		            (event.keyCode == 65 && event.ctrlKey === true) ||
		             // Allow: home, end, left, right
		            (event.keyCode >= 35 && event.keyCode <= 39)) {
		                 // let it happen, don't do anything
		                 return;
		        }
		        else {
		            // Ensure that it is a number and stop the keypress
		            //if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
		            	console.log('kc '+event.keyCode);
		            var kc = event.keyCode;
		            if (giftcard_swipe && !((kc >= 48 && kc <=57) || (kc >= 96 && kc <= 105) || kc == 13)) //Allow numbers only and enter
		            {
		            	console.log('numbers only');
	            		event.preventDefault();
	            	}
	            	else if (kc == 186 /*semi-colon*/ || kc == 187 /*equal sign*/|| kc == 191 /*forward slash*/|| (event.shiftKey && kc == 53) /*percentage sign*/)
		            {
		            	console.log('blocking special characters');
		            	giftcard_swipe = true;
		                event.preventDefault();
		            }
		        }
			});
			//gcn.focus();
		})
	</script>

	<!-- ************* THIS SCRIPT MOVED TO THE PAYMENT_WINDOW VIEW *************-->
	<!-- <script>
		$(document).ready(function(){
			$("#payment_credit_card").live('click',function(e){
				submitting_payment = true;
				$("input:[name=payment_type]").val('Credit Card');

				if ($('#mode').val() === 'sale' && mercury.is_active() && !e.shiftKey)
					mercury.payment_window();
				else
					mercury.add_payment();

			});
			$('#payment_cash').live('click',function(){
				submitting_payment = true;
				$("input:[name=payment_type]").val('Cash');
				mercury.add_payment();
				<?php if ($this->config->item('cash_drawer_on_cash')) { ?>
				open_cash_drawer();
				<?php } ?>
			});
			$('#payment_check').live('click',function(){
				submitting_payment = true;
				$("input:[name=payment_type]").val('Check');
				mercury.add_payment();
			});
			$('#payment_gift_card').live('click',function(){
				submitting_payment = true;
				$("input:[name=payment_type]").val('Gift Card');
				$('#giftcard_data').show();
				$('#back').show();
				$('#make_payment').hide();
				$.colorbox.resize();
				//$("#amount_tendered").val('');
				$("#giftcard_number").focus();
				//mercury.giftcard_window();
		//		mercury.add_payment();
			});
			$('#back').live('click',function(){
				submitting_payment = true;
				//$("input:[name=payment_type]").val('Gift Card');
				$('#giftcard_data').hide();
				$('#back').hide();
				$('#make_payment').show();
				$.colorbox.resize();
				//$("#amount_tendered").val('');
				$("#amount_tendered").focus();

			})
			<?php if($customer_account_balance > 0) {?>
			$('#payment_from_account').live('click',function(){
				$("input:[name=payment_type]").val("<?=$cab_name?>");
				mercury.add_payment();
			});
			<?php } ?>
			<?php if($is_member) {?>
			$('#payment_from_member_account').live('click',function(){
				$("input:[name=payment_type]").val("<?=$cmb_name?>");
				mercury.add_payment();
			});
			<?php } ?>
			$('#payment_card_on_file').live('click',function(){
				$("input:[name=payment_type]").val('Card on file');
				mercury.add_payment();
			});

			$("#add_payment_button").click(function()
			{
				if ($('#payment_types').val() === 'Credit Card' && $('#mode').val() === 'sale' && mercury.is_active())
					mercury.payment_window();
				else
					mercury.add_payment();
		    });
		})
	</script> -->

<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript" language="javascript">
<?php
if(isset($error))
{
	echo "set_feedback('$error','error_message',false);";
}

if (isset($warning))
{
	echo "set_feedback('$warning','warning_message',false);";
}

if (isset($success))
{
	echo "set_feedback('$success','success_message',false);";
}
?>
var submitting = false;
var mercury = {
	is_active:function(){
		return <?php echo (($this->config->item('mercury_id')!='' && $this->config->item('mercury_password')!='') || $this->config->item('ets_key')!='')?1:0; ?>;
	},
	payment_window:function(declined){
		declined = declined==undefined?false:declined;
		//$.colorbox.close();
		var amount_tendered = Number($('#amount_tendered').val());
		var due_amount = Number($('#due_amount').html().replace(/[^0-9\.]+/g,""));
		var total_tax = 0;
		if ($('.register_taxes.right')[0]) {
			total_tax = Number($($('.register_taxes.right')[0]).html().replace(/[^0-9\.]+/g,""));
			if ($('.register_taxes.right')[1])
				total_tax += Number($($('.register_taxes.right')[1]).html().replace(/[^0-9\.]+/g,""));
		}
		var total_in_cart = Number($('#basket_total').html().replace(/[^0-9\.]+/g,""));

		if (amount_tendered>due_amount)
			alert('Error: Charging more than due.');
		else if (amount_tendered <= 0)
			alert('Error: Amount must be greater than 0.');
		else {
			if (amount_tendered<total_in_cart)
				total_tax = (total_tax*(amount_tendered/total_in_cart)).toFixed(2);
			$.colorbox({href:'index.php/sales/open_payment_window/POS/Sale/OneTime/'+amount_tendered.toFixed(2)+'/'+(Number(total_tax)).toFixed(2)+'/'+declined,'width':702,'height':600});
		}
	},
	payments_window:function(){
		var submitting = false;
		$.colorbox({
			inline:true,
			href:'#payment_html_contents',
			title:'Add Payment',
			onComplete:function(){
				$('#amount_tendered').focus();
				$('#amount_tendered').select();
				$('#add_payment_form').validate({
				    submitHandler:function(form)
				    {
				        giftcard_swipe = false;
						if (submitting) return;
				        submitting = true;
				        $('#cboxLoadedContent').mask("<?php echo lang('common_wait'); ?>");
				        $(form).ajaxSubmit({
					        success:function(response)
					        {
					        	sales.update_page_sections(response);
					        	$('#giftcard_number').val('');
					        	$('#giftcard_data').hide();
								$('#punch_card_number').val('');
					        	$('#punch_card_data').hide();
					        	$('#raincheck_number').val('');
					        	$('#raincheck_data').hide();
					        	$('#member_account_info').show();
								$('#make_payment').show();
						        $.colorbox.resize();
					            //post_item_form_submit(response);
					            submitting = false;
//					            $('.payments_and_tender').replaceWith(response.payments);
								console.log(response.amount_due);
	//							$('#amount_tendered').val(response.amount_due).select();
	//				            if (response.amount_due)
	console.dir(response);
								if (response.payments_cover_total && response.amount_due == 0)
								{
									mercury.complete_sale();
								}
								else if (response.payments_cover_total)
								{
									mercury.complete_sale_window();
								}
								else
								{
		                      	    $('#cboxLoadedContent').unmask();
		                      	    $('#giftcard_data').hide();
									$('#punch_card_data').hide();
						        	$('#raincheck_data').hide();
						        	$('#member_account_info').show();
									$('#make_payment').show();
						        	$.colorbox.resize();
						        }
					        },
				            dataType:'json'
				        });
				    },
			       errorLabelContainer: "#error_message_box",
			       wrapper: "li"
			    });
			},
			width:500
		});
	},
	giftcard_window:function(){
		$.colorbox({href:'index.php/sales/open_giftcard_window/'+$('#amount_tendered').val(),'width':400,'title':'Charge Gift Card', onComplete:function(){$('#giftcard_number').focus()}});
	},
	close_window:function(){
		$.colorbox.close();
	},
	add_payment:function() {
		console.log('trying add_payment');
	    $('#add_payment_form').submit();



	    console.log('finished add_payment');
	    //$('#submit').click();
	},
	delete_payment:function(payment_type, payment_amount) {
		var answer = true;
		if (payment_type == 'Cash' ||
			payment_type == 'Check' ||
			payment_type == 'Gift Card' ||
			payment_type == 'Punch Card' ||
			payment_type == 'Debit Card' ||
			payment_type == 'Credit Card')

			//window.location = 'index.php/sales/delete_payment/'+payment_type;
			answer = true;
		else {
			var answer = confirm('Refund '+payment_amount+' to '+payment_type);
			//if (answer)
			//	window.location = 'index.php/sales/delete_payment/'+payment_type;
		}

		if (answer)
		{
			$('#cboxLoadedContent').mask("<?php echo lang('common_wait'); ?>");
			$.ajax({
               type: "POST",
               url: "index.php/sales/delete_payment/",
               data: {
               		payment_id:payment_type
               },
               success: function(response){
               		$('.payments_and_tender').replaceWith(response.payments);
					$('#amount_tendered').val(response.amount_due).select();
               		$('#cboxLoadedContent').unmask();
               		$.colorbox.resize();
			   },
			   dataType:'json'
            });
        }
	},
	complete_sale_window:function(){
		$.colorbox({href:"index.php/sales/complete_sale_window",width:650,height:170});
	},
	complete_sale:function(){
		//submitting_sale = true;
		submitting = false;
    	$("#finish_sale_form").submit();
	},
	initialize_complete_sale: function(){
		$('#finish_sale_form').validate({
           submitHandler:function(form)
           {
               if (submitting) return;
               submitting = true;
               $(form).mask("<?php echo lang('common_wait'); ?>");
               $(form).ajaxSubmit({
               success:function(response)
               {
               		if (response.error)
               		{
               			set_feedback(response.text,response.type,response.persist);
               		}
               		else
               		{
                   		sales.update_page_sections(response);
                        //post_item_form_submit(response);
		                submitting = false;
		                //Old Print method... to be phased out... has issues when printing
		                <?php if ($this->config->item('print_sales_receipt')) { ?>
		                <?php if ($this->config->item('webprnt')) { 
		                	$webprnt_ip = $this->config->item('webprnt_ip');	
		                ?>
		                //Print receipt
		                if ($('#no_receipt').val() != 1 && (response.no_auto_receipt == undefined || !response.no_auto_receipt))
		                {
			                var receipt_data = '';
			                // Items
							var data = response.receipt_data;

							has_cc_payment = 0;
			                //var receipt_data = build_webprnt_receipt(data);
			                // console.log('print_two_other <?=$this->config->item('print_two_receipts_other')?>');
 			                // console.log('print_two <?=$this->config->item('print_two_receipts')?>');
 			                var print_twice = false;
 			                if (('<?=$this->config->item('print_two_receipts_other')?>' == '1' && !has_cc_payment) || (has_cc_payment && '<?=$this->config->item('print_two_receipts')?>' == '1')) {
 				                //print_webprnt_receipt(receipt_data, false, data.sale_id, response.receipt_data, true);
				                print_twice = true;
			                }
				            print_webprnt(data, print_twice);
			                //Reload page or go to teesheet
		                }
		                <?php } else if (!$this->config->item('updated_printing') && $this->config->item('print_sales_receipt')) { ?>
		                //Print receipt
		                if ($('#no_receipt').val() != 1 && (response.no_auto_receipt == undefined || !response.no_auto_receipt))
		                {
			                var receipt_data = '';
			                // Items
							var data = response.receipt_data;

			                has_cc_payment = 0;
			                var receipt_data = build_receipt(data);
			                console.log('print_two_other <?=$this->config->item('print_two_receipts_other')?>');
 			                console.log('print_two <?=$this->config->item('print_two_receipts')?>');
 			                if (('<?=$this->config->item('print_two_receipts_other')?>' == '1' && !has_cc_payment) || (has_cc_payment && '<?=$this->config->item('print_two_receipts')?>' == '1')) {
                            for(var i = 1; i <= 2; i++)
       				                print_receipt(receipt_data, false, data.sale_id, response.receipt_data);
			                }
			                else
				                print_receipt(receipt_data, false, data.sale_id, response.receipt_data);
			                //Reload page or go to teesheet
		                }
		                <?php } else { ?>
		                //Print receipt
		                if ($('#no_receipt').val() != 1 && (response.no_auto_receipt == undefined || !response.no_auto_receipt))
		                {
			                var receipt_html = '<div style="font-size:6px;">';//"<table style='font-size:6px; width:150px; max-width:150px;'><tr>";
			                //receipt_html += '<div>----------------------------------------------------------------------</div>';
			                // Items
							var data = response.receipt_data;
			                var cart = data.cart;

			                //Itemized
			                for (var line in cart)
			                {
			                	receipt_html += "<table style='width:150px;'><tr><td style='text-align:left;'>"+add_nbsp(cart[line].name)+'</td>';
			                	receipt_html += "<td style='text-align:left; width:50px;'>"+add_nbsp(cart[line].quantity+' @ '+parseFloat(cart[line].price).toFixed(2))+'</td>';
			                	receipt_html += "<td style='text-align:right; width:20px;'>"+add_nbsp('$'+(cart[line].price*cart[line].quantity).toFixed(2))+"</td></tr></table>";
//			                    receipt_data += add_white_space(cart[line].name+' ',cart[line].quantity+' @ '+parseFloat(cart[line].price).toFixed(2)+'    $'+(cart[line].price*cart[line].quantity).toFixed(2))+'\n';
			                	if (parseInt(cart[line].discount) > 0)
			                	{
				                	receipt_html += "<table style='width:150px;'><tr><td style='text-align:left; padding-left:20px;'>"+parseFloat(cart[line].discount).toFixed(2)+'% discount</td>';
			                		receipt_html += "<td style='text-align:right;'>-$"+(cart[line].discount/100*cart[line].price*cart[line].quantity).toFixed(2)+'</td></tr></table>';
			                	}

			                	receipt_html += "</tr><tr>";
			                }
			                receipt_html += "<br/>";
			                receipt_html += '<div>______________________________________________</div>';
			                // Totals
			                receipt_html += "<table style='width:150px;'><tr><td style='width:100px; text-align:right'>Subtotal: </td>";
			                receipt_html += "<td style='width:50px; text-align:right'>$"+parseFloat(data.subtotal).toFixed(2)+'</td></tr></table>';

			                var taxes = data.taxes;
			                for (var tax in taxes)
			                {
			                	receipt_html += "<table style='width:150px;'><tr><td style='width:100px; text-align:right'>"+add_nbsp(tax)+':</td>';
			                	receipt_html += "<td style='width:50px; text-align:right'>$"+parseFloat(taxes[tax]).toFixed(2)+'</td></tr></table>';
			                }
		                	receipt_html += "<table style='width:150px;'><tr><td style='width:100px; text-align:right'>Total:</td>";
		                	receipt_html += "<td style='width:50px; text-align:right'>$"+parseFloat(data.total).toFixed(2)+'</td></tr></table>';

			                receipt_html += "<br/>";
							// Payment Types
		                	receipt_html += "<table style='width:150px;'><tr><td style='text-align:left'>Payments:</td>";
		                	receipt_html += "<td style='text-align:right'></td></tr></table>";

			                var payments = data.payments;
			                var has_cc_payment = 0;
			                for (var payment in payments)
 			                {
 			                	has_cc_payment += (payment+'').indexOf('xxxx') === -1 ? 0 : 1;
                                receipt_html += "<table style='width:150px;'><tr><td style='text-align:left'>"+payment+':</td>';
			                	receipt_html += "<td style='text-align:right'>$"+parseFloat(payments[payment].payment_amount).toFixed(2)+'</td></tr></table>';
			                }

			                   // Change due
		                	//receipt_html += "<td style='text-align:left'>Change Due:</td>";
		                	//receipt_html += "<td style='text-align:right'>"+data.amount_change+'</td>';
		                	//receipt_html += "</tr><tr>";

			                   // Sale id and barcode
			                receipt_html += "<br/>";
		                	receipt_html += "<table style='width:150px;'><tr><td style='text-align:center'>"+add_nbsp("Sale ID: "+data.sale_id)+'</td></tr></table>';
		                	//receipt_html += "<div style='text-align:center'><img style='margin:auto;' src='<?php echo site_url('barcode'); ?>?barcode="+encodeURIComponent(data.sale_id)+"&text=' /></div><br/><br/><hr style='width:130px;'/>";
		                	//receipt_html += "<div style='font-size:3px;'><?php echo site_url('barcode'); ?>?barcode="+encodeURIComponent(data.sale_id)+"&text="+encodeURIComponent(data.sale_id)+"</div>";
		                	var ob = (data.has_online_booking)?'true':'false';
		                	if (data.has_online_booking && data.website != '')
							{
								receipt_html += "<br/><div style='text-align:center;'>And&nbsp;remember,&nbsp;you&nbsp;can&nbsp;book&nbsp;your<br/>next&nbsp;tee&nbsp;time&nbsp;online&nbsp;at<br/>"+data.website+"</div>";
							}
							receipt_html += "</div>";
			                var customer = (data.customer)?data.customer:'';
			                if (('<?=$this->config->item('print_two_receipts_other')?>' == '1' && !has_cc_payment) || (has_cc_payment && '<?=$this->config->item('print_two_receipts')?>' == '1')) {
                            for(var i = 1; i <= 2; i++)
       				                print_postscript_receipt(receipt_html, customer);
			                }
			                else
				                print_postscript_receipt(receipt_html, customer);
			                //Reload page or go to teesheet
		                }
		                <?php } }?>
    					if (response.location == 'sales') {
		                	sales.update_page_sections(response);
						   	$.colorbox.close();
		                } else {
		                	window.location = '<?php echo site_url(''); ?>/'+response.location;
		                }
	               }
	               $("body").unmask();
               },
                   dataType:'json'
           });

           },
           errorLabelContainer: "#error_message_box",
           wrapper: "li"
       });
	},
	encrypted_data:'',
    encrypted_key:'', //if it is one at a time
    purchase:'1.00'

};

var submitting_sale = false;
var submitting_payment = false;
var has_cc_payment = 0;
$(document).ready(function()
{
	// STARTING PRINTING PROCESSES
	setInterval(function(){webprnt.print_all("http://<?=$webprnt_ip?>/StarWebPrint/SendMessage")}, 3000);

	$('#payments_button').click(function(){mercury.payments_window()});
	//Prevent people from leaving the page without completing sale
	$('#menubar_navigation td a').click(function(e){
		if (($('.reg_item_top').length > 0 || $('#payment_contents').length > 0) && !confirm('You have not completed this sale. Are you sure you want to leave this page?'))
			e.preventDefault();
	});

	//Print receipt if in queue
	$('.colbox').colorbox({
		width:550,
		href:function(){
			return $(this).attr('href')+'/'+$('#amount_tendered').val();
		}
	});
	//$('input:checkbox:not([safari])').checkbox();
	$('ul.sf-menu').superfish();
	$('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
	$('#new_customer_button').colorbox({'maxHeight':700, 'width':550});
	$('.edit_customer').colorbox({'maxHeight':700, 'width':550});
	$('#quickbutton_menu').qtip({
	   content: {
	      text: $('#quickbutton_section'),
	   },
	   show: {
	      event: 'click'
	   },
	   hide: {
	      event: 'click'
	   },
	   position: {
	      my: 'top center',  // Position my top left...
	      at: 'bottom center', // at the bottom right of...
	      target: $('#quickbutton_menu'), // my target
	      adjust: {
	          method: 'none shift'
	      }
	   }
	});
	$('#item').blur(function(){
		setTimeout(function(){console.log('Item Blurred');console.dir(document.activeElement);},500);
	});
	<?php if(!(count($payments) > 0 && $payments_cover_total)) { ?>
	//$('#item').focus();
	<?php } ?>
	//$('#item').blur(function()
    //{
    //	$(this).attr('value',"<?php echo lang('sales_start_typing_item_name'); ?>");
    //});

	//$('#item,#customer').click(function()
    //{
    //	$(this).attr('value','');
    //});

    //$('#customer').blur(function()
    //{
    //	$(this).attr('value',"<?php echo lang('sales_start_typing_customer_name'); ?>");
    //});

	$('#comment').change(function()
	{
		$.post('<?php echo site_url("sales/set_comment");?>', {comment: $('#comment').val()});
	});

	$('#email_receipt').change(function()
	{
		$.post('<?php echo site_url("sales/set_email_receipt");?>', {email_receipt: $('#email_receipt').is(':checked') ? '1' : '0'});
	});


    $("#finish_sale_button").click(function()
    {
    	//if (confirm('<?php echo lang("sales_confirm_finish_sale"); ?>'))
    	//{
    		if (submitting_sale)
				console.log('already submitting');
			else
	    	{
				submitting_sale = true;
	    		$('#finish_sale_form').submit();
	    	}
    	//}
    });
    $('#finish_sale_button').dblclick(function(){});
    $("#finish_sale_no_receipt_button").click(function()
    {
    	//if (confirm('<?php echo lang("sales_confirm_finish_sale"); ?>'))
    	//{
        $('#no_receipt').val(1);
    		if (submitting_sale)
				console.log('already submitting');
			else
			{
				submitting_sale = true;
				$('#finish_sale_form').submit();
			}
    	//}
    });
    $('#finish_sale_no_receipt_button').dblclick(function(){});
	$('#open_cash_drawer').click(function(){
       open_cash_drawer();
    });
    $('#override_sale').click(function(){
    	sales.override();
    });
    <?php //if ($this->config->item('print_after_sale')) { ?>
    var submitting = false;
    mercury.initialize_complete_sale();
<?php //} ?>
	$("#suspend_sale_button").colorbox({width:548,height:340,title:'Assign a table/number',href:'index.php/sales/suspend_menu',onComplete:function(){$.colorbox.resize()}});

    $("#cancel_sale_button").click(function()
    {
    	if (confirm('<?php echo lang("sales_confirm_cancel_sale"); ?>'))
    	{
            $('#cancel_sale_form').submit();
    	}
    });
    $("#delete_button").click(function()
    {
    	if (confirm('<?php echo lang("sales_confirm_cancel_sale"); ?>'))
    	{
            //$('#delete_form').submit();
            sales.delete_checked_lines();
    	}
    });

	$("#payment_types").change(checkPaymentTypeGiftcard).ready(checkPaymentTypeGiftcard);
	var ct = $('#column_transactions');
	ct.click(function(){
    	if (ct.hasClass('expanded'))
		{
			ct.removeClass('expanded');
    		$('#recent_transactions').hide();
		}
		else
		{
    		ct.addClass('expanded');
    		$('#recent_transactions').show();
		}
	});
	var ssh = $('#suspended_sales_header');
    ssh.click(function(){
    	if (ssh.hasClass('expanded'))
		{
			ssh.removeClass('expanded');
    		$("#suspended_sales").hide();
		}
		else
		{
    		ssh.addClass('expanded');
    		$("#suspended_sales").show();
		}
	});
    <?php if ($receipt_data) {
		?>
	 	console.log('attempting to print receipt');

		//*setTimeout(function(){*/print_receipt("<?=$receipt_data?>", true)/*}, 1000)*/;
 	<?php }
 	echo $process_sale;
	?>
});

function post_item_form_submit(response)
{
	if(response.success)
	{
		$("#item").attr("value", "");
		sales.add_item(response.item_id);
	}
}

function post_person_form_submit(response)
{
	if(response.success)
	{
		if ($("#select_customer_form").length == 1)
		{
			$("#customer").attr("value",'');
			selectCustomer(response.person_id)
		}
		else
		{
			window.location = '<?php echo site_url('sales/index');?>';
		}
	}
}

function checkPaymentTypeGiftcard()
{
	if ($("#payment_types").val() == "<?php echo lang('sales_giftcard'); ?>")
	{
		$("#amount_tendered").val('');
		$("#amount_tendered").focus();
	}
	else
	{

	}
}
function findPrinter() {
//     var applet = document.jZebra;
  //  if (applet != null) {
        // Searches for locally installed printer with "zebra" in the name
    //}
}
function monitorFinding() {
	var applet = document.getElementById('qz');
	if (applet != null) {
	   if (!applet.isDoneFinding()) {
	      window.setTimeout('monitorFinding()', 100);
	   } else {
	      var printer = applet.getPrinterName();
              console.log(printer == null ? "Printer not found" : "Printer \"" + printer + "\" found");
	   }
	} else {
            console.log("Applet not loaded!");
        }
      }
function build_receipt(data){
	console.dir(data);
	var receipt_data = '';
	var cart = data.cart;
    //Itemized
    for (var line in cart)
    {
        receipt_data += add_white_space(cart[line].name+' ',cart[line].quantity+' @ '+parseFloat(cart[line].price).toFixed(2)+'    $'+(cart[line].price*cart[line].quantity).toFixed(2))+'\n';
    	if (parseInt(cart[line].discount) > 0)
    		receipt_data += add_white_space('     '+parseFloat(cart[line].discount).toFixed(2)+'% discount', '-$'+(cart[line].discount/100*cart[line].price*cart[line].quantity).toFixed(2))+'\n';
    }
    // Totals
    receipt_data += '\n\n'+add_white_space('Subtotal: ','$'+parseFloat(data.subtotal).toFixed(2))+'\n';
    var taxes = data.taxes;
    for (var tax in taxes)
           receipt_data += add_white_space(tax+': ','$'+parseFloat(taxes[tax]).toFixed(2))+'\n';

       receipt_data += add_white_space('Total: ','$'+parseFloat(data.total).toFixed(2))+'\n';

    // Payment Types
    receipt_data += '\nPayments:\n';
    var payments = data.payments;
    for (var payment in payments)
    {
	    has_cc_payment += (payment+'').indexOf('xxxx') === -1 ? 0 : 1;
	    receipt_data += add_white_space(payment+': ','$'+parseFloat(payments[payment].payment_amount).toFixed(2))+'\n';
    }
       // Change due
       receipt_data += '\n'+add_white_space('Change Due: ',(data.amount_change))+'\n';
    <?php if ($this->config->item('print_tip_line')) { ?>
    receipt_data += '\n\n'+add_white_space('Tip: ','$________.____');
    receipt_data += '\n\n'+add_white_space('TOTAL CHARGE: ','$________.____');
    receipt_data += '\n\n\nX_____________________________________________\n';
	<?php } ?>
    receipt_data += chr(27)+chr(97)+chr(49);
    <?php if(trim($this->config->item('return_policy')) != '') { ?>
    receipt_data += "\n\n";// Some spacing at the bottom
    receipt_data += "<?=addcslashes($this->config->item('return_policy'), implode('',array('"',"'")))?>";
    <?php } ?>
	receipt_data += "\x1Dh" +chr(80);  // ← Set height
    receipt_data += "\n\n";// Some spacing at the bottom
    var sale_num = data.sale_id.replace('POS ', '');
    var len = sale_num.length;
	if (len == 3)
        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0A\x7B\x41\x50\x4F\x53\x20"+sale_num;
	else if (len == 4)
        receipt_data += "\x1D\x88\x01\x1D\x6B\x49\x0B\x7B\x41\x50\x4F\x53\x20"+sale_num;
    else if (len == 5)
        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0C\x7B\x41\x50\x4F\x53\x20"+sale_num;
    else if (len == 6)
        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0D\x7B\x41\x50\x4F\x53\x20"+sale_num;
    else if (len == 7)
        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0E\x7B\x41\x50\x4F\x53\x20"+sale_num;
    else if (len == 8)
        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0F\x7B\x41\x50\x4F\x53\x20"+sale_num;
    else if (len == 9)
        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x10\x7B\x41\x50\x4F\x53\x20"+sale_num;
    else if (len == 10)
        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x11\x7B\x41\x50\x4F\x53\x20"+sale_num;
    // Sale id and barcode
    receipt_data += '\n\nSale ID: '+data.sale_id+'\n';

    return receipt_data;
}
function print_receipt(receipt, add_delay, sale_id, data) {
	try
	{
		add_delay = add_delay == undefined ? false : add_delay;
		var applet = document.getElementById('qz');
		if (applet != null)
		{
	        //if ($.isFunction(applet.findPrinter))
	    	{
	    		applet.findPrinter("<?=$printer_name?>");
	    		//var t = new Date();
	    		//var ct = 0;
	   			//while (!applet.isDoneFinding() && ct < t.getTime() + 4000) {
		           // Wait
		        //   console.log('not done finding yet');
		        //   c = new Date();
		        //   ct = c.getTime();
		       // }
		       if (add_delay) {
			       	var t = new Date();
		    		var ct = 0;
		   			while (!applet.isDoneFinding() && ct < t.getTime() + 4000) {
			           // Wait
			        //   console.log('not done finding yet');
			           //c = new Date();
			           //ct = c.getTime();
			        }
			        setTimeout(function(){

				        // Send characters/raw commands to applet using "append"
				        // Hint:  Carriage Return = \r, New Line = \n, Escape Double Quotes= \"
				        applet.append(chr(27)+chr(64));//Resets the printer
				        applet.append(chr(29) + chr(33) + chr(16)); //Font-size x2
				        applet.append(chr(27) + "\x61" + "\x31"); // center justify

				        applet.append("<?php echo $this->config->item('name')?>\n");
				        applet.append(chr(27)+chr(64));//Resets the printer
				        applet.append(chr(27) + "\x61" + "\x31"); // center justify
				        applet.append(chr(27) + chr(77) + chr(48)); //Font - Normal font
				        applet.append("<?php echo str_replace(array("\r", "\r\n", "\n"), ' ', addslashes($this->config->item('address')))?>\n");
				        applet.append("<?php echo $this->config->item('city').', '.$this->config->item('state').' '.$this->config->item('zip')?>\n");
				        applet.append("<?php echo $this->config->item('phone')?>\n\n");
				        applet.append(chr(27) + chr(97) + chr(48));// Left justify
				        applet.append("Employee: <?php echo $user_info->last_name.', '.$user_info->first_name; ?>\n");
				        var d = new Date();
				        var h = (d.getHours() > 12)?d.getHours()-12:d.getHours();
				        var m = (d.getMinutes() < 10)?('0'+d.getMinutes().toString()):d.getMinutes();
				        var ap = (d.getHours() < 12)?'am':'pm';
				        applet.append("Date: "+(d.getMonth()+1)+'/'+d.getDate()+'/'+d.getFullYear()+' '+h+':'+m+ap+"\n\n");

				        applet.append(receipt);
				        //applet.appendImage('/images/test/test.png', "ESCP");
				        //applet.append("\r\n");
				        //applet.appendImage('http://10.150.13.82:8888/images/test/test.png', "ESCP");
				        //applet.append("\r\n");
				        //applet.appendImage('../../images/test/test.png', "ESCP");
				        //applet.append("\r\n");
				        applet.append("\n\n\n\n\n\n");// Some spacing at the bottom
				        applet.append(chr(27) + chr(105)); //Cuts the receipt

				        // Send characters/raw commands to printer
				        //applet.forceAccept();
				        applet.print();
				        open_cash_drawer();
				    }, 1);
			    }
			    else {
			    	console.log('not timing out');
					// Send characters/raw commands to applet using "append"
			        // Hint:  Carriage Return = \r, New Line = \n, Escape Double Quotes= \"
			        applet.append(chr(27)+chr(64));//Resets the printer
			        //applet.append(chr(29) + chr(33) + chr(16)); //Font-size x2
			        applet.append(chr(27) + "\x61" + "\x31"); // center justify

			        applet.append("<?php echo $this->config->item('name')?>\n");
			        applet.append(chr(27)+chr(64));//Resets the printer
			        applet.append(chr(27) + "\x61" + "\x31"); // center justify
			        applet.append(chr(27) + chr(77) + chr(48)); //Font - Normal font
			        applet.append("<?php echo str_replace(array("\r", "\r\n", "\n"), ' ', addslashes($this->config->item('address')))?>\n");
			        applet.append("<?php echo $this->config->item('city').', '.$this->config->item('state').' '.$this->config->item('zip')?>\n");
			        applet.append("<?php echo $this->config->item('phone')?>\n\n");
			        applet.append(chr(27) + chr(97) + chr(48));// Left justify
			        applet.append("Employee: <?php echo $user_info->last_name.', '.$user_info->first_name; ?>\n");
			        <?php //if ($customer != '') { ?>
			        if (data != undefined && data.customer != undefined)
				        applet.append("Customer: <?php //echo $customer; ?>"+data.customer+"\n");
			        <?php //} ?>
			        var d = new Date();
			        var h = (d.getHours() > 12)?d.getHours()-12:d.getHours();
			        var m = (d.getMinutes() < 10)?('0'+d.getMinutes().toString()):d.getMinutes();
			        var ap = (d.getHours() < 12)?'am':'pm';
			        applet.append("Date: "+(d.getMonth()+1)+'/'+d.getDate()+'/'+d.getFullYear()+' '+h+':'+m+ap+"\n\n");
			        //applet.append("<?php echo $cab_name.': '.$customer_account_balance; ?>\n");
			        //applet.append("<?php echo $cmb_name.': '.$customer_member_balance; ?>\n");
			        <?php if ($giftcard) { ?>
			        //applet.append("Giftcard Balance: <?php echo $giftcard_balance; ?>\n");
			        <?php } ?>

			        applet.append(receipt);
			        //applet.appendImage('/images/test/test.png', "ESCP");
			        //applet.append("\r\n");
			        //applet.appendImage('http://10.150.13.82:8888/images/test/test.png', "ESCP");
			        //applet.append("\r\n");
			        //applet.appendImage('../../images/test/test.png', "ESCP");
			        //applet.append("\r\n");
			        <?php if ($this->config->item('online_booking')) { ?>
		        	applet.append("\nAnd remember, you can book your\nnext tee time online at\n<?=$this->config->item('website')?>");
		        	<?php } ?>

			        applet.append("\n\n\n\n\n\n");// Some spacing at the bottom
			        applet.append(chr(27) + chr(105)); //Cuts the receipt

			        // Send characters/raw commands to printer
			        //applet.forceAccept();
			        applet.print();
			        open_cash_drawer();
		        }
			}
		}
	}
	catch(err)
	{
		set_feedback('Unable to print receipt at this time. Please print manually','error_message',false);
	}
}
function print_webprnt(data, double_print) {
	var receipt_data = '';
	receipt_data = webprnt.build_receipt_body(receipt_data, data);
	<?php if ($this->config->item('print_tip_line')) { ?>
		receipt_data = webprnt.add_tip_line(receipt_data);
		if (webprnt.credit_card_payments)
			receipt_data = webprnt.add_signature_line(receipt_data);
	<?php } ?>
	<?php if(trim($this->config->item('return_policy')) != '') { ?>
    	receipt_data = webprnt.add_return_policy(receipt_data, "<?=addcslashes($this->config->item('return_policy'), implode('',array('"',"'")))?>");
    <?php } ?>
    receipt_data = webprnt.add_barcode(receipt_data, 'Sale ID', data.sale_id);
    var header_data = {
    	course_name:'<?php echo $this->config->item('name')?>',
    	address:'<?php echo str_replace(array("\r", "\r\n", "\n"), ' ', addslashes($this->config->item('address')))?>',
    	address_2:'<?php echo $this->config->item('city').', '.$this->config->item('state').' '.$this->config->item('zip')?>',
    	phone:'<?php echo $this->config->item('phone')?>',
    	employee_name:'<?php echo $user_info->last_name.', '.$user_info->first_name; ?>',
    	customer:''// Need to load this in from data
    };
    receipt_data = webprnt.add_receipt_header(receipt_data, header_data)
    <?php if ($this->config->item('online_booking')) { ?>
    	receipt_data = webprnt.add_booking_reminder(receipt_data, '<?=$this->config->item('website')?>');
    <?php } ?>
    receipt_data = webprnt.add_paper_cut(receipt_data);
    if (double_print != undefined && double_print == true)
    	receipt_data += receipt_data;
    receipt_data = webprnt.add_cash_drawer_open(receipt_data);	
    console.log('----------------------- receipt_data ---------------------------');
    console.dir(receipt_data);
    receipt_data = webprnt.add_credit_card_slips(receipt_data);
    webprnt.print(receipt_data, "http://<?=$webprnt_ip?>/StarWebPrint/SendMessage");
}
function build_webprnt_receipt(data){
	// Old Receipt
	console.dir(data);
	var builder = new StarWebPrintBuilder();
	var receipt_data = '';
	var cart = data.cart;
    //Itemized
    for (var line in cart)
    {
        receipt_data += builder.createTextElement({data:add_white_space(cart[line].name+' ',cart[line].quantity+' @ '+parseFloat(cart[line].price).toFixed(2)+'    $'+(cart[line].price*cart[line].quantity).toFixed(2))+'\n'});
    	if (parseInt(cart[line].discount) > 0)
    		receipt_data += builder.createTextElement({data:add_white_space('     '+parseFloat(cart[line].discount).toFixed(2)+'% discount', '-$'+(cart[line].discount/100*cart[line].price*cart[line].quantity).toFixed(2))+'\n'});
    }
    // Totals
    receipt_data += builder.createTextElement({data:'\n\n'+add_white_space('Subtotal: ','$'+parseFloat(data.subtotal).toFixed(2))+'\n'});
    var taxes = data.taxes;
    for (var tax in taxes)
           receipt_data += builder.createTextElement({data:add_white_space(tax+': ','$'+parseFloat(taxes[tax]).toFixed(2))+'\n'});

       receipt_data += builder.createTextElement({data:add_white_space('Total: ','$'+parseFloat(data.total).toFixed(2))+'\n'});

    // Payment Types
    receipt_data += builder.createTextElement({data:'\nPayments:\n'});
    var payments = data.payments;
    for (var payment in payments)
    {
	    has_cc_payment += (payment+'').indexOf('xxxx') === -1 ? 0 : 1;
		receipt_data += builder.createTextElement({data:add_white_space(payment+': ','$'+parseFloat(payments[payment].payment_amount).toFixed(2))+'\n'});
    }
       // Change due
       receipt_data += builder.createTextElement({data:'\n'+add_white_space('Change Due: ',(data.amount_change))+'\n'});
    <?php if ($this->config->item('print_tip_line')) { ?>
    receipt_data += builder.createTextElement({data:'\n\n'+add_white_space('Tip: ','$________.____')});
    receipt_data += builder.createTextElement({data:'\n\n'+add_white_space('TOTAL CHARGE: ','$________.____')});
    receipt_data += builder.createTextElement({data:'\n\n\nX_____________________________________________\n'});
	<?php } ?>
    //receipt_data += chr(27)+chr(97)+chr(49);
    receipt_data += builder.createAlignmentElement({position:'center'});
    <?php if(trim($this->config->item('return_policy')) != '') { ?>
    receipt_data += builder.createTextElement({data:"\n\n"});// Some spacing at the bottom
    receipt_data += builder.createTextElement({data:"<?=addcslashes($this->config->item('return_policy'), implode('',array('"',"'")))?>"});
    <?php } ?>
    // NEED TO FIGURE OUT WHAT THE EQUIVALENT OF THIS IS FOR STAR WEBPRNT
	//receipt_data += "\x1Dh" +chr(80);  // ← Set height
    receipt_data += builder.createTextElement({data:"\n\n"});// Some spacing at the bottom
    var sale_num = data.sale_id.replace('POS ', '');
    var len = sale_num.length;
    // BARCODE STUFF // ADD EQUIVALENT OF THIS FOR STAR WEBPRNT
	// if (len == 3)
        // receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0A\x7B\x41\x50\x4F\x53\x20"+sale_num;
	// else if (len == 4)
        // receipt_data += "\x1D\x88\x01\x1D\x6B\x49\x0B\x7B\x41\x50\x4F\x53\x20"+sale_num;
    // else if (len == 5)
        // receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0C\x7B\x41\x50\x4F\x53\x20"+sale_num;
    // else if (len == 6)
        // receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0D\x7B\x41\x50\x4F\x53\x20"+sale_num;
    // else if (len == 7)
        // receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0E\x7B\x41\x50\x4F\x53\x20"+sale_num;
    // else if (len == 8)
        // receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0F\x7B\x41\x50\x4F\x53\x20"+sale_num;
    // else if (len == 9)
        // receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x10\x7B\x41\x50\x4F\x53\x20"+sale_num;
    // else if (len == 10)
        // receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x11\x7B\x41\x50\x4F\x53\x20"+sale_num;
    // Sale id and barcode
    receipt_data += builder.createBarcodeElement({symbology:'Code128', width:'width2', height:40, hri:false, data:data.sale_id});
    receipt_data += builder.createTextElement({data:'\n\nSale ID: '+data.sale_id+'\n'});

    return receipt_data;
}
var currently_printing = false;
function print_webprnt_receipt(receipt, add_delay, sale_id, data, double_print) {
	console.log('trying to print_webprnt_receipt -----------------------');
	return;
	var start_time = new Date();
	var current_time = new Date();
	while (currently_printing && current_time.getTime() - start_time.getTime() < 10000) // WAIT UP TO 10 SECONDS
	{
		console.log('waiting to print '+current_time.getTime());
		current_time = new Date();
	}
	try
	{
		var currentlly_printing = true;
		var builder = new StarWebPrintBuilder();
		var request = '';
        //applet.append(chr(27)+chr(64));//Resets the printer
        //applet.append(chr(29) + chr(33) + chr(16)); //Font-size x2
        //applet.append(chr(27) + "\x61" + "\x31"); // center justify
		request += builder.createAlignmentElement({position:'center'});
        request += builder.createTextElement({data:"<?php echo $this->config->item('name')?>\n"});
        //applet.append(chr(27)+chr(64));//Resets the printer
        //applet.append(chr(27) + "\x61" + "\x31"); // center justify
        //applet.append(chr(27) + chr(77) + chr(48)); //Font - Normal font
        request += builder.createTextElement({data:"<?php echo str_replace(array("\r", "\r\n", "\n"), ' ', addslashes($this->config->item('address')))?>\n"});
        request += builder.createTextElement({data:"<?php echo $this->config->item('city').', '.$this->config->item('state').' '.$this->config->item('zip')?>\n"});
        request += builder.createTextElement({data:"<?php echo $this->config->item('phone')?>\n\n"});
        
        //applet.append(chr(27) + chr(97) + chr(48));// Left justify
        request += builder.createAlignmentElement({position:'left'});
        
        request += builder.createTextElement({data:"Employee: <?php echo $user_info->last_name.', '.$user_info->first_name; ?>\n"});
        <?php //if ($customer != '') { ?>
        if (data != undefined && data.customer != undefined)
	        request += builder.createTextElement({data:"Customer: <?php //echo $customer; ?>"+data.customer+"\n"});
        <?php //} ?>
        var d = new Date();
        var h = (d.getHours() > 12)?d.getHours()-12:d.getHours();
        var m = (d.getMinutes() < 10)?('0'+d.getMinutes().toString()):d.getMinutes();
        var ap = (d.getHours() < 12)?'am':'pm';
        request += builder.createTextElement({data:"Date: "+(d.getMonth()+1)+'/'+d.getDate()+'/'+d.getFullYear()+' '+h+':'+m+ap+"\n\n"});
        //applet.append("<?php echo $cab_name.': '.$customer_account_balance; ?>\n");
        //applet.append("<?php echo $cmb_name.': '.$customer_member_balance; ?>\n");
        <?php if ($giftcard) { ?>
        //applet.append("Giftcard Balance: <?php echo $giftcard_balance; ?>\n");
        <?php } ?>

        request += receipt;
        //applet.appendImage('/images/test/test.png', "ESCP");
        //applet.append("\r\n");
        //applet.appendImage('http://10.150.13.82:8888/images/test/test.png', "ESCP");
        //applet.append("\r\n");
        //applet.appendImage('../../images/test/test.png', "ESCP");
        //applet.append("\r\n");
        request += builder.createAlignmentElement({position:'center'});
        <?php if ($this->config->item('online_booking')) { ?>
    	request += builder.createTextElement({data:"\nAnd remember, you can book your\nnext tee time online at\n<?=$this->config->item('website')?>"});
    	<?php } ?>

        request += builder.createTextElement({data:"\n\n"});// Some spacing at the bottom
        //applet.append(chr(27) + chr(105)); //Cuts the receipt
		request += builder.createCutPaperElement({feed:true});
		var url = "http://<?=$webprnt_ip?>/StarWebPrint/SendMessage";
		var trader = new StarWebPrintTrader({url:url});
        // Send characters/raw commands to printer
        //applet.forceAccept();
        //applet.print();
        //open_cash_drawer();
        	
        trader.onReceive = function (response) {
        	currently_printing = false;
	        var msg = '- onReceive -\n\n';
	
	        msg += 'TraderSuccess : [ ' + response.traderSuccess + ' ]\n';
	
	//      msg += 'TraderCode : [ ' + response.traderCode + ' ]\n';
	
	        msg += 'TraderStatus : [ ' + response.traderStatus + ',\n';
	
	        if (trader.isCoverOpen            ({traderStatus:response.traderStatus})) {msg += '\tCoverOpen,\n';}
	        if (trader.isOffLine              ({traderStatus:response.traderStatus})) {msg += '\tOffLine,\n';}
	        if (trader.isCompulsionSwitchClose({traderStatus:response.traderStatus})) {msg += '\tCompulsionSwitchClose,\n';}
	        if (trader.isEtbCommandExecute    ({traderStatus:response.traderStatus})) {msg += '\tEtbCommandExecute,\n';}
	        if (trader.isHighTemperatureStop  ({traderStatus:response.traderStatus})) {msg += '\tHighTemperatureStop,\n';}
	        if (trader.isNonRecoverableError  ({traderStatus:response.traderStatus})) {msg += '\tNonRecoverableError,\n';}
	        if (trader.isAutoCutterError      ({traderStatus:response.traderStatus})) {msg += '\tAutoCutterError,\n';}
	        if (trader.isBlackMarkError       ({traderStatus:response.traderStatus})) {msg += '\tBlackMarkError,\n';}
	        if (trader.isPaperEnd             ({traderStatus:response.traderStatus})) {msg += '\tPaperEnd,\n';}
	        if (trader.isPaperNearEnd         ({traderStatus:response.traderStatus})) {msg += '\tPaperNearEnd,\n';}
	
	        msg += '\tEtbCounter = ' + trader.extractionEtbCounter({traderStatus:response.traderStatus}).toString() + ' ]\n';
	
	//      msg += 'Status : [ ' + response.status + ' ]\n';
	//
	//      msg += 'ResponseText : [ ' + response.responseText + ' ]\n';
	
	        //alert(msg);
	    }
	
	    trader.onError = function (response) {
	    	currently_printing = false;
	        var msg = '- onError -\n\n';
	
	        msg += '\tStatus:' + response.status + '\n';
	
	        msg += '\tResponseText:' + response.responseText;
	
	        //alert(msg);
	    }
	
	    if (double_print != undefined && double_print == true)
			request = request+request;
		//request += builder.createRawDataElement({data:chr(27) + "\x70" + "\x30" + chr(25) + chr(25) + "\r"});
		trader.sendMessage({request:request});
		//request = builder.createRawDataElement({data:chr(27) + chr(112) + chr(0) + chr(64) + chr(240)});
		//trader.sendMessage({request:request});
			
	}
	catch(err)
	{
		set_feedback('Unable to WebPRNT receipt at this time. Please print manually','error_message',false);
	}
}
function print_postscript_receipt(receipt, customer) {
    var applet = document.getElementById('qz');
    if (applet != null) {
    	if ($.isFunction(applet.findPrinter))
    	{
	        applet.findPrinter("<?=$printer_name?>");
	   		while (!applet.isDoneFinding()) {
	           // Wait
	           console.log('not done finding yet');
	        }
	        // Send characters/raw commands to applet using "append"
	        // Hint:  Carriage Return = \r, New Line = \n, Escape Double Quotes= \"
	        applet.appendHTML("<html>");
	        applet.appendHTML("<div style='text-align:center; font-size:6px;'>"+add_nbsp('<?php echo $this->config->item('name')?>')+"</div>");
	        applet.appendHTML("<div style='text-align:center; font-size:6px;'>"+add_nbsp('<?php echo str_replace(array("\r", "\r\n", "\n"), ' ', addslashes($this->config->item('address')))?>')+"</div>");
	        applet.appendHTML("<div style='text-align:center; font-size:6px;'>"+add_nbsp('<?php echo $this->config->item('city').', '.$this->config->item('state').' '.$this->config->item('zip')?>')+"</div>");
	        applet.appendHTML("<div style='text-align:center; font-size:6px;'>"+add_nbsp('<?php echo $this->config->item('phone')?>')+"</div><br/>");
	        applet.appendHTML("<div style='font-size:6px;'>"+add_nbsp('Employee: <?php echo $user_info->last_name.', '.$user_info->first_name; ?>')+"</div>");
	        if (customer != undefined && customer != '')
	        	applet.appendHTML("<div style='font-size:6px;'>"+add_nbsp('Customer: '+customer)+"</div>");
	        var d = new Date();
	        var h = (d.getHours() > 12)?d.getHours()-12:d.getHours();
	        var m = (d.getMinutes() < 10)?('0'+d.getMinutes().toString()):d.getMinutes();
	        var ap = (d.getHours() < 12)?'am':'pm';
	        applet.appendHTML("<div style='font-size:6px;'>"+add_nbsp('Date: '+(d.getMonth()+1)+'/'+d.getDate()+'/'+d.getFullYear()+' '+h+':'+m+ap)+"</div><br/>");

	        applet.appendHTML(receipt);
	        applet.appendHTML("<br/>");
	        applet.appendHTML('<div style="font-size:6px;">______________________________________________</div>');
	        //applet.appendImage('/images/test/test.png', "ESCP");
	        //applet.append("\r\n");
	        //applet.appendImage('http://10.150.13.82:8888/images/test/test.png', "ESCP");
	        //applet.append("\r\n");
	        //applet.appendImage('../../images/test/test.png', "ESCP");
	        //applet.append("\r\n");

	        // Send characters/raw commands to printer
	        //applet.forceAccept();
	        console.log('Printing HTML');

			<?php if ($this->config->item('online_booking')) { ?>
	        applet.appendHTML("<br/><div style='text-align:center;'>And&nbsp;remember,&nbsp;you&nbsp;can&nbsp;book&nbsp;your<br/>next&nbsp;tee&nbsp;time&nbsp;online&nbsp;at<br/><?=$this->config->item('website')?></div>");
	        <?php } ?>
	        applet.printHTML();
	        console.log('Done Printing HTML');
        }
    }
}
function hexdec (hex_string) {
    // Returns the decimal equivalent of the hexadecimal number
    //
    // version: 1109.2015
    // discuss at: http://phpjs.org/functions/hexdec
    // +   original by: Philippe Baumann
    // *     example 1: hexdec('that');
    // *     returns 1: 10
    // *     example 2: hexdec('a0');
    // *     returns 2: 160
    hex_string = (hex_string + '').replace(/[^a-f0-9]/gi, '');
    return parseInt(hex_string, 16);
}
function update_recent_transaction(transaction_id)
{
    $('#transaction_'+transaction_id).remove();
}
function delete_recent_transaction(transaction_id)
{
    $('#transaction_'+transaction_id).remove();
}
function add_nbsp(string) {
	return string.replace(' ', '&nbsp;');
}
function chr(i) {
      return String.fromCharCode(i);
}
function open_cash_drawer() {
	if ('<?=$this->config->item('webprnt')?>' == '1')
	{
		var receipt_data = '';
		receipt_data = webprnt.add_cash_drawer_open(receipt_data);
		webprnt.print(receipt_data,  "http://<?=$webprnt_ip?>/StarWebPrint/SendMessage");
	}
	else
	{
	    var applet = document.getElementById('qz');
	    if (applet != null) {
	    	applet.findPrinter("<?=$printer_name?>");
	        applet.append(chr(27) + "\x70" + "\x30" + chr(25) + chr(25) + "\r");
	        //applet.forceAccept();
	        applet.print();
	    }
	}
}
function boldAndCenter() {
    var applet = document.getElementById('qz');
    if (applet != null) {
        applet.append(chr(27) + chr(69) + "\r");  // bold on
        applet.append(chr(27) + "\x61" + "\x31"); // center justify
    }
}
function add_white_space(str_one, str_two)
{
       var width = 42;
       var strlen_one = str_one.length;
       var strlen_two = str_two.length;
       var white_space = '';
       var white_space_length = 0;
       if (strlen_one + strlen_two >= width)
               return (str_one.substr(0, width - strlen_two - 4)+'... '+str_two); //truncated if text is longer than available space
       else
               white_space_length = width - (strlen_one + strlen_two);

       for (var i = 0; i < white_space_length; i++)
               white_space += ' ';
       return str_one+white_space+str_two;
}
</script>