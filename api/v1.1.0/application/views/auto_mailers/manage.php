<?php $this->load->view("partial/header"); ?>
<style type="text/css">
  .on-queue, .sent{width:25px;height:25px;}
  .on-queue{background: url("<?= base_url('images/yellow-circle.png'); ?>") no-repeat;}
  .sent{color:#396905}
  #marketing-status{height:78px; }
  #marketing-status h4{text-align:center;font-size:18px;}
  #email-status, #text-status{height:78px;width: 168px;}
  #marketing_message {
  	font-size:24px;
  	text-align:center;
  	padding-bottom:15px;
  }
  .info{
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    border: 1px solid #1d599a;
    height: 75px;
    margin-top: 5px;
    width: 168px;
    background: url("<?= base_url('images/status-bg.gif'); ?>") repeat-x;
    color: #fff;
    text-align: center;
    font-weight:bold;
  }

  .info table td.header{font-size: 12px;line-height: 25px;}
  .info table td.body{font-size: 20px;width: 50%;}
  .info table td.footer{font-size: 10px;}
</style>
<script type="text/javascript">
$(document).ready(function()
{
	$('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
	$('.colbox').colorbox({
    'maxHeight':700,
    //initialWidth:1200, 
    width:850,
    escKey: false,
    overlayClose: false,
    onComplete:function(){
   		$.colorbox.resize({width:850, maxHeight:700});
    }
  });

    init_table_sorting();
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo lang("common_confirm_search")?>');
    enable_delete('<?php echo lang($controller_name."_confirm_delete")?>','<?php echo lang($controller_name."_none_selected")?>');
	
	$('#generate_barcodes').click(function()
    {
    	var selected = get_selected_values();
    	if (selected.length == 0)
    	{
    		alert('<?php echo lang('items_must_select_item_for_barcode'); ?>');
    		return false;
    	}

    	$(this).attr('href','<?php echo site_url("giftcards/generate_barcodes");?>/'+selected.join('~'));
    });

	$('#generate_barcode_labels').click(function()
    {
    	var selected = get_selected_values();
    	if (selected.length == 0)
    	{
    		alert('<?php echo lang('items_must_select_item_for_barcode'); ?>');
    		return false;
    	}

    	$(this).attr('href','<?php echo site_url("giftcards/generate_barcode_labels");?>/'+selected.join('~'));
    });
});

function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(
		{
			sortList: [[3,1]],
			headers:
			{
				0: { sorter: false},
				6: { sorter: false}
			}
		});
	}
}

function post_campaign_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
		if(jQuery.inArray(response.campaign_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.campaign_id,'<?php echo site_url("$controller_name/get_row")?>');
		}
	}
	else
	{
    //This is an update, just update one row
		if(jQuery.inArray(response.campaign_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.campaign_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);

		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				highlight_row(response.campaign_id);
				set_feedback(response.message,'success_message',false);
			});
		}

    if(response.send_now == 1)
    {
      set_feedback('Sending...','success_message',true);

      $.ajax({
        url: "<?= site_url('marketing_campaigns/send_now/'); ?>",
        type: "POST",
        data: {
            campaign_id: response.campaign_id
        },
        success: function( data ) {
          $('#email_sent').html(data.email_sent);
          $('#email_remaining').html(data.email_remaining);
          $('#text_sent').html(data.text_sent);
          $('#text_remaining').html(data.text_remaining);
          set_feedback('Campaign sent successfully.','success_message',true);
          update_row(response.campaign_id,'<?php echo site_url("$controller_name/get_row")?>');
        },
        dataType:'json'
      });
    }
	}
}

</script>

<!--table id="title_bar">
	<tr>
		<td colspan='3'>
			<div id='marketing_message'>
	    		<?= $marketing_message ?>
	    	</div>
		</td>
	</tr>
  <tr>
		<td id="title_icon">
			<img src='<?php echo base_url()?>images/menubar/<?php echo $controller_name; ?>.png' alt='title icon' />
		</td>
		<td id="title">
			<?php echo lang('module_'.$controller_name); ?>
		</td>
		<td id="title_search">
			<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
				<input type="text" name='search' id='search'/>
				<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
			</form>
		</td>
	</tr>
</table-->

<table id="contents">
	<tr>
		<td id="commands">
			<div id="new_button">
				<?php echo 
					anchor("$controller_name/view/-1/width~$form_width",
					lang($controller_name.'_new'),
					array('class'=>'colbox none new', 
						'title'=>lang($controller_name.'_new')));
				?>
				
				<?php
        /* echo
					anchor("$controller_name/generate_barcode_labels",
					lang("common_barcode_labels"),
					array('id'=>'generate_barcode_labels', 
						'class' => 'generate_barcodes_inactive',
						'target' =>'_blank',
						'title'=>lang('common_barcode_labels'))); 
				?>
				
				<?php echo 
					anchor("$controller_name/generate_barcodes",
					lang("common_barcode_sheet"),
					array('id'=>'generate_barcodes', 
						'class' => 'generate_barcodes_inactive',
						'target' =>'_blank',
						'title'=>lang('common_barcode_sheet'))); */
				?>
					
				<?php echo 
					anchor("$controller_name/delete",
					lang("common_delete"),
					array('id'=>'delete', 
						'class'=>'delete_inactive')); 
				?>
                <?php /*echo anchor("$controller_name/excel_export",
					lang('common_excel_export'),
					array('class'=>'none import'));*/
				?>

			
			</div>
		</td>
		<td style="width:10px;"></td>
		<td id="item_table">
			<div id='table_top'>
				<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
					<input type="text" name ='search' id='search' placeholder="Search"/>
					<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
				</form>
			</div>
			<div class='fixed_top_table'>
				<div class='header-background'></div>
				<div id="table_holder">
				<?php echo $manage_table; ?>
				</div>
			</div>
			<div id="pagination">
				<?php echo $this->pagination->create_links();?>
			</div>
		</td>
	</tr>
</table>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>
	