<style type="text/css">
.etsFormGroup { margin: 10px 0; }
#ets_img { margin:40px auto; }

.etsButton {
	background: #349ac5;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#349ac5', endColorstr='#4173b3');
	background: -webkit-linear-gradient(top, #349ac5, #4173b3);
	background: -moz-linear-gradient(top, #349ac5, #4173b3);
	color: white;
	display: block;
	color: white !important;
	border: 1px solid #232323 !important;
	display: block;
	font-size: 14px;
	font-weight: normal;
	height: 22px;
	padding: 5px 0 0 10px;
	text-align: center;
	text-shadow: 0px -1px 0px black !important;
	border-radius: 4px;
	margin-bottom: 8px;
	box-shadow: inset 0px 1px 1px 0px rgba(255, 255, 255, 0.5), 0px 3px 1px -2px rgba(255, 255, 255, .2);
	border: 1px solid #232323;
}
</style>
<script>
var interval_id = '';
var count = 0;
$(document).ready(function(){
	interval_id = setInterval(add_ets_handlers, 200);
	setTimeout(function(){ $.colorbox2.resize(); }, 3000);
});

function add_ets_handlers()
{
	if ($('#ETSIFrame').length > 0)
	{
		$('#cbox2LoadedContent').unmask();

		ETSPayment.addResponseHandler("success", function(e){
			e.type = "creditcard";
			e.amount = e.transactions.amount;

			$.ajax({
				type: "POST",
				url: App.api_table + 'receipts/<?php echo $receipt_id; ?>/payments',
				data: e,
				success: function(response){
					// Add payment to receipt
					var receipt = App.receipts.get(<?php echo $receipt_id; ?>)
					receipt.get('payments').add(response);
					if(receipt.isPaid()){
						$.colorbox2.close();
					}

					// Close table (if everything is paid)
					App.closeTable();
				}
			});
		});

		//console.debug($('#ETSIFrame').contents()); //JBDEBUG
		/*
		ETSPayment.addResponseHandler("error", function(e){
			console.log('ets error');
			console.dir(e);
			$.ajax({
			   type: "POST",
			   url: "<?php echo site_url('sales/process_cancelled'); ?>",
			   data: JSON.stringify({'response':e}),
			   success: function(response){
					console.dir(response);
				},
				dataType:'json'
			 });
		});
		ETSPayment.addResponseHandler("validation", function(e){
			console.log('ets validation error');
			console.debug(e);
			$.ajax({
			   type: "POST",
			   url: "fu/index.php/sales/ets_payment_made/",
			   data: JSON.stringify({'response':e}),
			   success: function(response){
					console.dir(response);
				},
				dataType:'json'
			 });
		});
		*/
		clearInterval(interval_id);
	}
	else if (count > 50)
	{
		clearInterval(interval_id);
	}
	count ++;
}
</script>
<div class="wrapper" id="ets_payment_window">
	<a class="fnb_button show_payment_buttons" style="height: 30px; line-height: 30px; display: block; float: none; padding: 10px;" href="#">Back</a>
	<h1 style="margin-top: 0px;">Total: $<?=$amount?></h1>

	<!-- HTML5 Magic -->
	<div id='ets_session_id' data-ets-key="<?php echo trim($session->id); ?>">
		<img id='ets_img' src="http://www.loadinfo.net/main/download?spinner=3875&disposition=inline" alt="">
	</div>
</div>
<div id='ets_script_box'></div>
<script>
if (typeof ETSPayment == 'undefined')
{
	var e = document.createElement('script');
	e.src = "<?php echo ECOM_ROOT_URL ?>/init";
	$('head')[0].appendChild(e);
}
else
{
	ETSPayment.createIFrame();
}
</script>