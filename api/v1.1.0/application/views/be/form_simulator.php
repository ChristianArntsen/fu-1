<?php
//print_r($basket_info);
?>
<div class='teetime ui-corner-all ui-state-default'>
    <div class='reservation_info'>
        <div class='course_name'><? echo $course_name?></div>
        
    </div>
    <div class="price_box">$<span class="total_price"><?php echo $basket_info['price'] ?></span>*</div>
    <div class='clear'></div>
</div>
<div class="be_fine_print">
    *Fees will be paid at the course. Please login to complete your reservation.
</div>	
<div id="ajax_message" style="display:none">
</div>
	<?php if (validation_errors()) {?>
		<div id="welcome_message" class="top_message_error">
			<?php echo validation_errors(); ?>
		</div>
	<?php } ?>
<?php if ($logged_in) {
    $js = 'onClick="$(\'#cboxContent\').mask(\''.$common_wait.'\'); reservations.book_tee_time(); tb_remove();"';
    echo form_button('login_button','Reserve', $js); 
    
    } else {?>
<!--h2 class="be_header">Login</h2-->
<?php echo form_open('be/login',array('id'=>'login_form')) ?>
<fieldset id="employee_basic_info" class="ui-corner-all ui-state-default login_info">
<legend><?php echo lang("login_user_login"); ?></legend>
<div id="container">
    <table class="be_form login_form">
        <tr id="form_field_username">	
            <td class="form_field_label"><?php echo lang('common_email'); ?>: </td>
            <td class="form_field">
            <?php echo form_input(array(
            'name'=>'username', 
            'value'=>'',
            'size'=>'20')); ?>
            </td>
        </tr>
	<tr id="form_field_password">	
            <td class="form_field_label required"><?php echo lang('login_password'); ?>: </td>
            <td class="form_field">
            <?php echo form_password(array(
            'name'=>'password', 
            'value'=>'',
            'size'=>'20')); ?>
            </td>
        </tr>
        <tr id="form_field_submit">	
            <td id="submit_button" colspan="2">
                <?php echo form_submit('login_button',lang('login_login')); ?>
            </td>
        </tr>
    	</table>
</div>
</fieldset>
<?php echo form_close(); ?>
<!--h2 class="be_header">Register</h2-->

		
<?php echo form_open('be/register',array('id'=>'register_form')) ?>
<fieldset id="employee_basic_info" class="ui-corner-all ui-state-default register_info">
<legend><?php echo lang("login_new_user"); ?></legend>
<div id="container">
	<table class="be_form register_form">
	
		<tr id="form_field_first_name">	
			<td class="form_field_label required"><?php echo lang('common_first_name'); ?>: </td>
			<td class="form_field">
			<?php echo form_input(array(
			'name'=>'first_name', 
			'value'=>'',
			'size'=>'20')); ?>
			</td>
		</tr>
	
		<tr id="form_field_last_name">	
			<td class="form_field_label required"><?php echo lang('common_last_name'); ?>: </td>
			<td class="form_field">
			<?php echo form_input(array(
			'name'=>'last_name', 
			'value'=>'',
			'size'=>'20')); ?>
			</td>
		</tr>
	
		
		<tr id="form_field_phone_number">	
			<td class="form_field_label"><?php echo lang('common_phone'); ?>: </td>
			<td class="form_field">
			<?php echo form_input(array(
			'name'=>'phone_number', 
			'value'=>'',
			'size'=>'20')); ?>
			</td>
		</tr>
                <tr><td colspan="2"><hr/></td></tr>
		<tr id="form_field_email">	
			<td class="form_field_label"><?php echo lang('common_email'); ?>: </td>
			<td class="form_field">
			<?php echo form_input(array(
			'name'=>'email', 
			'value'=>'',
			'size'=>'20')); ?>
			</td>
		</tr>
	
		<tr id="form_field_password">	
			<td class="form_field_label required"><?php echo lang('login_password'); ?>: </td>
			<td class="form_field">
			<?php echo form_password(array(
			'name'=>'password',
                        'id'=>'password',
			'value'=>'',
			'size'=>'20')); ?>
			</td>
		</tr>
		
		<tr id="form_field_password_confirm">	
			<td class="form_field_label required"><?php echo lang('login_password_confirm'); ?>: </td>
			<td class="form_field">
			<?php echo form_password(array(
			'name'=>'password_confirm', 
			'value'=>'',
			'size'=>'20')); ?>
			</td>
		</tr>
		
		<tr id="form_field_submit">	
			<td id="submit_button" colspan="2">
				<?php echo form_submit('login_button',lang('login_register')); ?>
			</td>
		</tr>
	</table>
</div>
</fieldset>
<?php echo form_close(); ?>
<?php } ?>
<script type='text/javascript'>
var reservations = {
    book_tee_time:function() {
        $.ajax({
           type: "POST",
           url: "index.php/be/book_simulator",
           data: "",
           success: function(response){
               if (response.success) {
                   reservations.reservation_booked(response.reservation_ids);
               }
           },
           dataType:'json'
        });

    },
    reservation_booked:function(reservation_ids) {
        window.location = '/index.php/be/reservation_booked'+reservation_ids;
    },
    remove_thank_you_message:function(){
        $("#thank_you_box").fadeOut(2000);
    },
    show_error:function (message) {
        trace(message);
        $('#ajax_message').html(message).show();
        
    }
}
//validation and submit handling
$(document).ready(function()
{
    $('input:[name=phone_number]').mask("999-999-9999");
    $('input:[name=login_button]').button();
    $('button:[name=login_button]').button();
    $('#login_form').validate({
		submitHandler:function(form)
		{
            $(form).mask("<?php echo lang('common_wait'); ?>");
            $(form).ajaxSubmit({
                success:function(response)
                {
                    if(response.success) {
                        $.colorbox.close();
                        post_person_form_submit(response);
                        reservations.book_tee_time();
                    }
                    else {
                        reservations.show_error(response.message);
                        $('#cboxContent').unmask();
                    }
                },
                dataType:'json'
            });

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			username:{
                            required:true,
                            email:true
                        },
			password:'required'
   		},
		messages: 
		{
     		username: {
                    required:"<?php echo lang('employees_email_required'); ?>",
                    email: "<?php echo lang('common_email_invalid_format'); ?>"
                },
		password:"<?php echo lang('employees_password_required'); ?>"
                }
		
	});
        trace("assigned login_form validation");
        $('#register_form').validate({
		submitHandler:function(form)
		{
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
			success:function(response)
			{
                if (response.success) {
                    $.colorbox.close();
                    post_person_form_submit(response);
                    reservations.book_tee_time();
                }
                else {
                    reservations.show_error(response.message);
                	$('#cboxContent').unmask();
                }
            },
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			first_name: "required",
			last_name: "required",
                        phone_number:"required",
			username:
			{
				required:true,
				minlength: 5
			},
			
			password:
			{
				required:true,
				minlength: 8
			},	
			password_confirm:
			{
                                required:true,
 				equalTo: "#password"
			},
                        email: {
                            required:true,
                            email:true
                        }
   		},
		messages: 
		{
     		first_name: "<?php echo lang('common_first_name_required'); ?>",
     		last_name: "<?php echo lang('common_last_name_required'); ?>",
     		username:
     		{
     			required: "<?php echo lang('employees_username_required'); ?>",
     			minlength: "<?php echo lang('employees_username_minlength'); ?>"
     		},
     		
			password:
			{
				required:"<?php echo lang('employees_password_required'); ?>",
				minlength: "<?php echo lang('employees_password_minlength'); ?>"
			},
			confirm_password:
			{
				required:"<?php echo lang('employees_password_required'); ?>",
				equalTo: "<?php echo lang('employees_password_must_match'); ?>"
     		},
     		email: {
                    required:"<?php echo lang('employees_email_required'); ?>",
                    email: "<?php echo lang('common_email_invalid_format'); ?>"
                }
		}
	});
});

</script>