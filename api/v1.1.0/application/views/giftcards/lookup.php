<ul id="error_message_box"></ul>
<?php
	echo form_open('home/giftcard_lookup/',array('id'=>'giftcard_form'));
?>
<fieldset id="giftcard_basic_info">
<div class="field_row clearfix">
<?php echo form_label(lang('giftcards_giftcard_number').':<span class="required">*</span>', 'name',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'giftcard_number',
		'size'=>'24',
		'maxlength'=>'16',
		'id'=>'giftcard_number',
		'value'=>$giftcard_info->giftcard_number)
	);?>
	</div>
</div>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>'Lookup',
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<style>
	#details {
		width:470px;
	}
</style>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	var submitting = false;
    $('#giftcard_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
				success:function(response)
				{
					$.colorbox({href:'index.php/home/view_giftcard/'+response[0].giftcard_id+'/0/1/width~500', title:'Giftcard Information'});
//						$.colorbox.close();
	                submitting = false;
                	console.dir(response[0]);
				},
				dataType:'json'
			});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
		},
		messages:
		{
		}
	});
});
</script>
<script>
	$(document).ready(function(){
		var gcn = $('#giftcard_number');
		gcn.keydown(function(event){
			// Allow: backspace, delete, tab, escape, and enter
	        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || //event.keyCode == 13 || 
	             // Allow: Ctrl+A
	            (event.keyCode == 65 && event.ctrlKey === true) || 
	             // Allow: home, end, left, right
	            (event.keyCode >= 35 && event.keyCode <= 39)) {
	                 // let it happen, don't do anything
	                 return;
	        }
	        else {
	            // Ensure that it is a number and stop the keypress
	            //if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
	            	console.log('kc '+event.keyCode);
	            if (event.keyCode == 186 || event.keyCode == 187 || event.keyCode == 191 || (event.shiftKey && event.keyCode == 53)) {
	                event.preventDefault(); 
	            }   
	        }
		});
		//gcn.focus();
	})
</script>