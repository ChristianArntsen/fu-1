<?php $this->load->view("partial/header"); ?>
<script type="text/javascript">
$(document).ready(function()
{
	$('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
	$('.colbox').colorbox({'maxHeight':700, 'width':550});
    init_table_sorting();
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo lang("common_confirm_search")?>');
    enable_delete('<?php echo lang($controller_name."_confirm_delete")?>','<?php echo lang($controller_name."_none_selected")?>');
    enable_bulk_edit('<?php echo lang($controller_name."_none_selected")?>');

    $('#generate_barcodes').click(function()
    {
    	var selected = get_selected_values();
    	if (selected.length == 0)
    	{
    		alert('<?php echo lang('items_must_select_item_for_barcode'); ?>');
    		return false;
    	}

    	$(this).attr('href','<?php echo site_url("items/generate_barcodes");?>/'+selected.join('~'));
    });

	$('#generate_barcode_labels').click(function()
    {
    	var selected = get_selected_values();
    	if (selected.length == 0)
    	{
    		alert('<?php echo lang('items_must_select_item_for_barcode'); ?>');
    		return false;
    	}

    	$(this).attr('href','<?php echo site_url("items/generate_barcode_labels");?>/'+selected.join('~'));
    });
});

function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(
		{
			sortList: [[1,0]],
			headers:
			{
				0: { sorter: false},
				8: { sorter: false},
				9: { sorter: false}
			}

		});
	}
}

function post_item_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.item_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.item_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);

		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				highlight_row(response.item_id);
				set_feedback(response.message,'success_message',false);
			});
		}
	}
}

function post_bulk_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		set_feedback(response.message,'success_message',false);
		setTimeout(function(){window.location.reload();}, 2500);
	}
}
</script>

<table id="contents">
	<tr>
		<td id="commands">
			<div id="new_button">
				<?php echo 
					anchor("$controller_name/view/-1",
					lang($controller_name.'_new'),
					array('class'=>'colbox none new', 
						'title'=>lang($controller_name.'_new')));
				
					echo anchor("courses/manage_groups/",
					lang('customers_manage_groups'),
					array('class'=>'colbox none new', 'title'=>lang('customers_manage_groups')));
				
					echo anchor("courses/view_message/-1",
					lang('courses_new_message'),
					array('class'=>'colbox none new', 'title'=>lang('courses_new_message')));
					$messages = $this->Course_message->get_all(true);

					echo '<div id="message_box">';
					foreach ($messages as $message)
					{
						echo "<div class='message'>{$message['message']}</div>";
						echo "<div class='date_posted'>{$message['date_posted']}</div>";
					}
					echo '</div>';
					echo anchor("$controller_name/delete",
					lang("common_delete"),
					array('id'=>'delete', 
						'class'=>'delete_inactive')); 
				?>

			</div>
		</td>
		<td style="width:10px;"></td>
		<td id="item_table">
				<div id='table_top'>
				<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
					<input type="text" name ='search' id='search' placeholder="Search"/>
					<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
				</form>
			</div>
			<div class='fixed_top_table'>
				<div class='header-background'></div>
				<div id="table_holder">
				<?php echo $manage_table; ?>
				</div>
			</div>
			<div id="pagination">
				<?php echo $this->pagination->create_links();?>
			</div>
		</td>
	</tr>
</table>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>