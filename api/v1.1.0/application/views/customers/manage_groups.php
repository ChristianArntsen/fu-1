<?php
echo form_open('customers/save_groups/',array('id'=>'customer_groups_form'));
?>
<ul id="error_message_box"></ul>
<fieldset id="customer_groups">
<legend><?php echo lang("customers_create_new_group"); ?></legend>

<div class="field_row clearfix">	
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'new_group',
		'id'=>'new_group',
		'size'=>20,
		'placeholder'=>lang('customers_group_name'),
		'value'=>'')
	);?>
	<span class='' id='add_group_button'><?php echo lang('customers_create_group')?></span>
	</div>
</div>
<div id='groups_container'>
	<div class="groups_title">	
<?php echo form_label(lang('customers_groups'), 'groups'); ?>
</div>
<div class='groups_subtitle'>
<?php echo form_label(lang('customers_click_group_edit'), 'groups'); ?>
</div>
	
<?php 
$first_label = true;
foreach($groups as $group)
{
?>
	<div class='form_field'  id='row_<?php echo $group['group_id']; ?>'>
		<?	echo '<span id="gid_'.$group['group_id'].'"><span  class="group_label">'.$group['label'].'</span></span>'
			
		//' <span style="color:#ccc;">('.$group['member_count'].' members)</span>';
		?>
		<div class='clear'></div>
	</div>
<? 
$first_label = false;
} ?>
<div id='clear' class='clear'></div>
</div>
</fieldset>
<?php 
echo form_close();
?>
<script type='text/javascript'>
var groups = {
	add_group:function() {
		var title = $('#new_group').val();
		title = addslashes(title.replace('"', ''));
		if (title != '')
		{
			$.ajax({
	            type: "POST",
	            url: "index.php/customers/add_group/",
	            data: "group_label="+title,
	            success: function(response){
	            	$('#new_group').val('');
	           		var html = '<div class="form_field" id="row_'+response.group_id+'">'+	
							'<span id="gid_'+response.group_id+'"><span  class="group_label">'+title+'</span></span>';
						
					$('#groups_container #clear').before($(html));
					groups.add_group_click_event();
	    			$.colorbox.resize();
	            },
	            dataType:'json'
	        });
	    }
	},
	save_group_name:function(group_id) {
		var title = ($('#group_text_'+group_id).val().replace('"', ''));
		$.ajax({
            type: "POST",
            url: "index.php/customers/save_group_name/"+group_id,
            data: "group_label="+title,
            success: function(response){
            	var el = $('#gid_'+group_id);
            	console.log(el.html());
				el.html('<span  class="group_label">'+title+'</span>');
				groups.add_group_click_event();
            },
            dataType:'json'
        });
	},
	add_group_click_event:function() {
		console.log('adding group event');
		$('.group_label').click(function(e){
			var el = $(e.target).parent();
			var text = $(e.target).html();
			var id = el.attr('id');
			var gid = id.replace('gid_','');
			console.log('id '+id);
			el.html('<span class="delete_item" onclick="groups.delete_group(\''+gid+'\', \''+addslashes(text.replace('"', ''))+'\')">Delete</span><span class="save_item" onclick="groups.save_group_name(\''+gid+'\')">Save</span><input id="group_text_'+gid+'" value="'+text+'"/>');
		});
		console.log('added');
	},
	delete_group:function(group_id, title) {
		var answer = confirm('Are you sure you want to delete group: '+title+'?');
		if (answer) {
			$.ajax({
	            type: "POST",
	            url: "index.php/customers/delete_group/"+group_id,
	            data: "",
	            success: function(response){
					$('#row_'+group_id).remove();
					$.colorbox.resize();
	            },
	            dataType:'json'
	        });
	    }
	}
};

//validation and submit handling
$(document).ready(function()
{
	var submitting = false;
	$('#add_group_button').click(function(){
		groups.add_group();
	});
	$('#new_group').keypress(function(e){
		if(e.which == 13) {
			e.preventDefault();
			groups.add_group();
		}
	});
    $('#customer_groups_form').validate({
		submitHandler:function(form)
		{
			$.colorbox.close();
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li"
	});
	groups.add_group_click_event();
});
</script>