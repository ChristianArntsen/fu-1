var webprnt = {
	credit_card_payments:0,
	build_receipt_body: function (receipt_data, params) {
		var builder = new StarWebPrintBuilder();
		if (params != undefined)
		{
			if (params.cart != undefined)
			{
				var cart = params.cart;
			    //Itemized
			    for (var line in cart)
			    {
			        receipt_data += builder.createTextElement({data:add_white_space(cart[line].name+' ',cart[line].quantity+' @ '+parseFloat(cart[line].price).toFixed(2)+'    $'+(cart[line].price*cart[line].quantity).toFixed(2))+'\n'});
			    	if (parseInt(cart[line].discount) > 0)
			    		receipt_data += builder.createTextElement({data:add_white_space('     '+parseFloat(cart[line].discount).toFixed(2)+'% discount', '-$'+(cart[line].discount/100*cart[line].price*cart[line].quantity).toFixed(2))+'\n'});
			    }
			}
			if (params.subtotal != undefined)
			{
			    // Totals
			    receipt_data += builder.createTextElement({data:'\n\n'+add_white_space('Subtotal: ','$'+parseFloat(params.subtotal).toFixed(2))+'\n'});
			}
			if (params.taxes != undefined)
			{
			    var taxes = params.taxes;
			    for (var tax in taxes)
			           receipt_data += builder.createTextElement({data:add_white_space(tax+': ','$'+parseFloat(taxes[tax]).toFixed(2))+'\n'});
			}
			if (params.total != undefined)
			{
			       receipt_data += builder.createTextElement({data:add_white_space('Total: ','$'+parseFloat(params.total).toFixed(2))+'\n'});
			}
			if (params.payments != undefined)
			{	
			    // Payment Types
			    receipt_data += builder.createTextElement({data:'\nPayments:\n'});
			    var payments = params.payments;
			    for (var payment in payments)
			    {
				    this.credit_card_payments += (payment+'').indexOf('xxxx') === -1 ? 0 : 1;
				    receipt_data += builder.createTextElement({data:add_white_space(payment+': ','$'+parseFloat(payments[payment].payment_amount).toFixed(2))+'\n'});
			    }
			}
			if (params.amount_change != undefined)
			{
		       // Change due
		       receipt_data += builder.createTextElement({data:'\n'+add_white_space('Change Due: ', params.amount_change)+'\n'});
		    }
		}
		
	    return receipt_data;
	},
	add_return_policy: function (receipt_data, return_policy) {
		var builder = new StarWebPrintBuilder();
		receipt_data += builder.createAlignmentElement({position:'center'});
		receipt_data += builder.createTextElement({data:"\n\n"});// Some spacing at the bottom
	    receipt_data += builder.createTextElement({data:return_policy});
	    
	    return receipt_data;
	},
	add_signature_line: function (receipt_data) {
		var builder = new StarWebPrintBuilder();
	    receipt_data += builder.createTextElement({data:'\n\n\nX_____________________________________________\n'});
	    
	    return receipt_data;
	},
	add_tip_line: function (receipt_data) {
		var builder = new StarWebPrintBuilder();
		receipt_data += builder.createTextElement({data:'\n\n'+add_white_space('Tip: ','$________.____')});
	    receipt_data += builder.createTextElement({data:'\n\n'+add_white_space('TOTAL CHARGE: ','$________.____')});
	    
	    return receipt_data;
	},
	add_barcode: function (receipt_data, label, id) {
		var builder = new StarWebPrintBuilder();
		receipt_data += builder.createTextElement({data:"\n\n"});
		receipt_data += builder.createBarcodeElement({symbology:'Code128', width:'width2', height:40, hri:false, data:id});
	    receipt_data += builder.createTextElement({data:'\n\n'+label+': '+id+'\n'});
	    
	    return receipt_data;
	},
	add_receipt_header: function (receipt_data, params) {
		var builder = new StarWebPrintBuilder();
		var header_data = '';
       
       	if (params != undefined)
		{	
			header_data += builder.createAlignmentElement({position:'center'});
	        if (params.course_name != undefined)
		    	header_data += builder.createTextElement({data:params.course_name+"\n"});
	        
	        if (params.address != undefined)
		    	header_data += builder.createTextElement({data:params.address+"\n"});
	        if (params.address_2 != undefined)
		    	header_data += builder.createTextElement({data:params.address_2+"\n"});
	        if (params.phone != undefined)
		    	header_data += builder.createTextElement({data:params.phone+"\n\n"});
	        
	        header_data += builder.createAlignmentElement({position:'left'});
	        
	        if (params.employee_name != undefined)
		    	header_data += builder.createTextElement({data:"Employee: "+params.employee_name+"\n"});
	        if (params.customer != undefined)
		        header_data += builder.createTextElement({data:"Customer: "+params.customer+"\n"});
		}
        var d = new Date();
        var h = (d.getHours() > 12)?d.getHours()-12:d.getHours();
        var m = (d.getMinutes() < 10)?('0'+d.getMinutes().toString()):d.getMinutes();
        var ap = (d.getHours() < 12)?'am':'pm';
        header_data += builder.createTextElement({data:"Date: "+(d.getMonth()+1)+'/'+d.getDate()+'/'+d.getFullYear()+' '+h+':'+m+ap+"\n\n"});
        
        receipt_data = header_data + receipt_data;
        return receipt_data;
	},
	add_booking_reminder: function (receipt_data, website) {
		var builder = new StarWebPrintBuilder();
		receipt_data += builder.createAlignmentElement({position:'center'});
        receipt_data += builder.createTextElement({data:"\nAnd remember, you can book your\nnext tee time online at\n"+website});
    	return receipt_data;
	},
	add_paper_cut: function (receipt_data) {
		var builder = new StarWebPrintBuilder();
		receipt_data += builder.createCutPaperElement({feed:true});
		return receipt_data;
	},
	add_cash_drawer_open: function (receipt_data) {
		var builder = new StarWebPrintBuilder();
		receipt_data += builder.createRawDataElement({data:chr(27) + "\x70" + "\x30" + chr(25) + chr(25) + "\r"});
		return receipt_data;
	},
	print: function (receipt_data, url) {
		var trader = new StarWebPrintTrader({url:url});
        	
        trader.onReceive = function (response) {
        	currently_printing = false;
	        var msg = '- onReceive -\n\n';
	
	        msg += 'TraderSuccess : [ ' + response.traderSuccess + ' ]\n';
	        msg += 'TraderStatus : [ ' + response.traderStatus + ',\n';
	
	        if (trader.isCoverOpen            ({traderStatus:response.traderStatus})) {msg += '\tCoverOpen,\n';}
	        if (trader.isOffLine              ({traderStatus:response.traderStatus})) {msg += '\tOffLine,\n';}
	        if (trader.isCompulsionSwitchClose({traderStatus:response.traderStatus})) {msg += '\tCompulsionSwitchClose,\n';}
	        if (trader.isEtbCommandExecute    ({traderStatus:response.traderStatus})) {msg += '\tEtbCommandExecute,\n';}
	        if (trader.isHighTemperatureStop  ({traderStatus:response.traderStatus})) {msg += '\tHighTemperatureStop,\n';}
	        if (trader.isNonRecoverableError  ({traderStatus:response.traderStatus})) {msg += '\tNonRecoverableError,\n';}
	        if (trader.isAutoCutterError      ({traderStatus:response.traderStatus})) {msg += '\tAutoCutterError,\n';}
	        if (trader.isBlackMarkError       ({traderStatus:response.traderStatus})) {msg += '\tBlackMarkError,\n';}
	        if (trader.isPaperEnd             ({traderStatus:response.traderStatus})) {msg += '\tPaperEnd,\n';}
	        if (trader.isPaperNearEnd         ({traderStatus:response.traderStatus})) {msg += '\tPaperNearEnd,\n';}
	
	        msg += '\tEtbCounter = ' + trader.extractionEtbCounter({traderStatus:response.traderStatus}).toString() + ' ]\n';
	    }
	
	    trader.onError = function (response) {
	    	currently_printing = false;
	        var msg = '- onError -\n\n';
	        msg += '\tStatus:' + response.status + '\n';
	        msg += '\tResponseText:' + response.responseText;
	    }
	
		trader.sendMessage({request:receipt_data});
	}
};
