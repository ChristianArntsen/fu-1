String.prototype.regexLastIndexOf = function(regex, startpos) {
    regex = (regex.global) ? regex : new RegExp(regex.source, "g" + (regex.ignoreCase ? "i" : "") + (regex.multiLine ? "m" : ""));
    if(typeof (startpos) == "undefined") {
        startpos = this.length;
    } else if(startpos < 0) {
        startpos = 0;
    }
    var stringToWorkWith = this.substring(0, startpos + 1);
    var lastIndexOf = -1;
    var nextStop = 0;
    while((result = regex.exec(stringToWorkWith)) != null) {
        lastIndexOf = result.index;
        regex.lastIndex = ++nextStop;
    }
    return lastIndexOf;
}
var global_variables = {};
String.prototype.splice = function(
index,
howManyToDelete,
stringToInsert /* [, ... N-1, N] */
){
 
// Create a character array out of the current string
// by splitting it. In the context of this prototype
// method, THIS refers to the current string value
// being spliced.
var characterArray = this.split( "" );
        
// Now, let's splice the given strings (stringToInsert)
// into this character array. It won't matter that we
// are mix-n-matching character data and string data as
// it will utlimately be joined back into one value.
//
// NOTE: Because splice() mutates the actual array (and
// returns the removed values), we need to apply it to
// an existing array to which we have an existing
// reference.
Array.prototype.splice.apply(
characterArray,
arguments
);
 
// To return the new string, join the character array
// back into a single string value.
return(
characterArray.join( "" )
);
 
};

var calendar = '';
var calendarback = '';
var showingBackNine = false;
var currently_editing = '';
var ttArray = {'front':{},'back':{}};

var Calendar_actions = {
	moused_over_teetime:'',
        standby_to_teetime:function(id, date, side)
        {
            var standby_html = '';
            
            $.ajax({
                type:"POST",
                url: "index.php/teesheets/standby_to_teetime",
                data: 'id=' + id + '&date=' + date + '&side=' + side,
	        success: function(response){
                    
                    $.each(response.standby_change, function(data){
                                    
                                    //console.log('passed in datas:' + (response.standby_change[0][data].standby_id));
                                   // console.log('passed in data: ' + (response[0][data]));
                                            standby_html += "<div class='standby_entry' id='";
                                            
                                            standby_html += response.standby_change[data]['standby_id'] + 
                                            "'><div class='teetime_result' id='" + 
                                            response.standby_change[data]['standby_id'] + "'><div>" +
                                            response.standby_change[data]['name'] + ' </br>' + 
                                            response.standby_change[data]['time'] + '</br> ' + 
                                            response.standby_change[data]['holes'] + ' Holes ' +
                                            response.standby_change[data]['players'] + ' Players' + "</div> </div>";
                                    
                                         standby_html += '</div>';
                                    
                                });
                                
                                console.log('response teetime' + response.teetimes);
                                  Calendar_actions.update_teesheet(response.teetimes, true, 'auto_update');  
                                   
                                
                                $('#teetime_populate_standby_list').html(standby_html);
                                $('.standby_entry').draggable({zIndex:1000, containment:'#content_area_wrapper', 
                                    helper:'clone', appendTo:'body'
                              });
                },
                dataType:'json'
            });
        },
	mouseover_teetime:function(event,e){
		this.moused_over_teetime = event.id;
		setTimeout(function(){Calendar_actions.display_teetime_hover_data(event,e)}, 1000);
	},
	mouseout_teetime:function(){
		this.moused_over_teetime = '';
		this.hide_teetime_hover_data();
	}, 
	send_confirmation:function(teetime_id) {
		$.ajax({
           type: "POST",
           url: "index.php/teesheets/send_confirmation_email/"+teetime_id,
           data: '',
           success: function(response){
           		console.dir(response);
		    },
            dataType:'json'
         });
	},
	display_teetime_hover_data:function(event,e) {
		if (this.moused_over_teetime == event.id)
		{
			$.ajax({
	           type: "POST",
	           url: "index.php/teesheets/get_teetime_hover_data/",
	           data: 'teetime_id='+event.id,
	           success: function(response){
	           		if(response!='' && response!=undefined && $("#colorbox").css("display")!="block")
					{
						$('#teetime_info_bar').html(response);
						/*$(e.target).qtip({
							content:{
								text:response
							},
							position:{
								my:'left center',
								at:'right center',
								adjust: {
									method:'shift'
								}
							}
						});*/
						
						$('#teetime_info_bar').fadeIn(100, function(){
							if ($("#colorbox").css("display")=="block") {  
							    //alert('ColorBox is currently open');  
							    Calendar_actions.hide_teetime_hover_data();
							}else{  
							    //alert('ColorBox is currently closed');  
							}  
						});
						
					}
					else
					{
						$('#teetime_info_bar').hide();
					} 
				},
	            dataType:'html'
	         });
	    }
	},
	hide_teetime_hover_data:function(callback){
		if (callback == undefined)
			$('#teetime_info_bar').fadeOut(100);
		else
			$('#teetime_info_bar').fadeOut(100, callback);
	},
	update_teesheet:function(events, initialized, from) {
		console.dir(events);
		var beginning_date = new Date();
		var beginning_time = beginning_date.getTime();
		if (from == undefined)
			buildStats(null);
		if (initialized == undefined)
			initialized = true;
			
		for (var i in events)
		{
			events[i].start = getDateFromTimeString(events[i].start);
			events[i].end = getDateFromTimeString(events[i].end);
			var calendarname = 'calendar';
			var other_side = 'calendarback';
            if (events[i].side == 'back') 
            {
                calendarname = 'calendarback';
                other_side = 'calendar';
            }
            var existing_event = $('#'+calendarname).fullCalendar('clientEvents', events[i].id);
			if (existing_event != undefined && existing_event[0] != undefined)
			{
				var className = existing_event[0].className+''.replace(/,/i,'');
				var newClass = events[i].className.replace(' ', ',');
                if (events[i].status == 'deleted')
    	        	$('#'+calendarname).fullCalendar('removeEvents', existing_event[0].id);
            	else if (from == undefined || (from == 'auto_update')) 
				{
	                var different = 0;
	                if (existing_event[0].title != events[i].title) {
		                existing_event[0].title = events[i].title;
		                different = 1;
		            }
		            if (existing_event[0].player_count != parseInt(events[i].player_count)) {
		                existing_event[0].player_count = parseInt(events[i].player_count);
		                different = 1;
		            }
	                if (existing_event[0].carts != parseInt(events[i].carts)) {
	                	existing_event[0].carts = parseInt(events[i].carts);
	                	different = 1;
		            }
	                if (existing_event[0].name != events[i].title) {
	                	existing_event[0].name = events[i].title;
	                	different = 1;
		            }
	                if (existing_event[0].start == null || existing_event[0].start.getTime() != events[i].start.getTime()) {
	                	existing_event[0].start = events[i].start;
	                	different = 1;
		            }
		            if (existing_event[0].end == null || existing_event[0].end.getTime() != events[i].end.getTime()) {
	                	existing_event[0].end = events[i].end;
	                	different = 1;
		            }
	                if (existing_event[0].side != events[i].side) {
	                	existing_event[0].side = events[i].side;
	                	different = 1;
		            }
	                if (existing_event[0].backgroundColor != events[i].backgroundColor) {
	                	existing_event[0].backgroundColor = events[i].backgroundColor;
	                	different = 1;
		            }
	                if (existing_event[0].type != events[i].type) {
	                	existing_event[0].type = events[i].type;
	                	different = 1;
		            }
		            var className = ''+existing_event[0].className;
	                if ($.trim(className.replace(/,/g, ' ').replace(/\s/g, '')) != $.trim(events[i].className).replace(/\s/g, '')) {
		                //console.log('"'+$.trim(className.replace(/,/g,' '))+'" - "'+$.trim(events[i].className)+'"');
	                	existing_event[0].className = events[i].className;
	                	different = 1;
		            }
					if (different) {
						$('#'+calendarname).fullCalendar('updateEvent', existing_event[0]);
					}
	            }	
            } else {
            	if (events[i].status != 'deleted')
            	{
                    //console.log('RENDER EVENT');
    				//If we switched sides, we need to remove from the other side
					var switched_event = $('#'+other_side).fullCalendar('clientEvents', events[i].id);
					if (switched_event != undefined && switched_event[0] != undefined)
				    	$('#'+other_side).fullCalendar('removeEvents', switched_event[0].id);
            
	            	var new_event = {};
	            	new_event.id = events[i].id;
	                new_event.allDay = "";
	                new_event.title = events[i].title;
	                new_event.player_count = parseInt(events[i].player_count);
	                new_event.carts = parseInt(events[i].carts);
	                new_event.name = events[i].title;
	                new_event.start = events[i].start;
	                new_event.side = events[i].side;
	                new_event.end = events[i].end;
	                new_event.backgroundColor = events[i].backgroundColor;
	                new_event.borderColor = events[i].borderColor;
	                new_event.className = events[i].className;
	                new_event.initialized = initialized;
	                new_event.type = events[i].type;
	                $('#'+calendarname).fullCalendar('renderEvent', new_event, true);
	            }
            }
    	}
 	},
	add_new_teetimes:function() {
		$.ajax({
           type: "POST",
           url: "index.php/teesheets/getJSONTeeTimes/",
           data: '',
           success: function(response){
               if (response == 'logout')
                   logout();
               events = $.merge(response.caldata, response.bcaldata);
               Calendar_actions.update_teesheet(events, true, 'auto_update');
			},
            dataType:'json'
         }); 
			
    	
	},
	load_teetimes:function(view) {
		$('body').mask('Loading old teetimes');
        var vs = view.start;
		var ve = view.end;
		//console.log('ts '+view.start.getDate());
		//console.log('te '+view.end.getDate());
		var start_date = vs.getFullYear()+(vs.getMonth()<10?'0':'')+vs.getMonth()+(vs.getDate()<10?'0':'')+vs.getDate()+'0000';
		var end_date = ve.getFullYear()+(ve.getMonth()<10?'0':'')+ve.getMonth()+(ve.getDate()<10?'0':'')+ve.getDate()+'0000';
		//console.log('sd '+start_date+' ed '+end_date);
		$.ajax({
           type: "POST",
           url: "index.php/teesheets/get_json_teetimes/"+start_date+"/"+end_date,
           data: '',
           success: function(response){
               if (response == 'logout')
                   logout();
               events = $.merge(response.caldata, response.bcaldata);
               Calendar_actions.update_teesheet(events, true, 'auto_update');
               $('body').unmask();
			},
            dataType:'json'
         }); 
	},
    event:{
        remove_empty_teetime:function(id){
            ttArray = {'front':{},'back':{}};
            $.ajax({
               type: "POST",
               url: "index.php/teesheets/delete_teetime",
               data: 'id='+id,
               success: function(response){
                   if (response == 'logout')
                       logout();
                   //Open modal qtip with edit options.
                   calendar.fullCalendar( 'removeEvents', id);
                   calendarback.fullCalendar( 'removeEvents', id);
               }
             }); 
        },
		creating_teetime:false,
        create_empty_teetime:function(start, end, all_day, side) {
        	var view = calendar.fullCalendar('getView');
        	if (this.opened || view.name != 'agendaDay')
        	{
        		return;
        	}
        	else
        	{
        		this.opened = true;
        		
	            var title = '';//prompt('Reservation Name:');
	            var calendarname = 'calendar';
	            if (side == 'back')
	                calendarname = 'calendarback';
	            $.ajax({
	               type: "POST",
	               url: "index.php/teesheets/save_teetime",
	               data: "title="+title+"&start="+getTimeString(start)+"&end="+getTimeString(end)+"&allDay="+all_day+"&side="+side,
	               dataType:'json',
	               success: function(response){
	                   //trace('succeeding');
	                   if (response == 'logout')
	                       logout();
	                   //response = eval("("+response+")");
	                   Calendar_actions.update_teesheet(response.teetimes, false);
	                return;	
	                   $('#'+calendarname).fullCalendar('renderEvent',
	                        {
	                            id:response.tee_time_id,//'teetime_place_holder',
	                            //ttid:response.tee_time_id,
	                            title: title,
	                            name:title,
	                            start: start,
	                            end: end,
	                            phone:'',
	                            email:'',
	                            allDay: all_day,
	                            details:'',
	                            player_count:'',
	                            holes:'',
	                            carts:'',
	                            clubs:'',
	                            paid_player_count:0,
	                            paid_carts:0,
	                            initialized:false
	                        },
	                        true // make the event "stick"
	                    );
	               }
	             });
	            $('#'+calendarname).fullCalendar('unselect');
	        }
        },
        move_teetime:function(event, side){
            ttArray = {'front':{},'back':{}};
            var id = event.id;
            var newstart = getTimeString(event.start);
            var newend = getTimeString(event.end);
            $.ajax({
               type: "POST",
               url: "index.php/teesheets/save_teetime/" + id,
               data: "&start="+newstart+"&end="+newend+"&side="+side,
               dataType:'json',
               success: function(response){
	               //response = eval('('+response+')');
               	   Calendar_actions.update_teesheet(response.teetimes);
                	return;
               }
             });

        },
        create_event_title:function(event) {
            var dow = '';
            if (event.start.getDay() == 0)
                dow = 'Sun';
            else if (event.start.getDay() == 1)
                dow = 'Mon';
            else if (event.start.getDay() == 2)
                dow = 'Tue';
            else if (event.start.getDay() == 3)
                dow = 'Wed';
            else if (event.start.getDay() == 4)
                dow = 'Thu';
            else if (event.start.getDay() == 5)
                dow = 'Fri';
            else if (event.start.getDay() == 6)
                dow = 'Sat';

            var ampm = 'am';
            if (event.start.getHours() > 11)
                ampm = 'pm';

            var hours = '';
            if (event.start.getHours() > 12)
                hours = event.start.getHours() - 12;
            else
                hours = event.start.getHours();

            var leadingZero = '';
            if (event.start.getMinutes() < 10)
                leadingZero = '0';
            return 'Tee Time Details - '+event.name+' on '+dow+'. @ '+hours+':'+leadingZero+event.start.getMinutes()+ampm;
        },
        opened:false,
        event_render:function(event, element, side) {
        	$(element).click(function(){
        		Calendar_actions.hide_teetime_hover_data(function(){
	        		if (!this.opened)
	        		{
	        			currently_editing = event.id;
		        		$.colorbox({
			        		'speed':1,
			        		'width':840,
			        		'height':400,
			        		'href':'index.php/teesheets/view_teetime/'+event.id,
			        		'title':Calendar_actions.event.create_event_title(event),
			        		'onClosed':function() {
			        				Calendar_actions.event.opened = false;
			                        if (event.name == '') {
			                        	var id = event.id;
			                        	if (event.ttid)
			                        		id = event.ttid;
			                        	
			                            Calendar_actions.event.remove_empty_teetime(id);
			                        }
			                   },
			               'onComplete':function() {
			               		element.colorbox.resize();
			               		focus_on_title();
			               }
			        	});
			        }
        		});
        	});
        	var bindings = {
        		'raincheck':function(t) {
		        	$.colorbox({
		        		href:'index.php/sales/view_raincheck/'+event.id,
		        		title:'Issue Raincheck',
		        		width:650
		        	});
		        },
		        'move':function(t) {
		        	//alert('Moving');
		        	$.colorbox({
		        		href:'index.php/teesheets/view_move/'+event.id+'/'+event.side,
		        		title:'Move Teetime',
		        		width:750
		        	});
		        },
		        'repeat':function(t) {
		        	//alert('Repeating');
		        	$.colorbox({
		        		href:'index.php/teesheets/view_repeat/'+event.id,
		        		title:'Repeat Event',
		        		width:650,
		        		height:500
		        	});
		        },
				'switch': function(t) {
	        		$.ajax({
		               type: "POST",
		               url: "index.php/teesheets/switch_teetime_sides/"+event.id,
		               data: "",
		               success: function(response){
		               	   //Calendar_actions.update_teesheet(response.teetimes);
			               //alert(event.side);
			               var event_id_1 = '';
			               var event_id_2 = '';
			               var event_side_1 = '';
			               var event_side_2 = '';
			               if (event.id.length > 20)
			               {
			                   event_id_1 = event.id.substr(0,20);
				               event_id_2 = event.id;
				               event_side_1 = (event.side == 'front')?'calendarback':'calendar';
				               event_side_2 = (event.side == 'front')?'calendar':'calendarback';
			               }
			               else
			               {
			                   event_id_1 = event.id;
				               event_id_2 = event.id+'b';
				               event_side_1 = (event.side == 'front')?'calendar':'calendarback';
			                   event_side_2 = (event.side == 'front')?'calendarback':'calendar';
				           }
				           ttArray = {'front':{},'back':{}};
            				//fetch events
            			   var event_1 = $.extend({}, $('#'+event_side_1).fullCalendar('clientEvents', event_id_1));
            			   ttArray = {'front':{},'back':{}};
            			   var event_2 = $.extend({}, $('#'+event_side_2).fullCalendar('clientEvents', event_id_2));
            			   
            			   	//remove events
            			   	ttArray = {'front':{},'back':{}};
							$('#'+event_side_1).fullCalendar('removeEvents', event_id_1);
							ttArray = {'front':{},'back':{}};
							$('#'+event_side_2).fullCalendar('removeEvents', event_id_2);
            			   
		                	//add events
		                	ttArray = {'front':{},'back':{}};
		                	if (event_2 != undefined && event_2[0] != undefined)
							{
								var event_3 = {
									start:new Date(event_2[0].start),
									end:new Date(event_2[0].end),
									title:event_2[0].title,
									allDay:false,
									//borderColor:event_2[0].borderColor,
									backgroundColor:event_2[0].backgroundColor,
									carts:event_2[0].carts,
									className:event_2[0].className,
									etimestamp:event_2[0].etimestamp,
									id:event_2[0].id,
									name:event_2[0].name,
									side:event_2[0].side=='front'?'back':'front',
									player_count:event_2[0].player_count,
									stimestamp:event_2[0].stimestamp,
									type:event_2[0].type,
									status:event_2[0].status
								}
								$('#'+event_side_1).fullCalendar('renderEvent', event_3, true);
							}
			            ttArray = {'front':{},'back':{}};
		                    if (event_1 != undefined && event_1[0] != undefined)
							{
								var event_3 = {
									start:new Date(event_1[0].start),
									end:new Date(event_1[0].end),
									title:event_1[0].title,
									allDay:false,
									//borderColor:event_1[0].borderColor,
									backgroundColor:event_1[0].backgroundColor,
									carts:event_1[0].carts,
									className:event_1[0].className,
									etimestamp:event_1[0].etimestamp,
									id:event_1[0].id,
									name:event_1[0].name,
									side:event_1[0].side=='front'?'back':'front',
									player_count:event_1[0].player_count,
									stimestamp:event_1[0].stimestamp,
									type:event_1[0].type,
									status:event_1[0].status
								}
								$('#'+event_side_2).fullCalendar('renderEvent', event_3, true);
							}
		               },
		               dataType:'json'
		            });
		        },
				'teed_off': function(t) {
	        		$.ajax({
		               type: "POST",
		               url: "index.php/teesheets/mark_teetime_teedoff/"+event.id,
		               data: "",
		               success: function(response){
		  					Calendar_actions.update_teesheet(response.teetimes);
		               },
		               dataType:'json'
		            });
		        },
				'mark_turn': function(t) {
	        		$.ajax({
		               type: "POST",
		               url: "index.php/teesheets/mark_teetime_turned/"+event.id,
		               data: "",
		               success: function(response){
		  					Calendar_actions.update_teesheet(response.teetimes);
		               },
		               dataType:'json'
		            });
		        },
				'mark_finished': function(t) {
	        		$.ajax({
		               type: "POST",
		               url: "index.php/teesheets/mark_teetime_finished/"+event.id,
		               data: "",
		               success: function(response){
		  					Calendar_actions.update_teesheet(response.teetimes);
		               },
		               dataType:'json'
		            });
		        },
				'check_in_1': function(t) {
	        		$.ajax({
		               type: "POST",
		               url: "index.php/teesheets/check_in/"+event.id+"/1",
		               data: "",
		               success: function(response){
		  					Calendar_actions.update_teesheet(response.teetimes);
		               },
		               dataType:'json'
		            });
		        },
				'check_in_2': function(t) {
	        		$.ajax({
		               type: "POST",
		               url: "index.php/teesheets/check_in/"+event.id+"/2",
		               data: "",
		               success: function(response){
		  					Calendar_actions.update_teesheet(response.teetimes);
		               },
		               dataType:'json'
		            });
		        },
				'check_in_3': function(t) {
	        		$.ajax({
		               type: "POST",
		               url: "index.php/teesheets/check_in/"+event.id+"/3",
		               data: "",
		               success: function(response){
		  					Calendar_actions.update_teesheet(response.teetimes);
		               },
		               dataType:'json'
		            });
		        },
				'check_in_4': function(t) {
	        		$.ajax({
		               type: "POST",
		               url: "index.php/teesheets/check_in/"+event.id+"/4",
		               data: "",
		               success: function(response){
		  					Calendar_actions.update_teesheet(response.teetimes);
		               },
		               dataType:'json'
		            });
		        },
				'check_in_5': function(t) {
	        		$.ajax({
		               type: "POST",
		               url: "index.php/teesheets/check_in/"+event.id+"/5",
		               data: "",
		               success: function(response){
		  					Calendar_actions.update_teesheet(response.teetimes);
		               },
		               dataType:'json'
		            });
		        }
		    };
        	element.contextMenu('myMenu',{
        		bindings:bindings,
        		onShowMenu: function(e, menu) {
			        if (calInfo.holes != 18) {
			          $('#switch', menu).remove();
			        }
			        var p = $(e.target).closest(".teed_off");
			        var pt = $(e.target).closest(".mark_turn");
			        var pf = $(e.target).closest(".mark_finished");
			        if (p.length > 0)
			        {    
			        	$('#teed_off', menu).remove();

   				        if (pt.length > 0)
				            $('#mark_turn', menu).remove();
				        if (pf.length > 0)
				            $('#mark_finished', menu).remove();

			        }
			        else
			        {
				            $('#mark_turn', menu).remove();
				            $('#mark_finished', menu).remove();
			        }	
			        return menu;
			      }
        	});
		    $(element).dblclick(function(){
        		
        	});
    		if (event.initialized == false) {
                event.initialized = true;
                element.click();
            }
	    }    
    },
    create_print_button:function () {
        var cal_date = $("#calendar").fullCalendar('getDate');
        var year = cal_date.getFullYear();
        var month = cal_date.getMonth();
        var day = cal_date.getDate();
        var m_z = '';
        var d_z = '';
        if (month<10)
            m_z = '0';
        if (day<10)
            d_z = '0';
        var date = year+''+m_z+''+month+''+d_z+''+day;
        if ($('#print_button').length == 0)
            $('.fc-button-agendaDay').after('<a id="print_button" name="print_button" target="_blank" href="index.php/teesheets/print_teesheet/'+date+'">Save/Print</a>');
        else
            $('#print_button').replaceWith('<a id="print_button" name="print_button" target="_blank" href="index.php/teesheets/print_teesheet/'+date+'"><div id="teesheet_download_button">&nbsp;</div></a>');
        
    },
    
    // TODO: update... use only with auto updating teetimes
    refresh_events:function(){
        ttArray = {'front':{},'back':{}};
        if ($('#calendar')) {
	        $('#calendar').fullCalendar('removeEvents');
        	$('#calendar').fullCalendar('refetchEvents');
        }
    },
    // TODO: same as above
    update_events:function(){
        if ($('.ui-tooltip-focus')[0]==undefined) {
        	ttArray = {'front':{},'back':{}};
            if ($('#calendar')) {
	            $('#calendar').fullCalendar('refetchEvents');
	            $('#calendarback').fullCalendar('refetchEvents');
	        }
        }
    },
    adjust_formatting:function(event, element, view, side) {
    	return;
    	if (global_variables.simulator)
    		return;
    	var start_time = event.start.getTime();
        if (view.name == 'agendaDay') {
            var den = 252.5;
            var quar = 63.125;
            var ofl = 270;
            if (calInfo.holes == 9) {
                den = 570;
                quar = 142.5;
                ofl = 510;
            }
            var lmar = 65;
            if (ttArray[side][start_time] != '' 
                    && ttArray[side][start_time] != undefined 
                    && ttArray[side][start_time]['players'] != '' 
                    && ttArray[side][start_time]['players'] != undefined) 
            {
                lmar = (ttArray[side][start_time]['players']*quar)+65;
                ttArray[side]['count']++;
            }
            else {
                ttArray[side]['count'] = 1;
                ttArray[side][start_time] = {};
                ttArray[side][start_time]['players'] = 0;
                ttArray[side][start_time]['teetimes'] = 0;
                ttArray[side][start_time]['ids'] = [];
            }
            
            var width = den;
            if (event.type == 'tournament' || event.type == 'league' || event.type == 'event' || event.type == 'closed') {

            }
            else if (event.player_count == 1)
                width = den*.25;
            else if (event.player_count == 2)
                width = den*.5;
            else if (event.player_count == 3)
                width = den*.75;
            var pixels = $(element).css('width');
            var top = $(element).css('top');
            pixels = parseFloat(pixels.replace(/\./g, '').replace(/,/g,'.').replace(/[^\d\.]/g,''), 10);
            top = parseFloat(top.replace(/\./g, '').replace(/,/g,'.').replace(/[^\d\.]/g,''), 10);
            $(element).css('width',(width-2)+'px');

            $(element).css('left',(lmar)+'px');
            if (width + lmar - 65 > den) {
                lmar = ofl;
                $(element).css('top',top+(ttArray[side][start_time]['teetimes']*4)+'px');
                $(element).css('left',(lmar)+(ttArray[side][start_time]['teetimes']*8)+'px');
                event.overflow = true;
            }
            else
                event.overflow = false;
            if ($.inArray(event.id, ttArray[side][start_time]['ids']) == -1) {
                ttArray[side][start_time]['players'] += parseInt(event.player_count);
                ttArray[side][start_time]['teetimes']++;
                ttArray[side][start_time]['ids'].push(event.id);
            }
        }
    },
    setup_date_header_listeners: function () {
        $('th.fc-sun').click(Calendar_actions.goToDay);
        $('th.fc-mon').click(Calendar_actions.goToDay);
        $('th.fc-tue').click(Calendar_actions.goToDay);
        $('th.fc-wed').click(Calendar_actions.goToDay);
        $('th.fc-thu').click(Calendar_actions.goToDay);
        $('th.fc-fri').click(Calendar_actions.goToDay);
        $('th.fc-sat').click(Calendar_actions.goToDay);    
    },
    goToDay: function(e){
        var datetext = $(e.target).text().slice(4);
        var year = calendar.fullCalendar('getDate').getFullYear();
        var slInd = datetext.indexOf('/');
        var month = datetext.slice(0,slInd)-1;
        var date = datetext.slice(slInd+1);
        if (isNaN(year) || isNaN(month) || isNaN(date)) {
            trace('Did not click on a date');
        }
        else {
            $('#day_view').addClass('selected');
			$('#week_view').removeClass('selected');
			calendar.fullCalendar('changeView', 'agendaDay');
            calendar.fullCalendar('gotoDate',year, month, date);
            if(calendarback)
                calendarback.fullCalendar('gotoDate',year, month, date);
        }
    },
    setup_back_nine:function(view) {
    	if (calInfo.holes == 18) {
            if (view.name == 'agendaDay')
                showBackNine(view);
            else if (view.name == 'agendaWeek')
                hideBackNine(view);

           /* $("#calendarback .calScroller").scroll(function(e) {
                //if (e.currentTarget) {
                    //$(".calScroller").scrollTop(e.currentTarget.scrollTop);
                //}
                console.log('scrolling back');
                $('#calendar .calScroller').scrollTop($('#calendarback .calScroller').scrollTop());
            });*/
            $("#calendar table.fc-header").css('width', '700px');
        }
        if (view.name == 'agendaDay') {
            $('#calendar .fc-agenda-axis').unbind();
            $('#calendarback .fc-agenda-axis').unbind();
            $('#calendar .fc-agenda-axis').click(function(evt){
            	//This should only be accessible in day view.
                var time_text = $(evt.target).text();
                if (time_text != null && time_text != undefined && time_text != '') {
                var calendar_date = $('#calendar').fullCalendar('getDate');
                var calendar_date2 = $('#calendar').fullCalendar('getDate');
                var m_index = time_text.indexOf('m');
                var c_index = time_text.indexOf(':');
                var am_pm = time_text.slice(m_index - 1, m_index + 1);    

                var hour = (c_index == -1) ? parseInt(time_text.slice(0, m_index-1)) : parseInt(time_text.slice(0, c_index));
                var min = (c_index == -1) ? 0 : parseInt(time_text.slice(c_index+1,m_index-1));
                if (am_pm == 'pm' && hour != 12)
                    hour += 12;
            
                calendar_date.setHours(hour);
                calendar_date.setMinutes(min);
                min += parseFloat(calInfo.increment1);
            
                if (min > 59) {
                    min -= 60;
                    hour += 1;
                }
                
                calendar_date2.setHours(hour);
                calendar_date2.setMinutes(min);

                Calendar_actions.event.create_empty_teetime(calendar_date, calendar_date2, false, 'front');
                }
            });
            //$('tr', '.fc-agenda-slots').selectable();
            $('#calendarback .fc-agenda-axis').click(function(evt){
                //This should only be accessible in day view.
                var time_text = $(evt.target).text();
                if (time_text != null && time_text != undefined && time_text != '') {
                var calendar_date = $('#calendar').fullCalendar('getDate');
                var calendar_date2 = $('#calendar').fullCalendar('getDate');
                var m_index = time_text.indexOf('m');
                var c_index = time_text.indexOf(':');
                var am_pm = time_text.slice(m_index - 1, m_index + 1);    

                var hour = (c_index == -1) ? parseInt(time_text.slice(0, m_index-1)) : parseInt(time_text.slice(0, c_index));
                var min = (c_index == -1) ? 0 : parseInt(time_text.slice(c_index+1,m_index-1));
                if (am_pm == 'pm' && hour != 12)
                    hour += 12;

                calendar_date.setHours(hour);
                calendar_date.setMinutes(min);
                min += parseFloat(calInfo.increment1);

                if (min > 59) {
                    min -= 60;
                    hour += 1;
                }

                calendar_date2.setHours(hour);
                calendar_date2.setMinutes(min);

                Calendar_actions.event.create_empty_teetime(calendar_date, calendar_date2, false, 'back');
                }
            })
        }
    }
}
var calendar_over = '';
$(document).ready(function() {
    //updateTeetimes();
    //$('#stats').tabs();
    $('#calendar').mouseover(function(){
    	calendar_over = 'calendar';
    });
    $('#calendarback').mouseover(function(){
    	calendar_over = 'calendarback';
    });
    $('#weatherBox').tabs();
    $("#datepicker").datepicker({
        pickerClass:'datepicker_vista',
        monthNames:['January,','February,','March,','April,','May,','June,','July,','August,','September,','October,','November,','December,'],
        onSelect: function(dateText, inst) {
            calendar.fullCalendar('gotoDate',dateText.slice(6), dateText.slice(0,2)-1, dateText.slice(3,5));
            calendarback.fullCalendar('gotoDate',dateText.slice(6), dateText.slice(0,2)-1, dateText.slice(3,5));
        }
    });
    $.each(calInfo.caldata, function(key, value) { 
        value.start = getDateFromTimeString(value.stimestamp);
        value.end = getDateFromTimeString(value.etimestamp);
    });
    $.each(calInfo.bcaldata, function(key, value) { 
        value.start = getDateFromTimeString(value.stimestamp);
        value.end = getDateFromTimeString(value.etimestamp);
    });
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var h = date.getHours();
    var dow = date.getDay();
    // Creation of the front nine
    calendar = $('#calendar').fullCalendar({
            theme:true,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'agendaWeek,agendaDay'
            },
            allDaySlot:false,
            firstDay:dow,
            droppable: true,
            defaultView:'agendaDay',
            minTime:calInfo.openhour,
            maxTime:calInfo.closehour,
            //firstHour:h,//starts at time closest to now
            slotMinutes:parseFloat(calInfo.increment1),
            defaultEventMinutes:parseFloat(calInfo.increment2),
            selectable: true,
            selectHelper: true,
            drop:function(date, allDay) {
                 
                Calendar_actions.standby_to_teetime($(this)[0].id, date, 'front');
            },
            select: function(start, end, all_day) {
                Calendar_actions.event.create_empty_teetime(start, end, all_day, 'front');
            },
            eventDrop: function(event,day_delta,minute_delta,all_day,revert_func) {
                Calendar_actions.event.move_teetime(event, 'front');
            },
            eventMouseover:function(event, e, view){
                Calendar_actions.mouseover_teetime(event,e);
            },
            eventMouseout:function(event, e, view){
            	Calendar_actions.mouseout_teetime();
            },
            timeFormat:{
                agenda: '', // 5:00 - 6:30
                '': 'h:mmt'            // 7p
            },
            eventResize: function(event, day_delta, minute_delta, revert_func, js_event, ui, view ) {
                Calendar_actions.event.move_teetime(event, 'front');
            },
            editable: true,
            events: calInfo.caldata,
            eventAfterRender: function(event, element, view) {
                Calendar_actions.adjust_formatting(event, element, view, 'front');
            },
            eventRender: function(event, element) {
                Calendar_actions.event.event_render(event, element, 'front');
            },
            eventColor:'transparent',
            viewDisplay: function(view) {
            	var right_now = new Date();
                
                ttArray = {'front':{},'back':{}};
                Calendar_actions.setup_back_nine(view);
                buildStats(view);
                Calendar_actions.create_print_button();
                if (view.name == 'agendaWeek')
                {
                    //Calendar_actions.refresh_events()
                    Calendar_actions.setup_date_header_listeners();
                }
                else
                {
                	if (view.start.getMonth() != right_now.getMonth() || view.start.getDate() != right_now.getDate())//not today
                	{}
                	else
                	{
                		//console.log('today!!!');
                		//search for right_now.getHours();
                		var hour = (right_now.getHours()>12)?right_now.getHours()-12:right_now.getHours();
                		var am_pm = (right_now.getHours()>11)?'pm':'am';
                		//console.log('hours '+hour+' calScroller pos: '+$('.calScroller').offset().top+' th pos: '+$('th:contains('+hour+':):contains('+am_pm+')').offset().top+' or '+$('th:contains('+hour+':):contains('+am_pm+')').position().top);
               		//setTimeout(function(){$('.calScroller').scrollTop($('th:contains('+hour+':):contains('+am_pm+')').position().top)}, 100);
//$('th:contains('+hour+':):contains('+am_pm+')').css('color', '#369');
						var re = new RegExp("^"+hour+":.+"+am_pm);
						if ($('th').filter(function(){return re.test($(this).html())}).position() != null)
	                		setTimeout(function(){$('.calScroller').animate({scrollTop: $('th').filter(function(){return re.test($(this).html())}).position().top}, 'slow')});
                	}
                }
                if (view.start.getTime() < right_now.getTime() - (60*60*24*7*1000) || view.start.getTime() > right_now.getTime() + (60*60*24*20*1000))
                {
                	Calendar_actions.load_teetimes(view);
                }
                
                //An attempt to be scrolled to the right spot whenever the teesheet loads... this disregards that on any day not today... starts at beginning.
                //if (view.start.toDateString() !== new Date().toDateString())
                	//$('.calScroller').scrollTop(0);
            }
    });
    calendarback = $('#calendarback').fullCalendar({
            theme:true,
            header: {
                left: '',
                center: '',
                right: ''
            },
            aspectRatio:.671,
            allDaySlot:false,
            firstDay:dow,
            droppable: (calInfo.holes > 9 ? true : false),
			defaultView:'agendaDay',
            minTime:calInfo.openhour,
            maxTime:calInfo.closehour,
            //firstHour:h, // starts at time closest to now
            slotMinutes:parseFloat(calInfo.increment1),
            defaultEventMinutes:parseFloat(calInfo.increment2),
            selectable: true,
            selectHelper: true,
            drop:function(date, allDay) {
               
                Calendar_actions.standby_to_teetime($(this)[0].id, date, 'back');
            },
            timeFormat:{
                agenda: '', // 5:00 - 6:30
                '': 'h:mmt'            // 7p
            },
            select: function(start, end, all_day) {
                Calendar_actions.event.create_empty_teetime(start, end, all_day, 'back');
            },
            eventDrop: function(event,day_delta,minute_delta,all_day,revert_func) {
                Calendar_actions.event.move_teetime(event, 'back');
            },
            eventMouseover:function(event, e, view){
                Calendar_actions.mouseover_teetime(event,e);
            },
            eventMouseout:function(event, e, view){
            	Calendar_actions.mouseout_teetime();
            },
            eventResize: function(event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view ) {
                Calendar_actions.event.move_teetime(event, 'back');
            },
            editable: true,
            events: calInfo.bcaldata,
            eventAfterRender: function(event, element, view) {
                Calendar_actions.adjust_formatting(event, element, view, 'back');
            },
            eventRender: function(event, element) {
                Calendar_actions.event.event_render(event, element, 'back');
            },
            eventColor:'transparent',
            viewDisplay: function(view) {
                ttArray = {'front':{},'back':{}};
                var view_class = ".fc-view-"+view.name;
                $('#calendar '+view_class+' .calScroller').scroll(function(e) {
                    //if (e.currentTarget) {
                    //    //$(".calScroller").scrollTop(e.currentTarget.scrollTop);
                    //}
	                var front_top = $('#calendar '+view_class+' .calScroller').scrollTop();
	                var back_top = $('#calendarback  '+view_class+'.calScroller').scrollTop();
	                if (/*Math.abs(front_top - back_top) > 1 && */calendar_over == 'calendar')
					{
						$('#calendarback '+view_class+' .calScroller').scrollTop(front_top);
	               	}
                });
                $('#calendarback '+view_class+' .calScroller').scroll(function(e) {
	                //if (e.currentTarget) {
	                    //$(".calScroller").scrollTop(e.currentTarget.scrollTop);
	                //}
	                var front_top = $('#calendar '+view_class+' .calScroller').scrollTop();
	                var back_top = $('#calendarback '+view_class+' .calScroller').scrollTop();
	                if (/*Math.abs(front_top - back_top) > 1 && */calendar_over == 'calendarback')
		            {
		                $('#calendar '+view_class+' .calScroller').scrollTop(back_top);
		           	}
	            });
                $('#calendarback .fc-view-agendaDay th.fc-col0').html('Back');
 }
    });
    //move stats into calendar
    buildStats(null);
    $('.fc-button-next').click(function(){
        calendarback.fullCalendar('next');
    });
    $('.fc-button-prev').click(function(){
        calendarback.fullCalendar('prev');
    });
    $('.fc-button-today').click(function(){
        calendarback.fullCalendar('today');
    });
});
function showBackNine(viewObj){
    showingBackNine = true;
    ttArray = {'front':{},'back':{}};
    var date = viewObj.start;
    $('#calendarback').fullCalendar('changeView', viewObj.name);
    if ($('#frontnine').width() != '50%') {
        $('#frontnine').width('50%');
        $('#backnine').width('50%');
        //$('#calendar').fullCalendar('rerenderEvents');
    }
    ttArray = {'front':{},'back':{}};
    if (calendar != null) {
        if (viewObj.name == 'agendaWeek')
        {
            $('#calendar').fullCalendar('option', 'aspectRatio', 1.35);
        }
        else if (viewObj.name == 'agendaDay') {
            $('#calendar').fullCalendar('option', 'aspectRatio', .671);
        }
    }
    $("#calendarback").show();
    $('#calendarback').fullCalendar('render');
    $('#calendar .fc-view-agendaDay th.fc-col0').html('Front');
    $('.week_label').remove();
}
function hideBackNine(viewObj){
    var date = viewObj.start;
    $('#frontnine').width('100%');
    $('#backnine').width('100%');
    //$("#calendarback").hide();
    $('#calendarback').fullCalendar('changeView', viewObj.name);
    ttArray = {'front':{},'back':{}};
    if (calendar != null) {
        if (viewObj.name == 'agendaWeek')
            $('#calendar').fullCalendar('option', 'aspectRatio', 1.35);
        else if (viewObj.name == 'agendaDay') {
            $('#calendar').fullCalendar('option', 'aspectRatio', .671);
        }
    }
    $('#calendarback').fullCalendar('render');
    if ($('#frontnine .week_label').length == 0)
	    $('#calendar').before("<div class='week_label'>Front</div>")
    if ($('#backnine .week_label').length == 0)
		$('#calendarback').before("<div class='week_label'>Back</div>")
}
function changeTeeSheet() {
    var teesheet_id = $('#teesheetMenu').val();
    $('#changets').click();
}
function buildWeather(viewObj) {
    if (!viewObj){
        var view = calendar.fullCalendar('getView').name;
        var date = calendar.fullCalendar('getDate');
    }   
    else {
        var view = viewObj.name;
        var date = viewObj.start;
    }
    var year = date.getFullYear();
    var month = date.getMonth();
    var day = date.getDate();
    var dow = date.getDay();
    var zip = '84663';
    $.ajax({
       type: "POST",
       url: "api.php",
       data: "http://xml.weather.yahoo.com/forecastrss/"+zip+"&d=5_f.xml&key=4C1m7SLV34H6.YcOZ0QKmrcwzZC7xvGIha2tSkxYK1rCagGB2SIRO3Rrnask",
       success: function(response){
           if (view == 'agendaDay') {
               
               var html = '<table cellspacing="0" style="width:100%">'+
                    '<thead>'+
                    '<tr><th>Holes</th><th>Players</th><th>Carts</th></tr>'+
                    '<tr><th>9</th><td>'+cp9+'</td><td>'+cc9+'</td></tr>'+
                    '<tr><th>18</th><td>'+cp18+'</td><td>'+cc18+'</td></tr>'+
                    '<tr class="stattotals"><th>Total</th><td>'+(parseInt(cp9)+parseInt(cp18))+'</td><td>'+(parseInt(cc9)+parseInt(cc18))+'</td></tr>'+
                    '</thead>'+
                    '</table>'+
                    '<div class="statsNote">*These #\'s represent the golfers that actually played over the number reserved.</div>';

                $('#weatherBox').html(html);
           }
           else
               $('#weatherBox').hide();
       }
     });
}
function buildStats(viewObj) {
	$(calendar).fullCalendar('run_stats');
	return;
    if (!viewObj){
        var view = calendar.fullCalendar('getView').name;
        var date = calendar.fullCalendar('getDate');
    }   
    else {
        var view = viewObj.name;
        var date = viewObj.start;
    }
    var year = date.getFullYear();
    var month = date.getMonth();
    var day = date.getDate();
    var dow = date.getDay();
    $.ajax({
       type: "POST",
       url: "index.php/teesheets/get_stats",
       data: "view="+view+"&year="+year+"&month="+month+"&day="+day+"&dow="+dow,
       dataType:'json',
       success: function(response){
           if (view == 'agendaDay') {
               $('.stats').show();
               $('#weatherBox').tabs('option', 'selected', 0);
               result = response;//$.parseJSON(response);
               var cp9 = 0, cc9 = 0, cp18 = 0, cc18 = 0;
               var tcp9 = 0, tcc9 = 0, tcp18 = 0, tcc18 = 0;

               if (result.nine) {
                   if (result.nine.cplayers != undefined)
                       cp9 = result.nine.cplayers;
                   if (result.nine.ccarts != undefined)
                       cc9 = result.nine.ccarts;
                   if (result.nine.players != undefined)
                       tcp9 = result.nine.players;
                   if (result.nine.carts != undefined)
                       tcc9 = result.nine.carts;
               }
               if (result.eighteen) {
                   if (result.eighteen.cplayers != undefined)
                       cp18 = result.eighteen.cplayers;
                   if (result.eighteen.ccarts != undefined)
                       cc18 = result.eighteen.ccarts;
                   if (result.eighteen.players != undefined)
                       tcp18 = result.eighteen.players;
                   if (result.eighteen.carts != undefined)
                       tcc18 = result.eighteen.carts;
               }
               var stats18 = '';
               if (calInfo.holes == 18) {
                   stats18 = '<tr><th>18</th><td>'+cp18+'/'+tcp18+'</td><td>'+cc18+'/'+tcc18+'</td></tr>';
               }
               var html = '<table cellspacing="0" style="width:100%">'+
                    '<thead>'+
                    '<tr><th>Holes</th><th>Players</th><th>Carts</th></tr>'+
                    '<tr><th>9</th><td>'+cp9+'/'+tcp9+'</td><td>'+cc9+'/'+tcc9+'</td></tr>'+
                    stats18+
                    '<tr class="stattotals"><th>Total</th><td>'+(parseInt(cp9)+parseInt(cp18))+'/'+(parseInt(tcp9)+parseInt(tcp18))+'</td><td>'+(parseInt(cc9)+parseInt(cc18))+'/'+(parseInt(tcc9)+parseInt(tcc18))+'</td></tr>'+
                    '</thead>'+
                    '</table>'+
                    '<div class="statsNote">*These #\'s represent only the golfers that actually played.</div>';

                $('#tabs-1').html(html);
           }
           else {
               $('.stats').hide();
               $('#weatherBox').tabs('option', 'selected', 1);
           }
       }
     });
}
function getTimeString(date) {
    if (date == null)
        return null;
    var year = String(date.getFullYear());
    var month = String(date.getMonth());
    var day = String(date.getDate());
    var hour = String(date.getHours());
    var minutes = String(date.getMinutes());
    if (month.length == 1)
        month = '0'+String(month);
    if (day.length == 1)
        day = '0'+String(day);
    if (hour.length == 1)
        hour = '0'+String(hour);
    if (minutes.length == 1)
        minutes = '0'+String(minutes);

    return String(year)+String(month)+String(day)+String(hour)+String(minutes);
}
function getDateFromTimeString(tstring) {
    tstring = String(tstring);
    var year = tstring.slice(0, 4);
    var month = tstring.slice(4, 6);
    var day = tstring.slice(6, 8);
    var hour = tstring.slice(8, 10);
    var minutes = tstring.slice(10);

    return new Date(year, month, day, hour, minutes, 00, 00);
}
String.prototype.escapeSpecialChars = function() {
    return this.replace(/\n/g, '')
               .replace(/\'/g, "")
               .replace(/\"/g, '');
};
function moveToDay(date){
	var toDate = new Date(date);
	$('#day_view').addClass('selected');
	$('#week_view').removeClass('selected');
	$('#calendar').fullCalendar( 'changeView', 'agendaDay' );
	$('#calendar').fullCalendar( 'gotoDate', toDate );
}
function trace(traced){
    var type = typeof traced;
    if (type == 'number' || type == 'string' || type == 'boolean') 
        if (window.console && console.log)
            console.log("'"+traced+"'");
    else if (type == 'object') 
        if (window.console && console.dir)
            console.dir(traced);
    else if (type == null || type == 'undefined') 
        if (window.console && console.log)
            console.log('null or undefined');
}
function logout() {
    window.location = "/logout.php";
}