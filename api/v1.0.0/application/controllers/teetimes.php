<?php
require_once ("secure_area.php");
class Teetimes extends Secure_area 
{
	function __construct()
	{
		parent::__construct('teetimes');
		$this->load->model('teesheet');
        $this->load->model('teetime');
        $this->load->helper('url');
	}
	
	function index()
	{
		$dataArray = array();
		$tsMenu = '';
        if ($this->session->userdata('user_level') == 5 || $this->session->userdata('associated_courses') != '[]') {
            if ($this->input->post('teesheetMenu'))
                $this->teesheet->switch_tee_sheet();
            //build a menu with the tee sheet id's
            $tsMenu = $this->teesheet->get_tee_sheet_menu($this->session->userdata('teesheet_id'));
        }

        $JSONData = $this->teesheet->getJSONTeeTimes();
        
	    $dataArray = array(
            'tsMenu'=>$tsMenu,
            'JSONData'=>$JSONData,
            'openhour'=> $this->session->userdata('openhour'),
            'closehour'=> $this->session->userdata('closehour'),
            'increment'=> $this->session->userdata('increment'),
            'holes'=> $this->session->userdata('holes'),
            'fntime'=> $this->session->userdata('frontnine'),
            'fdweather'=> '',//$this->teesheet->make_5_day_weather(),
            'user_level'=> $this->session->userdata('user_level'),
            'associated_courses'=>$this->session->userdata('associated_courses'),
            'user_id'=> $this->session->userdata('user_id'),
            'purchase'=>$this->session->userdata('sales')
        );
        
        $this->load->view("teetimes/manage", $dataArray);
	}

    /*
	Inserts/updates a teetime
	*/
	function save($teetime_id=false)
	{
		$this->load->library('name_parser');
		$this->load->model('item');
		$this->load->library('sale_lib');
		$go_to_register = false;
		$employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
                    $cur_teetime_info = $this->teetime->get_info($teetime_id);
		
		$type = $this->input->post('event_type');
		
		$event_data = array(
			'TTID'=>$teetime_id,
			'teesheet_id'=>$this->session->userdata('teesheet_id'),//$this->input->post('teesheet_id'),
			'start'=>$this->input->post('start'),
			'end'=>$this->input->post('end'),
			'side'=>$this->input->post('side'),
			'allDay'=>'false'
		);
		if ($type)
			$event_data['type'] = $type;
	
		if ($type == 'closed') {
			$event_data['title'] = $this->input->post('closed_title');
			$event_data['details'] = $this->input->post('closed_details');
			$event_data['holes'] = 9;
		}
		else if ($type == 'tournament' || $type == 'league' || $type == 'event') {
			$event_data['title'] = $this->input->post('event_title');
			$event_data['details'] = $this->input->post('event_details');
			$event_data['holes'] = $this->input->post('event_holes');
			$event_data['player_count'] = $this->input->post('event_players');
			$event_data['carts'] = $this->input->post('event_carts');
		}
		else if ($type == 'teetime') {
			$person_data = $person_data_2 = $person_data_3 = $person_data_4 = $person_data_5 = array();
			if ($this->input->post('save_customer_1') == 'on') {
				$np = new Name_parser();
				$np->setFullName($this->input->post('teetime_title'));
				$np->parse();
				if (!$np->notParseable()) {
					$person_data['first_name'] = $np->getFirstName();
					$person_data['last_name'] = $np->getLastName();
					$person_data['phone_number'] = $this->input->post('phone');
					$person_data['email'] = $this->input->post('email');
					$person_id = ($this->input->post('person_id') == '')?false:$this->input->post('person_id');
					$customer_data = array('course_id'=>$this->session->userdata('course_id'));
					$this->Customer->save($person_data, $customer_data, $person_id); 
				}
			}
			if ($this->input->post('save_customer_2') == 'on') {
				$np = new Name_parser();
				$np->setFullName($this->input->post('teetime_title_2'));
				$np->parse();
				if (!$np->notParseable()) {
					$person_data_2['first_name'] = $np->getFirstName();
					$person_data_2['last_name'] = $np->getLastName();
					$person_data_2['phone_number'] = $this->input->post('phone_2');
					$person_data_2['email'] = $this->input->post('email_2');
					$person_id = ($this->input->post('person_id_2') == '')?false:$this->input->post('person_id_2');
					$customer_data = array('course_id'=>$this->session->userdata('course_id'));
					$this->Customer->save($person_data_2, $customer_data, $person_id); 
				}
			}
			if ($this->input->post('save_customer_3') == 'on') {
				$np = new Name_parser();
				$np->setFullName($this->input->post('teetime_title_3'));
				$np->parse();
				if (!$np->notParseable()) {
					$person_data_3['first_name'] = $np->getFirstName();
					$person_data_3['last_name'] = $np->getLastName();
					$person_data_3['phone_number'] = $this->input->post('phone_3');
					$person_data_3['email'] = $this->input->post('email_3');
					$person_id = ($this->input->post('person_id_3') == '')?false:$this->input->post('person_id_3');
					$customer_data = array('course_id'=>$this->session->userdata('course_id'));
					$this->Customer->save($person_data_3, $customer_data, $person_id); 
				}
			}
			if ($this->input->post('save_customer_4') == 'on') {
				$np = new Name_parser();
				$np->setFullName($this->input->post('teetime_title_4'));
				$np->parse();
				if (!$np->notParseable()) {
					$person_data_4['first_name'] = $np->getFirstName();
					$person_data_4['last_name'] = $np->getLastName();
					$person_data_4['phone_number'] = $this->input->post('phone_4');
					$person_data_4['email'] = $this->input->post('email_4');
					$person_id = ($this->input->post('person_id_4') == '')?false:$this->input->post('person_id_4');
					$customer_data = array('course_id'=>$this->session->userdata('course_id'));
					$this->Customer->save($person_data_4, $customer_data, $person_id); 
				}
			}
			if ($this->input->post('save_customer_5') == 'on') {
				$np = new Name_parser();
				$np->setFullName($this->input->post('teetime_title_5'));
				$np->parse();
				if (!$np->notParseable()) {
					$person_data_5['first_name'] = $np->getFirstName();
					$person_data_5['last_name'] = $np->getLastName();
					$person_data_5['phone_number'] = $this->input->post('phone_5');
					$person_data_5['email'] = $this->input->post('email_5');
					$person_id = ($this->input->post('person_id_5') == '')?false:$this->input->post('person_id_5');
					$customer_data = array('course_id'=>$this->session->userdata('course_id'));
					$this->Customer->save($person_data_5, $customer_data, $person_id); 
				}
			}
			
			$event_data['details'] = $this->input->post('teetime_details');
			$event_data['holes'] = $this->input->post('teetime_holes');
			$event_data['player_count'] = $this->input->post('players');
			$event_data['carts'] = $this->input->post('carts');
			$event_data['person_id'] = (isset($person_data['person_id']))?$person_data['person_id']:$this->input->post('person_id');
			$event_data['person_name'] = $this->input->post('teetime_title');
			$event_data['person_id_2'] = (isset($person_data_2['person_id']))?$person_data_2['person_id']:$this->input->post('person_id_2');
			$event_data['person_name_2'] = $this->input->post('teetime_title_2');
			$event_data['person_id_3'] = (isset($person_data_3['person_id']))?$person_data_3['person_id']:$this->input->post('person_id_3');
			$event_data['person_name_3'] = $this->input->post('teetime_title_3');
			$event_data['person_id_4'] = (isset($person_data_4['person_id']))?$person_data_4['person_id']:$this->input->post('person_id_4');
			$event_data['person_name_4'] = $this->input->post('teetime_title_4');
			$event_data['person_id_5'] = (isset($person_data_5['person_id']))?$person_data_5['person_id']:$this->input->post('person_id_5');
			$event_data['person_name_5'] = $this->input->post('teetime_title_5');
			$event_data['title'] = $this->input->post('teetime_title');
			$event_data['booking_source'] = 'POS';
			$event_data['booker_id'] = $employee_id;
			
			if ($this->input->post('purchase_quantity')) {
				$go_to_register = true;
				$item_num = '1_5';
		        $cart_num = '1_4';
		        $holes = $this->input->post('holes');
		        $quantity = $this->input->post('purchase_quantity');
		        $start_time = $this->input->post('start');
		        $dow = date('w',strtotime($start_time));
		        $carts = $this->input->post('carts')-$this->input->post('paid_carts');
				$TTID = $teetime_id;
		        
		        $type = 'regular';
		        $super_twilight = $twilight = false;
		        if ($dow == 5 || $dow == 6 || $dow == 0)
		            $type = 'weekend';
		        if ($start_time >= $this->session->userdata('super_twilight_hour'))
		            $super_twilight = true;
		        if ($start_time >= $this->session->userdata('twilight_hour'))
		            $twilight = true;
		        if ($holes == 9) {
	                $cart_num = '1_1';
		            if ($super_twilight) 
						$item_num = ($type == 'weekend')?'3_3':'3_2';
		            else if ($twilight) 
						$item_num = ($type == 'weekend')?'2_3':'2_2';
		            else 
						$item_num = ($type == 'weekend')?'1_3':'1_2';
		        }
		        else if ($holes == 18) {
		        	$cart_num = '1_4';
					if ($super_twilight) 
		                $item_num = ($type == 'weekend')?'3_6':'3_5';
		            else if ($twilight) 
						$item_num = ($type == 'weekend')?'2_6':'2_5';
		            else 
						$item_num = ($type == 'weekend')?'1_6':'1_5';
		        }
		        $item_id = $this->Item->get_item_id($this->session->userdata('course_id').'_'.$item_num);
		        $cart_id = $this->Item->get_item_id($this->session->userdata('course_id').'_'.$cart_num);
		        $this->sale_lib->empty_cart();
		        $this->sale_lib->empty_basket();
		        for ($i=0;$i<$quantity;$i++) {
		            $this->sale_lib->add_item($item_id);
		            $this->sale_lib->add_item_to_basket($item_id);
		            if ($i < $carts) {
		                $this->sale_lib->add_item($cart_id);
		                $this->sale_lib->add_item_to_basket($cart_id);
		            }
		        }
				$this->sale_lib->set_teetime($TTID);
				$this->sale_lib->delete_customer();
				$this->sale_lib->delete_customer_quickbuttons();
				$this->sale_lib->set_customer_quickbutton($event_data['person_id'], $event_data['person_name']);
				$this->sale_lib->set_customer_quickbutton($event_data['person_id_2'], $event_data['person_name_2']);
				$this->sale_lib->set_customer_quickbutton($event_data['person_id_3'], $event_data['person_name_3']);
				$this->sale_lib->set_customer_quickbutton($event_data['person_id_4'], $event_data['person_name_4']);
				$this->sale_lib->set_customer_quickbutton($event_data['person_id_5'], $event_data['person_name_5']);
			}
		}
		if ($this->input->post('delete_teetime') == 1)
			$event_data['status'] = 'deleted';
		// TODO: Needs revamping ... changing to teetime	
		$json_ready_events = array();
		if($this->teetime->save($event_data,$teetime_id, $json_ready_events))
		{
			//New teetime
			if(!$teetime_id)
			{
				echo json_encode(array('success'=>true,'message'=>lang('customers_successful_adding'), 'teetimes'=>$json_ready_events, 'go_to_register'=>$go_to_register));
			}
			else //previous customer
			{
				echo json_encode(array('success'=>true,'message'=>lang('customers_successful_updating'), 'teetimes'=>$json_ready_events, 'go_to_register'=>$go_to_register));
			}
		}
		else//failure
		{	
			echo json_encode(array('success'=>false,'message'=>lang('customers_error_adding_updating'), 'teetimes'=>$json_ready_events));
		}
	}
	/*
	This deletes teetimes from the teetimes table
	*/
	function delete($teetime_to_delete = -1)
	{
		if ($teetime_to_delete == -1)
			$teetime_to_delete=$this->input->post('id');
		
		if($this->teetime->delete($teetime_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('customers_successful_deleted')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('customers_cannot_be_deleted')));
		}
	}
	/*
	This deletes teesheet from the teesheet table
	*/
	function delete_teesheet($teesheet_to_delete = -1)
	{
		if ($teesheet_to_delete == -1)
			$teesheet_to_delete=$this->input->post('id');
		
		if($this->teesheet->delete($teesheet_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('customers_successful_deleted')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('customers_cannot_be_deleted')));
		}
	}
	function getJSONTeeTimes($side) {
        echo $this->teesheet->getJSONTeeTimes('','', $side);
    }
	
	function logout()
	{
            $this->Employee->logout();
	}
    
	/*
	Loads the teesheet edit form
	*/
	function view_teesheet($teesheet_id=-1)
	{
		$data['teesheet_info']=$this->Teesheet->get_info($teesheet_id);
		echo 'Stuff';
		return;
		$this->load->view("teesheets/form",$data);
	}
	
	/*
	Loads the teetime edit form
	*/
	function view($teetime_id=-1)
	{
		$this->load->model('customer');
		$data['teesheet_holes'] = $this->session->userdata('holes');
		$data['teetime_info']=$this->teetime->get_info($teetime_id);
		$data['customer_info'] = array();
		if ($data['teetime_info']->person_id != 0 && $data['teetime_info']->person_id != '')
			$data['customer_info'][$data['teetime_info']->person_id] = $this->Customer->get_info($data['teetime_info']->person_id);
		if ($data['teetime_info']->person_id_2 != 0 && $data['teetime_info']->person_id_2 != '')
			$data['customer_info'][$data['teetime_info']->person_id_2] = $this->Customer->get_info($data['teetime_info']->person_id_2);
		if ($data['teetime_info']->person_id_3 != 0 && $data['teetime_info']->person_id_3 != '')
			$data['customer_info'][$data['teetime_info']->person_id_3] = $this->Customer->get_info($data['teetime_info']->person_id_3);
		if ($data['teetime_info']->person_id_4 != 0 && $data['teetime_info']->person_id_4 != '')
			$data['customer_info'][$data['teetime_info']->person_id_4] = $this->Customer->get_info($data['teetime_info']->person_id_4);
		if ($data['teetime_info']->person_id_5 != 0 && $data['teetime_info']->person_id_5 != '')
			$data['customer_info'][$data['teetime_info']->person_id_5] = $this->Customer->get_info($data['teetime_info']->person_id_5);
		if ($data['teetime_info']->title == '') {
			$data['checkin_text'] = 'Walk In';
			$data['save_text'] = 'Reserve';
		}
		else {
			$data['checkin_text'] = 'Check In';
			$data['save_text'] = 'Update';
		}
		if ($this->config->item('simulator'))
			$this->load->view("teetimes/form_simulator",$data);
		else
			$this->load->view("teetimes/form",$data);
	}
}
?>