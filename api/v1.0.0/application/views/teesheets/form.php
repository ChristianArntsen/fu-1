<ul id="error_message_box"></ul>
<?php
echo form_open('teesheets/save/'.$teesheet_info->teesheet_id,array('id'=>'teesheet_form'));
?>
<fieldset id="teesheet_basic_info">
<legend><?php echo lang("teesheets_basic_information"); ?></legend>

<div class="field_row clearfix">
<?php echo form_label(lang('teesheets_teesheet_title').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'name',
		'size'=>'20',
		'id'=>'name',
		'value'=>$teesheet_info->title)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('teesheets_default').':', 'default',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_radio("default",1,$teesheet_info->default); ?>
	</div>
</div>    
<div class="field_row clearfix">
<?php echo form_label(lang('teesheets_holes').':', 'holes',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php echo form_dropdown('holes', array('9'=>'9','18'=>'18'), $teesheet_info->holes, 'id="holes"');?>
	</div>
</div>
<div class="field_row clearfix">	
<?php echo form_label(lang('config_increment').':', 'increment',array('class'=>'wide')); ?>
            <div class='form_field'>
        <?php echo form_dropdown('increment', array(
                '5'=>'5 min',
                '6'=>'6 min',
                '7'=>'7 min',
                '7.5'=>'7/8 min',
                '8'=>'8 min',
                '9'=>'9 min',
                '10'=>'10 min',
                '11'=>'11 min',
                '12'=>'12 min',
                '13'=>'13 min',
                '14'=>'14 min',
                '15'=>'15 min',
                '20'=>'20 min',
                '30'=>'30 min',
				'45'=>'45 min',
                '60'=>'60 min'),
                $teesheet_info->increment);
        ?>
            <span class="settings_note">
                (How often each tee time starts)
            </span>
        </div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('teesheets_frontnine').':', 'frontnine',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php echo form_dropdown('frontnine', array(
            '140'=>'1 hour 40 min',
            '145'=>'1 hour 45 min',
            '150'=>'1 hour 50 min',
            '155'=>'1 hour 55 min',
            '200'=>'2 hours',
            '205'=>'2 hours 5 min',
            '210'=>'2 hours 10 min',
            '215'=>'2 hours 15 min',
            '220'=>'2 hours 20 min',
            '225'=>'2 hours 25 min',
            '230'=>'2 hours 30 min'),
            $teesheet_info->frontnine);
        ?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('teesheets_online_booking').':', 'online_booking',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_radio("online_booking",0,$teesheet_info->online_booking==1?FALSE:TRUE); ?>
	Off
	<?php echo form_radio("online_booking",1,$teesheet_info->online_booking==1?TRUE:FALSE); ?>
	On
	</div>
</div> 
<div class="field_row clearfix">	
        <?php echo form_label(lang('config_online_open_time').':', 'online_open_time',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_dropdown('online_open_time', array(
                        '0000'=>'12:00am',
                        '0030'=>'12:30am',
                        '0100'=>'1:00am',
                        '0130'=>'1:30am',
                        '0200'=>'2:00am',
                        '0230'=>'2:30am',
                        '0300'=>'3:00am',
                        '0330'=>'3:30am',
                        '0400'=>'4:00am',
                        '0430'=>'4:30am',
                        '0500'=>'5:00am',
                        '0530'=>'5:30am',
                        '0600'=>'6:00am',
                        '0630'=>'6:30am',
                        '0700'=>'7:00am',
                        '0730'=>'7:30am',
                        '0800'=>'8:00am',
                        '0830'=>'8:30am',
                        '0900'=>'9:00am',
                        '0930'=>'9:30am',
                        '1000'=>'10:00am',
                        '1030'=>'10:30am',
						'1100'=>'11:00am',
						'1130'=>'11:30am',
						'1200'=>'12:00pm',
						'1230'=>'12:30pm',
						'1300'=>'1:00pm',
						'1330'=>'1:30pm',
						'1400'=>'2:00pm',
						'1430'=>'2:30pm',
						'1500'=>'3:00pm',
						'1530'=>'3:30pm',
						'1600'=>'4:00pm',
                        '1630'=>'4:30pm',
                        '1700'=>'5:00pm',
                        '1730'=>'5:30pm',
                        '1800'=>'6:00pm',
                        '1830'=>'6:30pm',
                        '1900'=>'7:00pm',
                        '1930'=>'7:30pm',
                        '2000'=>'8:00pm',
                        '2030'=>'8:30pm',
                        '2100'=>'9:00pm',
                        '2130'=>'9:30pm',
                        '2200'=>'10:00pm',
                        '2230'=>'10:30pm',
						'2300'=>'11:00pm',
						'2330'=>'11:30pm',
						'2400'=>'12:00am'),
                        $teesheet_info->online_open_time);
                ?>
                    <span class="settings_note">
                        (First available online tee time)
                    </span>
                </div>
        </div>
        <div class="field_row clearfix">	
        <?php echo form_label(lang('config_online_close_time').':', 'online_close_time',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_dropdown('online_close_time', array(
                        '0000'=>'12:00am',
                        '0030'=>'12:30am',
                        '0100'=>'1:00am',
                        '0130'=>'1:30am',
                        '0200'=>'2:00am',
                        '0230'=>'2:30am',
                        '0300'=>'3:00am',
                        '0330'=>'3:30am',
                        '0400'=>'4:00am',
                        '0430'=>'4:30am',
                        '0500'=>'5:00am',
                        '0530'=>'5:30am',
                        '0600'=>'6:00am',
                        '0630'=>'6:30am',
                        '0700'=>'7:00am',
                        '0730'=>'7:30am',
                        '0800'=>'8:00am',
                        '0830'=>'8:30am',
                        '0900'=>'9:00am',
                        '0930'=>'9:30am',
                        '1000'=>'10:00am',
                        '1030'=>'10:30am',
						'1100'=>'11:00am',
						'1130'=>'11:30am',
						'1200'=>'12:00pm',
						'1230'=>'12:30pm',
						'1300'=>'1:00pm',
						'1330'=>'1:30pm',
						'1400'=>'2:00pm',
						'1430'=>'2:30pm',
						'1500'=>'3:00pm',
						'1530'=>'3:30pm',
						'1600'=>'4:00pm',
                        '1630'=>'4:30pm',
                        '1700'=>'5:00pm',
                        '1730'=>'5:30pm',
                        '1800'=>'6:00pm',
                        '1830'=>'6:30pm',
                        '1900'=>'7:00pm',
                        '1930'=>'7:30pm',
                        '2000'=>'8:00pm',
                        '2030'=>'8:30pm',
                        '2100'=>'9:00pm',
                        '2130'=>'9:30pm',
                        '2200'=>'10:00pm',
                        '2230'=>'10:30pm',
						'2300'=>'11:00pm',
						'2330'=>'11:30pm',
						'2400'=>'12:00am'),
                        $teesheet_info->online_close_time);
                ?>
                    <span class="settings_note">
                        (Last available online tee time)
                    </span>
                </div>
        </div>
<!--div class="field_row clearfix">
<?php echo form_label(lang('teesheets_days_out').':', 'days_out',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php 
			$two_week_array = array(
				'0'=>'0',
				'1'=>'1',
				'2'=>'2',
				'3'=>'3',
				'4'=>'4',
				'5'=>'5',
				'6'=>'6',
				'7'=>'7',
				'8'=>'8',
				'9'=>'9',
				'10'=>'10',
				'11'=>'11',
				'12'=>'12',
				'13'=>'13',
				'14'=>'14'				
			);
			echo form_dropdown('days_out', $two_week_array, $teesheet_info->days_out, 'id="days_out"');
		?>
	</div>
</div-->   
<div class="field_row clearfix">
<?php echo form_label(lang('teesheets_days_in_booking_window').':', 'days_in_booking_window',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php 
			echo form_dropdown('days_in_booking_window', array('7'=>'7', '10'=>'10', '14'=>'14'), $teesheet_info->days_in_booking_window, 'id="days_in_booking_window"'); 
		?>
	</div>
</div>  
<div class="field_row clearfix">
<?php echo form_label(lang('teesheets_minimum_players').':', 'minimum_players',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php 
			echo form_dropdown('minimum_players', array('4'=>'4', '3'=>'3', '2'=>'2', '1'=>'1'), $teesheet_info->minimum_players, 'id="minimum_players"'); 
		?>
	</div>
</div>  
<div class="field_row clearfix">
<?php echo form_label(lang('teesheets_limit_holes').':', 'limit_holes',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php 
			echo form_dropdown('limit_holes', array('0'=>'No Limit', '9'=>'9 only', '18'=>'18 only'), $teesheet_info->limit_holes, 'id="limit_holes"'); 
		?>
	</div>
</div>  
<div class="field_row clearfix">
<?php echo form_label(lang('teesheets_booking_carts').':', 'booking_carts',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php 
			echo form_dropdown('booking_carts', array('0'=>'Off', '1'=>'On'), $teesheet_info->booking_carts, 'id="booking_carts"'); 
		?>
	</div>
</div>  

<div id='thank_you_options_box' style='display:none'>
	<div class="field_row clearfix">
		<?php echo form_label(lang('config_send_thank_you_email') . ':', 'send_thank_you');?>
		<div class='form_field'>
			<?php echo form_checkbox("send_thank_you",1,$teesheet_info->send_thank_you);?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label(lang('config_thank_you_campaign') . ':', 'thank_you_campaign');?>
		<div class='form_field'>
			<?php echo form_input(array('name' => 'thank_you_campaign', 'id' => 'thank_you_campaign', 'value' => $thank_you_campaign));?>
			<?php echo form_hidden('thank_you_campaign_id', $teesheet_info->thank_you_campaign_id);?>
		</div>
	</div>
</div>
<script type='text/javascript'>
	$('#thank_you_options_box').expandable({
		title : 'Thank You Options:'
	});
</script>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	$( "#thank_you_campaign" ).autocomplete({
		source: '<?php echo site_url("marketing_campaigns/suggest/"); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)
		{
			event.preventDefault();
			$("#thank_you_campaign").val(ui.item.label);
			$("#thank_you_campaign_id").val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$("#teetime_title").val(ui.item.label);
		}
	});
	
	var submitting = false;
    $('#teesheet_form').validate({
		submitHandler:function(form)
		{
			if ($.trim($('#name').val()) != '')
			{
				if (submitting) return;
				submitting = true;
				$(form).mask("<?php echo lang('common_wait'); ?>");
				$(form).ajaxSubmit({
					success:function(response)
					{
						$.colorbox.close();
						post_teesheet_form_submit(response);
		                submitting = false;
					},
					dataType:'json'
				});
			}
			else
				alert('Please enter a teesheet/course name');

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li"
	});
});
</script>