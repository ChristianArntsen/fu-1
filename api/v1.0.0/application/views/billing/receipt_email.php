<!DOCTYPE html>
<html>
<head>
	<title><?php echo $receipt_title; ?></title>
</head>
<body>
	<?php
		$bgc = '#c8c8c8';
		$bc = '#606060';
		$fc = '#606060';
		$hfs = '10px';
		$hfw = 'bold';
		$cp = '2px 5px';
	?>
<div id="receipt_wrapper">
	<table cellspacing="0" cellpadding="3" style='width:600px; font-family:arial,sans-serif; font-size:12px; border-collapse:collapse'>
		<thead>
			<tr>
				<th>
					<!-- Logo -->
					<img src='http://rc.foreup.com/images/header/header_logo2.png'/>
				</th>
				<th style='text-align:left; font-size:12px; font-weight: normal'>
					<div id="company_name">ForeUP</div>
					<div id="company_address">815 West 1250 South</div>
					<div id="company_address">Orem, UT 84058</div>
					<div id="company_phone">p: 801.215.9487</div>
					<div id="company_phone">e: billing@foreup.com</div>
				</th>
				<th>
					
				</th>
				<th colspan=2 style='font-size:24px; text-align: right;'>
					Invoice
				</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' colspan=2>BILL TO</td>
				<td></td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>INVOICE NUMBER</td>
				<td style='border:1px solid <?=$bc?>;'><?=$sale_id?></td>
			</tr>
			<tr>
				<td colspan=2 rowspan=3 style='padding:<?=$cp?>; border:1px solid <?=$bc?>;'>
					<div id="company_name"><?=$customer?></div>
					<div id="company_address"><?=$billing_email?></div>
					<!--div id="company_phone"><?=$course_info->email ?></div-->
					<div>Card: <?php echo $payment_data['card_type'].' '.$payment_data['masked_account']?></div>
				</td>
				<td></td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>INVOICE DATE</td>
				<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>;'><?=$transaction_time?></td>
			</tr>
			<tr>
				<td></td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>AMOUNT PAID</td>
				<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>;'><?php echo to_currency($totals['total']); ?></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>
					<?=$status_message?>
				</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>
					<div style='height:20px;width:20px;display:block;'></div>
				</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' colspan=2>PRODUCT</td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>QTY</td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>PRICE</td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>AMOUNT</td>
			</tr>
			<?php foreach(array_reverse($items, true) as $line=>$item) { ?>
			<tr>
				<td colspan=2 style='border:1px solid <?=$bc?>;'><?php echo $item['description']; ?></td>
				<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>;'>1</td>
				<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'><?php echo to_currency($item['amount']); ?></td>
				<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'><?php echo to_currency($item['amount']); ?></td>
			</tr>
			<?php } ?>
			<tr>
				<td colspan=3></td>
				<td style='padding:<?=$cp?>; border-left:1px solid <?=$bc?>;'>Subtotal</td>
				<td style='padding:<?=$cp?>; text-align:right; border-right:1px solid <?=$bc?>;'><?php echo to_currency($totals['subtotal']); ?></td>
			</tr>
			<?php foreach($tax_rates as $tax_name => $tax_info) { ?>
			<tr>
				<td colspan=3></td>
				<td style='padding:<?=$cp?>; border-left:1px solid <?=$bc?>;'><?=$tax_name?> (<?=$tax_info['tax_rate']?>%)</td>
				<td style='padding:<?=$cp?>; text-align:right; border-right:1px solid <?=$bc?>;'><?php echo to_currency($tax_info['tax_amount']); ?></td>
			</tr>
			<?php } ?>
			<tr>
				<td colspan=3></td>
				<td style='padding:<?=$cp?>; border-left:1px solid <?=$bc?>;'>Total with tax</td>
				<td style='padding:<?=$cp?>; text-align:right; border-right:1px solid <?=$bc?>;'><?php echo to_currency($totals['total']); ?></td>
			</tr>
			<tr>
				<td colspan=3></td>
				<td style='padding:<?=$cp?>; border-left:1px solid <?=$bc?>;'>Total paid</td>
				<td style='padding:<?=$cp?>; text-align:right; border-right:1px solid <?=$bc?>;'><?php echo to_currency($totals['total']); ?></td>
			</tr>
			<tr>
				<td colspan=3></td>
				<td style='padding:<?=$cp?>; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>AMOUNT DUE</td>
				<td style='padding:<?=$cp?>; text-align:right; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>
					<div>$0.00</div>
					<div style='color:<?=$fc?>; font-size:10px;'>USD - U.S. Dollars</div>
				</td>
			</tr>

		</tbody>
	</table>
	
	
	<!--div id="receipt_header">
		<div id="company_name">ForeUP</div>
		<div id="company_address">Orem, UT</div>
		<div id="company_phone">801-555-5555</div>
		<div id="sale_receipt"><?php echo $receipt_title; ?></div>
		<div id="sale_time"><?php echo $transaction_time ?></div><br/>
	</div>
	<div id="receipt_general_info">
		<?php if(isset($customer))
		{
		?>
			<div id="customer"><?php echo lang('customers_customer').": ".$customer; ?></div>
		<?php
		}
		?>
		<div id="sale_id"><?php echo lang('sales_id').": ".$sale_id; ?></div>
		<div id="employee"><?php echo lang('employees_employee').": ".$employee; ?></div>
	</div><br/>
	<!--div><?php print_r($items)?></div-->
	<!--div>Card: <?php echo $payment_data['card_type'].' '.$payment_data['masked_account']?></div>
	<div>Status: <?php echo $payment_data['status']?></div><br/>
	<table id="receipt_items" style='width:600px;'>
	<tr>
	<th style="width:33%;"><?php echo lang('items_item'); ?></th>
	<th style="width:20%;"><?php echo lang('common_price'); ?></th>
	<th style="width:15%;text-align:center;"></th>
	<th style="width:16%;text-align:center;"></th>
	<th style="width:16%;text-align:right;"><?php echo lang('sales_total'); ?></th>
	</tr>
	<?php
	foreach(array_reverse($items, true) as $line=>$item)
	{
	?>
		<tr>
		<td><span class='long_name'><?php echo $item['description']; ?></span></td>
		<td><?php echo to_currency($item['amount']); ?></td>
		<td style='text-align:center;'></td>
		<td style='text-align:center;'></td>
		<td style='text-align:right;'><?php echo to_currency($item['amount']); ?></td>
		</tr>

	<?php
	}
	?>
	<tr>
	<td colspan="4" style='text-align:right;border-top:2px solid #000000;'><?php echo lang('sales_subtotal'); ?></td>
	<td colspan="2" style='text-align:right;border-top:2px solid #000000;'><?php echo to_currency($totals['subtotal']); ?></td>
	</tr>
	<tr>
	<td colspan="4" style='text-align:right;'><?php echo lang('sales_tax'); ?></td>
	<td colspan="2" style='text-align:right;'><?php echo to_currency($totals['taxes']); ?></td>
	</tr>
	<tr>
	<td colspan="4" style='text-align:right;'><?php echo lang('sales_total'); ?></td>
	<td colspan="2" style='text-align:right;'><?php echo to_currency($totals['total']); ?></td>
	</tr>
	</table-->
</div>
</body>
</html>