<?php
echo form_open('config/save_holidays/',array('id'=>'holidays_form'));
?>
<ul id="error_message_box"></ul>
<fieldset id="holidays_fs">
<legend><?php echo lang("config_define_holiday"); ?></legend>

<div class="field_row clearfix">	
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'new_holiday',
		'id'=>'new_holiday',
		'placeholder'=> lang('config_new_holiday'),
		'value'=>'')
	);
	echo form_input(array('name'=>'holiday_date','id'=>'holiday_date','placeholder'=>lang('config_holiday_date'),'value'=>date('Y-m-d')));
	?>
	<span class='' id='add_holiday_button'><?php echo lang('config_add_holiday')?></span>
	</div>
</div>
<div id='holidays_container'>
	<div class="holidays_title">	
<?php echo form_label(lang('config_holidays'), 'holidays'); ?>
</div>
<div class='groups_subtitle'>
<?php //echo form_label(lang('customers_click_group_edit'), 'groups'); ?>
</div>

<?php 
$first_label = true;
foreach($holidays as $holiday)
{
?>
	<div class="form_field" id='row_<?php echo $holiday['date']; ?>'>
		<?	echo '<span id="hid_'.$holiday['date'].'"><span  class="holiday_label">'.$holiday['label'].'</span><span  class="holiday_date">'.$holiday['date'].'</span></span>';
		
		//echo '<span class="delete_item" onclick="holidays.delete_holiday(\''.$holiday['date'].'\', \''.addslashes(str_replace('"', '', $holiday['label'])).'\')">Delete</span> ';
		//.$holiday['label'].' <span style="color:#ccc;">('.$holiday['date'].')</span>';
		?>
		<div class='clear'></div>
	</div>
<? 
$first_label = false;
} ?>

<div id='clear' class='clear'></div>
</div>
</fieldset>
<?php 
echo form_close();
?>
<script type='text/javascript'>
var holidays = {
	add_holiday:function() {
		var title = $('#new_holiday').val();
		var holiday_date = $('#holiday_date').val();
		title = addslashes(title.replace('"', ''));
		if (title != '')
		{
			$.ajax({
	            type: "POST",
	            url: "index.php/config/add_holiday/",
	            data: "holiday_label="+title+'&holiday_date='+holiday_date,
            	success: function(response){
	            	$('#new_holiday').val('');
	           		var html = '<div class="form_field" id="row_'+holiday_date+'">'+	
							'<span id="hid_'+holiday_date+'"><span  class="holiday_label">'+title+'</span><span class="holiday_date">'+holiday_date+'</span></span><div class="clear"></div></div>';
						
					$('#holidays_fs #clear').before($(html));
					holidays.add_holiday_click_event();
	    			$.colorbox.resize();
	            },
	            dataType:'json'
	        });
	    }
	},
	save_holiday_name:function(holiday_date) {
		var title = ($('#holiday_text_'+holiday_date).val().replace('"', ''));
		var new_date = ($('#holiday_date_'+holiday_date).val().replace('"', ''));
		$.ajax({
            type: "POST",
            url: "index.php/config/save_holiday_name/"+holiday_date,
            data: "holiday_label="+title+'&new_date='+new_date,
            success: function(response){
            	var el = $('#hid_'+holiday_date);
            	//console.log(el.html());
            	el.html('<span  class="holiday_label">'+title+'</span><span  class="holiday_date">'+new_date+'</span>');
            	el.attr('id', 'hid_'+new_date);
				holidays.add_holiday_click_event();
            },
            dataType:'json'
        });
	},
	add_holiday_click_event:function() {
		console.log('adding holiday event');
		$('.holiday_label, .holiday_date').click(function(e){
			var el = $(e.target).parent();
			var text = $(el).children('.holiday_label').html();
			var id = el.attr('id');
			var hid = id.replace('hid_','');
			var date  = $(el).children('.holiday_date').html();
			console.log('id '+id);
			el.html('<span class="delete_item" onclick="holidays.delete_holiday(\''+hid+'\', \''+addslashes(text.replace('"', ''))+'\')">Delete</span><span class="save_item" onclick="holidays.save_holiday_name(\''+hid+'\')">Save</span><input id="holiday_text_'+hid+'" value="'+text+'"/><input class="holiday_date_input" id="holiday_date_'+hid+'" value="'+date+'"/>');
			$('#holiday_date_'+hid).datepicker({dateFormat:'yy-mm-dd'});
		});
		console.log('added');
	},
	delete_holiday:function(holiday_date, title) {
		var answer = confirm('Are you sure you want to delete holiday: '+title+'?');
		if (answer) {
			$.ajax({
	            type: "POST",
	            url: "index.php/config/delete_holiday/"+holiday_date,
	            data: "",
	            success: function(response){
					$('#row_'+holiday_date).remove();
					$.colorbox.resize();
	            },
	            dataType:'json'
	        });
	    }
	}
};

//validation and submit handling
$(document).ready(function()
{
	$('#holiday_date').datepicker({dateFormat:'yy-mm-dd'});
	var submitting = false;
	$('#add_holiday_button').click(function(){
		holidays.add_holiday();
	});
    $('#holidays_form').validate({
		submitHandler:function(form)
		{
			$.colorbox.close();
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li"
	});
	holidays.add_holiday_click_event();
});
</script>