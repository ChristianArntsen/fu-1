<?php
echo form_open('teesheets/save_teetime/'.$reservation_info->TTID,array('id'=>'teetime_form'));
//echo "teesheet holes: ".$teesheet_holes.'<br/>';
//print_r($customer_info);
?>
<div id='radioset' class='teebuttons'>
	<input type='radio' value='teetime' id='type_teetime' name='event_type'  <?php echo ($reservation_info->type == 'teetime' || $reservation_info->type == '')?'checked':''; ?>><label id='teetime_label' for='type_teetime' >Reservation</label>
    <input type='radio' value='event' id='type_event' name='event_type' <?php echo ($reservation_info->type == 'event')?'checked':''; ?>><label id='event_label' for='type_event'>Event</label>
    <input type='radio' value='closed' id='type_closed' name='event_type' <?php echo ($reservation_info->type == 'closed')?'checked':''; ?>><label id='closed_label' for='type_closed'>Block</label>
</div>
<table id='closed_table'  <?php echo ($reservation_info->type == 'closed')?'':'style="display:none"'?>>
	<tbody>
		<tr>
			<td>
				<input tabindex=1 class='teetime_name' id='closed_title' name='closed_title' placeholder='Title' value='<?php echo $reservation_info->title; ?>' style='width:160px, margin-right:5px;'/>
			</td>
        </tr>
        <tr>
        	<td colspan=4>
            	<textarea tabindex=2 class='teetime_details details_box' id='closed_details' name='closed_details' placeholder='Details' style='width:600px;height:50px;'><?php echo $reservation_info->details; ?></textarea>
          	</td>
        </tr>
        <tr>
        	<td colspan=3>
        		<span class='saveButtons'>
        			<button id='closed_save'>Save</button>
            	</span>
            	<button id='closed_delete' class='deletebutton'>Delete</button>
            </td>
        </tr>
	</tbody>
</table>
<table id='event_table'  <?php echo ($reservation_info->type == 'event')?'':'style="display:none"'?>>
	<tbody>
		<tr>
			<td>
				<input class='teetime_name' id='event_title' name='event_title' placeholder='Title' value='<?php echo $reservation_info->title; ?>' style='width:160px, margin-right:5px;'/>
			</td>
        	<td>
            </td>
            <td class='player_buttons'>
            </td>
        </tr>
        <tr>
        	<td colspan=3>
            	<textarea tabindex=4 class='teetime_details details_box' id='event_details' name='event_details' placeholder='Details' style='width:600px;height:50px;'><?php echo $reservation_info->details; ?></textarea>
          	</td>
        </tr>
        <tr>
        	<td colspan=3>
        		<span class='saveButtons'>
        			<button id='event_save'>Save</button>
            	</span>
            	<button id='event_delete' class='deletebutton'>Delete</button>
            </td>
        </tr>
	</tbody>
</table>
<table id='teetime_table' <?php echo ($reservation_info->type == 'teetime' || $reservation_info->type == '')?'':'style="display:none"'?>>
	<tbody>
    	<tr id='teetimes_row_2' >
        	<td>
            	<span class='holes_buttonset'>
            		<?php $length = $reservation_info->end - $reservation_info->start; ?>
	                <input type='radio' id='teetime_holes_30' name='teetime_time' value='30' <?php echo ($length == '30' || $length == '70')?'checked':''; ?>/>
	                <label for='teetime_holes_30' id='teetime_holes_30_label'>30</label>
	                <input type='radio' id='teetime_holes_60' name='teetime_time' value='100'  <?php echo ($length == '100')?'checked':''; ?>/>
	                <label for='teetime_holes_60' id='teetime_holes_60_label'>60</label>
                    <input type='radio' id='teetime_holes_90' name='teetime_time' value='130' <?php echo ($length == '130' || $length == '170')?'checked':''; ?>/>
	                <label for='teetime_holes_90' id='teetime_holes_90_label'>90</label>
	                <input type='radio' id='teetime_holes_120' name='teetime_time' value='200'  <?php echo ($length == '200')?'checked':''; ?>/>
	                <label for='teetime_holes_120' id='teetime_holes_120_label'>120</label>
                 </span>
            </td>
            <td class='player_buttons'>
                <div id='players'>
                	<span class='players_buttonset'>
	        				<input class='teetime_players' id='teetime_players' name='players' value='<?php echo $reservation_info->player_count; ?>'/>
	        		</span>
        		</div>
            </td>
        	<td>
        	</td>
        </tr>
        <tr id='teetimes_row_3'>
        	<td colspan=3 id='teetime_people_table_holder'>
        		<table id='teetime_people_table'>
        			<tbody>
        				<tr id='teetime_people_row_1'>
				        	<td>
				        		<input tabindex=1 class='teetime_name' id='teetime_title' name='teetime_title' placeholder='Last, First Name' value='<?php echo $reservation_info->title; ?>' style='width:160px, margin-right:5px;'/>
				        		<input type='hidden' id='person_id' name='person_id' value='<?php echo $reservation_info->person_id; ?>'/>
				        	</td>
				        	<td>
				        		<input tabindex=2 type=text value='<?php echo $customer_info[$reservation_info->person_id]->email; ?>' id='email' placeholder='Email' name='email' class='ttemail teetime_email'/>
				        	</td>
				            <td>
				        		<input  tabindex=3 type=text value='<?php echo $customer_info[$reservation_info->person_id]->phone_number; ?>' placeholder='888-888-8888' id='phone' name='phone' class='ttphone teetime_phone'/>
				        	</td>
				        	<td>
				        		<label for='save_customer_1' title='Save to Customers'>
				        			<span class='checkbox_image' id='save_customer_1_image'></span>
					        		<input type='checkbox' id='save_customer_1' name='save_customer_1' style='display:none'/>
				        		</label>
				        	</td>
				        	<td>
				        		<a class='expand_players down' href='#' title='Expand'>Expand</a>
				        	</td>
				        </tr>
        				<tr  id='teetime_people_row_label' style='display:none'>
        					<td colspan="5">Additional players</td>
        				</tr>
        				<?php for ($i = 2; $i <= 5; $i++) { ?>
        				<tr id='teetime_people_row_<? echo $i?>' style='display:none'>
				        	<td>
				        		<input tabindex=<? echo (($i-1)*3)+1?> class='teetime_name' placeholder='Last, First Name' id='teetime_title_<? echo $i?>' name='teetime_title_<? echo $i?>' value='<?php $person_name = 'person_name_'.$i; echo $reservation_info->$person_name; ?>' style='width:160px, margin-right:5px;'/>
				        		<input type='hidden' id='person_id_<? echo $i?>' name='person_id_<? echo $i?>' value='<?php $person_id = 'person_id_'.$i; echo $reservation_info->$person_id; ?>'/>
				        	</td>
				            <td>
				        		<input tabindex=<? echo (($i-1)*3)+2?> type=text placeholder='Email' value='<?php echo $customer_info[$reservation_info->$person_id]->email; ?>' id='email_<? echo $i?>' name='email_<? echo $i?>' class='ttemail teetime_email'/>
				        	</td>
				        	<td>
				        		<input  tabindex=<? echo (($i-1)*3)+3?> type=text placeholder='888-888-8888' value='<?php echo $customer_info[$reservation_info->$person_id]->phone_number; ?>' id='phone_<? echo $i?>' name='phone_<? echo $i?>' class='ttphone teetime_phone'/>
				        	</td>
				        	<td>
				        		<label for='save_customer_<? echo $i?>'>
				        			<span class='checkbox_image' id='save_customer_<? echo $i?>_image'></span>
					        		<input type='checkbox' id='save_customer_<? echo $i?>' name='save_customer_<? echo $i?>' style='display:none'/>
				        		</label>
				        	</td>
				        </tr>
				        <?php } ?>
        			</tbody>
        		</table>
        	</td>
        </tr>
        <tr>
        	<td colspan=4>
            	<textarea tabindex=20 class='teetime_details details_box' id='details' placeholder='Details' name='teetime_details' style='width:600px;height:50px;'><?php echo $reservation_info->details; ?></textarea>
          	</td>
        </tr>
        <tr>
        	<td colspan=3>
        		<button id='teetime_delete' class='deletebutton'>Delete</button>
            	<span class='saveButtons'>
	            <?php if ($this->config->item('sales')) {?>
	            	<button id='teetime_purchase'>Purchase</button>
	            <?php 
	            	for ($i = 1; $i <= 5; $i++) {
	            		//if ($i >= $reservation_info->paid_player_count) 
	            	//		echo "<button id='purchase_$i'>$i</button>";
					}
	            ?>	
	            	<!--button id='purchase_1'>1</button><button id='purchase_2'>2</button><button id='purchase_3'>3</button><button id='purchase_4'>4</button><button id='purchase_5'>5</button-->
	            <?php } else { ?>
	            	<button id='teetime_checkin'><? echo $checkin_text; ?></button>
	            <?php } ?>
	            </span>
	            <button id='teetime_save' style='float:right;'><? echo $save_text; ?></button>
	        	<button id='teetime_confirm' class='confirmButton' style='display:none'>Confirm</button>
	            <button id='teetime_cancel' style='display:none' class='cancelbutton'>Cancel</button>
            </td>
        </tr>
    </tbody>
</table>
<input type='hidden' name='event_holes' value='9'/>
<input type='hidden' name='teetime_holes' value='9'/>
<input type='hidden' name='event_players' value='0'/>
<input type='hidden' name='event_carts' value='0'/>
<input type='hidden' name='carts' value='0'/>
<input type='hidden' id='purchase_quantity' name='purchase_quantity' value='0'/>
<input type='hidden' id='delete_teetime' name='delete_teetime' value='0'/>
<input type='hidden' name='start' value='<? echo $reservation_info->start; ?>'/>
<input type='hidden' name='end' value='<? echo $reservation_info->end; ?>'/>
<input type='hidden' name='teesheet_id' value='<? echo $reservation_info->teesheet_id; ?>'/>
<input type='hidden' name='side' value='<? echo $reservation_info->side; ?>'/>
<input type='hidden' name='paid_carts' value='<? echo $reservation_info->paid_carts; ?>'/>
<input type='hidden' name='paid_player_count' value='<? echo $reservation_info->paid_player_count; ?>'/>
<?php form_close(); ?>
<script type='text/javascript'>
function focus_on_title() {
	console.log('trying to focus on title: <?php echo $reservation_info->type ?>');
	<?php if ($reservation_info->type == 'teetime' || $reservation_info->type == '')
				$focus = 'teetime_title';
		  else if ($reservation_info->type == 'closed')
		  		$focus = 'closed_title';
		  else
				$focus = 'event_title';
		  ?>
    $('#<?php echo $focus;?>').focus().select();
}
$(document).ready(function(){
	var submitting = false;
    $('#teetime_form').validate({
		submitHandler:function(form)
		{
			var type = $('input[name=event_type]:checked').val();
			var proceed_with_save = true;
			if ($('#delete_teetime').val() == 1)
				proceed_with_save = confirm('Are you sure you want to delete this event?');
			else if ((type == 'teetime' && $('#teetime_title').val() == '') ||
				    ((type == 'tournament' || type == 'league' || type == 'event') && $('#event_title').val() == '') ||
				     (type == 'closed' && $('#closed_title').val() == ''))
				 {
				     alert('Please enter a name/title for this event');
				     proceed_with_save = false;
				 }
			if (proceed_with_save)
			{
				if (submitting) return;
				submitting = true;
				$(form).mask("<?php echo lang('common_wait'); ?>");
				console.log( 'about to submit form');
				$(form).ajaxSubmit({
					success:function(response)
					{
						Calendar_actions.update_teesheet(response.teetimes);
		                submitting = false;
		                if (response.go_to_register)
		                	window.location = '/index.php/sales'; 
		                $.colorbox.close();
					},
					dataType:'json'
				});
			}

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li"
	});
	
	$('#teetime_name').focus().select();
	$('.expand_players').click(function(e){
    	if ($('.expand_players').hasClass('down')) 
    		$('.expand_players').removeClass('down').addClass('up');
    	else
    		$('.expand_players').removeClass('up').addClass('down');
    	e.preventDefault();
    	$('#teetime_people_row_label').toggle();
    	$('#teetime_people_row_2').toggle();
    	$('#teetime_people_row_3').toggle();
    	$('#teetime_people_row_4').toggle();
    	$('#teetime_people_row_5').toggle();
    	//var x = $('#teetime_form').height();
	    //var y = $('#teetime_form').width();
	
	    $.colorbox.resize();
    }); 
    for (var q = 1; q <= 5; q++) {
		// Save customer event listeners
	    $("#save_customer_"+q).click(function() {
	    	if ($(this).attr('checked'))
	        	$(this).prev().addClass('checked');
	        else
	   	    	$(this).prev().removeClass('checked');
	    });      
	}
	$('#teetime_purchase').click(function () {$('#purchase_quantity').val($('input[name=teetime_time]:checked', '#teetime_form').val());});
	$('#purchase_2').click(function () {$('#purchase_quantity').val(2);});
	$('#purchase_3').click(function () {$('#purchase_quantity').val(3);});
	$('#purchase_4').click(function () {$('#purchase_quantity').val(4);});
	$('#purchase_5').click(function () {$('#purchase_quantity').val(5);});
	$('#teetime_delete').click(function(){$('#delete_teetime').val(1);});
	$('#event_delete').click(function(){$('#delete_teetime').val(1);});
	$('#closed_delete').click(function(){$('#delete_teetime').val(1);});
	// Button icons and styles
	$('#paid_carts').button();
	$('#paid_players').button();
	$('#teetime_save').button({icons:{primary:'ui-icon-clock'}});
    $('#event_save').button({icons:{primary:'ui-icon-clock'}});
    $('#closed_save').button({icons:{primary:'ui-icon-clock'}});
    $('#teetime_delete').button({icons:{primary:'ui-icon-close'}});
    $('#event_delete').button({icons:{primary:'ui-icon-close'}});
    $('#closed_delete').button({icons:{primary:'ui-icon-close'}});
    $('#teetime_purchase').button().next().parent().buttonset();
    $('#teetime_checkin').button({icons:{primary:'ui-icon-check'}});
    $('#teetime_holes_9').button({icons:{primary:'ui-icon-flag'}});
    $('#teetime_holes_30').button({icons:{primary:'ui-icon-clock'}});
    $('#event_holes_9').button({icons:{primary:'ui-icon-flag'}});
    $('#teetime_confirm').button({icons:{primary:'ui-icon-check'}});
    $('#teetime_cancel').button({icons:{primary:'ui-icon-close'}});
    $('.teebuttons').buttonset();
    $('.holes_buttonset').buttonset();
    $('#players_0').button({icons:{primary:'ui-icon-person'}});
    $('#carts_0').button({icons:{primary:'ui-icon-print'}});
    $('.players_buttonset').buttonset();
    $('.carts_buttonset').buttonset();
    $('#players_0_label').removeClass('ui-button-disabled').removeClass('ui-state-disabled');
    $('#teetime_purchase').removeClass('ui-button-disabled').removeClass('ui-state-disabled');
    $('#players_0_label').children().css('color','#e9f4fe');

    // Teetime name settings
	$( "#teetime_title" ).autocomplete({
		source: '<?php echo site_url("sales/customer_search/last_name"); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui) {
			event.preventDefault();
			$("#teetime_title").val(ui.item.label);
			(ui.item.email != '')?$('#email').val(ui.item.email).removeClass('teetime_email'):'';
			(ui.item.phone_number != '')?$('#phone').val(ui.item.phone_number).removeClass('teetime_phone'):'';
			$("#person_id").val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$("#teetime_title").val(ui.item.label);
		}
	});
	$( "#teetime_title_2" ).autocomplete({
		source: '<?php echo site_url("sales/customer_search/last_name"); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui) {
			event.preventDefault();
			$("#teetime_title_2").val(ui.item.label);
			(ui.item.email != '')?$('#email_2').val(ui.item.email).removeClass('teetime_email'):'';
			(ui.item.phone_number != '')?$('#phone_2').val(ui.item.phone_number).removeClass('teetime_phone'):'';
			$("#person_id_2").val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$("#teetime_title_2").val(ui.item.label);
		}
	});
	$( "#teetime_title_3" ).autocomplete({
		source: '<?php echo site_url("sales/customer_search/last_name"); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui) {
			event.preventDefault();
			$("#teetime_title_3").val(ui.item.label);
			(ui.item.email != '')?$('#email_3').val(ui.item.email).removeClass('teetime_email'):'';
			(ui.item.phone_number != '')?$('#phone_3').val(ui.item.phone_number).removeClass('teetime_phone'):'';
			$("#person_id_3").val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$("#teetime_title_3").val(ui.item.label);
		}
	});
	$( "#teetime_title_4" ).autocomplete({
		source: '<?php echo site_url("sales/customer_search/last_name"); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui) {
			event.preventDefault();
			$("#teetime_title_4").val(ui.item.label);
			(ui.item.email != '')?$('#email_4').val(ui.item.email).removeClass('teetime_email'):'';
			(ui.item.phone_number != '')?$('#phone_4').val(ui.item.phone_number).removeClass('teetime_phone'):'';
			$("#person_id_4").val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$("#teetime_title_4").val(ui.item.label);
		}
	});
	$( "#teetime_title_5" ).autocomplete({
		source: '<?php echo site_url("sales/customer_search/last_name"); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui) {
			event.preventDefault();
			$("#teetime_title_5").val(ui.item.label);
			(ui.item.email != '')?$('#email_5').val(ui.item.email).removeClass('teetime_email'):'';
			(ui.item.phone_number != '')?$('#phone_5').val(ui.item.phone_number).removeClass('teetime_phone'):'';
			$("#person_id_5").val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$("#teetime_title_5").val(ui.item.label);
		}
	});
	// Phone number events
    $('#phone').keypress(function (e) {
    	$('#save_customer_1').attr('checked', true);
    	$('#save_customer_1').prev().addClass('checked');
    });
    $( "#phone" ).autocomplete({
		source: '<?php echo site_url("sales/customer_search/phone_number"); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)	{
			event.preventDefault();
			(ui.item.name != '')?$("#teetime_title").val(ui.item.name).removeClass('teetime_name'):'';
			(ui.item.email != '')?$('#email').val(ui.item.email).removeClass('teetime_email'):'';
			$('#phone').val(ui.item.label);
			$("#person_id").val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#phone').val(ui.item.label);
		}
		
	});
    $('#phone_2').keypress(function (e) {
    	$('#save_customer_2').attr('checked', true);
    	$('#save_customer_2').prev().addClass('checked');
    });
    $( "#phone_2" ).autocomplete({
		source: '<?php echo site_url("sales/customer_search/phone_number"); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)	{
			event.preventDefault();
			(ui.item.name != '')?$("#teetime_title_2").val(ui.item.name).removeClass('teetime_name'):'';
			(ui.item.email != '')?$('#email_2').val(ui.item.email).removeClass('teetime_email'):'';
			$('#phone_2').val(ui.item.label);
			$("#person_id_2").val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#phone_2').val(ui.item.label);
		}
		
	});
    $('#phone_3').keypress(function (e) {
    	$('#save_customer_3').attr('checked', true);
    	$('#save_customer_3').prev().addClass('checked');
    });
    $( "#phone_3" ).autocomplete({
		source: '<?php echo site_url("sales/customer_search/phone_number"); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)	{
			event.preventDefault();
			(ui.item.name != '')?$("#teetime_title_3").val(ui.item.name).removeClass('teetime_name'):'';
			(ui.item.email != '')?$('#email_3').val(ui.item.email).removeClass('teetime_email'):'';
			$('#phone_3').val(ui.item.label);
			$("#person_id_3").val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#phone_3').val(ui.item.label);
		}
		
	});
    $('#phone_4').keypress(function (e) {
    	$('#save_customer_4').attr('checked', true);
    	$('#save_customer_4').prev().addClass('checked');
    });
    $( "#phone_4" ).autocomplete({
		source: '<?php echo site_url("sales/customer_search/phone_number"); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)	{
			event.preventDefault();
			(ui.item.name != '')?$("#teetime_title_4").val(ui.item.name).removeClass('teetime_name'):'';
			(ui.item.email != '')?$('#email_4').val(ui.item.email).removeClass('teetime_email'):'';
			$('#phone_4').val(ui.item.label);
			$("#person_id_4").val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#phone_4').val(ui.item.label);
		}
		
	});
    $('#phone_5').keypress(function (e) {
    	$('#save_customer_5').attr('checked', true);
    	$('#save_customer_5').prev().addClass('checked');
    });
    $( "#phone_5" ).autocomplete({
		source: '<?php echo site_url("sales/customer_search/phone_number"); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)	{
			event.preventDefault();
			(ui.item.name != '')?$("#teetime_title_5").val(ui.item.name).removeClass('teetime_name'):'';
			(ui.item.email != '')?$('#email_5').val(ui.item.email).removeClass('teetime_email'):'';
			$('#phone_5').val(ui.item.label);
			$("#person_id_5").val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#phone_5').val(ui.item.label);
		}
		
	});
	// Email
	$('#email').keypress(function (e) {
    	$('#save_customer_1').attr('checked', true);
    	$('#save_customer_1').prev().addClass('checked');
    });
    $('#email_2').keypress(function (e) {
    	$('#save_customer_2').attr('checked', true);
    	$('#save_customer_2').prev().addClass('checked');
    });
    $('#email_3').keypress(function (e) {
    	$('#save_customer_3').attr('checked', true);
    	$('#save_customer_3').prev().addClass('checked');
    });
    $('#email_4').keypress(function (e) {
    	$('#save_customer_4').attr('checked', true);
    	$('#save_customer_4').prev().addClass('checked');
    });
    $('#email_5').keypress(function (e) {
    	$('#save_customer_5').attr('checked', true);
    	$('#save_customer_5').prev().addClass('checked');
    });
    
    // CODE BELOW REQUIRES REVIEW
	
    $('#type_teetime').click(function(){
    	$('#teetime_table').show();
    	$('#closed_table').hide();
    	$('#event_table').hide();
    	$.colorbox.resize();
    });
    $('#type_tournament').click(function(){
    	$('#teetime_table').hide();
    	$('#closed_table').hide();
    	$('#event_table').show();
    	$.colorbox.resize();
    });
    $('#type_league').click(function(){
    	$('#teetime_table').hide();
    	$('#closed_table').hide();
    	$('#event_table').show();
    	$.colorbox.resize();
    });
    $('#type_event').click(function(){
    	$('#teetime_table').hide();
    	$('#closed_table').hide();
    	$('#event_table').show();
    	$.colorbox.resize();
    });
    $('#type_closed').click(function(){
    	$('#teetime_table').hide();
    	$('#closed_table').show();
    	$('#event_table').hide();
    	$.colorbox.resize();
    });

	$('.teetime_name').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_name');
	});
    $('.teetime_phone').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_phone');
	});
	$('.teetime_email').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_email');
	});
    $('.teetime_details').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_details');
	});
    
    
});
</script>