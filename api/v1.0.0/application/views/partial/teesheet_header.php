<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- manifest='/offline.appcache'-->
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<base href="<?php echo base_url();?>" />
	<title><?php echo $this->config->item('name').' -- '.lang('common_powered_by').' ForeUP' ?></title>
	<link rel="icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/phppos.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/menubar.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/general.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/popupbox.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/register.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/receipt.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/reports.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/tables.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/thickbox.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/colorbox.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/datepicker.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/editsale.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/footer.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/css3.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/jquery.loadmask.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/contactable.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/ui-lightness/jquery-ui-1.8.14.custom.css?<?php echo APPLICATION_VERSION; ?>" />	
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/phppos_print.css?<?php echo APPLICATION_VERSION; ?>"  media="print"/>
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/redmond.css?<?php echo APPLICATION_VERSION; ?>" />
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/fullcalendar.css?<?php echo APPLICATION_VERSION; ?>" />
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/fullcalendar.print.css?<?php echo APPLICATION_VERSION; ?>" media="print"/>
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/styles.css?<?php echo APPLICATION_VERSION; ?>" />
    <!--link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/fullcalendar.print.css?<?php echo APPLICATION_VERSION; ?>" /-->
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/jquery.qtip.min2.css?<?php echo APPLICATION_VERSION; ?>" />

	<script type="text/javascript">
	var SITE_URL= "<?php echo site_url(); ?>";
	</script>
	<script src="<?php echo base_url();?>js/jquery-1.5.2.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery-ui.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.color.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.form.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.tablesorter.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.validate.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.contactable.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.qtip.min2.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/jquery.xml2json.pack.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/jquery.maskedinput.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/thickbox.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.colorbox.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/common.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/manage_tables.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/date.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/datepicker.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.loadmask.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<!--script src="<?php echo base_url();?>js/jquery.widget.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script-->
    <script src="<?php echo base_url();?>js/javascript.js?<?php echo APPLICATION_VERSION.time();?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/fullcalendar.js?<?php echo APPLICATION_VERSION.time(); ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/jquery.contextMenu.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script type="text/javascript">
	Date.format = '<?php echo get_js_date_format(); ?>';
	/*$(document).ready(function () {//debugger;
	    $(document.body).bind("online", checkNetworkStatus);
	    $(document.body).bind("offline", checkNetworkStatus);
	    checkNetworkStatus();
	});
	function checkNetworkStatus() {
	    if (navigator.onLine) {
	        // Just because the browser says we're online doesn't mean we're online. The browser lies.
	        // Check to see if we are really online by making a call for a static JSON resource on
	        // the originating Web site. If we can get to it, we're online. If not, assume we're
	        // offline.
	        $.ajaxSetup({
	            async: true,
	            cache: false,
	            context: $("#status"),
	            dataType: "json",
	            error: function (req, status, ex) {
	                console.log("Error: " + ex);
	                // We might not be technically "offline" if the error is not a timeout, but
	                // otherwise we're getting some sort of error when we shouldn't, so we're
	                // going to treat it as if we're offline.
	                // Note: This might not be totally correct if the error is because the
	                // manifest is ill-formed.
	                showNetworkStatus(false);
	            },
	            success: function (data, status, req) {
	                showNetworkStatus(true);
	            },
	            timeout: 5000,
	            type: "GET",
	            url: "/js/ping.js"
	        });
	        $.ajax();
	    }
	    else {
	        showNetworkStatus(false);
	    }
	}
	
	function showNetworkStatus(online) {
	    if (online) {
	        $("#online_status").html("Online");
	    }
	    else {
	        $("#online_status").html("Offline");
	    }
	
	    console.log("Online status: " + online);
	}*/
	</script>
<style type="text/css">
html {
    overflow: auto;
}
</style>

</head>
<body>
    <?php //print_r($user_info);?>
    <?php //print_r($allowed_modules->result_array());?>
<div id="menubar_background">	
<div id="menubar">
	<table id="menubar_container">

		<tr id="menubar_navigation">
			<td class="menu_item menu_item_home">
				<a href="<?php echo site_url(); ?>"><?php echo img(
				array(
					'src' => $this->Appconfig->get_logo_image()
				)); ?></a>
			</td>
			<?php
			foreach($allowed_modules->result() as $module)
			{
                        if ($this->permissions->is_super_admin() || $this->permissions->course_has_module($module->module_id))
                        {

			?>
			<td class="menu_item menu_item_<?php echo $module->module_id;?>">
				<a href="<?php echo site_url("$module->module_id");?>"><?php echo lang("module_".$module->module_id) ?></a>
			</td>
			<?php
                        }
			}
			?>
		</tr>

	</table>
</div>
</div>
<div id="content_area_wrapper">
<div id="content_area">
