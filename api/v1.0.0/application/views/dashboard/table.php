<?php if ($test) { ?>
<html>
  <head>
	<script src="<?php echo base_url();?>js/jquery-1.3.2.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery-ui.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.form.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/datepicker.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="/js/highcharts.src.js"></script>
	<script src="/js/exporting.src.js"></script>
	<script src="<?php echo base_url();?>js/jquery.validate.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/jquery.loadmask.css?<?php echo APPLICATION_VERSION; ?>" />
	<script src="<?php echo base_url();?>js/jquery.loadmask.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script type="text/javascript" src="/js/gray.js"></script>
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/redmond.css?<?php echo APPLICATION_VERSION; ?>" />
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/datepicker.css?<?php echo APPLICATION_VERSION; ?>" />
<?php } ?>
	
    <script type="text/javascript">
var <?=$container_name?>_chart;
var <?=$container_name_2?>_chart;
    
$(function () {
    var type = '<?=$type?>';
    var graph_type = '<?=$graph_type?>';
    var start = '<?=$start?>';
    var end = '<?=$end?>';
    
    $(document).ready(function() {
        $.ajax({
           type: "POST",
           url: "/index.php/dashboard/fetch_<?=$data_source?>_data/",
           data: 'type='+type+'&start='+start+'&end='+end,
           success: function(response){
	           	console.dir(response);
	           	console.log('gf data');
	           	console.dir(response.green_fees.data);
	           	<?=$container_name?>_chart = new Highcharts.Chart({
		            chart: {
		                renderTo: '<?=$container_name?>',
		                plotBackgroundColor: null,
		                plotBorderWidth: null,
		                plotShadow: false
		            },
		            title: {
		                text: response.green_fees.title
		            },
		            tooltip: {
		        	    pointFormat: '{series.name}: <b>{point.percentage}%</b>',
		            	percentageDecimals: 1
		            },
		            plotOptions: {
		                pie: {
		                    allowPointSelect: true,
		                    cursor: 'pointer',
		                    dataLabels: {
		                        enabled: true,
		                        color: '#ffffff',
		                        connectorColor: '#ffffff',
		                        formatter: function() {
		                            return '<b>'+ this.point.name +'</b>:<br/>'+ this.percentage.toFixed(2) +' %';
		                        }
		                    }
		                }
		            },
		            series: [{
		                type: 'pie',
		                name: 'Browser share',
		                data: response.green_fees.data
		            }]
		        });
		        <?=$container_name_2?>_chart = new Highcharts.Chart({
		            chart: {
		                renderTo: '<?=$container_name_2?>',
		                plotBackgroundColor: null,
		                plotBorderWidth: null,
		                plotShadow: false
		            },
		            title: {
		                text: response.carts.title
		            },
		            tooltip: {
		        	    pointFormat: '{series.name}: <b>{point.percentage}%</b>',
		            	percentageDecimals: 1
		            },
		            plotOptions: {
		                pie: {
		                    allowPointSelect: true,
		                    cursor: 'pointer',
		                    dataLabels: {
		                        enabled: true,
		                        color: '#ffffff',
		                        connectorColor: '#ffffff',
		                        formatter: function() {
		                            return '<b>'+ this.point.name +'</b>:<br/>'+ this.percentage.toFixed(2) +' %';
		                        }
		                    }
		                }
		            },
		            series: [{
		                type: 'pie',
		                name: 'Browser share',
		                data: response.carts.data
		            }]
		        });
	        },
            dataType:'json'
	    });
    });
    var submitting = false;
    $('#<?=$container_name?>_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
			success:function(response)
			{
				<?=$container_name?>_chart.changeData(response);
				submitting = false;
				$(form).unmask();
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
   		},
		messages: 
		{
		}
	});
    $('#<?=$container_name?>_start').datepicker({ dateFormat: "yy-mm-dd", defaultDate: '<?=$start?>'});
    $('#<?=$container_name?>_end').datepicker({ dateFormat: "yy-mm-dd", defaultDate: '<?=$end?>'});
 
});
		</script>

  </head>
  <body>
  	<style>
		.dashboard_filters {
			background:#555555;
			-webkit-border-radius:14px;
			border-radius:14px;
			margin-bottom:5px;
			padding:10px 0px 3px 0px;
		}
		.dashboard_widget:hover .dashboard_filters {
		}
		.dashboard_filters_inner {
			padding:0px 10px 0px 10px;
			float:right;
		}
		.filter_date {
			margin:0px 5px;
		}
	</style>
	<div class='dashboard_widget' style="width: <?=$width?>px;">
		<div id="<?=$container_name?>_filters" class='dashboard_filters' style="width: <?=$width?>px; height: 30px;">
			<div class='dashboard_filters_inner'>
				<?php
				echo form_open("dashboard/fetch_{$data_source}_data",array('id'=>$container_name.'_form'));
				echo form_input(array(
					'name'=>'start',
					'id'=>$container_name.'_start',
					'class'=>'filter_date',
					'placeholder'=>'Start date',
					'value'=>$start));
				echo form_input(array(
					'name'=>'end',
					'id'=>$container_name.'_end',
					'class'=>'filter_date',
					'placeholder'=>'End date',
					'value'=>$end));
				echo form_dropdown('type',array('hour'=>'Hourly','day'=>'Daily','dayname'=>'Day of Week','week'=>'Weekly','month'=>'Monthly','year'=>'Yearly'), $type);
				echo form_submit(array(
					'name'=>'submit',
					'id'=>$container_name.'_submit',
					'value'=>lang('common_submit'),
					'class'=>'submit_button float_right')
				);
				?>
				</form>
			</div>
		</div>
		<div id="<?=$container_name?>" style="width: <?=$width/2?>px; height: <?=$height?>px;"></div>
		<div id="<?=$container_name_2?>" style="width: <?=$width/2?>px; height: <?=$height?>px;"></div>
	</div>
<?php if ($test) { ?>
  </body>
</html>
<?php } ?>