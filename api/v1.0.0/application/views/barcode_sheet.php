<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo lang('items_generate_barcodes'); ?></title>
	<style>
		img {
			margin-top:11px;
		}
	</style>
</head>
<body>
<table width='100%' cellpadding='0'>
<tr>
<?php 
$count = 0;
foreach($items as $item)
{
	$barcode = $item['id'];
	$text = urlencode($item['name']);
	//$text = $item['unit_price'];
	$line_limit = $sheet_style == 5267?4:2;
	$column_width = $sheet_style == 5267?184:500;
	$font_size = $sheet_style == 5267?10:16;
	$label_font_size = $sheet_style == 5267?7:10;
	$name = '';//'<div>'.character_limiter($this->Appconfig->get('name'), 22).'</div>';//."<br />";
	$padding = '';
	if ($count % $line_limit ==0)
	{
		if ($count!=0)
			echo '</tr><tr>';
		$padding = 'padding-left:10px';
		$margin = 'margin-left:100px';
	}
	else if (($count % $line_limit == $line_limit - 1))
	{
		$padding = 'padding-right:10px';
		$margin = 'margin-right:100px';
	}
	echo "<td align='center' style='font-size:{$font_size}px;'><div $margin style='width:{$column_width}px; '>".$name."<img $margin src='".site_url('barcode')."?barcode=$barcode&text=$text&scale=$scale&thickness=$thickness&font_size=$label_font_size' /></div></td>";
	$count++;
}
?>
</tr>
</table>
</body>
</html>
