<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event extends CI_Model
{
  public function get_all()
  {
    $this->db->from('events');
		$this->db->where('deleted', 0);

    return $this->db->get();
  }

  public function save(&$event_data, $event_id)
  {
    if($event_id != -1)
    {
      $event_data['id'] = $event_id;
      $this->db->where('id', $event_id);
      return $this->db->update('events', $event_data);
    }

    if($this->db->insert('events',$event_data))
    {
      $event_data['id'] = $this->db->insert_id();
      return true;
    }

    return false;
  }

  function delete($event_id)
	{		
    $this->db->where("id = '$event_id'");
		return $this->db->update('events', array('deleted' => 1));
	}
}