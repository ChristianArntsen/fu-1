<?php
class Sendhub_account extends CI_Model
{
	/*
	Determines if a given phone number is linked to a send hub account
	*/
	function exists( $person_id )
	{		
// 		
// 				
		// $this->db->from('users');
		// $this->db->where('person_id',$person_id);
		// $this->db->where('deleted != 1');
		//$this->db->limit(1);
		// $query = $this->db->get();
		// // echo $this->db->last_query();
		// return ($query->num_rows()==1);
	}

	/*
	Returns all sedhub accounts
	*/
	function get_all($limit=10000, $offset=0)
	{
// 		
        // $this->db->from('marketing_sendhub_accounts');
		// $this->db->where("deleted = 0 $course_id");
		// $this->db->order_by("send_date", "desc");
		// $this->db->limit($limit);
		// $this->db->offset($offset);
		// return $this->db->get();
	}
	
	function count_all()
	{
		// $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        // $this->db->from('marketing_sendhub_accounts');
		// $this->db->where("deleted = 0 $course_id");
		// return $this->db->count_all_results();
	}

	/*
	Gets information about a particular phone
	*/
	function get_info($phone_number)
	{		
        $this->db->from('sendhub_accounts');
		$this->db->where('phone_number',$phone_number);				
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $sendhub_account_id is NOT an sendhub_account
			$sendhub_account = new stdClass();

			//Get all the fields from sendhub_accounts table
			$fields = $this->db->list_fields('sendhub_accounts');

			foreach ($fields as $field)
			{
				$sendhub_account->$field='';
			}

			return $sendhub_account;
		}
	}

	/*
	Gets information about multiple sendhub_accounts
	*/
	function get_multiple_info($sendhub_account_ids)
	{
		// $this->db->from('marketing_sendhub_accounts');
		// $this->db->where_in('sendhub_account_id',$sendhub_account_ids);
		// $this->db->where('deleted',0);
		// $this->db->order_by("send_date", "desc");
		// return $this->db->get();
	}
	
	function send_reminder_text($text_data)
	{		 	  		
		$response;
		
		if ($text_data['sendhub_id'] == '') {					
			$response = $this->create_sendhub_account($text_data);	
		}

		$message = $this->create_message_for_teetime($text_data);
		
		if ($text_data['sendhub_id'] != '') {
			$response = $this->sendhub->send_message($message, $text_data['sendhub_id']);
		}		
	}	
	
	function create_message_for_teetime($text_data)
	{
		$teetime = $text_data['teetime'];
		$player_count = $teetime['player_count'];
		$course_name = $text_data['course_name'];
		$course_phone = $text_data['course_phone'];
		$time = date('g:ia',strtotime($teetime['start']));				
		$message = "Tee Time Reservation for $player_count at $course_name tomorrow @ $time. $course_phone";
		
		return $message;
		
	}	
	
	function send_group_text($campaign_id, array $numbers, $message, $course_id = false)
	{		
		$error_msg = array();

		$contact_ids = array();
		
		log_message("debug", "sendhub_account send_group_text - starting recipient count ".count($numbers));
		foreach ($numbers as $key => $contact) {			
			if ($contact['sendhub_id'] != '' || $this->create_sendhub_account($contact) != false) {				
				$contact_ids[] = $contact['sendhub_id'];
			}						
		}		
		log_message("debug", "sendhub_account campaign  $campaign_id - recipient count ".count($contact_ids));
		$response = $this->sendhub->send_message($message, $contact_ids);
		log_message("error", "sendhub_account response ".json_encode($response));
	    $this->db->where('campaign_id', $campaign_id);
	    $this->db->update('marketing_campaigns', array('is_sent'=>1, 'status'=>'sent', 'send_date'=> date('Y-m-d H:i:s')));		
		$this->load->model("Communication");
		$this->Communication->record('Text', count($contact_ids), $this->config->item('phone'), $campaign_id, $course_id);		
	}
	
	function create_sendhub_account(&$text_data)
	{
		$phone_number = trim($text_data['person_phone'] != '' ? $text_data['person_phone'] : $text_data['phone_number']);						
		$phone_number = preg_replace('/(\W*)/', '', $phone_number);		
		if (strlen($phone_number) != 10) return false;
		
		$response =  $this->sendhub->add_contact($text_data['first_name'], $phone_number);
		log_message("debug", "sendhub_account create_sendhub_account - add_contact ".$phone_number.' response: '.json_encode($response));
		if (!$response->id) return false;
		
		$data = array(
			'phone_number'=> $phone_number,
			'sendhub_id'=>$response->id,
			'text_reminder_unsubscribe'=>0
		);
		
		$text_data['sendhub_id'] = $response->id;
		
		$this->save($data);
		return $response;
	}
	
	/*
	Inserts or updates a sendhub_account
	*/
	function save(&$sendhub_account_data)
	{						
		$phone_number = $sendhub_account_data['phone_number'];
		//adapted from http://snipplr.com/view/7862/
		$phone_number = preg_replace('/(\W*)/', '', $phone_number);
		$sendhub_account_data['phone_number'] = $phone_number;
							
		return $this->db->insert('sendhub_accounts',$sendhub_account_data);	
	}

	/*
	Updates multiple sendhub_accounts at once
	*/
	function update_multiple($sendhub_account_data,$sendhub_account_ids)
	{
		// $this->db->where_in('sendhub_account_id',$sendhub_account_ids);
		// return $this->db->update('marketing_sendhub_accounts',$sendhub_account_data);
	}

	/*
	Deletes one sendhub_account
	*/
	function delete($id)
	{		
        if ($this->db->delete('sendhub_accounts', array('sendhub_id'=>$id))) {
        	echo "deleting contact from sendhub server";	
        	$this->sendhub->delete_contact($id);
        };		
	}

	/*
	Deletes a list of sendhub_accounts
	*/
	function delete_list($sendhub_account_ids)
	{
		// $this->db->where('course_id', $this->session->userdata('course_id'));
        // $this->db->where_in('sendhub_account_id',$sendhub_account_ids);
		// return $this->db->update('marketing_sendhub_accounts', array('deleted' => 1));
 	}

 	/*
	Get search suggestions to find sendhub_accounts
	*/
	function get_search_suggestions($search,$limit=25)
	{
	}

	/*
	Preform a search on sendhub_accounts
	*/
	function search($search, $limit=20, $offset = 0)
	{
	}

}
