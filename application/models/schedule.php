<?php
class Schedule extends CI_Model 
{
	// Store course and teesheet information	
    private $course_info = '';
    private $teesheet_info = '';
	private $schedule_info = '';
	private $teetime_prices = '';
	private $schedule_prices = '';
	private $teesheet_id = '';
	private $schedule_id = '';
	private $tracks = '';

	// Start date and date range
	private $selected_date = '';
	private $offset_date = '';
    private $selected_year = '';
    private $selected_month = '';
    private $selected_day = '';
	private $days_out = 7;
	
	// Start and end times
	private $lead_hours = 1;
	private $booking_delay = '';
	private $now = '';
	private $start_time = '';
	private $end_time = '';
	
	// Filters
	private $filter_method = '';
	private $hole_minimum = 9;
	private $filter_holes = '';
	private $filter_index = 1;
    private $player_minimum = 1;
    private $filter_available_spots = 1;
	private $filter_deals = false;
    private $filter_price_range = array('min'=> 0, 'max'=>0);
    private $filter_date_range = array('start'=>'', 'end'=>'');
	private $filter_time_preset = 'morning';
    private $filter_time_range =  array('start'=>'0000', 'end'=>'1100');
    private $filter_time_range_presets = array(
        'morning'=> array('start'=>'0000','end'=>'1100'),
        'midday'=>  array('start'=>'1000','end'=>'1500'),
        'evening'=> array('start'=>'1400','end'=>'2400'),
        'full_day'=>array('start'=>'0000','end'=>'2400')
        );
    
	// Data to return
	private $booked_times = '';
	private $available_teetimes_array = array();
	private $available_teetimes_html = array();
	private $available_teetimes_json = array();

    private $month_length = array ('1'=>31,'2_0'=>28,'2_1'=>29,'3'=>31,'4'=>30,'5'=>31,'6'=>30,'7'=>31,'8'=>31,'9'=>30,'10'=>31,'11'=>30,'12'=>31);
	/*
	Determines if a given teesheet_id is a teesheet
	*/
	function exists($schedule_id)
	{
		$this->db->from('schedules');
		$this->db->where('schedule_id',$schedule_id);
		$this->db->where('deleted',0);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	function count_all()
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
		$this->db->from('schedules');
		$this->db->where("deleted = 0 $course_id");

		return $this->db->count_all_results();
	}
	
	/*
	Returns all the teesheets
	*/
	function get_all($limit=10000, $offset=0)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
		$this->db->from('schedules');
		$this->db->where("deleted = 0 $course_id");
        $this->db->order_by("default", "desc");
        $this->db->order_by("title", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		$results = $this->db->get();
		//echo $this->db->last_query();
		return $results;
	}
	function get_teesheet_array()
	{
		$schedule_array = array();
		$schedules = $this->get_all()->result_array();
		foreach($schedules as $schedule)
		{
			$schedule_array[$schedule['schedule_id']] = $schedule['title'];
		}
		return $schedule_array;
	}
	
    /*
	Gets information about a particular teesheet
	*/
	function get_info($schedule_id)
	{
	    $this->db->from('schedules');
        $this->db->where("schedule_id = '$schedule_id'");

        $query = $this->db->get();

        if($query->num_rows()==1)
        {
            return $query->row();
        }
        else
        {
            //Get empty base parent object, as $item_id is NOT an item
            $item_obj=new stdClass();

            //Get all the fields from items table
            $fields = $this->db->list_fields('courses');

            foreach ($fields as $field)
            {
                $item_obj->$field='';
            }

            return $item_obj;
        }
    }
    function get_default()
	{
		$this->db->select('schedule_id');
		$this->db->from('schedules');
		$this->db->where('default', 1);
		$this->db->where('deleted', 0);
		$this->db->where('course_id', $this->session->userdata("course_id"));
		
		$result = $this->db->get()->result_array();
		return $result[0]['schedule_id'];
	}
	/*
	Deletes teesheet
	*/
	function delete($schedule_id)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
            
        $this->db->where("schedule_id = '$schedule_id' $course_id");
        return $this->db->update('schedules', array('deleted' => 1));
    }
	/*
	Deletes a list of teesheets
	*/
	function delete_list($teesheet_ids)
	{
		if (!$this->permissions->is_super_admin())
            $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->where_in('teesheet_id',$teesheet_ids);
		return $this->db->update('teesheet', array('deleted' => 1));
 	}
	/*
	Inserts or updates a teesheet
	*/
	function save(&$schedule_data,$schedule_id=false)
	{
		if ($schedule_data['default'] == 1) {
			$this->db->where('course_id', $this->session->userdata('course_id'));
			$this->db->update('schedules',array('default'=>0));
		}
		if (!$schedule_id or !$this->exists($schedule_id))
		{
			if($this->db->insert('schedules',$schedule_data))
			{
				$schedule_data['schedule_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}
		$schedule_data['schedule_id'] = $schedule_id;
		$this->db->where('schedule_id', $schedule_id);
		return $this->db->update('schedules',$schedule_data);
	}
	
	/*
	Preform a search on teesheets
	*/
	function search($search, $limit=20)
	{
        $this->db->from('schedule');
		$this->db->where("title LIKE '%".$this->db->escape_like_str($search)."%'");
		$this->db->where("deleted != 1");
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->order_by("title", "asc");
		$this->db->limit($limit);
		return $this->db->get();	
	}
	
    function get_inventory_data_for_item($item_id)
    {
        $this->db->from('inventory');
        $this->db->where('trans_items',$item_id);
        $this->db->order_by("trans_date", "desc");
        return $this->db->get();		
    }
    function record_teetime_purchases($cart_data) {
		$teetimes = 0;
		$carts = 0;
		foreach ($cart_data as $line) {
			if ((isset($line['item_number']) && preg_match('/[0-9]+_[3-4,7-8]/',$line['item_number'])) ||
				($this->config->item('simulator') && isset($line['item_number']) && preg_match('/[0-9]+_[1-8]/',$line['item_number'])))
				$teetimes += $line['quantity'];
			if (isset($line['item_number']) && preg_match('/[0-9]+_[1-2,5-6]/',$line['item_number']))
				$carts += $line['quantity'];
		}
		$TTID = $this->sale_lib->get_teetime();
		$TTID = substr($TTID, 0, 20);
		if ($this->sale_lib->get_mode() == 'return')
		{
			$carts = $carts * -1;
			$teetimes = $teetimes * -1;
		}
		if ($this->db->query("UPDATE foreup_reservations SET player_count = CASE WHEN (paid_player_count + $teetimes) > player_count THEN (paid_player_count + $teetimes) ELSE player_count END, 
			carts = CASE WHEN (paid_carts + $carts) > carts THEN (paid_carts + $carts) ELSE carts END,
			paid_player_count = (paid_player_count + $teetimes), paid_carts = (paid_carts + $carts), status = CASE WHEN (status != 'teed off') THEN 'checked in' ELSE 'teed off' END WHERE reservation_id LIKE '{$TTID}%' AND status != 'deleted' LIMIT 2"))
			return $TTID;
		else 
			return -1;
	}
    function adjust_teetimes($teesheet_id = '') {
        //return;
        $old_open_time = $this->config->item('open_time');
        $old_close_time = $this->config->item('close_time');
        $old_increment = $this->session->userdata('increment');
        $new_open_time = $this->input->post('open_time')?$this->input->post('open_time'):$this->config->item('open_time');
        $new_close_time = $this->input->post('close_time')?$this->input->post('close_time'):$this->config->item('close_time');
        $new_increment = $this->input->post('increment')?$this->input->post('increment'):$this->session->userdata('increment');
        
        // Fetch tee times
        $date = date('Ymd', strtotime("-1 month")).'0000';
        $teetimes = $this->getTeeTimes($date, '', $teesheet_id);
        if ($old_increment != $new_increment) {
            foreach ($teetimes->result() as $teetime) {
                $start = $teetime->start;
                $end = $teetime->end;
                
                $start_time = $start % 10000;
                $end_time = $end % 10000;
                $front_dif = (floor(($start_time - $new_open_time) / 100) * 60 + (((($start_time - $new_open_time)%100>59)?($start_time - $new_open_time)%100-40:($start_time - $new_open_time))%100)) % $new_increment;
                $back_dif = (floor(($end_time - $new_open_time) / 100) * 60 + (((($end_time - $new_open_time)%100>59)?($end_time - $new_open_time)%100-40:($end_time - $new_open_time))%100)) % $new_increment;
//                $start_hour = substr($start, 8, 2);
  //              $start_min = substr($start, 10, 2);
                
                //echo 'dif '.$difference;
                if ($front_dif < $new_increment/2) {
                    $new_start = $teetime->start - $front_dif;
                    if ($new_start % 100 > 59)
                        $new_start -= 40;
                }
                else {
                    $new_start = $teetime->start - $front_dif + $new_increment;
                    if ($new_start % 100 > 59)
                        $new_start += 40;
                }
                if ($back_dif < $new_increment/2) {
                    $new_end = $teetime->end - $back_dif;
                    if ($new_end % 100 >59)
                        $new_end -= 40;
                }
                else {
                    $new_end = $teetime->end - $back_dif + $new_increment;
                    if ($new_end % 100 >59)
                        $new_end += 40;
                }
                
                $data = array(
                    'start' => $new_start,
                    'end' => $new_end
                );

                $this->db->where('TTID', $teetime->TTID);
                $this->db->update('teetime', $data); 
            }
        }
        else {
            // Just realign teetimes by a calculated amount
            // 630 600 7 dif = 630 - 600 = (30) %7 = 2 630, 637, 644 to 628, 635, 642
            // 600 630 7 dif = 600 - 630 = (-30) %7 = -2 635, 642, 649 to 6
            $time_dif = $old_open_time - $new_open_time;
            $difference = (floor($time_dif/100)*60 + $time_dif % 100)%$new_increment;
			$increment = $this->session->userdata('increment');
//            if ($old_open_time > $new_open_time)
  //              $d;
            //print_r($teetimes);
            foreach ($teetimes->result() as $teetime) {
                $start = $teetime->start;
                $end = $teetime->end;
                
                
				if ($difference < $increment/2) {
	                $new_start = $teetime->start - $difference;
	                $new_end = $teetime->end - $difference;
	                
	                if ($new_start % 100 > 59)
	                    $new_start -= 40;
	                if ($new_end % 100 >59)
	                    $new_end -= 40;
                }
                else {
                	$new_start = $teetime->start - $difference + $increment;
	                $new_end = $teetime->end - $difference + $increment;
	                
	                if ($new_start % 100 > 59)
	                    $new_start += 40;
	                if ($new_end % 100 >59)
	                    $new_end += 40;
                }
                $data = array(
                    'start' => $new_start,
                    'end' => $new_end
                );

                $this->db->where('TTID', $teetime->TTID);
                $this->db->update('teetime', $data); 
            }
        }
    }
    function make_5_day_weather() {
    	//Conditions array
    	$weatherHTML = '';
		$todaysHTML = '';
        if ($this->session->userdata('zip') && $this->session->userdata('zip')  != '' &&  preg_match("/[0-9]{5}/", $this->session->userdata('zip'))) {
            //$url = "http://www.google.com/ig/api?weather={$this->session->userdata('zip')}&hl=en";
            //$ch = curl_init();
            //echo $url;
            //$timeout = 0;

            //Set CURL options
            //curl_setopt ($ch, CURLOPT_URL, $url);
            //curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
            //curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

            //$xml_str=curl_exec($ch);
			$this->load->model('weather');
			$results = $this->weather->getLatest();
			$forecast = $results['forecast'];
            //close CURL cause we dont need it anymore
            //curl_close($ch);
            //print_r($xml_str);
            //$xml = new SimplexmlElement($xml_str);
            $weather_imgs = array(
            	0=>'rain',1=>'rain',2=>'rain',3=>'rain',4=>'rain',5=>'rain',6=>'rain',9=>'rain',11=>'rain',12=>'rain',35=>'rain',40=>'rain',
				7=>'snow',8=>'snow',10=>'snow',13=>'snow',14=>'snow',15=>'snow',16=>'snow',17=>'snow',18=>'snow',25=>'snow',41=>'snow',42=>'snow',43=>'snow',46=>'snow',
				26=>'cloudy',28=>'cloudy',
				37=>'lightning',38=>'lightning',39=>'lightning',45=>'lightning',47=>'lightning',
				20=>'partcloudy',21=>'partcloudy',22=>'partcloudy',30=>'partcloudy',44=>'partcloudy',
				32=>'sun',34=>'sun',36=>'sun',
				19=>'wind',23=>'wind',24=>'wind',
				27=>'night',29=>'night',31=>'night',33=>'night'
			);
            
            if ($forecast && isset($forecast[0])) {
            	$todaysHTML .= "<div id='weather_icon'>
		            	<img width=37 src='../images/icons/weather/{$weather_imgs[$results['today']['image']]}Big.png?yi={$results['today']['image']}'/>
	    			</div>
	    			<div id='current_temp'>
	    				{$results['today']['temp']}&deg;
	    			</div>
	    			<div id='low_high'>
	    				<div id='high'>{$forecast[0]['high_temp']}&deg;</div>
	    				<div id='low'>{$forecast[0]['low_temp']}&deg;</div>
	    			</div>";
                $weatherHTML .= "<div>
                    <!--div>
                        <img class='curpic' src='http://l.yimg.com/a/i/us/we/52/{$results['today']['image']}.gif'/>
                        <span class='curtemp'>{$results['today']['temp']}&deg;</span>
                        <div class='wcity'>{$results['today']['city']}</div>
                        <div class='clear'></div>
                    </div-->
                    <div>
                        <div class='dailyweather'>
                            <div class='dow'>".substr(date('D',strtotime($forecast[0]['day'])),0,2)."</div>
                            <img width=20 src='../images/icons/weather/{$weather_imgs[$forecast[0]['image']]}Sml.png'/>
                            <div class='hightemp'>{$forecast[0]['high_temp']}&deg;</div>
                            <div class='lowtemp'>{$forecast[0]['low_temp']}&deg;</div>
                        </div>
                        <div class='dailyweather'>
                            <div class='dow'>".substr(date('D',strtotime($forecast[1]['day'])),0,2)."</div>
                            <img width=20 src='../images/icons/weather/{$weather_imgs[$forecast[1]['image']]}Sml.png'/>
                            <div class='hightemp'>{$forecast[1]['high_temp']}&deg;</div>
                            <div class='lowtemp'>{$forecast[1]['low_temp']}&deg;</div>
                        </div>
                        <div class='dailyweather'>
                            <div class='dow'>".substr(date('D',strtotime($forecast[2]['day'])),0,2)."</div>
                            <img width=20 src='../images/icons/weather/{$weather_imgs[$forecast[2]['image']]}Sml.png'/>
                            <div class='hightemp'>{$forecast[2]['high_temp']}&deg;</div>
                            <div class='lowtemp'>{$forecast[2]['low_temp']}&deg;</div>
                        </div>
                        <div class='dailyweather'>
                            <div class='dow'>".substr(date('D',strtotime($forecast[3]['day'])),0,2)."</div>
                            <img width=20 src='../images/icons/weather/{$weather_imgs[$forecast[3]['image']]}Sml.png'/>
                            <div class='hightemp'>{$forecast[3]['high_temp']}&deg;</div>
                            <div class='lowtemp'>{$forecast[3]['low_temp']}&deg;</div>
                        </div>
                        <div class='dailyweather'>
                            <div class='dow'>".substr(date('D',strtotime($forecast[4]['day'])),0,2)."</div>
                            <img width=20 src='../images/icons/weather/{$weather_imgs[$forecast[4]['image']]}Sml.png'/>
                            <div class='hightemp'>{$forecast[4]['high_temp']}&deg;</div>
                            <div class='lowtemp'>{$forecast[4]['low_temp']}&deg;</div>
                        </div>
                        <div class='clear'></div>
                    </div>
                </div>";
            }
        }
        else {
            $weatherHTML .= "To see the forecast, set your ZIP code in <a href='index.php/config'>your settings.</a>";
        }
        //print_r($xml->weather->current_conditions->condition);
        return array('weather'=>$weatherHTML,'todays'=>$todaysHTML);
    }
    function switch_tee_sheet($teesheet_id = '', $online = false) {
        if ($teesheet_id == '')
	        $teesheet_id = $this->input->post('teesheetMenu');
	        
	    $teesheet_info = $this->get_info($teesheet_id);
        //$sql = "SELECT open_time, close_time, increment, holes, frontnine, fntime, course_name, zip FROM users WHERE teesheet_id = '$newTeeSheet' LIMIT 1";
        $this->db->from('schedules');
        $this->db->join('courses', 'courses.course_id = schedules.course_id');
        $this->db->select("schedules.holes AS holes, schedules.frontnine AS frontnine,
            courses.open_time AS open_time, courses.close_time AS close_time, courses.increment AS increment, 
            courses.name AS name, courses.zip AS zip");
        $this->db->where("schedules.schedule_id", $teesheet_id);
        $this->db->limit(1);
        $info = $this->db->get();
        //echo $this->db->last_query();
//        print_r($info);
        if ($info->num_rows() == 1)
            $in = $info->row();
        $this->session->set_userdata('teesheet_id', $teesheet_id);
		$this->session->set_userdata('schedule_id', $teesheet_id);
		$this->session->set_userdata('schedule_type', $teesheet_info->type);
		$this->session->set_userdata('default_price_class', $teesheet_info->default_price_class);
        $this->session->set_userdata('openhour', $in->open_time);
        $this->session->set_userdata('closehour', $in->close_time);
        $this->session->set_userdata('increment', $teesheet_info->increment);
        $this->session->set_userdata('holes', $teesheet_info->holes);
        $this->session->set_userdata('frontnine', $teesheet_info->frontnine);
        //$this->session->set_userdata('fntime', $teesheet_info->fntime);
        $this->session->set_userdata('course_name', $in->name);
        $this->session->set_userdata('zip', $in->zip);
        $this->session->set_userdata('teesheet_reminder_count', 0);
        if ($online)
        	$this->session->set_userdata('course_id', $teesheet_info->course_id);
    }
    function course_is_linked(&$group_ids) {
    	//Gathering a list of 'linked' course_groups that this course belongs to and returning true if any are found
    	$this->db->from('course_groups');
    	$this->db->select('course_groups.group_id');
    	$this->db->join('course_group_members', 'course_group_members.group_id = course_groups.group_id');
    	$this->db->where ('type', 'linked');
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->group_by('group_id');
    	$query = $this->db->get();
		$ids = $query->result_array();
		foreach ($ids as $id)
			$group_ids[] = $id['group_id'];
		return ($query->num_rows() > 0);
    }
    function get_tee_sheet_menu($teesheet_id, $online = false, $minimum = 1) {
    	$this->load->model('course');
        $tablesArray = array();
		$tableMenuHTML = "";
        $where = '';
        $group_ids = array();
		if ($this->course->is_linked($group_ids))
		{
	        $this->db->from('course_group_members');
			$this->db->select('schedules.schedule_id AS teesheet_id, schedules.title AS name');
			$this->db->join('schedules', 'course_group_members.course_id = schedules.course_id');
			$this->db->where('schedules.deleted', 0);
			if ($online)
			{
				$this->db->join('tracks', 'tracks.schedule_id = schedules.schedule_id');
				$this->db->where('online_booking', 1);
			}
			$this->db->where_in('group_id', array_values($group_ids));
			$this->db->order_by('schedules.title');
			$this->db->group_by('schedules.schedule_id');
	    	$tablesArray = $this->db->get();
		}
		else 
		{
			//return false;
			$this->db->from('schedules');
			$this->db->select('schedules.schedule_id AS teesheet_id, schedules.title AS name');
			if ($online)
			{
				$this->db->join('tracks', 'tracks.schedule_id = schedules.schedule_id');
				$this->db->where('online_booking', 1);
			}
			$this->db->where('schedules.deleted',0);
			$this->db->where('course_id', $this->session->userdata('course_id'));
			$this->db->order_by('schedules.title');
			$tablesArray = $this->db->get();	
		}
		//echo $this->db->last_query();
        if ($tablesArray->num_rows() > $minimum) {
        	$tableMenuHTML = "<select id='teesheetMenu' name='teesheetMenu' onblur='changeTeeSheet()'>";
            foreach ($tablesArray->result() as $table) {
	        	//echo 'stuff';
	            if ($teesheet_id == $table->teesheet_id)
	                $selected = 'selected';
	            else {
	                $selected = '';
	            }
	            if ($table->teesheet_id != '')
	                $tableMenuHTML .= "<option value='{$table->teesheet_id}' $selected>{$table->name}</option>";
	        }
	        $tableMenuHTML .= "</select>";
        }
        
        return $tableMenuHTML;
    }
    function format_time_string($timestring) {
        $month_zero = '';
        $month = (int)substr($timestring,4,2)+1;
        if ($month<10)
            $month_zero = '0';
        return substr($timestring, 0, 4).'-'.
                $month_zero.$month.'-'.
                substr($timestring,6,2).'T'.
                substr($timestring,8,2).':'.
                substr($timestring,10,2).':00';
    }
    function format_time($timestring){
        if (strlen($timestring) == 4) {
            $hours = (int)substr($timestring,0,2);
            $mins = substr($timestring,2,2);
        }
        else {
            $hours = (int)substr($timestring,0,1);
            $mins = substr($timestring,1,2);
        }
        $a_p = 'am';
        if ($hours > 11)
            $a_p = 'pm';
        if ($hours > 12)
            $hours -= 12;
        return $hours.':'.$mins.' '.$a_p;
        
    }
    function clean_for_json($string)
	{
		$string = str_replace("'", "", $string);
		$bad_characters = array("\r\n", "\n", "\r");
		return str_replace($bad_characters, ' ', $string);
	}
    function getJSONTeeTimes($startDate = '', $endDate = '', $side = '', $all = false, $updates_only = false){
    	ini_set('memory_limit', '160M');
    	    	
        $teetimes = $this->getTeeTimes($startDate, $endDate, '', $all, $updates_only);
        $JTTArray = array();
    //echo 'stuff';
    // print_r($teetimes);
        foreach ($teetimes->result() as $teetime) {
              /*if ($this->session->userdata('holes') == '18') {
                if ($teetime->side == 'back')
                    $group = 'bcaldata';
                else 
                    $group = 'caldata';
            }
            else
                $group = 'caldata';
			*/
			//print_r($teetime);
			$group = $teetime->track_id;
            $TTArray = array();
			$TTArray = $this->_reservation->add_teetime_styles((array)$teetime);
			if (!isset($JTTArray[$group]))
				$JTTArray[$group] = array();
			$JTTArray[$group][] = $TTArray;
        }
        /*if ($side != '')
            if ($side == 'front')
                $JSONTeeTimes = json_encode($JTTArray['caldata']);
            else if ($side == 'back')
                $JSONTeeTimes = json_encode($JTTArray['bcaldata']);
            else 
                $JSONTeeTimes = json_encode(array());
        else*/
        $JSONTeeTimes = json_encode($JTTArray);
        
        return $JSONTeeTimes;
    }
    function get_booking_delay($startDate, $track_id)
	{
		
		$this->db->select('end');
		$this->db->from('reservations');
		$this->db->where('track_id', $track_id);
		$this->db->where("start < $startDate and end > $startDate");
		//$this->db->where('side', $side);
		$this->db->where('status !=', 'deleted');
		$this->db->limit(1);
		$result = $this->db->get();
		
		return $result->result_array();
	}
    function get_trimmed_teetimes($startDate = '', $endDate = '', $side = '', $teesheet_id = '') {
        //echo "$startDate - $endDate";
        // Get Brand/Golf Course from Session
        $teesheet_id = ($teesheet_id != '')?$teesheet_id:$this->session->userdata('schedule_id');
    
        $this->db->where("schedule_id", $teesheet_id);
        if ($startDate != '') 
            $this->db->where("start >= '$startDate'");
        if ($endDate != '') 
            $this->db->where("end < '$endDate'");
        $this->db->select("*, sum(player_count) AS pcount");
        $this->db->from('reservations');
        if ($side != '')
            $this->db->where('side', $side);
		$this->db->where("status !=", 'deleted');
        $this->db->order_by('end, start');
        $this->db->group_by('end');
        $teetimes = $this->db->get();//createAssocArray($this->db->executeSQL($sql));
        //echo "<br/>".$this->db->last_query()."<br/>";
        $result_array = array();
        while ($teetime = $teetimes->fetch_assoc())
		{
			$result_array[] = $teetime;
		}
        return $result_array;
    }
    function get_trimmed_reservations($startDate = '', $endDate = '', $track_id = '', $schedule_id = '') {
        //echo "$startDate - $endDate";
        // Get Brand/Golf Course from Session
        $schedule_id = ($schedule_id != '')?$schedule_id:$this->session->userdata('schedule_id');
    
        //$this->db->where("schedule_id", $schedule_id);
        if ($startDate != '') 
            $this->db->where("start >= '$startDate'");
        if ($endDate != '') 
            $this->db->where("start <= '$endDate'");
        $this->db->select("*, sum(player_count) AS pcount");
        $this->db->from('reservations');
        $this->db->where('track_id', $track_id);
		$this->db->where("status !=", 'deleted');
        $this->db->order_by('end, start');
        $this->db->group_by('end');
        $reservations = $this->db->get();//createAssocArray($this->db->executeSQL($sql));
        //echo "<br/>".$this->db->last_query()."<br/>";
        $result_array = array();
        while ($reservation = $reservations->fetch_assoc())
		{
			$result_array[] = $reservation;
		}
        return $result_array;
    }
    function getTeeTimes($startDate = '', $endDate = '', $teesheet_id = '', $all = false, $updates_only = false) {
        	
        $teesheet_id = ($teesheet_id != '')?$teesheet_id:$this->session->userdata('schedule_id');
        
        if ($startDate != '')
            $this->db->where("start >= '$startDate'");
    
        if ($endDate != '') 
            $this->db->where("end < '$endDate'");
    
		//if ($startDate == '' && $endDate == '')
		if (!$all)
			$this->db->where('status !=', 'deleted');
		//Only get teetimes that have been updated in the last 5 minutes
		if ($updates_only) {			
            $this->set_timezone("America/Chicago");
			$this->db->where('last_updated >', date('Y-m-d H:i:s', strtotime('-1 minute')));
			$this->set_timezone();
		}
		//else
	    //	$this->db->where('last_updated >', date('Y-m-d H:i:s', strtotime('-30 minutes')));
	    $this->db->select("reservation_id, reservations.track_id as track_id, type, status, start, end, allDay, player_count, holes, carts, paid_player_count, paid_carts, reservations.title as title, phone, email, details, side, className, person_id, person_name, person_id_2, person_id_3, person_id_4, person_id_5, turn_time, finish_time");
		$this->db->from('reservations');
		$this->db->join('tracks', 'reservations.track_id = tracks.track_id');
		//$this->db->join('schedules', 'schedules.schedule_id = tracks.schedule_id');
		$this->db->where('schedule_id', $teesheet_id);      
        $teetimes = $this->db->get();
        //echo $this->db->last_query();
        return $teetimes;
    }
    
    function get_tcl_ranges($tracks, $today_time = 0){
    	$track_array = array();	
    	foreach($tracks as $track)
    	{
    		$track_array[] = $track['track_id'];
    	}
	//	return;
        $this->db->from('reservations');
        $this->db->where_in('type', array('tournament', 'closed', 'league', 'event'));
        $this->db->where("status !=", 'deleted');
        $this->db->where_in("track_id", $track_array);
        $this->db->where('start >', $today_time);
        $this->db->order_by('start', 'asc');
        $result = $this->db->get();
        //echo $this->db->last_query();
        //echo $result->result_array();
        //Sort by date
        $event_array = array();
        while ($event = $result->fetch_assoc()) {
            $event_array[substr($event['start'],0,8)][] = $event; 

        }
        
        return $event_array;
    }
	function set_timezone($timezone = false)
	{
		if ($timezone){
			date_default_timezone_set($timezone);	
		}
		else 
		{
			date_default_timezone_set($this->config->item('timezone'));
		}		
	}
	function get_time_string($offset = false, $portion = 'both', $when = 'now')
	{
		$time_string = '';
		if ($portion == 'date' || $portion == 'both')
		{
			if ($offset)//Adjusted by one month because javascript months range from 0-11
				$time_string = date('Ymd', strtotime("$when")) - 100;
			else
				$time_string = date('Ymd', strtotime($when));
		}
		if ($portion == 'time' || $portion == 'both')
		{
			$time_string .= date('Hi', strtotime($when));
		}
		return $time_string;
	}
	function load_booking_settings($course_id = false ,$schedule_id = false, $track_id = false)
	{
		if (!$course_id) $course_id = $this->session->userdata('course_id');
		if (!$schedule_id) $schedule_id = $this->session->userdata('schedule_id');
		
		$this->schedule_id = $schedule_id ? $schedule_id : $this->session->userdata('schedule_id');
		$this->track_id = $track_id ? $track_id : $this->session->userdata('track_id');
	// print_r($this->course->get_info($course_id));
		// echo $course_id;
		// print_r($this->course->get_info($course_id));
		$this->course_info = $this->course->get_info($course_id);			
		$timezone = $this->course_info->timezone; 		
		$this->set_timezone($timezone);
        $this->schedule_info = $this->schedule->get_info($this->schedule_id);
		        
		$this->teetime_prices = $this->Fee->get_info();
		//$this->teetime_prices = $this->Fee->get_info('',$this->schedule_id);
	    $this->offset_time = $this->get_time_string(true, 'both', "$this->lead_hours hours");
		$this->selected_date = $this->get_time_string(false, 'date');
	    $this->offset_date = $this->get_time_string(true, 'date');		
	    $this->selected_year = date('Y');
        $this->selected_month = date('m');
        $this->selected_day = (int)date('d');
		
		$course_open_time = $this->course_info->open_time;
		$course_open_min = (floor($course_open_time/100)*60+($course_open_time%100));
   		$online_open_time = $this->schedule_info->online_open_time;
   		$online_open_min = (floor($online_open_time/100)*60+($online_open_time%100));
		
 		// Using fmod instead of modulus(%) operator, % rounds to nearest int before calculating.
 		// fmod plays nice with floats (such as 7.5 for 7/8 teetimes)
 		$mod = fmod($online_open_min - $course_open_min, $this->schedule_info->increment);
 		
 		// If teetimes round nicely to the hour, start on the hour
 		if($mod == 0){ 
			$increment = 0;
		}else{ 
			$increment = $this->schedule_info->increment; 
		}
 		$this->open_time = $online_open_time + $increment - $mod;		

		$this->now = $this->get_time_string(true);
    }
	function apply_booking_filters($filter_holes = 18, $filter_available_spots = 1, 
            $filter_time_preset = '', $filter_start_hour = '', $filter_end_hour = '', 
            $filter_start_date = '', $filter_end_date = '', $tab_index = 1)
	{
		if ($filter_time_preset == '')
		{
			//TODO:The +1 hour should be changed to the setting value
			$cur_time = date('H', strtotime("+1 hour")).'00';
			//$am_pm = date('a', strtotime("+1 hour"));
			if ($cur_time >= $this->filter_time_range_presets['midday']['end'])
				$filter_time_preset = 'evening';
			else if ($cur_time >= $this->filter_time_range_presets['morning']['end'])
				$filter_time_preset = 'midday';
			else 
				$filter_time_preset = 'morning';
		}
		//print_r($this->filter_time_range_presets);
		//echo 'Curtime:'.$cur_time.' '.$this->filter_time_range_presets['midday']['end'];
		$this->filter_holes = $filter_holes;		
		$this->filter_available_spots = $filter_available_spots;		
        $this->filter_time_preset = $filter_time_preset;
			
		$this->filter_time_range['start'] = $this->filter_time_range_presets[$filter_time_preset]['start'];		
		$this->filter_time_range['end'] = $this->filter_time_range_presets[$filter_time_preset]['end'];		
		// Override preset time range if a start or end hour are set
		$this->filter_time_range['start'] = ($filter_start_hour != '')?$filter_start_hour:$this->filter_time_range['start'];
		$this->filter_time_range['end'] = ($filter_end_hour != '')?$filter_end_hour:$this->filter_time_range['end'];		
		
		$this->filter_index = $tab_index;
		//TODO: SET start and end date
	
		//TODO: Set lead time, days out, filter method, player minimum, 
		$filters = array(
			'filter_time_preset'=>$filter_time_preset
		);
		return $filters;
	}
	function determine_price_indexes($time, $holes, $day_of_week)
	{
		//print_r($this->course_info);
		//if (!isset($this->course_info->early_bird_hours_begin))
		$this->course_info = $this->course->get_info($this->session->userdata('course_id'));
		//echo $time.' - '.$this->course_info->super_twilight_hour;
		//print_r($this->course_info);
		
		if ($time >= $this->course_info->early_bird_hours_begin && $time < $this->course_info->early_bird_hours_end) 
	    	$col = 2;
		else if ($time >= $this->course_info->morning_hours_begin && $time < $this->course_info->morning_hours_end) 
	        $col = 3;
		else if ($time >= $this->course_info->afternoon_hours_begin && $time < $this->course_info->afternoon_hours_end) 
	        $col = 4;
		else if ($time >= $this->course_info->super_twilight_hour) 
	        $col = 6;
	    else if ($time >= $this->course_info->twilight_hour) 
	        $col = 5;
	    else 
	        $col = 1;
	    
	    if ($holes == 9) {
            $row = 0;
			$cart_row = 0;
        }
        else {
            $row = 4;
            $cart_row = 4;
        }
        if (($day_of_week == 'Fri' && $this->course_info->weekend_fri) || 
                ($day_of_week == 'Sat' && $this->course_info->weekend_sat) || 
                ($day_of_week == 'Sun' && $this->course_info->weekend_sun)) {
        	$cart_row += 2;
            $row += 4;
        }
        else {
        	$cart_row += 1;
            $row += 3;
        }
		
		$data = array(
			'price_category'=>'price_category_'.$col, 
			'green_fee_index'=>$row,
			'cart_price_index'=>$cart_row
		);
		return $data;		
		
	}
	function get_available_teetimes_2($response_format = 'html') {
		$this->load->model('billing');
		$billing_info = $this->billing->get_info_by_teesheet_id($this->schedule_id);
		//print_r($billing_info);
		//cycle through days
        for ($j = 0; $j < $this->days_out; $j++) {
        	//If closed for the day, we'll jump ahead to tomorrow
        	$days_out = $j;
        	if ($this->schedule_info->online_close_time-($this->lead_hours.'00')<date('H').'00')
				$days_out = $j + 1;
			//echo 'oct '.$this->teesheet_info->online_close_time.' lh '.$this->lead_hours.' curtime '.date('H').'00';		
            //Month is offset by one
            $this->offset_date = $this->get_time_string(true, 'date', "$days_out days $this->lead_hours hours");
            $day_of_week = date('D', strtotime($this->offset_date+100));
            
			$bartered_teetime_count = $this->billing->get_daily_bartered_teetime_count($this->offset_date);
			//echo '<br/>btc'.$bartered_teetime_count;
			$offset_month = $this->selected_month - 1;
            $month_zero = ($offset_month < 10)?'0':'';
            $day_zero = ($this->selected_day < 10)?'0':'';
            //echo $this->offset_date."$j days $this->lead_hours hours<br/>";
			//echo $this->get_time_string(true, 'date', "$j days $this->lead_hours hours").'<br/>';
			//Set start and end times
            $this->start_time = ($this->offset_time > $this->offset_date.$this->schedule_info->online_open_time)?$this->offset_time:$this->offset_date.$this->schedule_info->online_open_time;
			$this->end_time = $this->offset_date.$this->schedule_info->online_close_time;
			$this->booking_delay = $this->get_booking_delay($this->start_time, 'front');
			
		//	echo 'booking delay <br/>';
			//print_r($this->booking_delay[0]);
			$this->booked_times = $this->get_trimmed_teetimes($this->start_time, $this->offset_date.'2399', 'front', $this->filter_teesheet_ids);
     //echo '<br/>';
     //print_r($this->booked_times);
	 //echo '<br/>OT '.$this->open_time.'  CT '.$this->teesheet_info->online_close_time;
	   //echo $this->teesheet_info->online_open_time;
//	   $this->benchmark->mark('g_p_t_start');
	 $pot_teetimes =  $this->get_potential_teetimes($this->open_time, $this->schedule_info->online_close_time, $this->schedule_info->increment, $this->session->userdata('track_id'));
//	   $this->benchmark->mark('g_p_t_end');
//	   $this->benchmark->mark('e_p_t_start');
	 $this->evaluate_potential_teetimes($pot_teetimes, ''); 
//	 	   $this->benchmark->mark('e_p_t_end');
//		   	   $this->benchmark->mark('for_loop_start');
		   
	 
//	   print_r($pot_teetimes);
	   		for ($h = $this->open_time; $h < $this->schedule_info->online_close_time; $h = $h + $this->schedule_info->increment)
            {
                //make sure we don't get into weird times
                $h = ($h%100 >59)?$h + 40:$h;
                $i = $h;
                 
                $continue = false;
                $available_spots = 4;
                if (isset($this->booked_times[0])) {
                    if ((int)substr($this->booked_times[0]['start'],8) <= $i && ($this->booked_times[0]['type'] == 'tournament' || $this->booked_times[0]['type'] == 'closed' || $this->booked_times[0]['type'] == 'league' || $this->booked_times[0]['type'] == 'event'))
                        $continue = true;
                    else if ((int)substr($this->booked_times[0]['start'],8) <= $i && $this->booked_times[0]['pcount'] >= 4)
                        $continue = true;
                    else if ((int)substr($this->booked_times[0]['start'],8) <= $i && $this->booked_times[0]['pcount'] < 4)
                        $available_spots = 4 - $this->booked_times[0]['pcount'];
					//A booking delay exists when you have a teetime overlap your starting online booking time
			//echo "<br/>".(int)substr($this->booking_delay[0]['end'],8).' > '.$i;
                    $deletable_time = $i + $this->schedule_info->increment;
                    if ($deletable_time % 100 > 59)
                        $deletable_time = $deletable_time + 40;
                   
					if ((int)substr($this->booked_times[0]['end'],8) == (int)$deletable_time)
                        array_shift($this->booked_times);
					
                }
        		if (isset($this->booking_delay[0]) && (int)substr($this->booking_delay[0]['end'],8) > $i)
					$continue = true;
			    if ($continue) {
                    continue;
                }
				//Make time
                $hour = floor($i/100);
                $min = $i%100;
                $am_pm = (floor($i/100)<12)?'a':'p';
                $hour = ($hour > 12)?$hour -12:$hour;
                $min_leading_zero = ($min < 10)?'0':'';
                
                $time = $hour.':'.$min_leading_zero.$min.$am_pm;
                $sd_z =($this->selected_day < 10)?'0':'';
                $i_z = ((int)$i < 1000)?'0':'';
                $time_string = $i_z.(int)$i;
				$price_indexes = $this->determine_price_indexes($i, $this->filter_holes, $day_of_week);
				$price_category = $price_indexes['price_category'];
				$teesheet_id = $this->session->userdata('schedule_id');
				$course_id = $this->session->userdata('course_id');
				$wo_cart_price = number_format($this->teetime_prices["{$teesheet_id}"]["{$course_id}_{$price_indexes['green_fee_index']}"]->$price_category, 2);
                $w_cart_price = number_format($this->teetime_prices["{$teesheet_id}"]["{$course_id}_{$price_indexes['green_fee_index']}"]->$price_category+$this->teetime_prices["{$teesheet_id}"]["{$course_id}_{$price_indexes['cart_price_index']}"]->$price_category,2);
                $date_time = "{$this->selected_year}{$month_zero}{$offset_month}{$day_zero}{$this->selected_day}{$i_z}".(int)$i;
				$day_time = "$sd_z$this->selected_day$time_string";
				$date_time = "{$this->offset_date}{$i_z}".(int)$i;
				    
				if ($available_spots >= $this->filter_available_spots && 
                	$i >= (int)$this->filter_time_range['start'] && 
                	$i < (int)$this->filter_time_range['end'] &&
				    $date_time > $this->offset_time) {
				    $teetimes_html[] = "".
                    "<div class='teetime ui-corner-all ui-state-default' id='teetime_$day_time'>".
                        "<div class='left'>".
                            "<div class='time'>$time</div>".
                            "<div class='players'>".
                                "<span class='spots ui-corner-all ui-state-default'>".
                                    "<span class='ui-button-icon-primary ui-icon ui-icon-person'></span>".
                                    "<span class='ui-button-text' id='teetime_{$day_time}_spots'>$available_spots</span>".
                                "</span>".
                                "<span class='clear'></span>".
                            "</div>".
                        "</div>".
                        "<div class='center'>".
                            "<div class='course_name'>{$this->course_info->name} - {$this->schedule_info->title}</div>".
                            "<div class='price_details'>".
                                "<span class='holes ui-corner-all ui-state-default'>".
                                    "<span class='ui-button-icon-primary ui-icon ui-icon-flag'></span>".
                                    "$this->filter_holes".
                                "</span>".
                                "<span class='price_label'>".
                                    "With Cart ".
                                "</span>".
                                "<span class='price ui-corner-all ui-state-default'>".
                                    "<span class='ui-button-icon-primary ui-icon ui-icon-diamond'></span>".
                                    "$$w_cart_price".
                                "</span>".
                                "<span class='price_label'>".
                                    "Without Cart".
                                "</span>".
                                "<span class='price ui-corner-all ui-state-default'>".
                                    "<span class='ui-button-icon-primary ui-icon ui-icon-diamond'></span>".
                                    "$$wo_cart_price".
                                "</span>".
                            "</div>".
                        "</div>".
                        "<div class='right'>".
                            "<div class='reserve_button'>".
                                anchor("be/reserve/$course_id/$this->offset_date/$i/$this->filter_available_spots/$available_spots/$this->filter_holes/$w_cart_price/$wo_cart_price/", 'Reserve',array('class'=>'colbox','title'=>'Reserve Your Teetime')).
                                //"<input type='submit' value='Reserve' name='reserve' id='reserve_button'/>".
                            "</div>".
                        "</div>".
                        "<div class='clear'></div>".
                    "</div>";
                }
                    //    echo '<br/>here';
                //return;
            }
//	   $this->benchmark->mark('for_loop_end');
//echo $this->benchmark->elapsed_time('g_p_t_start', 'g_p_t_end');
//echo $this->benchmark->elapsed_time('e_p_t_start', 'e_p_t_end');
//echo $this->benchmark->elapsed_time('for_loop_start', 'for_loop_end');
            $response_array[$j]['day_label'] = "<div>".$day_of_week."</div><span>".date("M. j Y", strtotime($this->offset_date+100))."</span>";
            $response_array[$j]['deals'] = '';
            if (count($teetimes_html) > 0)
			{
				//This is to show a daily header saying if a deal is available 
				//if ($billing_info->per_day > $bartered_teetime_count)
				//	$response_array[$j]['deals'] = '<div>There are deals available</div>';
				$response_array[$j]['teetimes'] = $teetimes_html;
			}
			else 
			{
				$response_array[$j]['teetimes'] = array('<div class="no_teetimes">No teetimes available. Please select a different day/time.</div>');
			}
            $teetimes_html = array();
            $this->selected_day++;
            $leap_year = '';
            if ((int)$this->selected_month == 2)
                $leap_year = "_".date('L', mktime(0,0,0,$this->selected_month,$this->selected_day,$this->selected_year));
                
            if ($this->selected_day > $this->month_length[(int)$this->selected_month.$leap_year]) {
                $this->selected_day = 1;
                $this->selected_month ++;
            }
        }
        //print_r( $teetimes_html);
        return $response_array;
        
    }
    
//	function get_available_reservations($course_id, $filter_holes = 18, $filter_available_spots = 1, 
  //          $filter_time_range = 'full_day', $filter_start_hour = '', $filter_end_hour = '', 
    //        $filter_start_date = '', $filter_end_date = '', $response_format = 'html') {
    function get_available_reservations($is_api_request = false, $schedule_id = false, $days_to_future_date = false, $max_price = false, $start_time = false, $end_time = false, $teesheet = '', $response_format = 'html') {
        $response_array = array();
        $teetimes_html = array();
        $teetimes_json = array();
        $this->set_timezone();
		//echo "<br/><br/>TRACKS<br/><br/>";
		$this->tracks = $this->Track->get_all($this->session->userdata('schedule_id'))->result_array();
		//print_r($this->tracks);
		//$now = $this->get_current_time_string();
        // All prices for all green fee types for  teesheets
		$teetime_prices = $this->Fee->get_info();//$this->Fee->get_prices($this->get_all());
		
		$this->filter_available_spots = $filter_available_spots;
        //TODO: cleanup and validate incoming data
        
        //TODO: expand out so that we can search multiple teesheets at once
        if ($course_id != '')
            $this->course_info = $this->course->get_info($course_id);
		$schedule_id = $this->session->userdata('schedule_id');
		$this->schedule_info = $this->schedule->get_info($schedule_id);
		//print_r($this->tracks);
        if ($filter_time_range != '' && isset($this->filter_time_range_presets[$filter_time_range])) {
            $this->filter_time_range['start'] = $this->filter_time_range_presets[$filter_time_range]['start'];
            $this->filter_time_range['end'] = $this->filter_time_range_presets[$filter_time_range]['end'];
        }

        if ($filter_start_hour != '')
            $this->filter_time_range['start'] = $this->filter_time_range['start'];
        if ($filter_end_hour != '')
            $this->filter_time_range['end'] = $filter_end_hour;

$this->filter_time_range['start']='0000';
$this->filter_time_range['end']='2399';
        // Get tournaments, closures, and leagues
        $tcl_ranges = $this->get_tcl_ranges($this->tracks);
     
        //cycle through days
        //$this->selected_year = date('Y');
        //$this->selected_month = date('m');
        //$this->selected_day = (int)date('d');
		$this->filter_index = $filter_index ? $filter_index : $this->filter_index;
		//echo $this->selected_year.' - '.$this->selected_month.' - '.$this->selected_day.' : '.$this->filter_index;
        for ($j = $this->filter_index-1; $j < $this->filter_index; $j++) {
        	$days_out = $days_to_future_date ? $days_to_future_date : $j;    										
        	if ($this->schedule_info->online_close_time-($this->lead_hours.'00')<date('H').'00')
				$days_out = $j + 1;
		    //Month is offset by one
            $this->offset_date = $this->get_time_string(true, 'date', "$days_out days $this->lead_hours hours");            
            $day_of_week = date('D', strtotime($this->offset_date+100));
//            echo $this->offset_date;
			$this->selected_day = substr($this->offset_date,6);
			$bartered_teetime_count = $this->billing->get_daily_bartered_teetime_count($this->offset_date);
			$offset_month = substr($this->offset_date, 4, 2);//$this->selected_month - 1;
			
            $month_zero = '';//($offset_month < 10)?'0':'';
            $day_zero = ($this->selected_day < 10)?'0':'';
        	
        	//Set start and end times
            $this->start_time = ($this->offset_time > $this->offset_date.$this->schedule_info->online_open_time)?$this->offset_time:$this->offset_date.$this->schedule_info->online_open_time;
			$this->end_time = $this->offset_date.$this->schedule_info->online_close_time;
			
            //$start_time = $this->selected_year.$month_zero.$offset_month.$day_zero.$this->selected_day.$this->schedule_info->online_open_time;
			//$end_time = $this->selected_year.$month_zero.$offset_month.$day_zero.$this->selected_day.'2399';
			//TODO:Extract these out of the loop, and make one call to get all of the times and parse them into an associative array
			$booked_times = $booking_delay = array();
			foreach ($this->tracks as $track)
			{
				$end_date = date('Ymd0000', strtotime(($this->offset_date+100).' +1 day'))-1000000;
				//print_r($track);
				$booked_times[$track['track_id']] = $this->get_trimmed_reservations($this->start_time, $end_date, $track['track_id'], $this->filter_teesheet_ids);
				//echo $this->db->last_query();
				//print_r($booked_times);
				//echo "$this->start_time - {$this->offset_date}2399 {$track['track_id']} sql: ".$this->db->last_query();
				$booking_delay[$track['track_id']] = $this->get_booking_delay($this->start_time, $track['track_id']);
			}
			//print_r($booked_times);
			//echo "<br/><br/>Booking delays<br/><br/>";
			//print_r($booking_delay);
            //$booked_times = $this->get_trimmed_teetimes($start_time, $end_time, 'front', $this->filter_teeshet_ids);
            //$booked_times_2 = $this->get_trimmed_teetimes($start_time, $end_time, 'back', $this->filter_teeshet_ids);
	 		//$booking_delay = $this->get_booking_delay($start_time, 'front');
	 		//$booking_delay_2 = $this->get_booking_delay($start_time, 'back');
	 		//for ($h = $this->schedule_info->online_open_time; $h < $this->schedule_info->online_close_time; $h = $h + $this->schedule_info->increment)
	 		//echo $this->open_time;
            for ($h = $this->open_time; $h < $this->schedule_info->online_close_time; $h = $h + $this->schedule_info->increment)
            {
            	$time_reservation_count = 0;
                //make sure we don't get into weird times
                if ($h%100 >59) 
                    $h = $h + 40;
                $i = $h;
                 
				$continue = array();
				$available_spots = 4;
				$deletable_time = $i + $this->schedule_info->increment;
				if ($deletable_time % 100 > 59)
                    $deletable_time += 40;
				foreach ($this->tracks AS $track)
				{
					//echo $track['track_id'];
					//print_r ($booked_times[$track['track_id']]);
					//echo ' - ';
					//print_r ($booking_delay[$track['track_id']]);
					$continue[$track['track_id']] = false;
					//Determine which times we're going to skip
					
	            	if ((int)substr($booked_times[$track['track_id']][0]['start'],8) <= $i && ($booked_times[$track['track_id']][0]['type'] == 'closed' || $booked_times[$track['track_id']][0]['type'] == 'event' || $booked_times[$track['track_id']][0]['type'] == 'reservation'))
                    {
                        $continue[$track['track_id']] = true;
						//echo 'top';
					}
                    //if anything starts at this time, it's not available
                    else if ((int)substr($booked_times[$track['track_id']][0]['start'],8) == $i)
                    {
                        $continue[$track['track_id']] = true;
						//echo 'mid - '.(int)substr($booked_times[$track['track_id']]['start'],8).' - '.$i.'<br/>';
					}
					//A booking delay exists when you have a teetime overlap your starting online booking time
					else if (isset($booking_delay[$track['track_id']][0]) && ((int)substr($booking_delay[$track['track_id']][0]['end'],8) > $i || (int)substr($booking_delay[$track['track_id']][0]['end'],8) == 0))
					{
						$continue[$track['track_id']] = true;
						//echo 'bot';
					}
			
					if ((int)substr($booked_times[$track['track_id']][0]['end'],8) == (int)$deletable_time)
                    {
              //      	echo 'deleteing time '.$deletable_time;	
					    array_shift($booked_times[$track['track_id']]);
					}
			//print_r ($booked_times[$track['track_id']]);
				}
                
                //Make time
                $hour = $end_hour = floor($i/100);
                $min = $end_min = $i%100;
				$end_min += $this->schedule_info->increment;
				if ($end_min > 59)
				{
					$end_min -= 60;
					$end_hour += 1;
				}
                $min_leading_zero = $end_min_leading_zero = '';
                $am_pm = ($hour<12)?'a':'p';
				$end_am_pm = ($end_hour<12)?'a':'p';
                if ($hour > 12)
                    $hour = $hour -12;
                if ($min < 10)
                    $min_leading_zero = '0';
                if ($end_hour > 12)
                    $end_hour = $end_hour -12;
                if ($end_min < 10)
                    $end_min_leading_zero = '0';
                
                // Overriding row because everything is booked in 30 min increments, thus row index 1
				$row = 1;
                $time = $hour.':'.$min_leading_zero.$min.$am_pm;
				$end_time = $end_hour.':'.$end_min_leading_zero.$end_min.$end_am_pm;
				$sd_z = '';
                if ($this->selected_day < 10)
                    $sd_z = '0';
                $i_z = '';
                if ((int)$i < 1000)
                    $i_z = '0';
                $time_string = $i_z.(int)$i;
				$price_indexes = $this->determine_price_indexes($i, $this->filter_holes, $day_of_week);
				//print_r($price_indexes);
				$price_category = $price_indexes['price_category'];//$this->determine_price_category($i);//"price_category_{$col}";
				$wo_cart_price = number_format($teetime_prices["{$schedule_id}"]["{$course_id}_{$row}"]->$price_category);
                $w_cart_price = number_format($teetime_prices["{$schedule_id}"]["{$course_id}_{$row}"]->$price_category)+($teetime_prices["{$schedule_id}"]["{$course_id}_{$cart_row}"]->$price_category);
                $time_checkboxes = '';
				$date_time = "{$this->selected_year}{$month_zero}{$offset_month}{$day_zero}".(int)$this->selected_day."{$i_z}".(int)$i;
					
					
				foreach ($this->tracks AS $track)
				{
					if ($track['online_booking'])
					{
						if (!$continue[$track['track_id']] && $date_time > $this->now) {
			            	$time_checkboxes .= "<td class='teetime_cell'><span class='simulator_title'><input id='sim_{$track['track_id']}_$date_time' type='checkbox' value='sim_{$track['track_id']}_$date_time'/><label for='sim_{$track['track_id']}_$date_time' class='available_button'>$time - $end_time</label></span></td>";
							$time_reservation_count ++;
						}
						else {
							$time_checkboxes .= "<td class='teetime_cell'><span class='simulator_title'></span></td>";
							//For debugging
							//$time_checkboxes .= "<span class='simulator_title'>".substr($booked_times[0]['start'],8).' : '.substr($booked_times[0]['end'],8)."</span>";
						}
					}
				}
				if ($available_spots >= $this->filter_available_spots && $i >= (int)$this->filter_time_range['start'] && $i < (int)$this->filter_time_range['end'] && $time_reservation_count != 0 ) {
                    $teetimes_html[] = "".
                    	"<tr class='teetime ui-corner-all ui-state-default' id='teetime_$sd_z$this->selected_day$time_string'>".
                    		$time_checkboxes.
                    	"</tr>";
                } 
				else
				{
						//$teetimes_html[] = "".
                    	//"<tr class='teetime ui-corner-all ui-state-default' id='teetime_$sd_z$this->selected_day$time_string'>".
                    	//"</tr>";
                }
		    }
			$response_array[$j]['day_label'] = "<div>".$day_of_week."</div><span>".date("M. j Y", mktime(0,0,0,$this->selected_month,$this->selected_day,$this->selected_year))."</span>";
            $response_array[$j]['teetimes'] = $teetimes_html;
            $teetimes_html = array();
            $this->selected_day++;
            $leap_year = '';
			if ((int)$this->selected_month == 2)
                $leap_year = "_".date('L', mktime(0,0,0,$this->selected_month,$this->selected_day,$this->selected_year));
                
            if ($this->selected_day > $this->month_length[(int)$this->selected_month.$leap_year]) {
                $this->selected_day = 1;
                if ($this->selected_month == 12)
				{
					$this->selected_month = 1;
					$this->selected_year++;
				}
				else
	                $this->selected_month ++;
            }
        }
		for ($j = 0; $j < $this->days_out; $j++) {
        	//If closed for the day, we'll jump ahead to tomorrow
        	$days_out = $j;
        	if ($this->schedule_info->online_close_time-($this->lead_hours.'00')<date('H').'00')
				$days_out = $j + 1;
		    //Month is offset by one
		    $this->offset_date = $this->get_time_string(true, 'date', "$days_out days $this->lead_hours hours");
            $day_of_week = date('D', strtotime($this->offset_date+100));
            //echo "$days_out days $this->lead_hours hours $this->offset_date <br/>";
            
		
	   		
            $response_array[$j]['day_label'] = "<div>".$day_of_week."</div><span>".date("M. j Y", strtotime($this->offset_date+100))."</span>";
            $this->selected_day++;
            $leap_year = '';
            if ((int)$this->selected_month == 2)
                $leap_year = "_".date('L', mktime(0,0,0,$this->selected_month,$this->selected_day,$this->selected_year));
                
            if ($this->selected_day > $this->month_length[(int)$this->selected_month.$leap_year]) {
                $this->selected_day = 1;
                $this->selected_month ++;
            }
        }
		$items = $this->booking_lib->get_basket();
		//print_r($items);
		foreach ($items as $item)
			$response_array['selected'][] = $item['sim_number'];
		
        //print_r( $teetimes_html);
        return $response_array;
    }
    function get_available_deals($teesheet_id) {
    	$billing_info = $this->billing->get_info_by_teesheet_id($teesheet_id);
		
		return '';//'Pay online, and get a free cart. <br/>1 Available';
	}
    function has_online_booking($course_id, $schedule_id = false) {
        $this->db->select('online_booking');
        $this->db->from('schedules');
		$this->db->join('tracks', 'tracks.schedule_id = schedules.schedule_id');
		$this->db->where("course_id", $course_id);
		if ($schedule_id)
			$this->db->where('tracks.schedule_id', $schedule_id);
		$this->db->where('online_booking', 1);
        //$this->db->limit(1);
        $booking_available = $this->db->get();
		//echo $this->db->last_query();
        if ($booking_available->num_rows() > 0)
            return true;
        return false;
    }
	function get_booking_track($schedule_id) {
	    $this->db->select('track_id');
        $this->db->from('tracks');
		$this->db->where('schedule_id', $schedule_id);
		$this->db->where('online_booking', 1);
		$this->db->where('deleted', 0);
        $this->db->limit(1);
		//echo $this->db->last_query();
        return $this->db->get()->row_array();
    }
    function bookTeeTime($title, $start, $end, $allDay, $side, $booker_id = '', $person_id = '', $booking_source = 'teesheet', $player_count='', $holes='', $carts='', $details='', $type='', $email = '', $phone = '') {
        // Get Brand/Golf Course from Session
        $teesheet_id = $this->session->userdata('schedule_id');
        $course_holes = $this->session->userdata('holes');
        
        // Set the location of the second tee time (front or back 9)
        $primary_nine = 'front';
        $second_nine = 'back';
        if ($side == 'back') {
            $primary_nine = 'back';
            $second_nine = 'front';
        }
        else if ($course_holes == 9) {
            $primary_nine = 'front';
            $second_nine = 'front';
        }
        
        // Set the id of the two tee times
        $primary_id = $this->generateID('tt');
        $second_id = $primary_id.'b';
        $frontnine = $this->session->userdata('frontnine');
        $bStart = $start + $frontnine;
        $bEnd = $end + $frontnine;
        if ($bStart%100 >59)
            $bStart += 40;
        if ($bEnd%100 >59)
            $bEnd += 40;
        
        if ($booking_source == 'teesheet') {
            //Insert the tee times
            if (!$this->db->insert('reservations', array('reservation_id'=>$primary_id, 'schedule_id'=>$teesheet_id, 'start'=>$start, 'end'=>$end, 'title'=>$title, 'allDay'=>$allDay, 'side'=>$primary_nine)))
                return json_encode(array('success'=>false,'message'=>lang('customers_tee_time_reserved'), 'tee_time_id'=>$primary_id));
            $this->db->insert('reservations', array('reservation_id'=>$second_id, 'schedule_id'=>$teesheet_id, 'start'=>$bStart, 'end'=>$bEnd, 'title'=>$title, 'allDay'=>$allDay, 'side'=>$second_nine));
        }
        else if ($booking_source == 'online') {
            if (!$this->db->insert('reservations', array('reservation_id'=>$primary_id, 'schedule_id'=>$teesheet_id, 'start'=>$start, 'end'=>$end, 'title'=>$title, 'allDay'=>$allDay, 'side'=>$primary_nine, 'type'=>$type, 'player_count'=>$player_count,'holes'=>$holes,'carts'=>$carts,'details'=>$details,'email'=>$email,'phone'=>$phone, 'className'=>'','person_id'=>$person_id,'booking_source'=>$booking_source,'booker_id'=>$booker_id)))
                return json_encode(array('success'=>false,'message'=>lang('customers_reservation_error'), 'tee_time_id'=>$primary_id, 'sql'=>$this->db->last_query()));
            if ($holes == 18)
                $this->db->insert('reservations', array('reservation_id'=>$second_id, 'schedule_id'=>$teesheet_id, 'start'=>$bStart, 'end'=>$bEnd, 'title'=>$title, 'allDay'=>$allDay, 'side'=>$second_nine, 'type'=>$type, 'player_count'=>$player_count,'holes'=>$holes,'carts'=>$carts,'details'=>$details,'email'=>$email,'phone'=>$phone, 'className'=>'','person_id'=>$person_id,'booking_source'=>$booking_source,'booker_id'=>$booker_id));
        }
        //return $primary_id;
        return json_encode(array('success'=>true,'message'=>lang('customers_tee_time_reserved'), 'tee_time_id'=>$primary_id, 'sql'=>$this->db->last_query()));
    }
    function get_teetimes_to_print($date, $tracks = '', $report = false) {
		$this->db->from("reservations");
		if ($report)
		{
			$this->db->join('sales', 'sales.teetime_id = teetime.TTID', 'LEFT OUTER');
			$this->db->join('sales_items', 'sales.sale_id = sales_items.sale_id', 'LEFT OUTER');
			$this->db->where('course_id', $this->session->userdata('course_id'));
			//$this->db->where('sales_items.price_category !=', '');
			//$this->db->where('sales_items.teesheet !=', '');
			//$this->db->group_by('teetime_id');
			
		}
        //$this->db->like('start', $date, 'after');
        $this->db->where('start >', $date.'0000');
        $this->db->where('start <', $date.'9999');
        $this->db->where('status !=', 'deleted');
        $this->db->where_in('track_id', $tracks);
        $this->db->order_by('start', 'asc');
        $this->db->order_by('side', 'desc');
        $result = $this->db->get();
        //echo $this->db->last_query().'<br/>';
		$results = $result->result_array();
		$teetime_array = array();
        while ($result = array_shift($results)) {
        	if (!isset($teetime_array[$result['track_id']]) || !isset($teetime_array[$result['track_id']][$result['start']]) || !isset($teetime_array[$result['track_id']][$result['start']][$result['reservation_id']]))
	            $teetime_array[$result['track_id']][$result['start']][$result['reservation_id']] = $result;
			
			if ($result['price_category'] != '')
				$teetime_array[$result['track_id']][$result['start']][$result['reservation_id']]['purchases'][] = array('price_category'=>$result['price_category'], 'description'=>$result['description'],'price'=>to_currency($result['quantity_purchased'] * $result['item_unit_price']));
	    }
        return $teetime_array;
    }
    function get_teesheet_info($teesheet_id = '') {
        $teesheet_id = ($teesheet_id != '')? $teesheet_id : $this->session->userdata('schedule_id');
		//echo 'tsid: '.$teesheet_id;
        $this->db->from('schedules');
        $this->db->join('courses', 'schedules.course_id = courses.course_id');
        $this->db->select('schedules.holes AS holes,
            courses.open_time AS open_time,
            courses.close_time AS close_time,
            schedules.increment AS increment,
            schedules.title AS name');
        $this->db->where('schedules.schedule_id', $teesheet_id);
        $this->db->limit(1);
        $result = $this->db->get();
		//echo $this->db->last_query();
        return $result->result_array();
        
    }
    function get_stats($view, $year, $month, $day, $dow) {
        $teesheet_id = $this->session->userdata("schedule_id");
        $statsArray = array();
        $eday = $day+1;
        if ($month < 10)
            $month = "0".$month;
        if ($day < 10)
            $day = "0".$day;
        if ($eday < 10)
            $eday = "0".$eday;
        if ($view == 'agendaDay') {
            //get one day stats
            
            $dayString = $year.$month.$day.'0000';
            $edayString = $year.$month.$eday.'0000';
			if ($this->config->item('sales'))
	            $this->db->select("holes,
	                count(*) AS teetimes,
	                sum(case WHEN `status` = 'checked in' THEN 1 ELSE 0 END) AS tcheckin,
	                sum(case WHEN `status` = 'walked in' THEN 1 ELSE 0 END) AS twalkin,
	                sum(`player_count`) AS players,
	                sum(case WHEN (`status` = 'checked in' OR `status` = 'walked in') THEN `paid_player_count` ELSE 0 END) AS cplayers,
	                sum(case WHEN (`status` = 'checked in' OR `status` = 'walked in') THEN `paid_carts` ELSE 0 END) AS ccarts,
	                sum(`carts`) AS carts");
			else
	            $this->db->select("holes,
	                count(*) AS teetimes,
	                sum(case WHEN `status` = 'checked in' THEN 1 ELSE 0 END) AS tcheckin,
	                sum(case WHEN `status` = 'walked in' THEN 1 ELSE 0 END) AS twalkin,
	                sum(`player_count`) AS players,
	                sum(case WHEN (`status` = 'checked in' OR `status` = 'walked in') THEN `player_count` ELSE 0 END) AS cplayers,
	                sum(case WHEN (`status` = 'checked in' OR `status` = 'walked in') THEN `carts` ELSE 0 END) AS ccarts,
	                sum(`carts`) AS carts");
            $this->db->from('teetime');
            $this->db->where("schedule_id = '$teesheet_id'
                    AND `start` > '$dayString' 
                    AND `end` < '$edayString'
                    AND `status` != 'deleted'
                    AND `TTID` NOT LIKE '____________________b'");
            $this->db->group_by('holes');
            
            $results = $this->db->get();
            foreach($results->result() as $result) {
                if ($result->holes == '9')
                    $statsArray['nine'] = $result;
                else if ($result->holes == '18')
                    $statsArray['eighteen'] = $result;
            }
        }
        else if ($view == 'agendaWeek') {
            //get full week stats
        }
		//			$statsArray['sql'] = $this->db->last_query();
        return json_encode($statsArray);
    }
    function generateID($type = '') {
        if ($type == 'tt')
            $prefix = 'TTID_'.date('mdHis');
        else if ($type == 'ts')
            $prefix = 'teesheet_id_';
        else
            $prefix = '';
       
        $length = 20 - strlen($prefix);
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $string = $prefix;
        for ($p = 0; $p < $length; $p++) {
            $string .= $characters[mt_rand(0, strlen($characters)-1)];
        }
        return $string;
    }
    function sendEmail($to, $subject, $message, $from, $fromName = '') {
        //$message = '';
        //$data2 = file_get_contents("https://sendgrid.com/api/mail.send.xml?api_user=jhopkins@golfcompete.com&api_key=GolfCompete#17&to=$to&subject=$subject&html=$message&from=$from");
        $message = $this->urlEncode($message);
        $ch2=curl_init('https://sendgrid.com/api/mail.send.json');
        curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch2,CURLOPT_POST,1);
        curl_setopt($ch2,CURLOPT_POSTFIELDS,"api_user=jhopkins@golfcompete.com&api_key=GolfCompete#17&to=$to&subject=$subject&html=$message&from=$from&fromname=$fromName");
        //curl_setopt($ch2, CURLOPT_HEADER, false);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER,1);
        $data2 = curl_exec($ch2);
        
        $dataobj = json_decode($data2);
        if ($dataobj->message == 'error') {
            $message = $this->urlEncode("There was a problem sending to \n $to \n");
            $ch2=curl_init('https://sendgrid.com/api/mail.send.json');
            curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch2,CURLOPT_POST,1);
            curl_setopt($ch2,CURLOPT_POSTFIELDS,"api_user=jhopkins@golfcompete.com&api_key=GolfCompete#17&to=jhopkins@golfcompete.com&subject=Failed Email&html=$message&from=error@golfcompete.com");
            //curl_setopt($ch2, CURLOPT_HEADER, false);
            curl_setopt($ch2, CURLOPT_RETURNTRANSFER,1);
            $data2 = curl_exec($ch2);
        }
        
        //echo $data2;
        return $data2;

    }


// Redoing these functions based on new idea of how the data should be gathered and parsed.
	function get_potential_teetimes($start, $end, $increment, $track_id)
	{
		$potential_teetimes_array = array();
		for ($i = $start; $i < $end; $i=$i+$increment)
		{
			$i = ($i%100 > 59)?$i+40:$i;
			$i_z = ($i < 1000)?'0':'';
			$potential_teetimes_array[] = array('potential_teetime'=>$this->offset_date.$i_z.(int)$i, 'track_id'=>$track_id);
		}
//echo "$start - $end - $increment - $teesheet_id";
//print_r($potential_teetimes_array);
		return $potential_teetimes_array;
	}
	function get_teetime_array($start, $end, $increment, $teesheet_id)
	{
		$final_array = array();
		$teetimes = $this->get_potential_teetimes($start, $end, $increment, $teesheet_id);
		foreach($teetimes as $teetime)
		{
			$final_array[$teetime['potential_teetime']] = date('g:ia', strtotime('20120101'.$teetime['potential_teetime']));
		}
		return $final_array;
	}
	function evaluate_potential_teetimes($potential_teetimes_array, $date)
	{
		$final_results = array();
			
		if (count($potential_teetimes_array) > 0) {
			//Create temp table with potential teetimes
			$this->db->query('CREATE TEMPORARY TABLE foreup_potential_teetimes (potential_teetime bigint, track_id int)');
			//echo $this->db->last_query().'<br/>';
			//print_r($potential_teetimes_array);
			$this->db->insert_batch('potential_teetimes', $potential_teetimes_array);
			//echo $this->db->last_query().'<br/>';
			//echo 'Just inserted '.count($potential_teetimes_array).' rows into temp table';
			//return;
			//Perform a join to get relevant data
			$this->db->select('potential_teetime, sum(player_count) as player_count, sum(CASE WHEN type != "teetime" THEN 1 ELSE 0 END) as during_event');
			$this->db->from('reservations');
			$this->db->join('potential_teetimes', 'potential_teetimes.track_id = reservations.track_id');
			$this->db->where('status !=', 'deleted');
			$this->db->where('side', 'front');
			$this->db->where('start <= potential_teetime');
			$this->db->where('end > potential_teetime');
			$this->db->group_by('potential_teetime');
			
			$results = $this->db->get();
			//echo $this->db->last_query().'<br/>';
			//print_r($results);
			//Return relevant data
			while($result = $results->fetch_assoc())
			{
				$final_results[$result['potential_teetime']] = $result;
			}
		}
		return $final_results;
	}
	function get_trimmed_teetimes_2($startDate = '', $endDate = '', $side = '', $teesheet_id = '') {
		//We need an array of possible teetimes, a date (possibly expandable to multiple dates),
		//We will return an array of the possible teetimes, with a player_count, and an event flag if an even is occurring during that time
				        
        // Get Brand/Golf Course from Session
    
        $this->db->where("track_id", $this->session->userdata('track_id'));
        if ($startDate != '') 
            $this->db->where("start >= '$startDate'");
        if ($endDate != '') 
            $this->db->where("end < '$endDate'");
        $this->db->select("*, sum(player_count) AS pcount");
        $this->db->from('reservations');
        if ($side != '')
            $this->db->where('side', $side);
		$this->db->where("status !=", 'deleted');
        $this->db->order_by('end, start');
        $this->db->group_by('end');
        $teetimes = $this->db->get();//createAssocArray($this->db->executeSQL($sql));
        
        return $teetimes->result_array();
    }
    function get_available_teetimes($is_api_request = false, $schedule_id = false, $days_to_future_date = false, $max_price = false, $start_time = false, $end_time = false, $teesheet = '', $response_format = 'html') {			
						
		$this->load->model('billing');
		$billing_info = $this->billing->get_info_by_teesheet_id($this->schedule_id);
		$valid_teetimes = array();
		//cycle through days
		
		//when coming from the API filter_index will be set to represent the days in the future they are requesting
		$this->filter_index = $filter_index ? $filter_index : $this->filter_index;
		
        for ($j = $this->filter_index-1; $j < $this->filter_index; $j++) {
        	//If closed for the day, we'll jump ahead to tomorrow
        	$days_out = $days_to_future_date ? $days_to_future_date : $j; 
			$lead_hours = $this->lead_hours;   										
        	if ($this->schedule_info->online_close_time-($this->lead_hours.'00')<=date('H').'00')
			{
				$days_out = $j + 1;
				$lead_hours = floor($this->lead_hours/24)*24;
			}
		    //Month is offset by one
            $this->offset_date = $this->get_time_string(true, 'date', "$days_out days $lead_hours hours");            
            $day_of_week = date('D', strtotime($this->offset_date+100));
            
			$bartered_teetime_count = $this->billing->get_daily_bartered_teetime_count($this->offset_date);
			$offset_month = $this->selected_month - 1;
            $month_zero = ($offset_month < 10)?'0':'';
            $day_zero = ($this->selected_day < 10)?'0':'';
        	//Set start and end times
            $this->start_time = ($this->offset_time > $this->offset_date.$this->schedule_info->online_open_time)?$this->offset_time:$this->offset_date.$this->schedule_info->online_open_time;
			$this->end_time = $this->offset_date.$this->schedule_info->online_close_time;
			$this->booking_delay = $this->get_booking_delay($this->start_time, 'front');
			// return $this->open_time;
			$this->booked_times = $this->get_trimmed_teetimes_2($this->start_time, $this->offset_date.'2399', '', $this->filter_teesheet_ids);
			//print_r($this->booked_times);
			//echo $this->db->last_query();
$this->benchmark->mark('g_p_t_start');
      		$pot_teetimes =  $this->get_potential_teetimes($this->open_time, $this->schedule_info->online_close_time, $this->schedule_info->increment, $this->track_id);
$this->benchmark->mark('g_p_t_end');
$this->benchmark->mark('e_p_t_start');
			
	 		$blocked_times = $this->evaluate_potential_teetimes($pot_teetimes, ''); 
$this->benchmark->mark('e_p_t_end');
$this->benchmark->mark('for_loop_start');
			$col_class = 'odd';

	   		foreach ($pot_teetimes as $potential_teetime)
			{
				$time = (isset($blocked_times[$potential_teetime['potential_teetime']]))?$blocked_times[$potential_teetime['potential_teetime']]:false;				
				$i = (int)substr($potential_teetime['potential_teetime'],8);
				$i_z = ((int)$i < 1000)?'0':'';
                $date_time = "{$this->offset_date}{$i_z}".(int)$i;
						
				if (($time && ($time['during_event'] || (4 - $time['player_count']) < $this->filter_available_spots)) || //Either the event happens during an event, or is already booked
					((int)substr($potential_teetime['potential_teetime'],8) < (int)$this->filter_time_range['start'] || (int)substr($potential_teetime['potential_teetime'],8) >= (int)$this->filter_time_range['end']) || //Outside of time range filter
					($date_time < $this->offset_time)
				){
					continue;
				}					
				else
				{
										
					//Make time
	                $hour = floor($i/100);
	                $min = $i%100;
	                $am_pm = (floor($i/100)<12)?'am':'pm';
	                $hour = ($hour > 12)?$hour -12:$hour;
	                $min_leading_zero = ($min < 10)?'0':'';
	                $available_spots = ($time)?4-$time['player_count']:4;
	                $time = $hour.':'.$min_leading_zero.$min.$am_pm;
	                $sd_z =($this->selected_day < 10)?'0':'';
	                $time_string = $i_z.(int)$i;
					$price_indexes = $this->determine_price_indexes($i, $this->filter_holes, $day_of_week);
					$price_category = $price_indexes['price_category'];
					$schedule_id = $this->schedule_id;
					$track_id = $this->track_id;
					//print_r($this->teetime_prices);
					$course_id = $this->course_info->course_id;		
					$wo_cart_price = number_format($this->teetime_prices["{$schedule_id}"]["{$course_id}_{$price_indexes['green_fee_index']}"]->$price_category, 2);
	                $w_cart_price = number_format($this->teetime_prices["{$schedule_id}"]["{$course_id}_{$price_indexes['green_fee_index']}"]->$price_category+$this->teetime_prices["{$schedule_id}"]["{$course_id}_{$price_indexes['cart_price_index']}"]->$price_category,2);
	                $date_time = "{$this->selected_year}{$month_zero}{$offset_month}{$day_zero}{$this->selected_day}{$i_z}".(int)$i;
					$day_time = "$sd_z$this->selected_day$time_string";
					$cart_fee = $w_cart_price - $wo_cart_price;
					
					if ($is_api_request && $start_time <= $potential_teetime['potential_teetime'] && $end_time >= $potential_teetime['potential_teetime'])
					{
						$potential_teetime['reservation_time'] = $this->format_time_string($potential_teetime['potential_teetime']);
						unset($potential_teetime['potential_teetime']);
						$potential_teetime['schedule_id'] = $potential_teetime['teesheet_id'];
						unset($potential_teetime['teesheet_id']);
						$potential_teetime['available_spots'] = $available_spots;
						$potential_teetime['green_fee'] = $wo_cart_price;
						$potential_teetime['cart_fee'] = "$cart_fee";					
						
						//only add this potential tee time if it is <= max_price												
						if (!$max_price || ($max_price >= $w_cart_price)){							
							$valid_teetimes[] = $potential_teetime;
						}
					}
					else 
					{
						$teetimes_html[] = "".
	                    "<div class='teetime ui-corner-all ui-state-default $col_class' id='teetime_$day_time'>".
	                        "<div class='top'>".
	                            "<div class='time'>$time</div>".
	                            anchor("be/reserve/$course_id/$this->offset_date/$i/$this->filter_available_spots/$available_spots/$this->filter_holes/$w_cart_price/$wo_cart_price/", 'Reserve',array('class'=>'colbox reserve_link','title'=>'Reserve Your Teetime')).
	                            "<div class='clear'></div>".
	                        "</div>".
	                        "<div class='bottom'>".
	                            "<span class='flag_icon_sm'></span>".
	                            "<span class='holes ui-corner-all ui-state-default'>".
		                            "$this->filter_holes".
		                        "</span>".
	                            "<span class='person_icon_sm'></span>".
		                        "<span class='spots ui-corner-all ui-state-default'>".
	                                    "<span class='ui-button-text' id='teetime_{$day_time}_spots'>$available_spots</span>".
	                            "</span>".
	                            "<span class='cart_icon_sm'></span>".
		                        "<span class='price ui-corner-all ui-state-default'>".
		                            "$$w_cart_price".
		                        "</span>".
		                        "<span class='no_cart_icon_sm'></span>".
		                        "<span class='price ui-corner-all ui-state-default'>".
		                            "$$wo_cart_price".
		                        "</span>".
		                       "<div class='clear'></div>".
	                       "</div>".	
				       "</div>";
					   $col_class = ($col_class == 'odd') ? 'even':'odd';	
					}
				}
			}
            $response_array[$j]['day_label'] = "<div>".$day_of_week.' '.date("M. j", strtotime($this->offset_date+100))."</div>";
            $response_array[$j]['deals'] = '';
            if (count($teetimes_html) > 0)
			{
				//This is to show a daily header saying if a deal is available 
				//if ($billing_info->per_day > $bartered_teetime_count)
				//	$response_array[$j]['deals'] = '<div>There are deals available</div>';
				$response_array[$j]['teetimes'] = $teetimes_html;
			}
			else 
			{
				$response_array[$j]['teetimes'] = array('<div class="no_teetimes">No teetimes available. Please select a different day/time.</div>');
			}
            $teetimes_html = array();
            $this->selected_day++;
            $leap_year = '';
            if ((int)$this->selected_month == 2)
                $leap_year = "_".date('L', mktime(0,0,0,$this->selected_month,$this->selected_day,$this->selected_year));
                
            if ($this->selected_day > $this->month_length[(int)$this->selected_month.$leap_year]) {
                $this->selected_day = 1;
                $this->selected_month ++;
            }
        }
		for ($j = 0; $j < $this->days_out; $j++) {
        	//If closed for the day, we'll jump ahead to tomorrow
        	$days_out = $j;
        	$lead_hours = $this->lead_hours;
        	if ($this->schedule_info->online_close_time-($this->lead_hours.'00')<=date('H').'00')
			{
				$days_out = $j + 1;
				$lead_hours = floor($this->lead_hours/24)*24;
			}
		    //Month is offset by one
            $this->offset_date = $this->get_time_string(true, 'date', "$days_out days $lead_hours hours");
            $day_of_week = date('D', strtotime($this->offset_date+100));
            
		
	   		
            $response_array[$j]['day_label'] = "<div>".$day_of_week."</div><span>".date("M. j Y", strtotime($this->offset_date+100))."</span>";
            $this->selected_day++;
            $leap_year = '';
            if ((int)$this->selected_month == 2)
                $leap_year = "_".date('L', mktime(0,0,0,$this->selected_month,$this->selected_day,$this->selected_year));
                
            if ($this->selected_day > $this->month_length[(int)$this->selected_month.$leap_year]) {
                $this->selected_day = 1;
                $this->selected_month ++;
            }
        }
	   $this->benchmark->mark('for_loop_end');
// echo 'g_p_t '.$this->benchmark->elapsed_time('g_p_t_start', 'g_p_t_end');
// echo 'e_p_t '.$this->benchmark->elapsed_time('e_p_t_start', 'e_p_t_end');
// echo 'f_l '.$this->benchmark->elapsed_time('for_loop_start', 'for_loop_end');
		if (count($valid_teetimes) == 0) {
			$valid_teetimes[] = "No tee times available";
		}
        return $is_api_request? $valid_teetimes : $response_array;
    }
	function print_teesheet($date, $teesheet_id = '', $report = false)
	{
		// NEED TO GATHER ALL INFO FROM ALL TRACKS TO PUT ON PRINTOUT
		$this->load->model("Track");
		$teesheet_id = ($teesheet_id != '')? $teesheet_id : $this->session->userdata('schedule_id');
		$tracks = $this->Track->get_all($teesheet_id)->result_array();
		$tracks_array = array();
		$cols = array('time'=>array('width'=>55));
		$db_data_empty = array('time'=>'');
		foreach($tracks as $track)
		{
			$tracks_array[] = $track['track_id'];
			$db_data_empty['tf'.$track['track_id']] = '';
			$cols['tf'.$track['track_id']] = array('width'=>705/count($tracks));
		}
        $teetimes = $this->get_teetimes_to_print($date, $tracks_array, $report);
		$teesheet_info = $this->get_teesheet_info($teesheet_id);
        $this->load->library('cezpdf');
		$this->load->helper('pdf');
		$this->cezpdf->Cezpdf('a4','landscape');
        $this->cezpdf->ezSetMargins(30,65,30,30);
        
        prep_pdf('landscape'); // creates the footer for the document we are creating.
        $persisting_fevent = '';
        //$persisting_bevent = '';
		
		//echo 'running the for loop<br/>';
		for ($i = (int)$teesheet_info[0]['open_time']; $i <= (int)$teesheet_info[0]['close_time']; $i = $i + (int)$teesheet_info[0]['increment'])
        {
            if ($i % 100 > 59)
                $i = $i + 40;
            $time = $i;
            $time_string = '';
            $t_z = '';
            if ($time < 1000)
                $t_z = '0';
            $time_string = $t_z.$time;
            foreach ($tracks_array as $track_id)
			{
                $labels_array = $db_data_empty;

                $labels_array['time'] = $this->format_time($time);
                $db_data[$time_string] = $labels_array;
	        }
        }
        // ADD RESERVATIONS TO DATA
		foreach ($teetimes  as $track_id => $ttimes)//[$track_id][$date.$time_string] as $ttime)
        {
        	foreach ($ttimes as $time_string => $ttime)
			{
				$booked_time = $time_string;
				foreach ($ttime as $tt)
				{
					$end = $tt['end'] + 1000000;
					$booked_time += 1000000;
					while ($end > $booked_time)
					{
						//echo ($end + 1000000).' > '.$booked_time.' + '.$teesheet_info[0]['increment'].'<br/>';
						$time_string = substr($booked_time, 8);
		        		$db_data[$time_string]['tf'.$track_id] = "{$tt['title']}";
						$booked_time = date('YmdHi', strtotime(($booked_time)." +".(int)$teesheet_info[0]['increment']." minutes"));
					}
						//echo ($end + 1000000).' > '.$booked_time.' + '.$teesheet_info[0]['increment'].'<br/>';
				}
			}
		}
		$col_names = array('time'=>'Time');
		foreach ($tracks as $track)
		{
			$col_names["tf".$track['track_id']] = $track['title'];
		}
		$title = ($report)?'Teesheet Report':'Teesheet';
        $this->cezpdf->selectFont('./fonts/Helvetica.afm');
		// echo '<br/>----------<br/>';
		// print_r($db_data);
		// echo '<br/>----------<br/>';
		$this->cezpdf->ezTable($db_data, 
                        $col_names, 
                        $teesheet_info[0]['name']." $title - ".((int)substr($date, 4,2)+1).'/'.(int)substr($date, 6,2).'/'.(int)substr($date, 0,4), 
                        array('width'=>750,'fontSize'=>10,'titleFontSize'=>14,
                            'cols'=>$cols)
                        );
		$this->cezpdf->ezStream();
		
	}    
}

?>
