<?php
class Marketing_campaign extends CI_Model
{
	/*
	Determines if a given campaign_id is an campaign
	*/
	function exists( $campaign_id )
	{
		$this->db->from('marketing_campaigns');
		$this->db->where('campaign_id',$campaign_id);
		$this->db->where('deleted',0);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the campaigns
	*/
	function get_all($limit=10000, $offset=0)
	{
		$course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        $this->db->from('marketing_campaigns');
		$this->db->where("deleted = 0 $course_id");
		$this->db->order_by("send_date", "desc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}

	function count_all()
	{
		$this->db->from('marketing_campaigns');
		$this->db->where("deleted", 0);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		return $this->db->count_all_results();
	}

	/*
	Gets information about a particular campaign
	*/
	function get_info($campaign_id, $cron = false)
	{
		if (!$cron)
			$this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->from('marketing_campaigns');
		$this->db->where('campaign_id',$campaign_id);
		$this->db->where("deleted", 0);

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			$row = $query->row();
			if(!empty($row->images)){
				$row->images = json_decode($row->images, true);
			}
			return $row;
		}
		else
		{
			//Get empty base parent object, as $campaign_id is NOT an campaign
			$campaign_obj=new stdClass();

			//Get all the fields from campaigns table
			$fields = $this->db->list_fields('marketing_campaigns');

			foreach ($fields as $field)
			{
				$campaign_obj->$field='';
			}

			return $campaign_obj;
		}
	}

	/*
	Gets information about multiple campaigns
	*/
	function get_multiple_info($campaign_ids)
	{
		$this->db->from('marketing_campaigns');
		$this->db->where_in('campaign_id',$campaign_ids);
		$this->db->where('deleted',0);
		$this->db->order_by("send_date", "desc");
		return $this->db->get();
	}

	/*
	 * Checks to see if the account has sufficient credits
	*/
	function has_sufficient_credits($type, $campaign_count, $campaign_id = false)
	{
		$limits = $this->Billing->get_monthly_limits($this->session->userdata('course_id'));
		$this->load->model('Communication');
		$stats = $this->Communication->get_stats($this->session->userdata('course_id'));
		$scheduled_credits = $this->get_scheduled_credits($type, $campaign_id);
		//return true;

		return ($limits[$type.'_limit'] >= ($stats[$type.'s_mk_this_month'] + $campaign_count + $scheduled_credits));
	}


	/*
	 * Gets a count for the scheduled campaigns
	*/
	function get_scheduled_credits($type, $campaign_id = false)
	{
		$this->db->select('SUM(recipient_count) AS recipient_count');
		$this->db->from('marketing_campaigns');
		$this->db->where('queued', 1);
		$this->db->where('deleted', 0);
		$this->db->where('is_sent', 0);
		$this->db->where('type', $type);
		if ($campaign_id)
			$this->db->where('campaign_id !=', $campaign_id);
		$this->db->where('course_id', $this->session->userdata('course_id'));

		$result = $this->db->get();
		//echo $this->db->last_query();
		//return 5;
		$result = $result->result_array();
		return $result[0]['recipient_count'];
	}
	/*
	Inserts or updates a campaign
	*/
	function save(&$campaign_data,$campaign_id=false)
	{
		if(!empty($campaign_data['images']) && is_array($campaign_data['images'])){
			$campaign_data['images'] = json_encode($campaign_data['images']);
		}else{
			$campaign_data['images'] = '';
		}

		if (!$campaign_id or !$this->exists($campaign_id))
		{
			if($this->db->insert('marketing_campaigns',$campaign_data))
			{
				$campaign_data['campaign_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('campaign_id', $campaign_id);
		return $this->db->update('marketing_campaigns',$campaign_data);
	}

	/*
	Updates multiple campaigns at once
	*/
	function update_multiple($campaign_data,$campaign_ids)
	{
		$this->db->where_in('campaign_id',$campaign_ids);
		return $this->db->update('marketing_campaigns',$campaign_data);
	}

	/*
	Deletes one campaign
	*/
	function delete($campaign_id)
	{
		$course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        $this->db->where("campaign_id = '$campaign_id' $course_id");
		return $this->db->update('marketing_campaigns', array('deleted' => 1));
	}

	/*
	Deletes a list of campaigns
	*/
	function delete_list($campaign_ids)
	{
		$this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->where_in('campaign_id',$campaign_ids);
		return $this->db->update('marketing_campaigns', array('deleted' => 1));
 	}

 	/*
	Get search suggestions to find campaigns
	*/
	function get_search_suggestions($search,$limit=25)
	{
        $suggestions = array();

		$this->db->from('marketing_campaigns');
		$this->db->like('lower(name)', strtolower($search));
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where("deleted", 0);
		$this->db->order_by("campaign_id", "asc");
		$by_number = $this->db->get();

		//$suggestions[] = array('label'=>$this->db->last_query());
		foreach($by_number->result() as $row)
		{
			$suggestions[]=array('label' => $row->name, 'value'=>$row->campaign_id);
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	/*
	Preform a search on campaigns
	*/
	function search($search, $limit=20, $offset = 0)
	{
	//	$course_id = '';
    //if (!$this->permissions->is_super_admin())
      $pr = $this->db->dbprefix('marketing_campaigns');
      $course_id = "AND {$pr}.course_id = '{$this->session->userdata('course_id')}'";
	  $this->db->from('marketing_campaigns');
	  $this->db->like('lower(name)', strtolower($search));
	  $this->db->where("deleted = 0 $course_id");
	  $this->db->order_by("campaign_id", "asc");
	  // Just return a count of all search results
      if ($limit == 0)
          return $this->db->get()->num_rows();
      // Return results
      $this->db->offset($offset);
      $this->db->limit($limit);
      return $this->db->get();
	}

	public function get_campaign_value( $campaign_number )
	{
		if ( !$this->exists( $this->get_campaign_id($campaign_number)))
			return 0;

		$this->db->from('marketing_campaigns');
		$this->db->where('campaign_id',$campaign_number);
		return $this->db->get()->row()->value;
	}

	function update_campaign_value( $campaign_number, $value )
	{
		$course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        $this->db->where("campaign_id = '$campaign_number' $course_id");
		$this->db->update('marketing_campaigns', array('value' => $value));
	}

  function get_all_unsent_campaigns()
  {
    $this->db->from('marketing_campaigns');
    // this will filter out all campaigns that are not sent on that hour or before
		$this->db->where("send_date_cst <", date("Y-m-d H:i:s"));
		$this->db->where('is_sent', 0);
		$this->db->where('deleted', 0);
		$this->db->where("queued", 1);
		$this->db->where("attempts <", 1);
		$this->db->order_by("send_date", "desc");
		$this->db->limit(100);
		$this->db->offset(0);
		return $this->db->get()->result_object();
  }
  function mark_attempted($campaign_id)
  {
  		$this->db->query("
  			UPDATE foreup_marketing_campaigns
  			SET attempts = attempts + 1
  			WHERE campaign_id = $campaign_id");
  }
  /**
   * @function send_mails utility function to send batch mails
   * @param Int $campaign_id the campaign id to be updated
   * @param array $mails list of mail recipients
   */
  function send_mails($campaign_id, array $mails, $contents, $course, $subject = '')
  {
  	log_message('error', 'starting marketing_campaign->send_mails campaign_id - '.$campaign_id);
    // send mails here
    $subject = $subject != '' ? $subject : $course->name;//$this->session->userdata('course_name');
    $from = $course->email;//$this->config->item('email');
    $fromName = $course->name;//$this->session->userdata('course_name');
    //echo 'course';
    //print_r($course);

    //foreach($mails as $mail)
    //{
      //$contents = str_replace("__customer_id__",$mail['customer_id'], $contents);
//	  send_sendgrid($mail['email'], $subject, $contents, $from, $fromName, $campaign_id);
    //}
        
    $campaign_obj = $this->get_info($campaign_id);
    $promotion_id = $campaign_obj->promotion_id;
//    $promotion_id = '-1';
    $discout_type = '';
    
    
    $just_emails = array();
    
    foreach($mails as $mail)
	{
		if (filter_var($mail['email'], FILTER_VALIDATE_EMAIL)) {
			$just_emails[] = $mail['email'];
		}
	}
		log_message('error', 'marketing_campaign->send_mails email campaign_id - '.$campaign_id.' count '.count($just_emails));
	send_sendgrid($just_emails, $subject, $contents, $from, $fromName, $campaign_id, $course->course_id);

    $this->db->where('campaign_id', $campaign_id);
    $this->db->update('marketing_campaigns', array('is_sent'=>1, 'status'=>'sent', 'send_date'=>date('Y-m-d H:i:s')));
    //log_message('error', 'FINEE');
  }

  function send_text($campaign_id, array $numbers, $message)
  {
    $error_msg = array();
    /**
     * this is just a temporary place holder, the number should be mapped with the current course,
     * and saved in a session
     */
    $cfrom = $this->config->item('number', 'twilio');

    $check_session_number = $this->config->item('phone');

    // suppose its empty, then just set the default number
    $from = (empty($check_session_number) || true) ? $cfrom : $check_session_number;

    foreach($numbers as $number)
    {
      $num = $number['phone_number'];

      $response = $this->twilio->sms($from, $num, $message, $campaign_id);


      if($response->IsError)
      {
        $error_msg[$num] = 'Error: ' . $response->ErrorMessage;
      }
    }

    // TO DO: log the error_msg

    $this->db->where('campaign_id', $campaign_id);
    $this->db->update('marketing_campaigns', array('is_sent'=>1, 'status'=>'sent', 'send_date'=> date('Y-m-d H:i:s')));
  }

  // function send_sendhub_text($campaign_id, array $numbers, $message)
  // {
//
    // $error_msg = array();
//
	// foreach ($numbers as $key => $contact) {
		// $response;
//
		// if ($contact['sendhub_id'] == '') {
			// $response = $this->sendhub->add_contact($contact['name'], $contact['phone_number']);
		// }
//
		// if ($contact['sendhub_id'] != '') {
			// $response = $this->sendhub->send_message($message, $contact['sendhub_id']);
		// }
//
		// else if ($response->id) {
			// $customer_info = $this->Customer->get_info($contact['customer_id']);
			// $customer_info->sendhub_id = $response->id;
			// $person_data = array('sendhub_id'=>$response->id);
			// $customer_data = array();
//
			// if ($this->Customer->save($person_data, $customer_data, $contact['customer_id'])) {
				// $this->sendhub->send_message($message, $customer_info->sendhub_id);
			// }
		// }
	// }
//
    // $this->db->where('campaign_id', $campaign_id);
    // $this->db->update('marketing_campaigns', array('is_sent'=>1, 'status'=>'sent', 'send_date'=> date('Y-m-d H:i:s')));
  // }

  /**
   * @function opt_out this utility function will handle opt out request
   * @param String $type
   * @param Integer $customer_id
   * @param Integer $campaign_id
   * @param Integer $course_id
   */
  function opt_out($type, $customer_id, $campaign_id, $course_id)
  {
    $type = strtolower($type);

    $this->db->where('person_id', $customer_id);
    $this->db->update('customers', array("opt_out_{$type}" => 'Y'));

    $data = array(
      'customer_id' => $customer_id,
      'campaign_id' => $campaign_id,
      'course_id' => $course_id,
      'action' => "opt out by {$type}",
      'action_date' => date('Y-m-d H:i:s')
    );

    $this->db->insert('subscription_actions', $data);
  }
  /**
   * Utility function to get status count of marketing email and texts
   * returns an array of status
   */
  function get_status_count()
  {
    $course_id = '';
    //if (!$this->permissions->is_super_admin())
    {
      $pr = $this->db->dbprefix('marketing_campaigns');
      $course_id = "AND {$pr}.course_id = '{$this->session->userdata('course_id')}'";
    }
    $this->db->select("type , is_sent, COUNT( * ) count");
    $this->db->from('marketing_campaigns');
		$this->db->where("deleted = 0 $course_id");
    $this->db->group_by(array("type", "is_sent"));
    $res = $this->db->get();

    $data = array(
        'email_sent' => 0,
        'email_remaining' => 0,
        'text_sent' => 0,
        'text_remaining' => 0
    );

    if($res->num_rows() >0)
    {
       foreach($res->result_array() as $res)
       {
         if($res['type'] == 'email')
         {
           if($res['is_sent'] == 1) $data['email_sent'] = $res['count'];
           if($res['is_sent'] == 0) $data['email_remaining'] = $res['count'];

         }
         else
         {
           if($res['is_sent'] == 1) $data['text_sent'] = $res['count'];
           if($res['is_sent'] == 0) $data['text_remaining'] = $res['count'];
         }

       }
    }

    return $data;
  }
}
