<?php
require_once("report.php");
class Summary_items extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array(
			array('data'=>lang('reports_item'), 'align'=> 'left'),
			array('data'=>lang('reports_qty_purchased'), 'align'=> 'left'), 
			array('data'=>lang('reports_subtotal'), 'align'=> 'right'), 
			array('data'=>lang('reports_total'), 'align'=> 'right'), 
			array('data'=>lang('reports_cost'), 'align'=> 'right'), 
			array('data'=>lang('reports_tax'), 'align'=> 'right'),
			array('data'=>lang('reports_profit'), 'align'=> 'right'), 
			array('data'=>lang('reports_margin'), 'align'=> 'right'), 
			array('data'=>lang('reports_qty_remaining'), 'align'=> 'right'), 
			array('data'=>lang('reports_potential_revenues'), 'align'=> 'right'));
	}
	
	public function getData()
	{
		$this->db->select($this->db->dbprefix('sales_items_temp').'.name as name, sum(quantity_purchased) as quantity_purchased, sum(subtotal) as subtotal, sum(total) as total, sum(item_cost_price) as item_cost_price, sum(tax) as tax, sum(profit) as profit, quantity, (unit_price * quantity) as potential_revenues, is_unlimited');
		$this->db->from('sales_items_temp');
		$this->db->join('items', 'sales_items_temp.item_id = items.item_id');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		if ($this->params['value'] != 'all')
		{
			$this->db->where($this->db->dbprefix('sales_items_temp').'.'.$this->params['filter'], urldecode(urldecode($this->params['value'])));
		}
		$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);
		$this->db->group_by($this->db->dbprefix('sales_items_temp').'.item_id');
		$this->db->order_by($this->db->dbprefix('sales_items_temp').'.name');

		$items = $this->db->get();
		//echo $this->db->last_query();
		return $items->result_array();		
	}
	
	public function getSummaryData()
	{
		if (!$this->permissions->is_super_admin())
            $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->select('sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax, sum(profit) as profit');
		$this->db->from('sales_items_temp');
		$this->db->join('items', 'sales_items_temp.item_id = items.item_id');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		if ($this->params['value'] != 'all')
		{
			$this->db->where($this->db->dbprefix('sales_items_temp').'.'.$this->params['filter'], urldecode(urldecode($this->params['value'])));
		}
		$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);
		return $this->db->get()->row_array();
	}
}
?>