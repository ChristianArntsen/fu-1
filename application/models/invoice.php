<?php
class Invoice extends CI_Model
{
	function __construct(){
		$this->load->model('Account_transactions');
		$this->load->model('sale');
		$this->load->model('Customer_billing');
		$this->load->model('Payment');		
		$this->load->library('sale_lib');
		$this->load->library('zip');
	}

	/*
	Determines if a given item_id is an item
	*/
	function exists($invoice_id)
	{
		$this->db->from('invoices');
		$this->db->where("invoice_id", $invoice_id);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the items
	*/
	function get_all($customer_id = false, $limit=10000, $offset=0, $join = false)
	{
		$this->db->from('invoices');
		//$this->db->order_by("name", "asc");
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('invoices.deleted !=', 1);
		if ($join)
			$this->db->join('people', 'people.person_id = invoices.person_id');
		if ($customer_id)
			$this->db->where('invoices.person_id', $customer_id);
		$this->db->offset($offset);
		$this->db->limit($limit);
		$this->db->order_by('date DESC');
		$all = $this->db->get();

		return $all;
	}
	function get_specific_invoices($course_id, $start, $end, $beginning_number, $ending_number)
	{
		$this->db->from('invoices');
		$this->db->where('course_id', $course_id);
		$this->db->where('invoices.deleted !=', 1);
		if ($start)
			$this->db->where("(date >= '$start 00:00:00' AND date <= '$end 23:59:59')");
		if ($beginning_number)
			$this->db->where("(invoice_number >= $beginning_number AND invoice_number <= $ending_number)");
		// if ($join)
			// $this->db->join('people', 'people.person_id = invoices.person_id');
		// if ($customer_id)
			// $this->db->where('invoices.person_id', $customer_id);
		$this->db->order_by('invoice_number ASC');		
		$all = $this->db->get();
		//echo $this->db->last_query();
		return $all;
	}
	function count_all()
	{
		$this->db->from('invoices');
		return $this->db->count_all_results();
	}
	function get_course_payments($course_id)
	{
		$this->db->from('invoice_items');
		$this->db->join('invoices', 'invoice_items.charge_id = invoices.charge_id');
		$this->db->join('customer_credit_cards', 'customer_credit_cards.credit_card_id = invoices.credit_card_id');
		$this->db->where('customer_credit_cards.course_id', $course_id);
		$this->db->order_by('date DESC, invoice_items.charge_id');
		//$result = $this->db->get();
		//echo $this->db->last_query();
		return $this->db->get()->result_array();
	}

	// Apply payment to an invoice (called from Sale model), if payment
	// is over invoice total, go back and pay overdue invoices with excess
	function pay_invoice($sale_id, $invoice_id, $employee_id = false, $payment = 0)
	{
		if($payment == 0){
			return false;
		}

		$this->db->select('total, paid, person_id, employee_id');
		$this->db->from('invoices');
		$this->db->where('invoice_id', $invoice_id);
		$row = $this->db->get()->row_array();

		$amount_due = (float) ($row['total'] - $row['paid']);
		$customer_id = $row['person_id'];
		$course_id = (int) $this->session->userdata('course_id');

		if(empty($employee_id)){
			$employee_id = (int) $row['employee_id'];
		}

		$excess_payment = 0;
		if($payment > $amount_due){
			$excess_payment = (float) ($payment - $amount_due);
		}

		if($excess_payment > 0){
			$payment_applied = $amount_due;
		}else{
			$payment_applied = $payment;
		}

		// Apply total invoice payment to general "invoice" account
		// REDUNDANT... REMOVED 3/26/14 by JOEL HOPKINS
		//$this->Account_transactions->save('invoice', $customer_id, 'Invoice Payment', $payment, lang('sales_point_of_sale').' '.$sale_id, $sale_id, $invoice_id, $employee_id);

		// Update invoice with new paid amount
		$this->save(array('paid' => $row['paid'] + $payment_applied), $invoice_id);

		// If payment applied is greater than invoice amount due,
		// retrieve overdue invoices to apply excess payment to
		if($excess_payment > 0){

			// Retrieve overdue invoices from oldest to newest
			$this->db->select('invoice_id, total, paid');
			$this->db->from('invoices');
			$this->db->where('total - paid > 0');
			$this->db->where('deleted', 0);
			$this->db->where('person_id', $customer_id);
			$this->db->where('course_id', $course_id);
			$this->db->order_by('date ASC');
			$overdue_invoices = $this->db->get()->result_array();

			// Loop through each overdue invoice and apply payment to as many as possible
			foreach($overdue_invoices as $overdue_invoice){
				$due = (float) ($overdue_invoice['total'] - $overdue_invoice['paid']);
				$overdue_invoice_id = (int) $overdue_invoice['invoice_id'];

				if($excess_payment >= $due){
					$overdue_payment = $due;
				}else{
					$overdue_payment = $excess_payment;
				}

				// Update overdue invoice with new paid amount
				$this->db->update('invoices', array('paid' => $overdue_invoice['paid'] + $overdue_payment), array('invoice_id' => $overdue_invoice_id));
				$excess_payment -= $overdue_payment;

				// If we are out of excess payment to apply, exit the loop
				if($excess_payment <= 0){
					break;
				}
			}
		}

		return true;
	}

	/*
	Gets invoice by course_id and invoice number
	*/
	function get_by_invoice_number($invoice_number)
	{
		$this->db->select('invoice_id');
		$this->db->from('invoices');
		$this->db->where('invoice_number', $invoice_number);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$row = $this->db->get()->row_array();
		$invoice_id = $row['invoice_id'];

		return $this->get_details($invoice_id);
	}
	/*
	Gets information about a particular item
	*/
	function get_info($invoice_id)
	{
		$this->db->from('invoices');
		$this->db->where("invoice_id", $invoice_id);

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->result_array();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('invoices');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return (array) $item_obj;
		}
	}

	function has_access($invoice_id, $person_id){
		
		$this->db->select('invoice_id');
		$this->db->from('invoices');
		$this->db->where(array('invoice_id' => (int) $invoice_id, 'person_id' => (int) $person_id));
		$row = $this->db->get()->row_array();
		
		if(!empty($row['invoice_id'])){
			return true;
		}else{
			return false;
		}
	}

	function get_details($invoice_id){
		$invoice_data = array();

		$this->db->select('invoice.*');
		$this->db->from('invoices AS invoice');
		$this->db->where('invoice.invoice_id', (int) $invoice_id);
		$invoice_data = $this->db->get()->row_array();

		$date = $invoice_data['date'];
		$bill_start = $invoice_data['bill_start'];
		$bill_end = $invoice_data['bill_end'];

		// Retrieve customer information
		if(!empty($invoice_data['person_id'])){
			$invoice_data['person_info'] = $this->Customer->get_info($invoice_data['person_id']);
		}

		// Get course information
		$this->db->select('name, address, city, state, state_name, postal,
			woeid, zip, country, phone, email, company_logo');
		$this->db->from('courses');
		$this->db->where('course_id', $invoice_data['course_id']);
		$invoice_data['course_info'] = $this->db->get()->row();

		$total_due = $invoice_data['total'];
		$subtotal = 0.0;
		$tax = 0.0;
		$current_total = 0.0;

		// Loop through invoice items to seperate out taxes
		$invoice_data['current_items'] = $this->get_items($invoice_id);
		foreach($invoice_data['current_items'] as $item){
			$subtotal += (float) $item['subtotal'];
			$tax += (float) $item['tax'];
		}
		$current_total = $subtotal + $tax;

		$invoice_data['current_total'] = $current_total;
		$invoice_data['current_subtotal'] = $subtotal;
		$invoice_data['current_tax'] = $tax;

		// Format dates
		$invoice_data['date'] = date('m/d/Y', strtotime($invoice_data['date']));
		if(empty($invoice_data['due_date']) || $invoice_data['due_date'] == '0000-00-00'){
			$invoice_data['due_date'] = 'n/a';
		}else{
			$invoice_data['due_date'] = date('m/d/Y', strtotime($invoice_data['due_date']));
		}
		
		$invoice_data['account_transactions'] = array();
		
		// Retrieve account transactions (if they should be displayed on invoice)
		if($invoice_data['show_account_transactions'] == 1){

			$account_filter = array();
			$account_filter[] = 'invoice';
			if($invoice_data['pay_customer_account'] == 1){
				$account_filter[] = 'customer';
			}
			if($invoice_data['pay_member_account'] == 1){
				$account_filter[] = 'member';
			}

			// Retrieve transactions for customer,member and invoice accounts
			if(!empty($account_filter)){
				
				$filters = array(
					'date_start' => $bill_start, 
					'date_end' => $bill_end, 
					'exclude_invoice_id' => $invoice_id, 
					'hide_positive' => false, 
					'hide_balance_transfers' => true,
					'only_sales' => true
				);		
				
				// If invoice was manually created, and should show account transactions, show them from begining of time
				if($invoice_data['billing_id'] == 0){
					unset($filters['date_start']);
				}
				
				$invoice_data['account_transactions'] = $this->Account_transactions->get_transactions(
					$account_filter,
					$invoice_data['person_id'],
					$filters,
					$invoice_data['course_id']
				);
			}
		}
		$invoice_data['customer_current_charges'] = $current_total;
		$invoice_data['customer_overdue_charges'] = abs($invoice_data['overdue']);
		$invoice_data['customer_total_due'] = $current_total + $invoice_data['customer_overdue_charges'] - $invoice_data['paid'];

		return $invoice_data;
	}

	/*
		Inserts new or updates existing invoice
	*/
	function save($invoice_data, $invoice_id = false)
	{
		$success = false;
		$invoice_response = false;

		// If creating a new invoice
		if (!$invoice_id or !$this->exists($invoice_id))
		{
			if(empty($invoice_data['items'])){
				return false;
			}
			$invoice_items = $invoice_data['items'];
			unset($invoice_data['items']);
			
			if(empty($invoice_data['date'])){
				$invoice_data['date'] = date('Y-m-d H:i:s');
			}
			
			if(empty($invoice_data['bill_start'])){
				$invoice_data['bill_start'] = '0000-00-00 00:00:00';
			}

			// If no bill end date is selected, use the date being created as bill end date
			if(empty($invoice_data['bill_end']) || $invoice_data['bill_end'] == '0000-00-00 00:00:00'){
				$invoice_data['bill_end'] = $invoice_data['date'];
			}

			// Retrieve outstanding invoice total (for overdue amount)
			$invoice_data['overdue'] = $this->Account_transactions->get_total('invoice', $invoice_data['person_id'], array(
				'date_end' => $invoice_data['bill_end']
			), $invoice_data['course_id']);
			
			// If invoice account is actually positive, reset it to 0 (no amount due)
			if($invoice_data['overdue'] > 0){
				$invoice_data['overdue'] = 0;
			}		

			// Get/set the next invoice_number
			$this->db->select_max('invoice_number');
			$this->db->from('invoices');
			$this->db->where('course_id', $invoice_data['course_id']);
			$result = $this->db->get()->row_array();
			$invoice_data['invoice_number'] = (int) $result['invoice_number'] + 1;

			// Insert new invoice into database
			if($this->db->insert('invoices', $invoice_data)){
				$invoice_id = $this->db->insert_id();
				$success = $invoice_id;
			}
			
			// Create a new sale with invoice items
			$sale = $this->save_items($invoice_items, $invoice_id, $invoice_data['person_id'], $invoice_data['employee_id'], $invoice_data['course_id']);
			$invoice_total = $sale['total'];
			$sale_id = (int) $sale['sale_id'];

			$paid = 0;
			$partial_payment = 0;
			$total_due = $invoice_total;
			// If there is a positive balance on the customer balance or member account and we have those marked to pay off, then we'll use those funds to pay down the invoice
			// Check invoice account first (This isn't actually paying off with the invoice account, it's checking to see if it already paid off the invoice)
			if ($total_due > 0) {
				$customer = $this->Customer->get_info($invoice_data['person_id']);
				if ($customer->invoice_balance > -$total_due) {
					$partial_payment = $total_due + $customer->invoice_balance;
					if ($partial_payment > $total_due) {
						$partial_payment = $total_due;
					}
				}
				$total_due = $total_due - $partial_payment;
				$paid = $paid + $partial_payment;
			}
			// Then check Member Account
			$partial_payment = 0;
			if ($total_due > 0) {
				if ($invoice_data['pay_member_account'] === 1 && $customer->member_account_balance > 0)	{
					if ($customer->member_account_balance > $total_due) {
						$partial_payment = $total_due;
					}
					else {
						$partial_payment = $customer->member_account_balance;
					} 
					// Deduct paid amount from account
					$member_nickname = ($this->config->item('member_balance_nickname') == '' ? lang('customers_member_account_balance'):$this->config->item('member_balance_nickname'));
					$this->Account_transactions->save('member', $invoice_data['person_id'], 'Invoiced Balance Transfer', -$partial_payment, lang('sales_point_of_sale').' '.$sale_id, $sale_id, $invoice_id, $invoice_data['employee_id']);
					$this->Account_transactions->save('invoice', $invoice_data['person_id'], 'Invoice Payment - '.$member_nickname, $partial_payment, lang('sales_point_of_sale').' '.$sale_id, $sale_id, $invoice_id, $invoice_data['employee_id']);
				}
				$total_due = $total_due - $partial_payment;
				$paid = $paid + $partial_payment;
			}			
			// Then check Customer Credit
			$partial_payment = 0;
			if ($total_due > 0) {
				if ($invoice_data['pay_customer_account'] === 1 && $customer->account_balance > 0)	{
					if ($customer->account_balance > $total_due) {
						$partial_payment = $total_due;
					}
					else {
						$partial_payment = $customer->account_balance;
					} 
					// Deduct paid amount from account
					$customer_credit_nickname = ($this->config->item('customer_credit_nickname') == '' ? lang('customers_account_balance'):$this->config->item('customer_credit_nickname'));
					$this->Account_transactions->save('customer', $invoice_data['person_id'], 'Invoiced Balance Transfer', -$partial_payment, lang('sales_point_of_sale').' '.$sale_id, $sale_id, $invoice_id, $invoice_data['employee_id']);
					$this->Account_transactions->save('invoice', $invoice_data['person_id'], 'Invoice Payment - '.$customer_credit_nickname, $partial_payment, lang('sales_point_of_sale').' '.$sale_id, $sale_id, $invoice_id, $invoice_data['employee_id']);
				}
				$total_due = $total_due - $partial_payment;
				$paid = $paid + $partial_payment;
			}			
						
			// Update invoice with sale total
			$this->db->where('invoice_id', $invoice_id);
			$this->db->update('invoices', array(
				'total' => $invoice_total,
				'paid'	=> $paid,
				'sale_id' => $sale_id
			));
			
			$invoice_response['sale_id'] = $sale_id;
			$invoice_response['invoice_id'] = $invoice_id;
			$invoice_response['total'] = $invoice_total;

		// If saving an existing invoice
		}else{
			$this->db->where('invoice_id', $invoice_id);
			$success = $this->db->update('invoices', $invoice_data);

			$invoice_response = array(
				'invoice_id' => $invoice_id
			);
		}

		return $invoice_response;
	}
	function record_manual_change($manual_change_data)
	{
		return $this->db->insert('invoice_changes', $manual_change_data);
	}
	// Creates a new sale with the items
	function save_items($items, $invoice_id, $customer_id, $employee_id, $course_id)
	{
		if(empty($items)){
			return false;
		}

		if(empty($employee_id)){
			$employee_id = 0;
		}
		$sale_total = 0;

		// Reset order of lines so there are no gaps in line numbers
		$items = array_values($items);

		// Loop through invoice items and build array to be logged as sale
		foreach($items as $line => $invoice_item){
			$line = $line + 1;

			// If line items are member or customer account balances
			if($invoice_item['item_type'] == 'member_balance' || $invoice_item['item_type'] == 'customer_balance'){

				if($invoice_item['item_type'] == 'member_balance'){
					$serial_number = 'MemberBalance';
				}else{
					$serial_number = 'CustomerBalance';
				}

				$item = array(
					'item_id' => 0,
					'item_kit_id' => 0,
					'line' => $line,
					'serialnumber' => $serial_number,
					'modifiers' => null,
					'modifier_total' => 0,
					'quantity' => 1,
					'invoice_id' => $invoice_id,
					'description' => $invoice_item['description'],
					'price' => $invoice_item['price'],
					'base_price' => $invoice_item['price'],
					'subtotal' => $invoice_item['price'],
					'tax' => 0,
					'discount' => 0,
					'total' => $invoice_item['price']
				);

			}else if(!empty($invoice_item['item_id']) && $invoice_item['item_type'] == 'item'){

				$item_taxes = $this->Item_taxes->get_info($invoice_item['item_id']);
				$subtotal = $this->sale_lib->calculate_subtotal($invoice_item['price'], $invoice_item['quantity'], 0);
				$tax = $this->sale_lib->calculate_tax($subtotal, $item_taxes);
				$total = $tax + $subtotal;

				$item = array(
					'item_id' => $invoice_item['item_id'],
					'invoice_id' => $invoice_id,
					'modifiers' => null,
					'modifier_total' => 0,
					'line' => (int) $line,
					'quantity' => (float) $invoice_item['quantity'],
					'discount' => 0,
					'price' => $invoice_item['price'],
					'base_price' => $invoice_item['price'],
					'subtotal' => $subtotal,
					'tax' => $tax,
					'total' => $total
				);

			// If line is an item kit
			}else if(!empty($invoice_item['item_id']) && $invoice_item['item_type'] == 'item_kit'){

				$item_taxes = $this->Item_kit_taxes->get_info($invoice_item['item_kit_id']);
				$subtotal = $this->sale_lib->calculate_subtotal($invoice_item['price'], $invoice_item['quantity'], 0);
				$tax = $this->sale_lib->calculate_tax($subtotal, $item_taxes);
				$total = $tax + $subtotal;

				$item = array(
					'item_kit_id' => $invoice_item['item_id'],
					'invoice_id' => $invoice_id,
					'modifiers' => null,
					'modifier_total' => 0,
					'line' => (int) $line,
					'quantity' => (float) $invoice_item['quantity'],
					'discount' => 0,
					'price' => $invoice_item['price'],
					'base_price' => $invoice_item['price'],
					'subtotal' => $subtotal,
					'tax' => $tax,
					'total' => $total
				);

			// If line is a manually entered item
			}else{
				$subtotal = $this->sale_lib->calculate_subtotal($invoice_item['price'], $invoice_item['quantity'], 0);
				$tax = round($subtotal * ($invoice_item['tax_percentage'] / 100), 2);
				$total = $tax + $subtotal;

				$item = array(
					'item_id' => 0,
					'item_kit_id' => 0,
					'description' => $invoice_item['description'],
					'invoice_id' => $invoice_id,
					'modifiers' => null,
					'modifier_total' => 0,
					'line' => (int) $line,
					'quantity' => (float) $invoice_item['quantity'],
					'discount' => 0,
					'price' => $invoice_item['price'],
					'base_price' => $invoice_item['price'],
					'subtotal' => $subtotal,
					'tax' => $tax,
					'total' => $total
				);
			}
			$sale_total += $item['total'];
			$sale_items[] = $item;
		}

		// Invoice is automatically charged against the "invoice" account
		$payments = array();
		$payments[0] = array(
			'payment_type' => 'Invoice Charge',
			'payment_amount' => $sale_total,
			'invoice_id' => 0,
			'customer_id' => $customer_id
		);
		
		// Create new sale with the charged invoice items
		$sale_id = $this->sale->save($sale_items, $customer_id, $employee_id, '', $payments, false, -1, $course_id);
		
		return array(
			'sale_id' => $sale_id,
			'total' => $sale_total
		);
	}

	function get_items($invoice_id)
	{
		$invoice_id = (int) $invoice_id;

		$query = $this->db->query("SELECT *
			FROM (
				SELECT 'item' AS type, si.item_id, si.line, si.serialnumber, IF(si.item_id = 0, si.description, i.name) AS name,
					si.description, si.quantity_purchased AS quantity,
					si.item_unit_price AS price, si.discount_percent AS discount, si.subtotal, si.tax, si.total
				FROM foreup_sales_items	AS si
				LEFT JOIN foreup_items AS i
					ON i.item_id = si.item_id
				WHERE invoice_id = {$invoice_id}
				GROUP BY si.line
				UNION ALL
				SELECT 'item_kit' AS type, si.item_kit_id, si.line, '', kit.name AS name,
					si.description, si.quantity_purchased AS quantity,
					si.item_kit_unit_price AS price, si.discount_percent AS discount, si.subtotal, si.tax, si.total
				FROM foreup_sales_item_kits AS si
				LEFT JOIN foreup_item_kits AS kit
					ON kit.item_kit_id = si.item_kit_id
				WHERE invoice_id = {$invoice_id}
				GROUP BY si.line
			) AS invoice_items
			GROUP BY invoice_items.line
			ORDER BY invoice_items.line ASC");
		$rows = $query->result_array();

		return $rows;
	}

	// Generates a new invoice from a recurring bill
	function generate_from_billing($billing_id, $date = null){

		if(empty($billing_id)){
			return false;
		}
		if(empty($date)){
			$date = date('Y-m-d H:i:s');
		}
		
		// Retrieve recurring bill data
		$bill_data = $this->Customer_billing->get_details($billing_id, false);

		// When this bill was last ran, start date of this bill is the day after the last bill
		$bill_start_timestamp = strtotime($bill_data['last_invoice_bill_end']) + 86400;
		$bill_start = date('Y-m-d 00:00:00', $bill_start_timestamp);
		$bill_end_timestamp = strtotime($date) - 86400;
		$bill_end = date('Y-m-d 23:59:59', $bill_end_timestamp);

		$bill_start_date = date('m/d/Y', $bill_start_timestamp);
		$bill_end_date = date('m/d/Y', $bill_end_timestamp);

		// Loop through billing items, if the item is to pay off balance,
		// get the total balance of account for the billing period
		foreach($bill_data['current_items'] as $key => $bill_item){
			
			if($bill_item['item_type'] != 'member_balance' && $bill_item['item_type'] != 'customer_balance'){
				continue;
			}
			$current_balance_due = (float) 0;

			if($bill_item['item_type'] == 'member_balance'){
				
				// Get balance of member account as of the bill end date
				$bill_end_balance = (float) $this->Account_transactions->get_total('member', $bill_data['person_id'], array(
					'date_end' => $bill_end
				), $bill_data['course_id']);
				
				// Get current balance of member account
				$balance = $this->db->select('member_account_balance')->from('customers')->where('person_id', $bill_data['person_id'])->get()->row_array();
				$current_balance = (float) $balance['member_account_balance'];

			}else if($bill_item['item_type'] == 'customer_balance'){
				
				// Get balance of customer account as of the bill end date
				$bill_end_balance = (float) $this->Account_transactions->get_total('customer', $bill_data['person_id'], array(
					'date_end' => $bill_end
				), $bill_data['course_id']);
				
				// Get current balance of  customer account
				$balance = $this->db->select('account_balance')->from('customers')->where('person_id', $bill_data['person_id'])->get()->row_array();
				$current_balance = (float) $balance['account_balance'];
			}
			$bill_end_credit = '';
			
			// If any balances are positive, set them to 0 (we don't need
			// to charge anything, just display the positive balance)
			if($bill_end_balance > 0){
				$bill_end_credit = '- Credit: '.to_currency(abs($bill_end_balance));
				$bill_end_balance = 0;
			}
			$bill_end_balance = abs($bill_end_balance);
			
			if($current_balance > 0){
				$current_balance = 0;
			}
			$current_balance = abs($current_balance);			
			
			// Restrict the bill end balance to be no more than 
			// what is actually owed on account as of now (in case bill 
			// is generated for past date)
			if($bill_end_balance > $current_balance){
				$bill_end_balance = $current_balance;
			}
			
			// Set price of line to reflect current balance of account
			$bill_data['current_items'][$key]['description'] = $bill_item['description']. ' '.$bill_end_credit.' (As of '.$bill_end_date.')';
			$bill_data['current_items'][$key]['price'] = $bill_end_balance;
		}

		if(empty($bill_data['due_days'])){
			$bill_data['due_days'] = 30;
		}
		$due_date = date('Y-m-d', strtotime('+'.$bill_data['due_days'].' days'));

		// Create new invoice structure
		$invoice_data = array(
			'course_id' => (int) $bill_data['course_id'],
			'bill_start' => $bill_start,
			'bill_end' => $bill_end,
			'billing_id' => $billing_id,
			'person_id' => $bill_data['person_id'],
			'employee_id' => $bill_data['employee_id'],
			'credit_card_id' => $bill_data['credit_card_id'],
			'email_invoice' => (int) $bill_data['email_invoice'],
			'show_account_transactions' => (int) $bill_data['show_account_transactions'],
			'pay_member_account' => (int) $bill_data['pay_member_balance'],
			'pay_customer_account' => (int) $bill_data['pay_account_balance'],
			'items' => $bill_data['current_items'],
			'due_date' => $due_date,
			'date' => $date
		);

		$response = $this->save($invoice_data);
		return $response;
	}

	/*
		Deletes one invoice
	*/
	function delete($invoice_id)
	{
		$invoice_data = $this->db->select('paid, sale_id')
			->from('invoices')
			->where('invoice_id', (int) $invoice_id)
			->get()
			->row_array();
		$sale_id = (int) $invoice_data['sale_id'];
		
		// Delete sale associated with invoice
		$this->Sale->delete($sale_id);
		
		$this->db->where('invoice_id',$invoice_id);
		return $this->db->update('invoices', array('deleted' => 1));
	}
	/*
		Deletes a list of invoices
	*/
	function delete_list($item_ids)
	{
		$this->db->where_in('billing_id',$item_ids);
		return $this->db->update('billing', array('deleted' => 1));
 	}

	/*
		Email invoice to customer
 	*/
	function send_email($invoice_id)
	{
		$data = $this->get_details($invoice_id);

		$data['is_invoice'] = true;
		$data['sent'] = true;
		$data['popup'] = 1;
		$data['emailing_invoice'] = true;
		$email = $data['person_info']->email;
		
		$success = send_sendgrid(
			$email,
			"Invoice From {$data['course_info']->name}",
			$this->load->view('customers/invoice', $data, true),
			$data['course_info']->email,
		 	$data['course_info']->name
		);

		if ($success)
		{
			$data = array('emailed'=> 1, 'send_date'=>date('Y-m-d'));
			$this->save($data, $invoice_id);
		}

		return ($success ? true : false);
	}

	// Retrieves invoices that need emails sent out, optional filter by day
	function get_invoices_to_email($date = null){

		$dateFilter = '';
		if(!empty($date)){
			$date = date('Y-m-d', strtotime($date));
			$dateFilter = "AND DATE(date) = ".$this->db->escape($date);
		}

		$query = $this->db->query("SELECT invoice_id
			FROM foreup_invoices
			WHERE email_invoice = 1
				AND deleted = 0
				AND emailed = 0");
//				{$dateFilter}");

		return $query->result_array();
	}

	// Get which courses had recurring bills that ran on a specific date
	function get_billed_courses($date){

		// Default to today
		if(empty($date)){
			$date = date('Y-m-d');
		}else{
			$date = date('Y-m-d', strtotime($date));
		}
		$date = $this->db->escape($date);

		$query = $this->db->query("SELECT i.course_id, c.name, c.email,
				c.billing_email
			FROM foreup_invoices AS i
			INNER JOIN foreup_courses AS c
				ON c.course_id = i.course_id
			WHERE DATE(date) = {$date}
				AND billing_id > 0
			GROUP BY course_id");

		return $query->result_array();
	}

	function clean_filename($string) {
		$string = str_replace(' ', '_', $string); // Replaces all spaces with underscores
		$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars
		return preg_replace('/-+/', '-', $string); // Replaces multiple underscores with single one
	}

	// Generate PDFs from invoices stored in database
	function generate_pdfs($course_id, $filters = array()){

		$course_id = (int) $course_id;
		$filter_sql = '';

		// Filter rows by specific date
		$date = date('m-d-Y');
		if(!empty($filters['date'])){
			$date = date('m-d-Y', strtotime($filters['date']));
			$sql_date = date('Y-m-d', strtotime($filters['date']));
			$filter_sql .= " AND DATE(date) = ".$this->db->escape($sql_date);
		}

		if(!empty($filters['recurring'])){
			$filter_sql .= " AND billing_id > 0";
		}

		// Filter rows by billing ID
		if(!empty($filters['billing_id'])){
			$ids = array();
			if(is_array($filters['billing_id'])){
				foreach($filters['billing_id'] as $billing_id){
					$ids[] = (int) $billing_id;
				}
			}else{
				$ids[] = (int) $filters['billing_id'];
			}
			$filter_sql .= " AND billing_id IN(".implode(',', $ids).")";
		}

		// Filter rows by Invoice ID
		if(!empty($filters['invoice_id'])){
			$ids = array();
			if(is_array($filters['invoice_id'])){
				foreach($filters['invoice_id'] as $invoice_id){
					$ids[] = (int) $invoice_id;
				}
			}else{
				$ids[] = (int) $filters['invoice_id'];
			}
			$filter_sql .= " AND invoice_id IN(".implode(',', $ids).")";
		}

		// Retrieve invoice_ids
		$query = $this->db->query("SELECT invoice_id
			FROM foreup_invoices
			WHERE deleted = 0 
				AND course_id = {$course_id}
			{$filter_sql}");
		$invoices = $query->result_array();

		if(empty($invoices)){
			return false;
		}

		// Initialize combined PDF and zip class
		$this->zip->clear_data();
		$all_pdf = null;
		$all_pdf = new Html2pdf('P','A4','fr');
		$all_pdf->pdf->SetDisplayMode('fullpage');
		$all_pdf->setDefaultFont('Arial');

		// Loop through invoices to generate PDFs
		foreach($invoices as $invoice){

			$invoice_data = $this->get_details($invoice['invoice_id']);
			$invoice_data['pdf'] = true;
			$invoice_data['popup'] = 1;
			$invoice_data['is_invoice'] = true;
			$invoice_data['sent'] = true;
			$customer = $invoice_data['person_info'];

			// Get HTML to generate PDF
			$invoice_html = $this->load->view('customers/invoice', $invoice_data, true);

			// Generate individual PDF
			$pdf = new Html2pdf('P','A4','fr');
			$pdf->pdf->SetDisplayMode('fullpage');
			$pdf->setDefaultFont('Arial');
			$pdf->writeHTML("<page style'width:600px;'>".$invoice_html."</page>");

			// Add individual PDF to combiniation PDF
			$all_pdf->writeHTML("<page style'width:600px;'>".$invoice_html."</page>");

			// Replace spaces with underscores and filter invalid filename characters
			$first_name = $this->clean_filename($customer->first_name);
			$last_name = $this->clean_filename($customer->last_name);

			$cc_id = '';
			if($invoice_data['credit_card_id'] > 0){
				$cc_id = '_'.$invoice_data['credit_card_id'];
			}

			// Build filename for individual PDF
			$pdf_filename = 'Invoice'.$invoice_data['invoice_number'].'_'.$first_name.'_'.$last_name . $cc_id .'_'. $date . '.pdf';

			// Add newly generated PDF to zip file
			$this->zip->add_data($pdf_filename, $pdf->Output('', true));

			unset($invoice_html, $invoice_data, $pdf, $customer);
		}

		// Add combined PDF to zip file
		$all_pdf_filename = $date."_Combined_Invoices".".pdf";
		$this->zip->add_data($all_pdf_filename, $all_pdf->Output('', true));

		$folder_name = "archives/invoices/{$course_id}";

		// Create necessary folders to store zip file of invoices
		if (!is_dir('archives')){
			mkdir('archives', 0777);
		}
		if (!is_dir('archives/invoices')){
			mkdir('archives/invoices', 0777);
		}
		if (!is_dir($folder_name)){
			mkdir($folder_name, 0777);
		}
		$path = "{$folder_name}/invoices_".$date.".zip";

		// SAVE ZIPPED PDF INVOICES TO THE GOLF COURSE
		$this->zip->archive($path);

		return $path;
	}

	// Retrieves invoices that have credit cards attached that need charged
	function get_invoices_to_charge($date = null){

		$dateFilter = '';
		if(!empty($date)){
			$date = date('Y-m-d', strtotime($date));
			$dateFilter = "AND DATE(date) = ".$this->db->escape($date);
		}

		$query = $this->db->query("SELECT invoice_id, sale_id,
				credit_card_id, course_id, total, paid, (total - paid) AS due
			FROM foreup_invoices
			WHERE deleted = 0
				AND started = 0
				AND charged = 0
				AND last_billing_attempt = '0000-00-00'
				AND credit_card_id > 0
				AND credit_card_payment_id = 0
				{$dateFilter}");

		return $query->result_array();
	}

	// Retrieves any automatic invoice credit card charges that failed
	// on a specific day for a specific course
	function get_charge_report($course_id = null, $date = null){

		if(empty($date)){
			$date = date('Y-m-d');
		}

		$dateFilter = '';
		if(!empty($date)){
			$date = date('Y-m-d', strtotime($date));
			$dateFilter = "AND DATE(i.date) = ".$this->db->escape($date);
		}

		$courseFilter = '';
		if(!empty($course_id)){
			$courseFilter = "AND i.course_id = ".(int) $course_id;
		}

		$query = $this->db->query("SELECT
			(
				SELECT COUNT(i.invoice_id)
				FROM foreup_invoices AS i
				WHERE i.started = 1
					AND i.charged = 1
					AND i.credit_card_payment_id > 0
					{$dateFilter}
					{$courseFilter}
			) AS num_success,
			(
				SELECT COUNT(i.invoice_id)
				FROM foreup_invoices AS i
				WHERE i.started = 1
					AND i.charged = 0
					AND i.credit_card_payment_id > 0
					{$dateFilter}
					{$courseFilter}
			) AS num_failed");
		$report = $query->row_array();

		if($report['num_success'] == 0 && $report['num_failed'] == 0){
			return $report;
		}
		
		// Get list of failed credit cards/customers
		$query = $this->db->query("SELECT i.invoice_id, i.invoice_number,
				i.credit_card_id, i.course_id, i.total, i.paid,
				(i.total - i.paid) AS due, cc_payment.masked_account,
				 cc_payment.card_type, cc_payment.amount, i.person_id,
				 person.first_name, person.last_name
			FROM foreup_invoices AS i
			INNER JOIN foreup_sales_payments_credit_cards AS cc_payment
				ON cc_payment.invoice = i.credit_card_payment_id
			LEFT JOIN foreup_people AS person
				ON person.person_id = i.person_id
			WHERE i.deleted = 0
				AND (cc_payment.status = 'Declined' OR cc_payment.status = 'error')
				AND i.started = 1
				AND i.charged = 0
				AND i.last_billing_attempt != '0000-00-00'
				AND i.credit_card_id > 0
				AND i.credit_card_payment_id > 0
				{$dateFilter}
				{$courseFilter}");
				
		$report['failed_charges'] = $query->result_array();
		return $report;
	}

	function charge_invoice($invoice_id, $course_id, $credit_card_id, $amount){
		// Mark that we have started charge on invoice
		$this->save(array('started' => 1), $invoice_id);

		$charge_info = array(
			'course_id' => (int) $course_id,
			'credit_card_id' => (int) $credit_card_id
		);
		$due = (float) $amount;
		
		$invoice_data = $this->db->select('person_id, employee_id, course_id')
			->from('invoices')
			->where('invoice_id', $invoice_id)
			->get()
			->row_array();
		
		$customer_id = $invoice_data['person_id'];
		$employee_id = $invoice_data['employee_id'];
		$course_id = $invoice_data['course_id'];
		
		// Charge the card, charge_info is passed by reference and
		// will be updated with credit_card_payment_id
		//echo 'about to charge credit card';
		$success = $this->Customer_credit_card->charge($charge_info, $due);

		// If card was charged successfully, update invoice with paid amount
		if($success){
			$this->pay_invoice(0, $invoice_id, 0, $due);
			$invoice_data = array(
				'credit_card_payment_id' => $charge_info['credit_card_payment_id'],
				'charged' => 1,
				'last_billing_attempt' => date('Y-m-d')
			);
			
			// Create a new sale to log that the invoice was paid for
			$sale_items = array();
			$sale_items[] = array(
				'invoice_id' => $invoice_id,
				'line' => 1,
				'price' => $due			
			);
			$sale_payments = array();
			$sale_payments[] = array(
				'payment_type' => $charge_info['payment_type'],
				'payment_amount' => abs($due),
				'invoice_id' => $charge_info['credit_card_payment_id']
			);	
			$this->Sale->save($sale_items, $customer_id, $employee_id, 'Auto-billed invoice', $sale_payments, -1, $course_id);

		// If charge failed, update invoice that an attempt was made
		}else{
			$invoice_data = array(
				'charged' => 0,
				'last_billing_attempt' => date('Y-m-d'),
				'credit_card_payment_id' => $charge_info['credit_card_payment_id']
			);
		}
		// Apply charge update to invoice
		$this->save($invoice_data, $invoice_id);

		return $success;
	}

	function mark_as_started($invoice_id)
	{
		$data = array(
			'last_billing_attempt'=>date('Y-m-d 00:00:00'),
			'started'=>1,
			'charged'=>0,
			'emailed'=>0
		);
		$this->db->where('invoice_id', $invoice_id);
		$this->db->update('foreup_invoices', $data);
	}

	function mark_as_charged($invoice_id)
	{
		$data = array(
			'charged'=>1
		);
		$this->db->where('invoice_id', $invoice_id);
		$this->db->update('foreup_invoices', $data);
	}

	function mark_as_emailed($billing_id)
	{
		$data = array(
			'emailed'=>1
		);
		$this->db->where('invoice_id', $invoice_id);
		$this->db->update('foreup_invoices', $data);
	}
}
?>
