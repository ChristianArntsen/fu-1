<?php
$lang['courses_simulator'] = 'Simulator';
$lang['courses_mercury_id'] = 'Merchant ID';
$lang['courses_mercury_password'] = 'Merchant Password';
$lang['courses_course_number']='UPC/EAN/ISBN';
$lang['courses_retrive_course_info']='Retrive Course Info';
$lang['courses_description']='Description';
$lang['courses_amazon']='Amazon';
$lang['courses_upc_database']='UPC Database';
$lang['courses_cannot_find_course']='Cannot find any information about course';
$lang['courses_foreup_discount_percent'] = 'Discount Percent';
$lang['courses_info_provided_by']='Info provided by';
$lang['courses_basic_information']='Course Information';
$lang['courses_number_information']='Course Number';
$lang['courses_new']='New Course';
$lang['courses_update']='Update Course';
$lang['courses_course']='Course';
$lang['courses_edit_multiple_courses']='Editing Multiple Courses';
$lang['courses_name']='Course Name';
$lang['courses_category']='Category';
$lang['courses_teetimes_per_day']='Teetimes/Day';
$lang['courses_teetimes_per_week']='Teetimes/Week';
$lang['courses_cost_price']='Cost Price';
$lang['courses_unit_price']='Unit Price';
$lang['courses_tax_1']='Tax 1';
$lang['courses_tax_2']='Tax 2';
$lang['courses_sales_tax_1'] = 'Sales Tax';
$lang['courses_sales_tax_2'] = 'Sales Tax 2';
$lang['courses_tax_percent']='Tax';
$lang['courses_tax_percents']='Tax(es)';
$lang['courses_reorder_level']='Reorder Level';
$lang['courses_quantity']='Quantity';
$lang['courses_reorder_level']='Reorder Level';
$lang['courses_no_courses_to_display']='No Courses to display';
$lang['courses_bulk_edit']='Bulk Edit';
$lang['courses_confirm_delete']='Are you sure you want to delete the selected courses?';
$lang['courses_none_selected']='You have not selected any courses to edit';
$lang['courses_confirm_bulk_edit']='Are you sure you want to edit all the courses selected?';
$lang['courses_successful_bulk_edit']='You have successfully updated the selected courses';
$lang['courses_error_updating_multiple']='Error updating courses';
$lang['courses_edit_fields_you_want_to_update']='Edit the fields you want to edit for ALL selected courses';
$lang['courses_error_adding_updating'] = 'Error adding/updating course';
$lang['courses_successful_adding']='You have successfully added course';
$lang['courses_successful_updating']='You have successfully updated course';
$lang['courses_successful_deleted']='You have successfully deleted';
$lang['courses_one_or_multiple']='course(s)';
$lang['courses_cannot_be_deleted']='Could not deleted selected courses, one or more of the selected courses has sales.';
$lang['courses_name_required']='Course Name is a required field';
$lang['courses_category_required']='Category is a required field';
$lang['courses_buy_price_required']='Buy price is a required field';
$lang['courses_unit_price_required']='Selling Price is a required field';
$lang['courses_cost_price_required']='Cost Price is a required field';
$lang['courses_tax_percent_required']='Tax Percent is a required field';
$lang['courses_teesheet_required']='Tee sheet is a required field';
$lang['courses_number_of_teetimes_per_day_required'] = 'Number of teetimes per day required';
$lang['courses_number_of_teetimes_per_week_required'] = 'Number of teetimes per week required';
$lang['courses_weekday_price_required']='Weekday price is a required field';
$lang['courses_weekend_price_required']='Weekend price is a required field';
$lang['courses_quantity_required']='Quantity is a required field';
$lang['courses_reorder_level_required']='Reorder level is a required field';
$lang['courses_unit_price_number']='Unit price must be a number';
$lang['courses_cost_price_number']='Cost price must be a number';
$lang['courses_quantity_number']='Quantity must be a number';
$lang['courses_reorder_level_number']='Reorder level must be a number';
$lang['courses_none'] = 'None';
$lang['courses_teesheet'] = 'Tee sheet';
$lang['courses_time'] = 'Time';
$lang['courses_weekday_price'] = 'Weekday Price';
$lang['courses_weekend_price'] = 'Weekend Price';
$lang['courses_terms'] = 'Terms';
$lang['courses_supplier'] = 'Supplier';
$lang['courses_generate_barcodes'] = 'Generate Barcodes';
$lang['courses_must_select_course_for_barcode'] = 'You must select at least 1 course to generate barcodes';
$lang['courses_excel_import_failed'] = 'Import failed';
$lang['courses_allow_alt_desciption'] = 'Allow Alt Description';
$lang['courses_is_serialized'] = 'Course has Serial Number';
$lang['courses_low_inventory_courses'] = 'Low Inventory Courses';
$lang['courses_serialized_courses'] = 'Serialized Courses';
$lang['courses_no_description_courses'] = 'No Description Courses';
$lang['courses_inventory_comments']='Comments';
$lang['courses_count']='Update Inventory';
$lang['courses_details_count']='Inventory Count Details';
$lang['courses_add_minus']='Inventory to add/subtract';
$lang['courses_current_quantity']='Current Quantity';
$lang['courses_quantity_required']='Quantity is a required field. Please Close ( X ) to cancel';
$lang['courses_do_nothing'] = 'Do Nothing';
$lang['courses_change_all_to_serialized'] = 'Change All To Serialized';
$lang['courses_change_all_to_unserialized'] = 'Change All To Unserialized';
$lang['courses_change_all_to_allow_alt_desc'] = ' Allow Alt Desc For All';
$lang['courses_change_all_to_not_allow_allow_desc'] = 'Not Allow Alt Desc For All';
$lang['courses_use_inventory_menu'] = 'Use Inv. Menu';
$lang['courses_manually_editing_of_quantity'] = 'Manual Edit of Quantity';
$lang['courses_inventory'] = 'Inventory';
$lang['courses_location'] = 'Location';
$lang['courses_most_imported_some_failed'] = 'Most courses imported. But some were not, here is list of their CODE';
$lang['courses_import_successful'] = 'Import courses successful';
$lang['courses_full_path_to_excel_file_required'] = 'Full path to excel file required';
$lang['courses_mass_import_from_excel'] = 'Mass import data from excel sheet';
$lang['courses_download_import_template'] = 'Download Import Excel Template (CSV)';
$lang['courses_import'] = 'Import';
$lang['courses_new_message'] = 'New Message';
$lang['courses_message'] = 'Message';
$lang['courses_import_courses_from_excel'] = 'Import Courses from Excel';
$lang['courses_ets_key'] = 'ETS Key';
?>
