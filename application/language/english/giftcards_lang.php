<?php
$lang['giftcards_giftcard_number']='Giftcard Number';
$lang['giftcards_card_value']='Value';
$lang['giftcards_basic_information']='Giftcard Information';
$lang['giftcards_number_required']='Giftcard Number is a required field';
$lang['giftcards_value_required']='Amount Tendered is a required field';
$lang['giftcards_number']='Giftcard Number must be a number';
$lang['giftcards_value']='Giftcard Value must be a number';
$lang['giftcards_cleanup_old_giftcards'] = 'Cleanup old giftcards';
$lang['giftcards_cleanup_sucessful'] = 'Giftcards cleaned successfuly';
$lang['giftcards_confirm_cleanup'] = 'Are you sure you want to clean ALL deleted giftcards? (This will remove giftcard numbers from deleted giftcards so they can be reused)';
$lang['giftcards_details'] = 'Details';
$lang['giftcards_notes'] = 'Notes';
$lang['giftcards_export']='Export Giftcards';
$lang['giftcards_import']='Import Giftcards';
$lang['giftcards_retrive_giftcard_info']='Retrive Giftcard Info';
$lang['giftcards_description']='Description';
$lang['giftcards_expiration_date']='Expiration Date';
$lang['giftcards_issue_date']='Date Issued';
$lang['giftcards_amazon']='Amazon';
$lang['giftcards_upc_database']='UPC Database';
$lang['giftcards_cannot_find_giftcard']='Cannot find any information about giftcard';
$lang['giftcards_info_provided_by']='Info provided by';
$lang['giftcards_number_information']='Giftcard Number';
$lang['giftcards_new']='New Giftcard';
$lang['giftcards_update']='Update Giftcard';
$lang['giftcards_giftcard']='Giftcard';
$lang['giftcards_edit_multiple_giftcards']='Editing Multiple Giftcards';
$lang['giftcards_department']='Department';
$lang['giftcards_category']='Category';
$lang['giftcards_cost_price']='Cost Price';
$lang['giftcards_unit_price']='Unit Price';
$lang['giftcards_tax_1']='Tax 1';
$lang['giftcards_tax_2']='Tax 2';
$lang['giftcards_sales_tax_1'] = 'Sales Tax';
$lang['giftcards_sales_tax_2'] = 'Sales Tax 2';
$lang['giftcards_tax_percent']='Tax';
$lang['giftcards_tax_percents']='Tax(es)';
$lang['giftcards_reorder_level']='Reorder Level';
$lang['giftcards_quantity']='Quantity';
$lang['giftcards_no_giftcards_to_display']='No Giftcards to display';
$lang['giftcards_bulk_edit']='Bulk Edit';
$lang['giftcards_confirm_delete']='Are you sure you want to delete the selected giftcards?';
$lang['giftcards_none_selected']='You have not selected any giftcards to edit';
$lang['giftcards_confirm_bulk_edit']='Are you sure you want to edit all the giftcards selected?';
$lang['giftcards_successful_bulk_edit']='You have successfully updated the selected giftcards';
$lang['giftcards_error_updating_multiple']='Error updating giftcards';
$lang['giftcards_edit_fields_you_want_to_update']='Edit the fields you want to edit for ALL selected giftcards';
$lang['giftcards_error_adding_updating'] = 'Error adding/updating giftcard';
$lang['giftcards_successful_adding']='You have successfully added giftcard';
$lang['giftcards_successful_updating']='You have successfully updated giftcard';
$lang['giftcards_successful_deleted']='You have successfully deleted';
$lang['giftcards_one_or_multiple']='giftcard(s)';
$lang['giftcards_cannot_be_deleted']='Could not deleted selected giftcards, one or more of the selected giftcards has sales.';
$lang['giftcards_none'] = 'None';
$lang['giftcards_supplier'] = 'Supplier';
$lang['giftcards_generate_barcodes'] = 'Generate Barcodes';
$lang['giftcards_must_select_giftcard_for_barcode'] = 'You must select at least 1 giftcard to generate barcodes';
$lang['giftcards_excel_import_failed'] = 'Import failed';
$lang['giftcards_allow_alt_desciption'] = 'Allow Alt Description';
$lang['giftcards_is_serialized'] = 'Giftcard has Serial Number';
$lang['giftcards_low_inventory_giftcards'] = 'Low Inventory Giftcards';
$lang['giftcards_serialized_giftcards'] = 'Serialized Giftcards';
$lang['giftcards_no_description_giftcards'] = 'No Description Giftcards';
$lang['giftcards_inventory_comments']='Comments';
$lang['giftcards_count']='Update Inventory';
$lang['giftcards_details_count']='Inventory Count Details';
$lang['giftcards_add_minus']='Inventory to add/subtract';
$lang['giftcards_current_quantity']='Current Quantity';
$lang['giftcards_quantity_required']='Quantity is a required field. Please Close ( X ) to cancel';
$lang['giftcards_do_nothing'] = 'Do Nothing';
$lang['giftcards_change_all_to_serialized'] = 'Change All To Serialized';
$lang['giftcards_change_all_to_unserialized'] = 'Change All To Unserialized';
$lang['giftcards_change_all_to_allow_alt_desc'] = 'Allow Alt Desc For All';
$lang['giftcards_change_all_to_not_allow_allow_desc'] = 'Not Allow Alt Desc For All';
$lang['giftcards_use_inventory_menu'] = 'Use Inv. Menu';
$lang['giftcards_manually_editing_of_quantity'] = 'Manual Edit of Quantity';
$lang['giftcards_customer_name'] = 'Customer Name';
$lang['giftcards_excel_import_failed'] = 'Import failed';
$lang['giftcards_most_imported_some_failed'] = 'Most items imported. But some were not, here is list of their CODE';
$lang['giftcards_import_successful'] = 'Import items successful';
$lang['giftcards_full_path_to_excel_file_required'] = 'Full path to excel file required';
$lang['giftcards_mass_import_from_excel'] = 'Mass import data from excel sheet';
$lang['giftcards_download_import_template'] = 'Download Import Excel Template (CSV)';
$lang['giftcards_import_giftcards_from_excel'] = 'Import Giftcards from Excel';
$lang['giftcards_duplicate_item_ids'] = 'Your import has failed due to duplicate item ids or invalid data, please check your .csv file and try again';
$lang['giftcards_format_giftcard_numbers'] = 'Your import has failed due to improperly formatted giftcard numbers. Please use number format and attempt reupload.';
$lang['giftcards_giftcard_details'] = 'Giftcard Details';

// PUNCH CARDS LANGUAGE
$lang['giftcards_punch_card_number'] = 'Punch Card Number';
$lang['giftcards_punch_card_details'] = 'Punch Card Details';

$lang['giftcards_punch_card_number'] = 'Punch Card Number';
$lang['giftcards_punch_card_total_value'] = 'Total Value';
$lang['giftcards_punch_card_redeemed'] = 'Redeemed';
$lang['giftcards_punch_card_customer'] = 'Customer';
$lang['giftcards_punch_card_issue_date'] = 'Date Issued';
$lang['giftcards_punch_card_expiration_date'] = 'Expiration';
$lang['giftcards_punch_card_total_punches'] = 'Total Punches';
$lang['giftcards_punch_card_punches_used'] = 'Punches Used';
$lang['giftcards_punch_card_purchase_date'] = 'Purchase Date';
?>
