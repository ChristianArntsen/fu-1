<style>
	.inv_header {
		font-size:16px;
		padding:10px;
	}
	.inv_data {
		padding:10px;
	}
	.audit_list_entry, input#submit_inventory_audit, #audit_export_link, #reset_item_counts_link,#audit_edit,input#save_count {
		background: #349ac5;
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#349ac5',endColorstr='#4173b3');
		background: -webkit-linear-gradient(top,#349AC5,#4173B3);
		background: -moz-linear-gradient(top,#349AC5,#4173B3);
		color: white;
		display: block;
		font-size: 12px;
		font-weight: normal;
		height: 16px;
		padding: 7px 0 0 10px;
		text-align: center;
		text-shadow: 0px -1px 0px black;
		border-radius: 4px;
		margin-bottom: 10px;
		box-shadow: inset 0px 1px 1px 0px rgba(255,255,255,0.5),0px 3px 1px -2px rgba(255,255,255,.2);
		border: 1px solid #232323;
		line-height:10px;
		cursor:pointer;
	}
	input#submit_inventory_audit,input#save_count {
		height:27px;
		line-height:0px;
		padding:0px;
		width:104px;
	}
	#audit_export_link, #reset_item_counts_link,#audit_edit{
		padding-left:0px;
		margin-right:5px;
	}
	.save_count{
		visibility: hidden;
	}
	.edit_count{
		width: 55px;	
	}

</style>
<table>
	<tbody>
		<tr>
			<td class='inv_header'>
				Audits
			</td>
			<td class='inv_header'>
				Items
			</td>
		</tr>
		<tr>
			<td style='vertical-align:top;' class='inv_data'>
				<div id='inventory_audit_list'>
					<?php echo $this->load->view('inventory_audits/audits'); ?>
				</div>
			</td>
			<td class='inv_data'>
				<div id='inventory_audit'>
					<form id='inventory_audit_form' action='index.php/items/save_inventory_audit' method='POST'>
						<div id='inventory_audit_items'>
						<?php echo $this->load->view('inventory_audits/items'); ?>
						</div>
						<input type='submit' id='save_count' class='save_count update_count' value='Save' />
						<input type='submit' id='submit_inventory_audit' value='Run Report'/>
					</form>
				</div>
			</td>
		</tr>
	</tbody>
</table>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	var submitting = false;
    $('#inventory_audit_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
				success:function(response)
				{
					$(form).unmask();
					if (response.success == 1 && response.status == 'update') {
						$( "#save_count" ).hide();
						$("table td .m_count").each(function() {
		   					var item_id = $('.edit_count').val();
			            	if (item_id == '') {
			            		item_id = 0;
			            	};
			            	$(this).html('<td class="count_column">'+item_id+'</td>');
	        			});
        			};
					//$.colorbox.close();
	                submitting = false;
	                console.dir(response);
	                //reports.generate(0);
	                // UPDATE LIST OF AUDITS
	                $('#inventory_audit_list').html(response.audit_list);
	                // UPDATE AUDIT DATA
	                $('#inventory_audit_items').html(response.audit_items);
				},
				dataType:'json'
			});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
   		},
		messages:
		{
		}
	});
});
</script>