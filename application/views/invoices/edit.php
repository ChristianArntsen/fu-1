<ul id="error_message_box"></ul>
<?php
// print_r($invoice_info);
echo form_open('customers/save_invoice_totals/'.$invoice_info[0]['invoice_id'], array('id'=>'invoice_edit_form'));
?>
<fieldset id="invoice_basic_info">
<div class="field_row clearfix">
<div class="field_row clearfix">
<p style='margin:0 0 10px 10px;'>Any changes made from this window do not affect account balances for this customer.</p>
<p style='margin:0 0 10px 10px;'>All changes only show on this specific invoice.</p> 
<p style='margin:0 0 10px 10px;'>If account balance changes are needed, adjust them in the account balance edit window.</p>
</div>
<?php echo form_label(lang('invoices_paid').':', 'paid',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'paid',
		'id'=>'paid',
		'value'=>$invoice_info[0]['paid'])
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('invoices_overdue').':', 'overdue',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'overdue',
		'id'=>'overdue',
		'value'=>$invoice_info[0]['overdue'])
	);?>
	</div>
</div>
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script>
$(document).ready(function(){
	var submitting = false;
    $('#invoice_edit_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
			success:function(response)
			{
				//post_item_form_submit(response);
				var person_id = $('#customer_id').val();
				var invoice_id = "<?=$invoice_info[0]['invoice_id']?>";
				customer.invoice.load(invoice_id, person_id);
                submitting = false;
				if (response.success)
					$.colorbox2.close();
				else
					$(form).unmask();
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			paid:
			{
				required:true,
				isFloat:true
			},
			overdue:
			{
				required:true,
				isFloat:true
			}
   		},
		messages:
		{
			paid:
			{
				required:"<?php echo lang('invoices_paid_amount_required'); ?>",
				number:"<?php echo lang('invoices_paid_amount_number'); ?>"
			},
			overdue:
			{
				required:"<?php echo lang('invoices_overdue_amount_required'); ?>",
				number:"<?php echo lang('invoices_overdue_amount_number'); ?>"
			}

		}
	});
});
</script>