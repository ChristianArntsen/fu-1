<div style="padding: 15px; background-color: #EFEFEF; overflow: hidden; height: 525px;">
	<?php if(!empty($no_show_policy)){ ?>
	<h4>No Show Policy</h4>
	<div style="margin-bottom: 25px;"><?php echo html_entity_decode($no_show_policy); ?></div>
	<?php } ?>
	<h3 style="color: #c44b00; font-weight: bold; font-size: 20px;"><? echo $user_message ?></h3>

	<iFrame id='ct100_MainContent_ifrm' src="<?php echo $url?>" height="500px" width="660px" scrolling="auto" frameborder="0" runat="server" style="text-align: center;">
		Your browser does not support iFrames. To view this content,
		please download and use the latest version of one of the following browsers:
		Internet Explorer, Firefox, Google Chrome or Safari.
	</iFrame>
</div>