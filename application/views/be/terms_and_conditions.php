<?php
$this->load->view("partial/course_header", array('booked'=>$booked)); ?>
<style>
html {
	background: url(<?php echo base_url(); ?>images/backgrounds/course.png)no-repeat center center fixed;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
	font-family: Quicksand, Helvetica, Arial, sans-serif;
}
#menubar_background, #menubar_full{
	background:none;
}
#menubar_full {
	width: 100%;
	box-shadow: 0px 1px 0px 0px #B9B4AB, 0px 2px 0px 0px #ECE9E4;
	padding: 3px 3px 0px 3px;
}
#content_area {
	width:800px;
}
</style>
<h2>Terms and Conditions</h2><br/>
<p>This tee time is non-cancelable and non-refundable unless the course is closed due to weather.</p><br/>
<p>Booking fees are non-refundable.</p><br/>
<p>Fees are 100% due online to ForeUP Inc.</p><br/>
<p>Please call Foreup.com at 801.215.9487 with any questions or need of assistance.</p><br/>
<p>Expecting more terms and conditions? So are we. We'll have our full terms and conditions available soon.</p>
<?php //$this->load->view("partial/course_footer"); ?>