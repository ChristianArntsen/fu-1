<div style="padding: 15px; background-color: #EFEFEF; overflow: hidden;">
	<?php if(!empty($no_show_policy)){ ?>
	<h4>No Show Policy</h4>
	<div style="margin-bottom: 25px;"><?php echo html_entity_decode($no_show_policy); ?></div>
	<?php } ?>

	<?php if(!empty($customer_credit_cards[0]['credit_card_id'])){ ?>
		<label for="credit_card_id" style="display: block; font-weight: bold; color: #666; font-size: 12px;">Use Existing Card</label>
		<?php echo $this->Customer_credit_card->dropdown($this->session->userdata('customer_id'), $customer_credit_cards[0]['credit_card_id']); ?>

		<a id="use_new_card" href="" style="display: block; margin-top: 10px;" class="">Use New Card</a>
		<div class="ui-state-default ui-corner-all" style="width: 150px; margin: 0 auto;">
			<a id="finish_reservation" href="#" style="color: white;">Reserve</a>
		</div>
	<?php } ?>
</div>