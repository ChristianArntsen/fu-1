<script type="text/html" id="template_cart_totals">
<table>
	<tbody>
		<tr>
			<td>
				<div class="left" id="items_in_basket"><%=num_items%> Items</div>
			</td>
			<td style="width: 150px;">
				<div id="cart_subtotal" style="overflow: hidden; margin-bottom: 5px;">
					<div style="float: right; margin-right: 10px;"><%=accounting.formatMoney(subtotal)%></div>
					<div style="float: left; text-align: right; width: 50px;">Subtotal</div>
				</div>
				<div id="cart_tax" style="overflow: hidden; margin-bottom: 5px;">
					<div style="float: right; margin-right: 10px;"><%=accounting.formatMoney(tax)%></div>
					<div style="float: left; text-align: right; width: 50px;">Tax</div>
				</div>
				<div id="cart_total" style="overflow: hidden;">
					<h3 id="cart_final_total" style="font-size: 18px; display: inline; float: right; margin-right: 10px;"><%=accounting.formatMoney(total)%></h3>
					<h3 style="font-size: 18px; float: left; text-align: right; width: 50px;">Total</h3>
				</div>
			</td>
		</tr>
	</tbody>
</table>
</script>
