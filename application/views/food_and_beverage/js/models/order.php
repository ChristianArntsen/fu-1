var Order = Backbone.Model.extend({
	url: function(){
		return this.instanceUrl;
	},
	idAttribute: "ticket_id",
	defaults: {
		"ticket_id": null,
		"items": null,
		"message": null,
		"printed": false,
		"deleted": false,
		"completed": false,
		"date_created": null,
		"date_ordered": null,
		"date_completed": null
	},

	initialize: function(attributes, options){

		if(options && options.url){
			this.instanceUrl = options.url;
		}

		// When order is made, mark items in cart as "ordered"
		this.listenTo(this, 'sync', this.markItems, this);

		// When order is made, print order receipt in kitchen
		this.listenTo(this, 'sync', this.printOrder, this);
		this.listenTo(this, 'invalid', displayError, this);
	},

	// Override the default backbone save function
	// We only need to send certain fields to the server when making an order
    save: function(attrs, options) {
		options || (options = {});

		// Only send line numbers to order function
		var modifiedAttrs = {'items':[], 'message':this.get('message')};
		_.each(this.get('items'), function(item){
			modifiedAttrs.items.push({'line': item.get('line')});
		});

		options.data = JSON.stringify(modifiedAttrs);
		options.contentType = 'application/json';

        // Proxy the call to the original save function
        Backbone.Model.prototype.save.call(this, attrs, options);
    },

	markItems: function(order){
		// Loop through each item in order, mark as ordered
		_.each(order.get('items'), function(orderItem){
			var cartItem = App.cart.get(orderItem.get('line'));
			cartItem.set('is_ordered', true);
		});

		App.cart.unSelectItems();
		set_feedback('Order sent', 'success_message', false, 1500);
	},

	validate: function(){
		var complete = true;

		// Loop through each item in order and check if they are complete
		_.each(this.get('items'), function(orderItem){
			var item = App.cart.get(orderItem.get('line'));
			if(!item.isComplete()){
				item.set({'incomplete':true});
				complete = false;
			}
		});

		if(!complete){
			App.cart.unSelectItems();
			return 'Order failed. Please complete marked items';
		}
	},

	// Print function called AFTER order is successfully sent to database
	// and has received a response. Each item/side has its associated
	// kitchen printer IP stored in field 'printer_ip'
	printOrder: function(){
		
		var builder = new StarWebPrintBuilder();
		var ticket_data = {};
		var header_data = '';
		
		// Add Header... date, time, table, employee, ticket #...
		header_data += builder.createTextElement({data:'\n\n\n'});
		header_data += builder.createTextElement({width:2, data:'Prep Ticket #' + this.get('ticket_id') + '\n'});
		header_data += builder.createTextElement({data:'TABLE #' + App.table_num + '\n'});
		header_data += builder.createTextElement({width:1, data:'\n******************************************\n'});
		
		var receiptText = '';
		var EOL = '\r\n';
		var items = this.get('items');

		// Ticket ID is from server response, it is unique primary key
		// of ticket from database
		//header_data += builder.createTextElement({data:'Ticket #' + this.get('ticket_id') + '\n'});
		var d = new Date(this.get('date_created'));
		var minutes = (d.getMinutes() < 10 ? '0'+d.getMinutes() : d.getMinutes());
		var date_string = (d.getMonth()+1)+'-'+d.getDate()+'-'+(d.getFullYear().toString().substr(2))+' '+d.getHours()+':'+minutes;
		header_data += builder.createTextElement({width:1, data:'SERVER: ' + App.receipt.header.employee_name + '\n'});
		
		// If there is a customer attached to sale, add their name to order
		if(App.table_name && App.table_name != ''){
			header_data += builder.createTextElement({width:1, data:'CUSTOMER: ' + App.table_name + '\n'});
		}		
		
		header_data += builder.createTextElement({width:1, data:'DATE: ' + date_string + '\n'});
		header_data += builder.createTextElement({data:'******************************************\n\n'});

		if (this.get('message')){
			header_data += builder.createTextElement({width:2, data:'***** ORDER EDIT ****' + EOL + EOL})
			header_data += builder.createTextElement({width:1, emphasis: true, data:'     ' + this.get('message') + EOL + EOL})
		}
		header_data += builder.createTextElement({width:2, emphasis: false, data:'ITEMS' + '\n\n'});

		var cart = {};
		
		// Loop through items on ticket
		_.each(items, function(item){
			var printer_ip = item.get('printer_ip');
			ticket_data[printer_ip] += builder.createTextElement({width:2, height: 2, invert: false, emphasis: true, data: parseInt(item.get('quantity')) + ' ' + item.get('name')+'\n'});

			receiptText += item.get('name') + ' (' + item.get("printer_ip") + ')' + EOL;

			// Loop through item modifiers
			if(item.get('modifiers').nonDefault().length > 0){
				
				_.each(item.get('modifiers').nonDefault(), function(modifier){
					var selected_option = modifier.get('selected_option')
					if(selected_option == ''){
						selected_option = 'n/a';
					}
					receiptText += '-- ' + modifier.get('name') + ': ' + selected_option + EOL;
					ticket_data[printer_ip] += builder.createTextElement({width:2, height:1, emphasis:false, invert: true, data:' * '+modifier.get('name')+' - '+ selected_option +'\n'});
				});
			}
			
			// Loop through sides, soups, and salads (if any of each)
			// If side item_id is set to 0, it means the guest did not
			// want a side and it can be ignored
			if(item.get('sides')){
				_.each(item.get('sides').models, function(side){
					receiptText += side.get('name') + ' (' + side.get("printer_ip") + ')' + EOL;
					ticket_data[side.get("printer_ip")] += builder.createTextElement({width:2, height:1, invert: false, emphasis:false, data:' - '+side.get('name')+'\n'});
				});
			}
			if(item.get('soups')){
				_.each(item.get('soups').models, function(soup){
					receiptText += soup.get('name') + ' (' + soup.get("printer_ip") + ')' + EOL;
					ticket_data[soup.get("printer_ip")] += builder.createTextElement({width:2, height:1, invert: false,   emphasis:false, data:' - '+soup.get('name')+'\n'});
				});
			}
			if(item.get('salads')){
				_.each(item.get('salads').models, function(salad){
					receiptText += salad.get('name') + ' (' + salad.get("printer_ip") + ')' + EOL;
					ticket_data[salad.get("printer_ip")] += builder.createTextElement({width:2, height:1, invert: false,   emphasis:false, data:' - '+salad.get('name')+'\n'});
				});
			}
			
			if (item.get('comments')){
				ticket_data[printer_ip] += builder.createTextElement({width:2, height:1,  emphasis:false, invert: false,  data:'COMMENTS: '+item.get('comments') +  EOL});
			}
		});
		var footer_data = builder.createTextElement({data:'\n\n\n'});
		footer_data += builder.createCutPaperElement({feed:true});
		
		for (var ip in ticket_data){
			var receipt_data = ticket_data[ip];
			webprnt.print(header_data + receipt_data + footer_data, "http://"+ip+"/StarWebPrint/SendMessage");
		}
	
		return false;
	}
});
