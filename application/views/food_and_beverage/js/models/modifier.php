var Modifier = Backbone.Model.extend({
	idAttribute: "modifier_id",
	defaults: {
		"modifier_id": null,
		"name": "",
		"selected_price": 0.00,
		"selected_option": "",
		"options": [],
		"price": 0.00,
		"item_id": null,
		"category_id": 0,
		"required": false,
		"default": ""
	}
});
