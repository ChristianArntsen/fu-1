<?php
echo form_open('reports/save_register_log/'.$rl_data['register_log_id'],array('id'=>'register_log_form'));
//print_r($employee_data);
//print_r($rl_data);
?>
<ul id="error_message_box"></ul>
<fieldset id="register_log_info">
<legend><?php echo lang("sales_edit_register_log"); ?></legend>
<div id='member_account_info' class="field_row clearfix">
<?php echo form_hidden('log_terminal_id', $rl_data['terminal_id']); ?>
<?php echo form_label(lang('reports_employee'), 'employee',array('class'=>'')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'employee_name',
		'id'=>'employee_name',
		'size'=>'20',
		'value'=>$employee_data['first_name'].' '.$employee_data['last_name']
		)
	);
	echo form_hidden('employee_id', $rl_data['employee_id']);
	?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('reports_shift_start'), 'shift_start',array('class'=>'')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'shift_start',
		'id'=>'shift_start',
		'size'=>'20',
		'value'=>$rl_data['shift_start'])
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('reports_shift_end'), 'shift_end',array('class'=>'')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'shift_end',
		'id'=>'shift_end',
		'size'=>'20',
		'value'=>$rl_data['shift_end'])
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('reports_open_amount'), 'open_amount',array('class'=>'')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'open_amount',
		'id'=>'open_amount',
		'size'=>'20',
		'value'=>$rl_data['open_amount'])
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('reports_close_amount'), 'close_amount',array('class'=>'')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'close_amount',
		'id'=>'close_amount',
		'size'=>'20',
		'value'=>$rl_data['close_amount'])
	);?>
	</div>
</div>
<div class='clear' style='text-align:center'>
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</div>
</fieldset>

<?php
echo form_close();
?>
<style>
	html[xmlns] div.hidden {
		display:none;
	}
</style>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	$( "#employee_name" ).autocomplete({
		source: "<?php echo site_url('employees/suggest');?>",
		delay: 200,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)
		{
			event.preventDefault();
			$('#employee_name').val(ui.item.label);
			$('#employee_id').val(ui.item.value);
		}
	});
	$('#shift_start').datetimepicker();
	$('#shift_end').datetimepicker();
	var submitting = false;
    $('#register_log_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			submitting_payment = true;
			$(form).ajaxSubmit({
				success:function(response)
				{
					$.colorbox.close();
					submitting = false;
					if (response.success)
					{
						$('#register_edit_'+response.register_log_id).closest('tr').replaceWith(response.row_data)
					}
				},
				dataType:'json'
			});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
		},
		messages:
		{
     	}
	});
});
</script>