<?php 
//print_r($recent_transactions);
	if (count($recent_transactions) > 0) 
	{
	    foreach ($recent_transactions  as $rt) 
	    { 
	        if ($rt['sale_id'] != '')
	        {
?>
	<div class='recent_transaction' id='transaction_<?=$rt['sale_id']?>'>
		<?php for ($i = 0; $i <5; $i++) { 
   		$cci = $rt['credit_card_invoice'][$i];
			if (strpos($cci['payment_type'], 'Bank') !== false) {
				$cci['invoice_id'] = '';	
			}	
	    ?>
	    <input type='hidden' id='credit_card_invoice_<?=$rt['sale_id']?>_<?=$i?>' value='<?=$cci['invoice_id']?>'/>
	    <input type='hidden' id='credit_card_type_<?=$rt['sale_id']?>_<?=$i?>' value='<?=$cci['payment_type']?>'/>
	    <?php } ?>
	    <a href='index.php/sales/edit/<?=$rt['sale_id']?>/popup/sales/width~720' title='Edit Sale - POS <?=$rt['sale_id']?>' class='colbox'>
	       <div class='transaction_info'>
	           <span class='transaction_time'><?php echo date('g:i A', strtotime($rt['sale_time']))?></span>
	           <span class='transaction_id <?php echo ($rt['total']<0?'red':'')?>'><?=$rt['sale_id']?></span>
	           <div class='clear'></div>
	       </div>
	       <div class='transaction_name'>
	           <?php echo $rt['customer_name']?>
	           <span class='transaction_total'><?php echo to_currency($rt['total']); ?></span>
	           <div class='clear'></div>
	       </div>
	       <div class='hover_mask' style='display:none'>
	               
	       </div>
	    </a>
	</div>
<?php 		}
		}
?>
	<a id='more_recent_transactions' href='#'>
		<div class='more_holder'>
			More...
		</div>
	</a>
	<script>
		$('#more_recent_transactions').click(function(e){
			e.preventDefault();
			$.ajax({
	           type: "POST",
	           url: "index.php/sales/get_recent_transactions/<?=count($recent_transactions)+20;?>",
	           data: '',
	           success: function(response){
	           	   $('#recent_transactions').html(response);
				},
	            dataType:'html'
	         }); 
			})
	</script>
<?php
	}
	else 
	{
?>
	 <div class='recent_transaction'>
	       <div class='transaction_info'>
	           <span class='transaction_time'></span>
	           <span class='transaction_id'></span>
	       </div>
	       <div class='transaction_name'>
	       	No recent transactions
	       </div>
	       <span class='transaction_total'></span>
	       <div class='hover_mask' style='display:none'>
	               
	       </div>
	 </div>
<?php
	}
?>
<script type="text/javascript">
var tbindings = {
   <?php for ($i = 0; $i <5; $i++) { 
   		//$rec_tran = $recent_transactions[$i];	
   ?>
   'add_tip_<?=$i?>': function(t) {
		var sale_id = $(t).attr('id').replace('transaction_','');
		var invoice_number = $('#credit_card_invoice_'+sale_id+'_<?=$i?>').val();
		var payment_type = $('#credit_card_type_'+sale_id+'_<?=$i?>').val();
		//var invoice_number = '<?=$rec_tran['invoice_id'] ?>';
		//var payment_type = '<?=$rec_tran['payment_type'] ?>';
		//console.log(sale_id);
		//return;
		$.colorbox2({'href':"index.php/sales/tip_window/"+sale_id+"/"+invoice_number, 'width':500, 'title':'Add Tip - '+payment_type})
   },
    <?php } ?>
    'add_tip': function(t) {
		var sale_id = $(t).attr('id').replace('transaction_','');
		$.colorbox2({'href':"index.php/sales/tip_window/"+sale_id+"/", 'width':500, 'title':'Add Tip'})
   },
   'print_transaction_receipt': function(t) {
		var sale_id = $(t).attr('id').replace('transaction_','');
		//console.log(sale_id);
		<?php if ($this->config->item('print_after_sale')) { ?>
		//return;
		$.ajax({
           type: "POST",
           url: "index.php/sales/receipt/"+sale_id+"/1",
           data: "",
           success: function(response){
		   	//Delete suspended sale button
		   	    var receipt_data = '';
                // Items
				var data = response;
                <?php if ($this->config->item('webprnt')) { ?>
                    var receipt_data = build_webprnt_receipt(data);
	                console.log(receipt_data);
	                var header_data = {
                        course_name:'<?php echo $this->config->item('name')?>',
                        address:'<?php echo str_replace(array("\r", "\r\n", "\n"), ' ', addslashes($this->config->item('address')))?>',
                        address_2:'<?php echo $this->config->item('city').', '.$this->config->item('state').' '.$this->config->item('zip')?>',
                        phone:'<?php echo $this->config->item('phone')?>',
                        employee_name:'<?php echo addslashes($user_info->last_name).', '.addslashes($user_info->first_name); ?>',
                        customer:''// Need to load this in from data
                    };
                    receipt_data = webprnt.add_receipt_header(receipt_data, header_data)
                    receipt_data = webprnt.add_paper_cut(receipt_data);

                    webprnt.print(receipt_data, "http://<?=$this->config->item('webprnt_ip')?>/StarWebPrint/SendMessage");
	                //print_webprnt_receipt(receipt_data, false, data.sale_id, data);
	            <?php } else { ?>
	                var receipt_data = build_receipt(data);
	                console.log(receipt_data);
	                print_receipt(receipt_data, false, data.sale_id, data);
			    <?php } ?>        
		   	console.dir(response);
		   	//print_receipt(response);
		   },
           dataType:'json'
        });
        <?php } else { ?>
        	window.location = '<?php echo site_url(''); ?>/sales/receipt/'+sale_id;
        <?php } ?>	
   },
    'email_receipt': function(t) {
		var sale_id = $(t).attr('id').replace('transaction_','');
		$.colorbox2({'href':"index.php/sales/email_reciept_window/"+sale_id+"/", 'width':500, 'title':'Email Receipt'})
   },
};
$('.recent_transaction').contextMenu('myTransactionMenu',{
	bindings:tbindings,
	onContextMenu:function(e, menu) {
		var sale_id = $(e.target).closest('.recent_transaction').attr('id').replace('transaction_','');
		console.log('#credit_card_invoice_'+sale_id);
		//console.log('invoice '+invoice);
		<?php for ($i = 0; $i <5; $i++) { 
   		$rec_tran = $recent_transactions[$i];	
  		?>
  		//var invoice_number = '<?=$rec_tran['invoice_id'] ?>';
		var invoice_number = $('#credit_card_invoice_'+sale_id+'_<?=$i?>').val();
		var payment_type = $('#credit_card_type_'+sale_id+'_<?=$i?>').val();
		if (payment_type != '')
			$('#add_tip_<?=$i?>').html('Add Tip - '+payment_type.replace('xxxxxxxxxxxx', ''));
		<?php } ?>
        //return menu;
        return true;
	},
	onShowMenu: function(e, menu) {
		var sale_id = $(e.target).closest('.recent_transaction').attr('id').replace('transaction_','');
		<?php for ($i = 0; $i <5; $i++) { 
   		$rec_tran = $recent_transactions[$i];	
  		?>
  		var invoice_number = $('#credit_card_invoice_'+sale_id+'_<?=$i?>').val();
		if (!invoice_number || invoice_number == 0 || invoice_number == '')
			$('#add_tip_<?=$i?>', menu).remove();
		<?php } ?>
        return menu;
      }
});
$(".colbox").colorbox();
</script>