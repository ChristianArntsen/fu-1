<fieldset>
	<legend></legend>
<div class="field_row clearfix">	
    <?php echo form_label(lang('employees_employee').':', 'employee',array('class'=>'wide')); ?>
    <div class='form_field'> 	
    <?php echo form_dropdown('employees_dropdown', $employees, $employee_id, 'id="employees_dropdown"');?>
    </div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('login_password').':', 'password',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_password(array(
		'name'=>'password',
		'id'=>'password',
		'value'=>'')
	);?>
	</div>
</div>
<div class="field_row clearfix">	
    <?php echo form_label(lang('common_terminal').':', 'terminal',array('class'=>'wide')); ?>
    <div class='form_field'> 	
    <?php echo form_dropdown('terminals_dropdown', $terminals, $this->session->userdata('terminal_id'), 'id="terminals_dropdown"');?>
    </div>
</div>

<div id='employee_data'>
	<?php $this->load->view('timeclock/employee_data');?>
</div>
</fieldset>
<script>
$(document).ready(function() {
	$('#employees_dropdown').change(function(){
		$.ajax({
            type: "POST",
            url: "index.php/timeclock/load_employee_data/"+$('#employees_dropdown').val(),
            data: "",
            success: function(response){
                $('#employee_data').html(response);
            },
            dataType:'html'
        });
	});
});
</script>