<style>
	#home_module_list .module_item a {
		width: 265px;
	}
</style>
<div id='user_menu_contents'>
	<div id='user_menu_header'>
	</div>
	<div id='user_actions'>
		<div id="home_module_list">
			<div class="module_item">
				<a href="index.php/sales/closeregister?continue=logout">
					<span class="module_icon logout_icon"></span>
					<div>
						<div>Close Cash Log</div>
						<div class='module_desc'>Close Cash Log and Sign out</div>
					</div>
				</a>
			</div>
			<div class="module_item">
				<a href="index.php/sales/logout" id='switch_user'>
					<span class="module_icon switch_user_icon"></span>
					<div>
						<div>Switch User</div>
						<div class='module_desc'>Keep Cash Log and Switch User</div>
					</div>
				</a>
			</div>
			<?php if ($this->permissions->is_admin()) { ?>
			<div class="module_item">
				<a href="index.php/home/logout/bypass" id='bypass_cash_log'>
					<span class="module_icon logout_icon"></span>
					<div>
						<div>Bypass Cash Log</div>
						<div class='module_desc'>Bypass Cash Log & Sign out</div>
					</div>
				</a>
			</div>
			<?php } ?>
			<div class='clear'></div>
		</div>
	</div>
</div>