<?php if (!$hide_jquery) { ?>
<script src="<?php echo base_url();?>js/jquery-1.10.2.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
<?php } ?>
<div>
	<?php
		if ($card_captured)
			echo 'Card capture successful';
		else {
			echo 'Card capture did not occur. Closing window.';
		}
	//echo $test_value;
	//echo $payment_amount;
	//echo $payment_type;
	?>
</div>
<?php
if ($controller_name == 'teesheets')
{
	?>
	<script>
		//$(document).ready(function(){
			window.parent.added_credit_card("<?=$credit_card_id?>");
		//})
	</script>
	<?php
}
else
{
	$cc_checkboxes = "";
	$cc_dropdown = "<select id='cc_dropdown' name='cc_dropdown'>";
	$cc_dropdown .= "<option value='invoice'>Only Generate Invoice</option>";

	foreach($credit_cards as $credit_card)
	{
		$cc_checkboxes .= "<div id='checkbox_holder_".$credit_card['credit_card_id']."'class='field_row clearfix ".(($credit_card['customer_id'] != '' && $credit_card['customer_id'] == $person_info->person_id)?'':'hidden')."'>".
				"<div class='form_field'>".
					form_checkbox('groups[]', $credit_card['credit_card_id'], ($credit_card['customer_id'] != '' && $credit_card['customer_id'] == $person_info->person_id) ? true:FALSE, "id='group_checkbox_{$credit_card['credit_card_id']}'")
					.' '.$credit_card['card_type'].' '.$credit_card['masked_account'].
					" - <span class='' onclick='customer.add_billing_row(\'".$credit_card['credit_card_id']."\')'>Add Billing</span>".
				"</div>
			</div>";
			$selected = '';
			if ($credit_card_id == $credit_card['credit_card_id'])
			{
				$selected = 'selected';
				$selected_card = "{$credit_card['card_type']} {$credit_card['masked_account']}";
			}
		$cc_dropdown .= "<option value='{$credit_card['credit_card_id']}' $selected>Charge to {$credit_card['card_type']} {$credit_card['masked_account']}</option>";

	}
	$cc_dropdown .= '</select>';
?>
<script>
<?php if ($billing_id > -1) { ?>
	$(document).ready(function(){
		window.parent.reload_customer("<?=$cc_dropdown ?>");
	});
<?php } else { ?>$
	$(document).ready(function(){
		window.parent.reload_customer_credit_cards(<?=$customer_id?>);
	});
<?php } ?>
</script>
<?php } ?>