<?php
$card_logos = array(
	'M/C' => base_url('images/sales/mastercard.png'),
	'AMEX' => base_url('images/sales/amex.png'),
	'DISCOVER' => base_url('images/sales/discover.png'),
	'VISA' => base_url('images/sales/visa.png'),
	'DINERS' => base_url('images/sales/diners.png'),
	'JCB' => base_url('images/sales/jcb.png'),
); ?>
<style>
#accordion ul.credit-cards {
	display: block;
	overflow: hidden;
	padding: 10px 0px 0px 0px;
	margin: 0px;
}

#accordion ul.credit-cards > li.card {
	width: 190px;
	height: 115px;
	border-radius: 5px;
	border: 1px solid #E0E0E0;
	background: #EFEFEF;
	padding: 10px 15px;
	display: block;
	float: left;
	margin: 0px 10px 10px 0px;
	position: relative;
}

#accordion ul.credit-cards > li.card.new {
	border: 5px dashed #E0E0E0;
	width: 180px;
	height: 105px;
	cursor: pointer;
}

#accordion ul.credit-cards > li.card.new:hover {
	background-color: #f1ffe5;
	border-color: #c9e2b3;
	color: #c9e2b3;
}

#accordion ul.credit-cards > li.card.new:hover h3 {
	color: #c9e2b3;
}

li.card.new > h3 {
	text-align: center;
	font-weight: bold;
	font-size: 24px;
	margin-top: 25px;
	color: #BBB;
}

li.card span.number {
	display: block;
	position: absolute;
	top: 60px;
	font-size: 14px;
	text-shadow: 0px 1px 0px #AAA;
	letter-spacing: 1px;
}

li.card span.expiration {
	display: block;
	position: absolute;
	bottom: 10px;
	left: 10px;
	letter-spacing: 1px;
}

li.card span.type {
	top: 10px;
	left: 10px;
	display:block;
	position: absolute;
	letter-spacing: 1px;
	font-size: 12px;
	font-weight: bold;
}

li.card span.expiration > h4 {
	font-size: 8px;
	display: block;
	margin: 0px;
	padding-bottom: 4px;
}

li.card a.delete {
	color: #d34545;
	font-size: 12px;
	width: 60px;
	height: 12px;
	background: pink;
	line-height: 12px;
	text-align: right;
	text-decoration: underline;
	position: absolute;
	right: 10px;
	top: 10px;
	display: block;
	background: none;
}

li.card.new > a, li.card.new > a:active, li.card.new > a:visited {
	display: block;
	position: absolute;
	top: 0px;
	bottom: 0px;
	left: 0px;
	right: 0px;
	padding: 0px;
	margin: 0px;
	padding-top: 45px;
	text-align: center;
	font-size: 18px;
	color: #BBB;
	border: none !important;
}

li.card a.delete:hover {
	color: #993232;
}

li.card img.card-logo {
	display: block;
	position: absolute;
	bottom: 10px;
	right: 10px;
	width: 55px;
	height: 34px;
}
</style>
<h2>Credit Cards</h2>
<pre><?php //print_r($credit_cards); ?></pre>
<ul style="margin-bottom: 25px;" class="credit-cards">
<?php foreach($credit_cards as $card){
$number = 'XXXX XXXX XXXX '.preg_replace('/[^0-9]/', '', $card['masked_account']);

if($card['token_expiration'] != '0000-00-00'){
	$expiration = $card['token_expiration'];
}else{
	$expiration = $card['expiration'];
}
$expiration = date('m/y', strtotime($expiration));
?>
	<li class="card">
		<span class="type"><?php echo $card['card_type']; ?></span>
		<span class="number"><?php echo $number; ?></span>
		<span class="expiration"><h4>VALID THRU</h4><?php echo $expiration; ?></span>
		<img src="<?php echo $card_logos[$card['card_type']]; ?>" class="card-logo" />
		<a class="delete" data-credit-card-id="<?php echo $card['credit_card_id']; ?>" href="<?php echo site_url('member/delete_credit_card'); ?>">Delete</a>
	</li>
<?php } ?>
	<li class="card new">
		<a class="colbox new-credit-card" title="Add New Card" href="<?php echo site_url('be/load_card_auth_window'); ?>">
			<h3>Add New Card</h3>
		</a>
	</li>
</ul>

<script>
$(function(){
	$('a.new-credit-card').colorbox({
		'width': 600,
		'data': {
			return_url: 'member/add_credit_card/<?php echo $customer_id; ?>'
		}
	});

	$('ul.credit-cards a.delete').live('click', function(e){
		if(!confirm('Are you sure you want to delete this credit card?')){
			return false;
		}
		var card = $(this).parents('li.card');
		var card_id = $(this).attr('data-credit-card-id');
		var url = $(this).attr('href');

		$.post(url, {credit_card_id:card_id}, function(response){
			if(response.success){
				card.fadeOut('fast');
				set_feedback(response.message, 'success_message');
			}else{
				set_feedback(response.message, 'error_message');
			}
		},'json');

		return false;
	});
});
</script>
