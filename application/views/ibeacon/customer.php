<div id="ibeacon_customer_<?php echo $person_id; ?>" class="ibeacon">
	<div class="photo">
		<img src="<?php echo $photo; ?>" style="width: 100px; height: 100px;" />
	</div>
	<div class="details">
		<h3><?php echo $first_name; ?> <?php echo $last_name; ?></h3>
		<?php if($account_number){ ?><span>Account #: <?php echo $account_number; ?></span><?php } ?>
		<?php if($phone){ ?><span><?php echo $phone; ?></span><?php } ?>
		<?php if($email){ ?><span><?php echo $email; ?></span><?php } ?>
		<?php if(!empty($comments)){ ?><span class="comments"><h4 style="margin-top: 5px;">Comments</h4> <?php echo $comments; ?></span><?php } ?>
		<span class="balance" style="border-top: 1px solid #CCC; margin-top: 5px; padding-top: 5px;">
			<?php echo $customer_account_name; ?> <span class="amount <?php if($account_balance < 0){ ?>red<?php } ?>"><?php echo to_currency($account_balance); ?></span>
		</span>
		<span class="balance">
			<?php echo $member_account_name; ?> 
			<span class="amount <?php if($member_account_balance < 0){ ?>red<?php } ?>"><?php echo to_currency($member_account_balance); ?></span>
		</span>
		<span class="balance">
			Billing Account <span class="amount <?php if($invoice_balance < 0){ ?>red<?php } ?>"><?php echo to_currency($invoice_balance); ?></span>
		</span>			
		<span class="balance">
			Loyalty Points <span class="amount"><?php echo $loyalty_points; ?></span>
		</span>		
	</div>
	<a class="close" title="Close notification">&times;</a>
</div>
