<?php
//print_r($teetime_info);
//echo form_open('teesheets/move/'.$teetime_id.'/'.$teetime_info->player_count, array('id'=>'move_form'));
echo form_open("teesheets/save_teetime/".$teetime_id, array('id'=>'move_form'));
?>
<style>
	#time, #new_date {
		width:80px;
	}
</style>
<fieldset id="move_basic_info">
<legend><?php echo 'Change Day/Time'; ?></legend>
<div class="field_row clearfix">
<?php echo form_label(ucwords($teetime_info->type).':', 'name',array('class'=>'wide lbllong')); ?>
	<div class='form_field'>
		<?php echo $teetime_info->title; ?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label('Tee Sheet:', 'new_teesheet',array('class'=>'wide label-override lbllong')); ?>
	<div class='form_field'>
		<?php echo form_dropdown('new_teesheet', $teesheets, $this->session->userdata('teesheeet_id'));?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label('Date/Time:', 'name',array('class'=>'wide lbllong')); ?>
	<div class='form_field'>
		<?php echo date('m/d/Y g:ia', strtotime($teetime_info->start+1000000)); ?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label('New Date/Time:', 'name',array('class'=>'wide lbllong label-override')); ?>
	<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'new_date',
			'id'=>'new_date',
			'value'=> date('m/d/Y', strtotime($teetime_info->start+1000000)))
			); 
		?>
<?php//		"&start="+newstart+"&end="+newend+"&side="+side ?>
		<?php echo form_dropdown('new_time', $teetimes, date('Hi', strtotime($teetime_info->start+1000000)));?>
		<?php echo form_hidden('difference', $difference); ?>
		<?php echo form_hidden('frontnine', $frontnine); ?>
		<?php echo form_hidden('start', ''); ?>
		<?php echo form_hidden('end', ''); ?>
		<?php echo form_hidden('side', $side); ?>
		<?php //echo form_hidden('event_type', $teetime_info->type); ?>

		<!--?php echo form_input(array(
			'name'=>'time',
			'id'=>'time',
			'disabled'=>'disabled',
			'value'=> date('g:ia', strtotime($teetime_info->start+1000000)))
			); 
		?-->
		<?php //echo form_hidden('new_time', date('g:ia', strtotime($teetime_info->start+1000000)));
		?>
	</div>
</div>
<!--div class="field_row clearfix">
<?php echo form_label('New Time:', 'name',array('class'=>'required wide lbllong label-override')); ?>
	<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'new_time',
			'id'=>'new_time',
			'value'=> date('g:ia', strtotime($teetime_info->start)))
			); 
		?>
	</div>
</div-->
<?php
echo form_submit(array(
	 'name'=>'submitButton',
 	'id'=>'submitButton',
	'value'=> lang('common_save'),
	'class'=>'submit_button')
);
?>
</fieldset>
</form>
<script>
	$(document).ready(function(){
		$("#new_date").datepicker();
		//$("#new_time").timepicker();
		var submitting = false;
		$('#new_teesheet').change(function() {
			$.ajax({
	           type: "POST",
	           url: "index.php/teesheets/get_teetime_dropdown",
	           data: 'teesheet_id='+$('#new_teesheet').val(),
	           success: function(response){
	           		$('#new_time').replaceWith(response.teetime_dropdown);
	           		$('#difference').replaceWith(response.difference_input);
	           		$('#frontnine').replaceWith(response.frontnine_input);
	           },
	           dataType:'json'
	         }); 
		});
	    $('#move_form').validate({
			submitHandler:function(form)
			{
		        if (submitting) return;
					submitting = true;
				$(form).mask("<?php echo lang('common_wait'); ?>");
				var d = new Date($('#new_date').val());
				var t = $('#new_time').val();
				var et = parseInt(t, 10) + parseInt($('#difference').val(), 10);
				et = parseInt(et, 10)%100>59?parseInt(et,10)+40:et;
				//console.log('<?php echo $teetime_info->end.' - '.$teetime_info->start?>');
				//console.log(parseInt(t, 10)+' + '+parseInt($('#difference').val(), 10));
				//console.log(t+' - '+et+' OR '+(parseInt(et, 10)+40));
				var new_start = d.getFullYear()+(d.getMonth()<10?'0':'')+d.getMonth()+(d.getDate()<10?'0':'')+d.getDate()+(parseInt(t, 10)<1000?'0':'')+parseInt(t, 10);
				var new_end = d.getFullYear()+(d.getMonth()<10?'0':'')+d.getMonth()+(d.getDate()<10?'0':'')+d.getDate()+(parseInt(et, 10)<1000?'0':'')+parseInt(et, 10);
				//et = et%100>59?parseInt(et,2)+40:et;
				$('#start').val(new_start);
				$('#end').val(new_end);
				//console.log('submitting');
				$(form).ajaxSubmit({
			        success:function(response)
			        {
						Calendar_actions.update_teesheet(response.teetimes);
                    	//console.dir(response);
                        $.colorbox.close();
			            submitting = false;
			        },
			        dataType:'json'
			    });
		
			},
			errorLabelContainer: "#error_message_box",
	 		wrapper: "li",
			rules:
			{
			
			},
			messages:
			{
			
			}
		});
	});
</script>
