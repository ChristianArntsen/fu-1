<style>
	#cboxContent h1 {
		font-size:16px;
	}
</style>
<ul id="error_message_box"></ul>
<?php
echo form_open('courses/save/'.$item_info->course_id,array('id'=>'item_form'));
?>
<fieldset id="item_basic_info">
<legend>Admin Course Information</legend>
<div id='basic_course_info' style='display:none'>
	<div class="field_row clearfix">
	<?php echo form_label(lang('courses_name').':', 'name',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'name',
			'id'=>'name',
			'value'=>$item_info->name)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('config_address').':', 'address',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'address',
			'id'=>'address',
			'value'=>$item_info->address)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('common_city').':', 'city',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'city',
			'id'=>'city',
			'value'=>$item_info->city)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('common_state').':', 'state',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'state',
			'id'=>'state',
			'value'=>$item_info->state)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('common_zip').':', 'zip',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'zip',
			'id'=>'zip',
			'value'=>$item_info->zip)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('common_phone').':', 'phone',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'phone',
			'id'=>'phone',
			'value'=>$item_info->phone)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('common_email').':', 'email',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'email',
			'id'=>'email',
			'value'=>$item_info->email)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('config_website').':', 'website',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'website',
			'id'=>'website',
			'value'=>$item_info->website)
		);?>
		</div>
	</div>
</div>
<script type='text/javascript'>
	$('#basic_course_info').expandable({title:'Basic Course Info:', open:function(){$.colorbox.resize()}, close:function(){$.colorbox.resize()}});
</script>

<div id='booking_info' style='display:none'>
	<div class="field_row clearfix">
	<?php echo form_label(lang('courses_foreup_discount_percent').':', 'foreup_discount_percent',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'foreup_discount_percent',
			'id'=>'foreup_discount_percent',
			'value'=>$item_info->foreup_discount_percent)
		);?>
		</div>
	</div>
</div>
<script type='text/javascript'>
	$('#booking_info').expandable({title:'Online Booking Info:', open:function(){$.colorbox.resize()}, close:function(){$.colorbox.resize()}});
</script>
<div id='mercury_info' style='display:none'>
	<div class="field_row clearfix">
	<?php echo form_label(lang('courses_mercury_id').':', 'mercury_id',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'mercury_id',
			'id'=>'mercury_id',
			'value'=>$item_info->mercury_id)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('courses_mercury_password').':', 'mercury_password',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_password(array(
			'autocomplete'=>'off',
			'name'=>'mercury_password',
			'id'=>'mercury_password',
			'value'=>$item_info->mercury_password)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('courses_ets_key').':', 'ets_key',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'ets_key',
			'id'=>'ets_key',
			'value'=>$item_info->ets_key)
		);?>
		</div>
	</div>
</div>
<script type='text/javascript'>
	$('#mercury_info').expandable({title:'Credit Card Processing Info:', open:function(){$.colorbox.resize()}, close:function(){$.colorbox.resize()}});
</script>
<!--div id='payment_info' style='display:none'>
	<div class="field_row clearfix">
	<?php echo form_label(lang('courses_teetimes_per_day').':', 'teetimes_per_day',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'teetimes_per_day',
			'id'=>'teetimes_per_day',
			'value'=>$item_info->teetimes_per_day)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('courses_teetimes_per_week').':', 'teetimes_per_week',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_password(array(
			'autocomplete'=>'off',
			'name'=>'teetimes_per_week',
			'id'=>'teetimes_per_week',
			'value'=>$item_info->teetimes_per_week)
		);?>
		</div>
	</div>
</div>
<script type='text/javascript'>
	$('#payment_info').expandable({title:'Payment Info:', open:function(){$.colorbox.resize()}, close:function(){$.colorbox.resize()}});
</script-->
<div id='course_simulator' style='display:none'>
	<div class="field_row clearfix">
	<?php echo form_label(lang('courses_simulator').':', 'simulator',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="simulator_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->simulator == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'simulator',
	            'id'=>'simulator0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'simulator',
	            'id'=>'simulator1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="simulator0">No</label><?php
	            echo form_radio($data1);?><label for="simulator1">Yes</label><?php
	        ?>
	            </div>
		</div>
	</div>
</div>
<script type='text/javascript'>
	$('#course_simulator').expandable({title:'Course Simulator:', open:function(){$.colorbox.resize()}, close:function(){$.colorbox.resize()}});
</script>
<div id='course_groups' style='display:none'>
	<?php
	$first_label = true;
	foreach($groups as $group)
	{
	?>
	<div class="field_row clearfix">
	<?php echo form_label(($first_label)?lang('customers_groups').':':'', 'groups'); ?>
		<div class='form_field'>
			<?	echo form_checkbox('groups[]', $group['group_id'], ($group['is_member']) ? true:FALSE).' '.$group['label'].' <span style="color:#ccc; font-size:smaller;">('.$group['type'].')</span>';?>
		</div>
	</div>
	<?php
	$first_label = false;
	} ?>
</div>
<div id='course_managed_groups' style='display:none'>
	<?php
	$first_label = true;
	foreach($managed_groups as $group)
	{
	?>
	<div class="field_row clearfix">
	<?php echo form_label(($first_label)?lang('customers_groups').':':'', 'managed_groups'); ?>
		<div class='form_field'>
			<?	echo form_checkbox('managed_groups[]', $group['group_id'], ($group['is_management']) ? true:FALSE).' '.$group['label'].' <span style="color:#ccc; font-size:smaller;">('.$group['type'].')</span>';?>
		</div>
	</div>
	<?php
	$first_label = false;
	} ?>
</div>
<script type='text/javascript'>
	$('#course_groups').expandable({title:'Course Groups:', open:function(){$.colorbox.resize()}, close:function(){$.colorbox.resize()}});
	$('#course_managed_groups').expandable({title:'Course Managed Groups:', open:function(){$.colorbox.resize()}, close:function(){$.colorbox.resize()}});
</script>
<div id='course_permissions' style='display:none'>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_teesheets').':', 'teesheets',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="teesheets_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->teesheets == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'teesheets',
	            'id'=>'teesheets0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'teesheets',
	            'id'=>'teesheets1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="teesheets0">Off</label><?php
	            echo form_radio($data1);?><label for="teesheets1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_reservations').':', 'reservations',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="reservations_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->reservations == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'reservations',
	            'id'=>'reservations0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'reservations',
	            'id'=>'reservations1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="reservations0">Off</label><?php
	            echo form_radio($data1);?><label for="reservations1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_schedules').':', 'schedules',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="schedules_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->schedules == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'schedules',
	            'id'=>'schedules0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'schedules',
	            'id'=>'schedules1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="schedules0">Off</label><?php
	            echo form_radio($data1);?><label for="schedules1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_sales').':', 'sales',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="sales_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->sales == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'sales',
	            'id'=>'sales0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'sales',
	            'id'=>'sales1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="sales0">Off</label><?php
	            echo form_radio($data1);?><label for="sales1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_food_and_beverage').':', 'food_and_beverage',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="food_and_beverage_radio">    
		<?php 
	        $zero = false;
	        $one = false;
	        if ($item_info->food_and_beverage == 0)
	            $zero = true;
	        else 
	            $one = true;
	        $data0 = array(
	            'name'=>'food_and_beverage',
	            'id'=>'food_and_beverage0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'food_and_beverage',
	            'id'=>'food_and_beverage1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="food_and_beverage0">Off</label><?php
	            echo form_radio($data1);?><label for="food_and_beverage1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_items').':', 'items',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="items_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->items == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'items',
	            'id'=>'items0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'items',
	            'id'=>'items1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="items0">Off</label><?php
	            echo form_radio($data1);?><label for="items1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_item_kits').':', 'item_kits',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="item_kits_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->item_kits == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'item_kits',
	            'id'=>'item_kits0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'item_kits',
	            'id'=>'item_kits1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="item_kits0">Off</label><?php
	            echo form_radio($data1);?><label for="item_kits1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_employees').':', 'employees',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="employees_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->employees == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'employees',
	            'id'=>'employees0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'employees',
	            'id'=>'employees1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="employees0">Off</label><?php
	            echo form_radio($data1);?><label for="employees1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_customers').':', 'customers',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="customers_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->customers == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'customers',
	            'id'=>'customers0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'customers',
	            'id'=>'customers1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="customers0">Off</label><?php
	            echo form_radio($data1);?><label for="customers1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_invoices').':', 'invoices',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="invoices_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->invoices == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'invoices',
	            'id'=>'invoices0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'invoices',
	            'id'=>'invoices1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="invoices0">Off</label><?php
	            echo form_radio($data1);?><label for="invoices1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_reports').':', 'reports',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="reports_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->reports == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'reports',
	            'id'=>'reports0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'reports',
	            'id'=>'reports1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="reports0">Off</label><?php
	            echo form_radio($data1);?><label for="reports1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_suppliers').':', 'suppliers',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="suppliers_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->suppliers == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'suppliers',
	            'id'=>'suppliers0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'suppliers',
	            'id'=>'suppliers1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="suppliers0">Off</label><?php
	            echo form_radio($data1);?><label for="suppliers1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_receivings').':', 'receivings',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="receivings_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->receivings == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'receivings',
	            'id'=>'receivings0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'receivings',
	            'id'=>'receivings1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="receivings0">Off</label><?php
	            echo form_radio($data1);?><label for="receivings1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_config').':', 'config',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="config_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->config == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'config',
	            'id'=>'config0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'config',
	            'id'=>'config1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="config0">Off</label><?php
	            echo form_radio($data1);?><label for="config1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_giftcards').':', 'giftcards',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="giftcards_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->giftcards == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'giftcards',
	            'id'=>'giftcards0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'giftcards',
	            'id'=>'giftcards1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="giftcards0">Off</label><?php
	            echo form_radio($data1);?><label for="giftcards1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_marketing_campaigns').':', 'marketing_campaigns',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="marketing_campaigns_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->marketing_campaigns == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'marketing_campaigns',
	            'id'=>'marketing_campaigns0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'marketing_campaigns',
	            'id'=>'marketing_campaigns1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="marketing_campaigns0">Off</label><?php
	            echo form_radio($data1);?><label for="marketing_campaigns1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_promotions').':', 'promotions',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="promotions_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->promotions == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'promotions',
	            'id'=>'promotions0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'promotions',
	            'id'=>'promotions1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="promotions0">Off</label><?php
	            echo form_radio($data1);?><label for="promotions1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('module_quickbooks').':', 'quickbooks',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div  id="quickbooks_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->quickbooks == 0)
	            $zero = true;
	        else
	            $one = true;
	        $data0 = array(
	            'name'=>'quickbooks',
	            'id'=>'quickbooks0',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'quickbooks',
	            'id'=>'quickbooks1',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="quickbooks0">Off</label><?php
	            echo form_radio($data1);?><label for="quickbooks1">On</label><?php
	        ?>
	            </div>
		</div>
	</div>
</div>
<script type='text/javascript'>
	$('#course_permissions').expandable({title:'Course Permissions:', open:function(){$.colorbox.resize()}, close:function(){$.colorbox.resize()}});
</script>
<div id='course_pricing' style='display:none'>
	<div class="field_row clearfix">
	<?php echo form_label('Seasonal Pricing', 'pricing',array('class'=>'wide')); ?>
		<div class='form_field'>
	            <div id="pricing_radio">
		<?php
	        $zero = false;
	        $one = false;
	        if ($item_info->seasonal_pricing == 0)
	            $zero = true;
	        else
	            $one = true;
	        
	        $data0 = array(
	            'name'=>'seasonal_pricing',
	            'id'=>'seasonal_pricing_off',
	            'value'=>0,
	            'checked'=>$zero
	        );
	        $data1 = array(
	            'name'=>'seasonal_pricing',
	            'id'=>'seasonal_pricing_on',
	            'value'=>1,
	            'checked'=>$one
	        );
	            echo form_radio($data0);?><label for="seasonal_pricing_off">Off</label><?php
	            echo form_radio($data1);?><label for="seasonal_pricing_on">On</label><?php
	        ?>
	            </div>
		</div>
	</div>	
</div>
<script type='text/javascript'>
	$('#course_pricing').expandable({title:'Course Pricing:', open:function(){$.colorbox.resize()}, close:function(){$.colorbox.resize()}});
</script>
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	$("#simulator_radio").buttonset();
    $("#teesheets_radio").buttonset();
    $("#pricing_radio").buttonset();
    $("#reservations_radio").buttonset();
    $("#schedules_radio").buttonset();
    $("#sales_radio").buttonset();
    $("#food_and_beverage_radio").buttonset();
    $("#items_radio").buttonset();
    $("#item_kits_radio").buttonset();
    $("#employees_radio").buttonset();
    $("#customers_radio").buttonset();
    $("#invoices_radio").buttonset();
    $("#reports_radio").buttonset();
    $("#suppliers_radio").buttonset();
    $("#receivings_radio").buttonset();
    $("#config_radio").buttonset();
    $("#giftcards_radio").buttonset();
	$("#marketing_campaigns_radio").buttonset();
	$("#promotions_radio").buttonset();
	$("#quickbooks_radio").buttonset();
	$( "#category" ).autocomplete({
		source: "<?php echo site_url('items/suggest_category');?>",
		delay: 10,
		autoFocus: false,
		minLength: 0
	});

	$('#item_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				$.colorbox.close();
				post_item_form_submit(response);
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			name:"required",
			category:"required",
			cost_price:
			{
				required:true,
				number:true
			},

			unit_price:
			{
				required:true,
				number:true
			},
			tax_percent:
			{
				required:true,
				number:true
			},
			quantity:
			{
				required:true,
				number:true
			},
			reorder_level:
			{
				required:true,
				number:true
			}
   		},
		messages:
		{
			name:"<?php echo lang('items_name_required'); ?>",
			category:"<?php echo lang('items_category_required'); ?>",
			cost_price:
			{
				required:"<?php echo lang('items_cost_price_required'); ?>",
				number:"<?php echo lang('items_cost_price_number'); ?>"
			},
			unit_price:
			{
				required:"<?php echo lang('items_unit_price_required'); ?>",
				number:"<?php echo lang('items_unit_price_number'); ?>"
			},
			tax_percent:
			{
				required:"<?php echo lang('items_tax_percent_required'); ?>",
				number:"<?php echo lang('items_tax_percent_number'); ?>"
			},
			quantity:
			{
				required:"<?php echo lang('items_quantity_required'); ?>",
				number:"<?php echo lang('items_quantity_number'); ?>"
			},
			reorder_level:
			{
				required:"<?php echo lang('items_reorder_level_required'); ?>",
				number:"<?php echo lang('items_reorder_level_number'); ?>"
			}

		}
	});
});
</script>
