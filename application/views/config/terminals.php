<div class="field_row clearfix">	
	<?php echo form_label(lang('config_use_terminals').' :', 'use',array('class'=>'wide')); ?>
    <div class='form_field'>
    	<?php echo form_checkbox(array('name'=>'use_terminals', 'value'=>'1', 'checked'=>$use_terminals, 'id'=>'use_terminals')); ?>
    	<div id='add_terminal'><?=lang('config_add_terminal')?></div>
    </div>
</div>
<div class="field_row clearfix">	
	<?php echo form_label('', '',array('class'=>'wide')); ?>
	<?php $this->load->view('config/terminal_table');?>
</div>
<style>
	.edit_terminal {
		margin-top: 6px;
		float: left;
		cursor: pointer;
		background: transparent url("../images/pieces/edit.png") no-repeat scroll 0px -1px;
		text-indent: 2000px;
		width: 14px;
		height: 14px;
		display: block;
		margin-right: 5px;
	}
	fieldset div.field_row label {
		width: 300px;
	}
</style>
<script>
	function add_delete_click() 
	{
		$('.delete_terminal').click(function(){
			var id = $(this).attr('id').replace('delete_terminal_','');
			//$('#terminal_'+id+' input[name=delete_terminal_id[]]').val(1);
			if (confirm('Are you sure you want to delete this terminal? This action can not be undone.'))
			{
				$.ajax({
	               type: "POST",
	               url: "index.php/config/delete_terminal/"+id,
	               data: "",
	               success: function(response){
	  					$('#terminal_'+id).hide();
	               },
	               dataType:'json'
	            });
	        }
		});
	}
	function add_edit_click() 
	{
		$('.edit_terminal').click(function(){
			var id = $(this).attr('id').replace('edit_terminal_','');
			$.colorbox({'href':'index.php/config/view_terminal_options/'+id+'/width~550'});
		});
	}
	$(document).ready(function(){
		$('#add_terminal').click(function(){
			$.colorbox({'href':'index.php/config/view_terminal_options/-1/width~550'});
			return;
			var id=Math.floor(Math.random()*101);
			var mercury_opts = '';
			var ets_opts = '';
			var webprnt_opts = '';
			var track_cash_opts = '';
	   		<?php if ($this->config->item('track_cash')) { ?>
					track_cash_opts = "<td><input class='track_cash_box' type='checkbox' name='track_cash_box[]' value='"+id+"' id='track_cash_box_"+id+"'>"+
						"<input type='hidden' name='track_cash[]' id='track_cash_"+id+"' value='0' /></td>";
			<?php } ?>
			<?php if ($this->config->item('webprnt')) { ?>
	    		webprnt_opts = '<td><?php echo form_input(array('name' => 'webprnt_ip[]', 'value' => $terminal->webprnt_ip, 'size'=>15));?></td>'+
	    			'<td><?php echo form_input(array('name' => 'webprnt_kitchen_ip[]', 'value' => $terminal->webprnt_kitchen_ip));?></td>';
	    	<? } ?>
	    	<?php if ($this->config->item('mercury_id')) { ?>
	    		mercury_opts = '<td><?php echo form_input(array('name' => 'mercury_id[]', 'value' => $terminal->mercury_id));?></td>';
	    			'<td><?php echo form_input(array('name' => 'mercury_password[]', 'value' => $terminal->mercury_password));?></td>';
	    	<?php } ?>
	    	<?php if ($this->config->item['ets_key']) { ?>
	    		ets_opts = '<td><?php echo form_input(array('name' => 'ets_key[]', 'value' => $terminal->ets_key));?></td>';
	    	<?php } ?>
			$('#terminal_list').append(
				"<tr id='terminal_"+id+"'>"+
				"<td><span id='delete_terminal_"+id+"' class='delete_terminal'></span></td>"+
				"<td><input name='terminal_label[]' value='' size='25'/><input type='hidden' name='terminal_id[]' value='0'/>"+
				"<input type='hidden' name='delete_terminal_id[]' value='0'/></td>"+
				"<td><select id='terminal_tab[]' name='terminal_tab[]'>"+
					"<option value='1' selected='selected'>1</option><option value='2'>2</option><option value='3'>3</option>"+
				"</select></td>"+
				track_cash_opts+
				webprnt_opts+
				mercury_opts+
				ets_opts+
				"</tr>");
			add_delete_click();
			add_edit_click();
		});
		add_delete_click();
		add_edit_click();
	});
</script>
