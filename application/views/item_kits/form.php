<ul id="error_message_box"></ul>
<?php
echo form_open('item_kits/save/'.$item_kit_info->item_kit_id,array('id'=>'item_kit_form'));
?>
<fieldset id="item_kit_info">
<legend><?php echo lang("item_kits_info"); ?></legend>
<!--table id="item_kit_items">
	<tr>
		<th><?php echo lang('common_delete');?></th>
		<th><?php echo lang('item_kits_item');?></th>
		<th><?php echo lang('item_kits_quantity');?></th>
	</tr>

	<?php foreach ($this->Item_kit_items->get_info($item_kit_info->item_kit_id) as $item_kit_item) {?>
		<tr>
			<?php
			$item_info = $this->Item->get_info($item_kit_item->item_id);
			?>
			<td><a href="#" onclick='return deleteItemKitRow(this);'>X</a></td>
			<td><?php echo $item_info->name; ?></td>
			<td><input class='quantity' onchange="calculateSuggestedPrices();" id='item_kit_item_<?php echo $item_kit_item->item_id ?>' type='text' size='3' name=item_kit_item[<?php echo $item_kit_item->item_id ?>] value='<?php echo $item_kit_item->quantity ?>'/></td>
		</tr>
	<?php } ?>
</table-->
<div class='left_side'>
<div class="field_row clearfix">
<?php echo form_label(lang('items_item_number').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'item_kit_number',
		'id'=>'item_kit_number',
		'value'=>$item_kit_info->item_kit_number)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('item_kits_name').':<span class="required">*</span>', 'name',array('class'=>'wide ')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'name',
		'id'=>'name',
		'value'=>$item_kit_info->name)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_department').':<span class="required">*</span>', 'department',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'department',
		'id'=>'department',
		'value'=>$item_kit_info->department)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('items_category').':', 'category',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'category',
		'id'=>'category',
		'value'=>$item_kit_info->category)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('items_subcategory').':', 'subcategory',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'subcategory',
		'id'=>'subcategory',
		'value'=>$item_kit_info->subcategory)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('item_kits_add_item').':', 'item',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'item',
			'id'=>'item'
		));?>
	</div>
</div>

</div>
<div class='right_side'>
<div class="field_row clearfix">
<?php echo form_label(lang('items_cost_price').':<span class="required">*</span>', 'cost_price',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'cost_price',
		'id'=>'cost_price',
		'value'=>$item_kit_info->cost_price)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_unit_price').':<span class="required">*</span>', 'unit_price',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'unit_price',
		'id'=>'unit_price',
		'value'=>$item_kit_info->unit_price)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_tax_1').':', 'tax_percent_1',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'tax_names[]',
		'id'=>'tax_name_1',
		'size'=>'8',
		'value'=> isset($item_kit_tax_info[0]['name']) ? $item_kit_tax_info[0]['name'] : $this->config->item('default_tax_1_name'))
	);?>
	<?php echo form_input(array(
		'name'=>'tax_percents[]',
		'id'=>'tax_percent_name_1',
		'size'=>'3',
		'value'=> isset($item_kit_tax_info[0]['percent']) ? $item_kit_tax_info[0]['percent'] : $default_tax_1_rate)
	);?>
	%
	<?php echo form_hidden('tax_cumulatives[]', '0'); ?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_tax_2').':', 'tax_percent_2',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'tax_names[]',
		'id'=>'tax_name_2',
		'size'=>'8',
		'value'=> isset($item_kit_tax_info[1]['name']) ? $item_kit_tax_info[1]['name'] : $this->config->item('default_tax_2_name'))
	);?>
	<?php echo form_input(array(
		'name'=>'tax_percents[]',
		'id'=>'tax_percent_name_2',
		'size'=>'3',
		'value'=> isset($item_kit_tax_info[1]['percent']) ? $item_kit_tax_info[1]['percent'] : $default_tax_2_rate)
	);?>
	%
	<?php echo form_checkbox('tax_cumulatives[]', '1', isset($item_kit_tax_info[1]['cumulative']) && $item_kit_tax_info[1]['cumulative'] ? (boolean)$item_kit_tax_info[1]['cumulative'] : (boolean)$default_tax_2_cumulative); ?>
	<?php echo lang('common_cumulative'); ?>
	</div>
</div>
<div id='additional_taxes' style='display:none; overflow: hidden; display: block; width: 450px;'>
	<div class="field_row clearfix">
	<?php echo form_label(lang('items_tax_3').':', 'tax_percent_3',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'tax_names[]',
			'id'=>'tax_name_3',
			'size'=>'8',
			'value'=> isset($item_kit_tax_info[2]['name']) ? $item_kit_tax_info[2]['name'] : $this->config->item('default_tax_3_name'))
		);?>
		<?php echo form_input(array(
			'name'=>'tax_percents[]',
			'id'=>'tax_percent_name_3',
			'size'=>'3',
			'value'=> isset($item_kit_tax_info[2]['percent']) ? $item_kit_tax_info[2]['percent'] : $default_tax_3_rate)
		);?>
		%
		<?php echo form_hidden('tax_cumulatives[]', '0'); ?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('items_tax_4').':', 'tax_percent_4',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'tax_names[]',
			'id'=>'tax_name_4',
			'size'=>'8',
			'value'=> isset($item_kit_tax_info[3]['name']) ? $item_kit_tax_info[3]['name'] : $this->config->item('default_tax_4_name'))
		);?>
		<?php echo form_input(array(
			'name'=>'tax_percents[]',
			'id'=>'tax_percent_name_4',
			'size'=>'3',
			'value'=> isset($item_kit_tax_info[3]['percent']) ? $item_kit_tax_info[3]['percent'] : $default_tax_4_rate)
		);?>
		%
		<?php echo form_hidden('tax_cumulatives[]', '0'); ?>
		</div>
	</div>

	<div class="field_row clearfix">
	<?php echo form_label(lang('items_tax_5').':', 'tax_percent_5',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'tax_names[]',
			'id'=>'tax_name_5',
			'size'=>'8',
			'value'=> isset($item_kit_tax_info[4]['name']) ? $item_kit_tax_info[4]['name'] : $this->config->item('default_tax_5_name'))
		);?>
		<?php echo form_input(array(
			'name'=>'tax_percents[]',
			'id'=>'tax_percent_name_5',
			'size'=>'3',
			'value'=> isset($item_kit_tax_info[4]['percent']) ? $item_kit_tax_info[4]['percent'] : $default_tax_5_rate)
		);?>
		%
		<?php echo form_hidden('tax_cumulatives[]', '0'); ?>
		</div>
	</div>

	<div class="field_row clearfix">
	<?php echo form_label(lang('items_tax_6').':', 'tax_percent_6',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'tax_names[]',
			'id'=>'tax_name_6',
			'size'=>'8',
			'value'=> isset($item_kit_tax_info[5]['name']) ? $item_kit_tax_info[5]['name'] : $this->config->item('default_tax_6_name'))
		);?>
		<?php echo form_input(array(
			'name'=>'tax_percents[]',
			'id'=>'tax_percent_name_6',
			'size'=>'3',
			'value'=> isset($item_kit_tax_info[5]['percent']) ? $item_kit_tax_info[5]['percent'] : $default_tax_6_rate)
		);?>
		%
		<?php echo form_hidden('tax_cumulatives[]', '0'); ?>
		</div>
	</div>
</div>
<script>
	$('#additional_taxes').expandable({
		title : 'Additional Taxes'
	});
</script>

<div class="field_row clearfix">
<?php echo form_label(lang('items_is_punch_card').':', 'is_punch_card',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_checkbox('is_punch_card', '1', $item_kit_info->is_punch_card); ?>
	</div>
</div>
</div>
<div class='clear'></div>
<div id='item_kit_items_list'>
<?php foreach ($this->Item_kit_items->get_info($item_kit_info->item_kit_id) as $item_kit_item) {?>
	<div class='item_kit_item_box'>
		<?php
		$item_info = $this->Item->get_info($item_kit_item->item_id);
		?>
		<a class='item_delete' href="#" onclick='return deleteItemKitRow(this);'>X</a>
		<div class='item_name'><?php echo $item_info->name; ?></div>
		<input class='quantity' onchange="calculateSuggestedPrices();" id='item_kit_item_<?php echo $item_kit_item->item_id ?>' type='text' size='3' name=item_kit_item[<?php echo $item_kit_item->item_id ?>] value='<?php echo $item_kit_item->quantity ?>'/>
		<div class='clear'></div>
	</div>
<?php } ?>
<div id='clear' class='clear'></div>
</div>

<div class="field_row clearfix">
<?php if((int) $this->config->item('quickbooks') > 0){ ?>
<div id='quickbooks_accounts' style='display:none; overflow: hidden; display: block; width: 450px;'>
	<div class="field_row clearfix">
	<?php echo form_label('Income Acct.', 'quickbooks_income', array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_dropdown(
			'quickbooks_income',
			$quickbooks_accounts,
			$item_kit_info->quickbooks_income
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label('COGS Acct.', 'quickbooks_cogs', array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_dropdown(
			'quickbooks_cogs',
			$quickbooks_accounts,
			$item_kit_info->quickbooks_cogs
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label('Asset Acct.', 'quickbooks_assets', array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_dropdown(
			'quickbooks_assets',
			$quickbooks_accounts,
			$item_kit_info->quickbooks_assets
		);?>
		</div>
	</div>
</div>
<script>
$('#quickbooks_accounts').expandable({
	title : 'QuickBooks Account Maps'
});
</script>
<?php } ?>

<?php echo form_label(lang('item_kits_description').':', 'description',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_textarea(array(
		'name'=>'description',
		'id'=>'description',
		'value'=>$item_kit_info->description,
		'rows'=>'3',
		'cols'=>'17')
	);?>
	</div>
</div>
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>

$( "#item" ).autocomplete({
	source: '<?php echo site_url("items/item_search"); ?>',
	delay: 200,
	autoFocus: false,
	minLength: 1,
	select: function( event, ui )
	{
		$( "#item" ).val("");
		if ($("#item_kit_item_"+ui.item.value).length ==1)
		{
			$("#item_kit_item_"+ui.item.value).val(parseFloat($("#item_kit_item_"+ui.item.value).val()) + 1);
		}
		else
		{
			//$("#item_kit_items").append("<tr><td><a href='#' onclick='return deleteItemKitRow(this);'>X</a></td><td>"+ui.item.label+"</td><td><input class='quantity' onchange='calculateSuggestedPrices();' id='item_kit_item_"+ui.item.value+"' type='text' size='3' name=item_kit_item["+ui.item.value+"] value='1'/></td></tr>");
			$("#item_kit_items_list div#clear").before("<div class='item_kit_item_box'><a class='item_delete' href='#' onclick='return deleteItemKitRow(this);'>X</a><div class='item_name'>"+ui.item.label+"</div><input class='quantity' onchange='calculateSuggestedPrices();' id='item_kit_item_"+ui.item.value+"' type='text' size='3' name=item_kit_item["+ui.item.value+"] value='1'/></div>");
		}

		calculateSuggestedPrices();
		$.colorbox.resize();

		return false;
	}
});

//validation and submit handling
$(document).ready(function()
{
	var dept = $( "#department" );
	dept.autocomplete({
		source: "<?php echo site_url('items/suggest_department');?>",
		delay: 10,
		autoFocus: false,
		minLength: 0
	});
	dept.click(function(){dept.autocomplete('search','')});

	var cat = $( "#category" );
	cat.autocomplete({
		source: "<?php echo site_url('items/suggest_category');?>",
		delay: 10,
		autoFocus: false,
		minLength: 0
	});
	cat.click(function(){cat.autocomplete('search','')});

	var subcat = $( "#subcategory" );
	subcat.autocomplete({
		source: "<?php echo site_url('items/suggest_subcategory');?>",
		delay: 10,
		autoFocus: false,
		minLength: 0
	});
	subcat.click(function(){subcat.autocomplete('search','')});

	var submitting = false;
	$('#item_kit_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				$.colorbox.close();
				post_item_kit_form_submit(response);
                submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			name:"required",
			department:"required",
			cost_price:
			{
				required:true,
				isFloat:true
			},

			unit_price:
			{
				required:true,
				isFloat:true
			}
		},
		messages:
		{
			name:"<?php echo lang('items_name_required'); ?>",
			department:"<?php echo lang('items_department_required'); ?>",
			cost_price:
			{
				required:"<?php echo lang('items_cost_price_required'); ?>",
				number:"<?php echo lang('items_cost_price_number'); ?>"
			},
			unit_price:
			{
				required:"<?php echo lang('items_unit_price_required'); ?>",
				number:"<?php echo lang('items_unit_price_number'); ?>"
			}
		}
	});
});

function deleteItemKitRow(link)
{
	$(link).parent().parent().remove();
	calculateSuggestedPrices();
	return false;
}

function calculateSuggestedPrices()
{
	var items = [];
	$("#item_kit_items").find('input').each(function(index, element)
	{
		var quantity = parseFloat($(element).val());
		var item_id = $(element).attr('id').substring($(element).attr('id').lastIndexOf('_') + 1);

		items.push({
			item_id: item_id,
			quantity: quantity
		});
	});
	calculateSuggestedPrices.totalCostOfItems = 0;
	calculateSuggestedPrices.totalPriceOfItems = 0;
	getPrices(items, 0);
}

function getPrices(items, index)
{
	if (index > items.length -1)
	{
		$("#unit_price").val(calculateSuggestedPrices.totalPriceOfItems);
		$("#cost_price").val(calculateSuggestedPrices.totalCostOfItems);
	}
	else
	{
		$.get('<?php echo site_url("items/get_info");?>'+'/'+items[index]['item_id'], {}, function(item_info)
		{
			calculateSuggestedPrices.totalPriceOfItems+=items[index]['quantity'] * parseFloat(item_info.unit_price);
			calculateSuggestedPrices.totalCostOfItems+=items[index]['quantity'] * parseFloat(item_info.cost_price);
			getPrices(items, index+1);
		}, 'json');
	}
}
</script>
