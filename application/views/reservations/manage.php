<?php $this->load->view("partial/header");
//echo 'TSID '.$this->session->userdata('teesheet_id');
//echo 'increment '.$this->session->userdata('increment');
?>

<script type='text/javascript'>
//Begin offline mode code /application cache
/*
	var cache = window.applicationCache;

        cache.addEventListener("cached", function () {
            console.log("All resources for this web app have now been downloaded. You can run this application while not connected to the internet");
        }, false);
        cache.addEventListener("checking", function () {
            console.log("Checking manifest");
        }, false);
        cache.addEventListener("downloading", function () {
            console.log("Starting download of cached files");
        }, false);
        cache.addEventListener("error", function (e) {
            console.log("There was an error in the manifest, downloading cached files or you're offline: " + e);
            //console.dir(e);
        }, false);
        cache.addEventListener("noupdate", function () {
            console.log("There was no update needed");
        }, false);
        cache.addEventListener("progress", function () {
            console.log("Downloading cached files");
        }, false);
        cache.addEventListener("updateready", function () {
            cache.swapCache();
            console.log("Updated cache is ready");
            // Even after swapping the cache the currently loaded page won't use it
            // until it is reloaded, so force a reload so it is current.
            window.location.reload(true);
            console.log("Window reloaded");
        }, false);*/
//End offline mode code
	/*var old = '';
	var source = new EventSource('index.php/teesheets/teesheet_updates/<?php echo $this->session->userdata('course_id');?>');

	source.onmessage = function(e)
	{
		var nd = new Date();
		console.log('running '+nd.getTime());
		if(old!=e.data){
			console.log(e.data);
			var  response = eval("("+e.data+")");
	  		var events = $.merge(response.caldata, response.bcaldata);
               Calendar_actions.update_teesheet(events, true, 'auto_update');

			old = e.data;
		}
	};*/

	var calInfo = {};
    alldata = eval('(<?php echo $JSONData ?>)');
    calInfo.caldata = {};//alldata.caldata;
    calInfo.bcaldata = {};//alldata.bcaldata;
    calInfo.tracks = eval('(<?= $tracks ?>)');
    calInfo.openhour = convertFormat('<?php echo $openhour?>');
    calInfo.closehour = convertFormat('<?php echo $closehour?>');
    calInfo.increment1 = '<?php echo $increment?>';
    calInfo.increment2 = '<?php echo $increment?>';
    calInfo.holes = '<?php echo $holes?>';
    calInfo.frontnine = '<?php echo $fntime?>';
    calInfo.purchase = '<?php echo $purchase?>';

    function convertFormat(hourString) {
        var h = parseInt(hourString.slice(0,2), 10);
        var m = parseInt(hourString.slice(2), 10);
        var mz = '';
        if (m < 10)
            mz = 0;
        //console.log(hourString+' '+hourString.slice(0,2)+' '+h+' : '+mz+m);
        return h+':'+mz+m
    }
    function add_new_teetimes() {
    	//console.log('starting add new teetimes');
    	setTimeout('Calendar_actions.add_new_teetimes()', 1);
		//console.log('just set cal ac');
    	setTimeout('add_new_teetimes()', 15000);
    	//console.log('just reset self');
    }
	var teesheet_controls = {
		change_date:function(date){
			var dow = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
			var month = ['January','February','March','April','May','June','July','August','September','October','November','December'];
			$('#teesheet_day').html(date.getDate());
			$('#teesheet_dow').html(dow[date.getDay()]);
			$('#teesheet_month').html(month[date.getMonth()]+', '+date.getFullYear());
		},
		change_teesheet:function(){

		}
	};
    $(document).ready(function(){
    	$('#settings_button').click(function(){
    		$.colorbox({'href':'index.php/schedules/view/<?php echo $this->session->userdata('schedule_id'); ?>','width':1100,'title':'Teesheet Settings'})
    	});
    	var wb = $('#weatherBox ul');
    	wb.click(function(){
    		if (wb.hasClass('expanded'))
    		{
    			wb.removeClass('expanded');
        		$('#wbtabs-1').hide();
			}
    		else
    		{
	    		wb.addClass('expanded');
        		$('#wbtabs-1').show();
    		}
    	});
    	var ft = $('#facebook_title');
    	ft.click(function(){
    		if (ft.hasClass('expanded'))
    		{
    			ft.removeClass('expanded');
        		$('#facebook_content').hide();
			}
    		else
    		{
	    		ft.addClass('expanded');
        		$('#facebook_content').show();
    		}
    	})
    	var tst = $('#teetime_search_title');
    	tst.click(function(e){
    		if (tst.hasClass('expanded'))
    		{
    			tst.removeClass('expanded');
        		$('#teetime_search_content').hide();
			}
    		else
    		{
	    		tst.addClass('expanded');
        		$('#teetime_search_content').show();
    		}
    	})
    	$('#month_view').click(function(){
			$('.track_calendar').each(function() {
				$(this).fullCalendar( 'changeView', 'month' );
			});
			$('#teesheet_view_buttons .selected').removeClass('selected');
			$(this).addClass('selected');
		});
    	$('#week_view').click(function(){
			$('.track_calendar').each(function() {
				$(this).fullCalendar( 'changeView', 'agendaWeek' );
			});
			$('#teesheet_view_buttons .selected').removeClass('selected');
			$(this).addClass('selected');
		});
    	$('#day_view').click(function(){
			$('.track_calendar').each(function() {
				$(this).fullCalendar( 'changeView', 'agendaDay' );
			});
			$('#teesheet_view_buttons .selected').removeClass('selected');
			$(this).addClass('selected');
		});
		$('#previous_day').click(function(){
			$('.track_calendar').each(function() {
				$(this).fullCalendar('prev');
			});
		})
		$('#today').click(function(){
			$('.track_calendar').each(function() {
				$(this).fullCalendar('today');
			});
		})
		$('#next_day').click(function(){
			$('.track_calendar').each(function() {
				$(this).fullCalendar('next');
			});
		})
    	<?php if ($this->config->item('teesheet_updates_automatically')) { ?>
    		//Only if it's enabled in settings
    		if (!!window.EventSource && false) {
    			var old = '';
				var source = new EventSource('index.php/teesheets/teesheet_updates/<?php echo $this->session->userdata('course_id');?>');

				source.onmessage = function(e)
				{
					var nd = new Date();
					//console.log('running '+nd.getTime());
					if(old!=e.data){
						//console.log(e.data);
						var  response = eval("("+e.data+")");
				  		var events = $.merge(response.caldata, response.bcaldata);
			               Calendar_actions.update_teesheet(events, true, 'auto_update');

						old = e.data;
					}
				};
    		}
    		else {
    			add_new_teetimes();
    		}
    	<?php } ?>
    	$.ajax({
           type: "POST",
           url: "index.php/reservations/make_5_day_weather",
           data: '',
           success: function(response){
          	 	$('#wbtabs-1').html(response.weather);
   			 	$('#teesheet_weather_box').html(response.todays);
           },
           dataType:'json'
         });
    });
    function post_teetime_form_submit(response)
	{
		set_feedback(response.message,'warning_message',false, 5000);
	}

</script>
<style>
	#teesheets .fc-event-bg, #reservations .fc-event-bg {
		background: #f8f8f8; /* for non-css3 browsers */
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#f8f8f8', endColorstr='#bebebe'); /* for IE */
		background: -webkit-linear-gradient(top, #f8f8f8, #bebebe);
		background: -moz-linear-gradient(top,  #f8f8f8,  #bebebe); /* for firefox 3.6+ */
	}
	#teesheets .teetime, #reservations .teetime, #teesheets .reservation, #reservations .reservation {
		background: #f8f8f8; /* for non-css3 browsers */
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#f8f8f8', endColorstr='#bebebe'); /* for IE */
		background: -webkit-linear-gradient(top, #f8f8f8, #bebebe);
		background: -moz-linear-gradient(top,  #f8f8f8,  #bebebe); /* for firefox 3.6+ */
	}
	#teesheets .tournament, #reservations .tournament {
		background: #fff9e8; /* for non-css3 browsers */
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff9e8', endColorstr='#fccc58'); /* for IE */
		background: -webkit-linear-gradient(top, #fff9e8, #fccc58);
		background: -moz-linear-gradient(top,  #fff9e8,  #fccc58); /* for firefox 3.6+ */
	}
	#teesheets .closed, #reservations .closed {
		background: #fdf2f2; /* for non-css3 browsers */
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fdf2f2', endColorstr='#fac1c1'); /* for IE */
		background: -webkit-linear-gradient(top, #fdf2f2, #fac1c1);
		background: -moz-linear-gradient(top,  #fdf2f2,  #fac1c1); /* for firefox 3.6+ */
	}
	#teesheets .event, #reservations .event {
		background: #FBEDFF; /* for non-css3 browsers */
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#FBEDFF', endColorstr='#F2BDFF'); /* for IE */
		background: -webkit-linear-gradient(top, #FBEDFF, #F2BDFF);
		background: -moz-linear-gradient(top,  #FBEDFF,  #F2BDFF); /* for firefox 3.6+ */
	}
	#teesheets .league, #reservations .league {
		background: #bddeff; /* for non-css3 browsers */
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ecfaf1', endColorstr='#b8ffd6'); /* for IE */
		background: -webkit-linear-gradient(top, #ecfaf1, #b8ffd6);
		background: -moz-linear-gradient(top,  #ecfaf1,  #b8ffd6); /* for firefox 3.6+ */
	}
	#teesheets .teetime div.fc-event-inner, #reservations .teetime div.fc-event-inner, #teesheets .reservation div.fc-event-inner, #reservations .reservation div.fc-event-inner {
		border: 1px solid #929292;
	}
	#teesheets .tournament div.fc-event-inner, #reservations .tournament div.fc-event-inner {
		border: 1px solid #B18106;
	}
	#teesheets .closed div.fc-event-inner, #reservations .closed div.fc-event-inner {
		border: 1px solid #B10606;
	}
	#teesheets .event div.fc-event-inner, #reservations .event div.fc-event-inner {
		border: 1px solid #993399;
	}
	#teesheets .league div.fc-event-inner, #reservations .league div.fc-event-inner {
		border: 1px solid #396;
/*		border: 1px solid #1AB106;*/
	}
	#teesheets .paid_1_1 .fc-event-bg, #teesheets .paid_2_2 .fc-event-bg, #teesheets .paid_3_3 .fc-event-bg, #teesheets .paid_4_4 .fc-event-bg, #teesheets .paid_5_5 .fc-event-bg,
	#reservations .paid_1_1 .fc-event-bg, #reservations .paid_2_2 .fc-event-bg, #reservations .paid_3_3 .fc-event-bg, #reservations .paid_4_4 .fc-event-bg, #reservations .paid_5_5 .fc-event-bg {
		background: #edf8ff; /* for non-css3 browsers */
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#edf8ff', endColorstr='#bddeff'); /* for IE */
		background: -webkit-linear-gradient(top, #edf8ff, #bddeff);
		background: -moz-linear-gradient(top,  #edf8ff,  #bddeff); /* for firefox 3.6+ */
		/*background:-webkit-gradient(linear, left top, right top, color-stop(0%,#ecfaf1), color-stop(100%,#b8ffd6))*/
	}
	#teesheets .paid_4_5 .fc-event-bg, #reservations .paid_4_5 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#edf8ff), color-stop(80%,#bddeff), color-stop(80%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_3_5 .fc-event-bg, #reservations .paid_3_5 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#edf8ff), color-stop(60%,#bddeff), color-stop(60%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_2_5 .fc-event-bg, #reservations .paid_2_5 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#edf8ff), color-stop(40%,#bddeff), color-stop(40%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_1_5 .fc-event-bg, #reservations .paid_1_5 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#edf8ff), color-stop(20%,#bddeff), color-stop(20%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_3_4 .fc-event-bg, #reservations .paid_3_4 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#edf8ff), color-stop(75%,#bddeff), color-stop(75%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_2_4 .fc-event-bg, #teesheets .paid_1_2 .fc-event-bg, #reservations .paid_2_4 .fc-event-bg, #reservations .paid_1_2 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#edf8ff), color-stop(50%,#bddeff), color-stop(50%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_1_4 .fc-event-bg, #reservations .paid_1_4 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#edf8ff), color-stop(25%,#bddeff), color-stop(25%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_2_3 .fc-event-bg, #reservations .paid_2_3 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#edf8ff), color-stop(67%,#bddeff), color-stop(67%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_1_3 .fc-event-bg, #reservations .paid_1_3 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#edf8ff), color-stop(34%,#bddeff), color-stop(34%,transparent), color-stop(100%,transparent))
	}
	<?php /*if ($holes == 18) {?>
		#teesheets #frontnine, #teesheets #backnine, #reservations #frontnine, #reservations #backnine {
			width:50%;
		}
	<?php } else { ?>
		#teesheets #frontnine, #reservations #frontnine {
			width:100%;
		}
	<?php } */?>
	<?php if (count($tracks) == 1) {?>
		#teesheets #frontnine, #teesheets #backnine, #reservations #frontnine, #reservations #backnine, #reservations .schedule_track {
			width:100%;
		}
	<?php } else if (count($tracks) == 2){ ?>
		#teesheets #frontnine, #reservations #frontnine, #reservations .schedule_track {
			width:50%;
		}
	<?php } else if (count($tracks) == 3){ ?>
		#teesheets #frontnine, #reservations #frontnine, #reservations .schedule_track {
			width:33.3%;
		}
	<?php } else if (count($tracks) > 3){ ?>
		#teesheets #frontnine, #reservations #frontnine, #reservations .schedule_track {
			width:250px;
		}
		#track_calendar_holder .content {
			width:<?php echo count($tracks) * 250;?>px;
		}
	<?php } ?>
</style>
<div class="wrapper">
    <div class="column">
    	<div id='teetime_search_box'>
    		<div id='teetime_search_title'>Search<span class='icon minimize_icon'></span></div>
    		<div id='teetime_search_content' style='display:none'>
    			<input type='text' placeholder='Customer Name' value='' id='teetime_search_name'/>
    			<input type='hidden' value='' id='teetime_search_id'/>
    			<div id='teetime_search_results'>
    				<div class='teetime_result'>
	    				No results
	    			</div>
    			</div>
    		</div>
    	</div>
        <div id="datepicker"></div>
        <?php if (!$this->config->item('simulator')) {?>
        <div id="weatherBox" class="weatherBox">
            <ul class='expanded'>
                <!--li class="stats"><a href="#tabs-1">Players</a></li-->
                <li class="weather"><a href="#wbtabs-1">Weather</a><span class='icon minimize_icon'></span></li>
            </ul>
            <!--div id='tabs-1' class="stats">

            </div-->
            <div id='wbtabs-1'>
                <?php //echo $fdweather; ?>
            </div>
        </div>
        <?php } ?>
        <!--div id="stats">
            <ul>

            </ul>

        </div-->
         <div class="facebook_messages">
         	<div id='facebook_title'>Facebook<span class='icon minimize_icon'></span></div>
        	<div id='facebook_content' style='display:none'>
				<div id="selected_page" class="truncated"></div>
	        	<ol id="selectable">

				</ol>
				<textarea id="message" rows="8" maxlength="150" placeholder="Enter message to post to wall"></textarea>
		    	<input class="post" type="button" value="Post to Page">
	        	<!--input class="post" type="button" value="Post" style='display:none'/>
			  	<input class="login" type="button" value="Login"/>
			  	<input class="logout" type="button" value="Logout" style='display:none'/-->
			</div>
    	</div>
        <div id='settings_button' class='ui-widget-content'>
        	<span id='settings_icon'></span> Teesheet Settings
        </div>
    </div>
    <?php if ($current_teesheet == '') { ?>
    	<div style='padding-top:30px; font-size:18px;'>
    	No default schedule available, please create one under schedules.
    	</div>
    <?php } else { ?>
    <div id="main" class="container">
    	<div id='teesheet_controls'>
    		<div id='course_selector'>
    		<?php if ($tsMenu != '') {?>
	            <form action="" method="post">
	                    <?php echo $tsMenu; ?>
	                    <input type="submit" value="changets" name="changets" id="changets" style="display:none"/>
	            </form>
	        <?php } ?>
	       </div>
	       <a id='print_button'>
	       		<div id='teesheet_download_button'>&nbsp;</div>
	       </a>
    	   <div id='teesheet_view_buttons'>
	       		<ul>
	       			<!--li id='month_view'>Month</li-->
	       			<li id='week_view'>Week</li>
	       			<li id='day_view' class='last selected'>Day</li>
	       		</ul>
	       </div>
	    </div>
    	<div id='teesheet_info'>
    		<div id='teesheet_date_box'>
    			<div id='teesheet_day'>

    			</div>
    			<div class='date_info'>
    				<div id='teesheet_dow'>

    				</div>
    				<div id='teesheet_month'>

    				</div>
    			</div>
    			<div class='clear'></div>
    		</div>
    		<div class='line_spacer'>

    		</div>
    		<div id='teesheet_weather_box'>
    			<div id='weather_icon'>

    			</div>
    			<div id='current_temp'>

    			</div>
    			<div id='low_high'>
    				<div id='high'></div>
    				<div id='low'></div>
    			</div>
    		</div>
    		<div id='teesheet_day_navigator'>
    			<ul>
	       			<li id='previous_day'>Previous</li>
	       			<li id='today'>Today</li>
	       			<li id='next_day' class='last'>Next</li>
	       		</ul>
		    </div>
    	</div>
    	<div id='track_calendar_holder' class="inner clearfix">
        <?
            if ($user_id) {?>
            <? } ?>
            <div class="content">
                <!--div id="frontnine">
                    <div id='calendar'></div>
                </div>
                <div id="backnine">
                    <div id='calendarback' style='width:100%; display:none'></div>
                </div-->
                <?php foreach ($tracks as $track) { ?>
                <div id="track_<?=$track['track_id']?>" class='schedule_track'>
                    <div id='calendar_<?=$track['track_id']?>' cal_title='<?=$track['title']?>' class='track_calendar'></div>
                </div>
                <?php } ?>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
<!-- Right Click Menu -->
<div class='contextMenu' id='myMenu' style='display:none'>
	<ul>
	    <li id="switch"><!--img src="../../images/pieces/switch.png" /-->Switch Sides</li>
	    <li id="repeat">Repeat Event</li>
	    <li id="move">Move</li>
	    <li id="teed_off">Tee'd Off</li>
	    <li id="mark_turn">Mark the Turn</li>
	    <li id="mark_finished">Mark as Done</li>
	    <!--li id="raincheck">Issue Rain Check</li-->
    	<li id="check_in_1">Check In 1</li>
    	<li id="check_in_2">Check In 2</li>
    	<li id="check_in_3">Check In 3</li>
    	<li id="check_in_4">Check In 4</li>
    	<li id="check_in_5">Check In 5</li>
    	<li id="resend_confirmation">Resend Confirmation</li>
	</ul>
</div>
<div id="feedback_bar"></div>
<div id="teetime_info_bar"></div>
<div id="fb-root"></div>

<?php $this->load->view("partial/footer"); ?>
<script>
var teesheet = {
	teetime:{
		search:function(){
			$.ajax({
	           type: "POST",
	           url: "index.php/teesheets/teetime_search",
	           data: 'customer_id='+$('#teetime_search_id').val(),
	           success: function(response){
	           		$('#teetime_search_results').html(response.search_results);
	           },
	           dataType:'json'
	         });
		}
	}
}
var teetime_html = "<?php //echo (preg_replace('/\s+/',' ',str_replace(array(chr(10),chr(13),'\r\n','\t','\r','\n'), '', ($this->config->item('simulator')) ? $this->load->view("teetimes/form_simulator",$data,true) : $this->load->view("teetimes/form",$data,true))));?>";
	$(document).ready(function(){
		var teetime_search_name = $('#teetime_search_name');
		teetime_search_name.focus(function() { $(this).val('') });
		teetime_search_name.autocomplete({
			source: '<?php echo site_url("teesheets/customer_search/ln_and_pn"); ?>',
			delay: 200,
			autoFocus: false,
			minLength: 1,
			select: function(event, ui)
			{
				event.preventDefault();
				teetime_search_name.val(ui.item.label);
				$("#teetime_search_id").val(ui.item.value);
				teetime_search_name.blur();
				teesheet.teetime.search();
			}
		});
		$('#teesheetMenu').msDropDown(/*{blur:function(){changeTeeSheet()},click:alert('stuff')}*/)
	<?php if ($this->session->userdata('is_simulator')) { ?>
    $('#calendar th.fc-col0').html('Sim 1');
    $('#calendarback th.fc-col0').html('Sim 2');
	<?php } else { ?>
    $('#calendar th.fc-col0').html('Front');
    $('#calendarback th.fc-col0').html('Back');
	<?php } ?>

	/**********************************************************************************
	 **********************************FACEBOOK CODE***********************************
	 **********************************************************************************/

	window.my_config =
	{
		access_token : '',
		page_id : '',
		name : '',
		user_accounts : '',
		// app_id : '317906091649413', // app_id for LOCALHOST testing only
		app_id : '583489044999943', // App ID for ForeUP Dev
		// app_id : '411789792224848', // App ID for ForeUP Master
		// app_secret : '5a47cce8b870df193418cbdc1490913b', //app_secret for LOCALHOST testing only
		app_secret : '5f8b249b9473fd74b13814be106d45ad', //app_secret for ForeUP Dev
		// app_secret : 'f69cd64778c32cb7dda1c1ec6960e1b0', //app_secret for ForeUP Master
		extended_access_token : '',
		can_post_with_extended_access_token : false
	};

	window.my_config.page_id = '<?php echo $this->session->userdata('facebook_page_id'); ?>';
	window.my_config.name = '<?php echo $this->session->userdata('facebook_page_name'); ?>';
	window.my_config.extended_access_token = '<?php echo $this->session->userdata('facebook_extended_access_token'); ?>';
	if (window.my_config.extended_access_token !== '') window.my_config.can_post_with_extended_access_token = true;

	//hide the select list so the textarea can be clicked on from anywhere inside the box
	$('#selectable').hide();

	//listner to hide/show the select list of facebook Pages
	$('#selected_page').live('click', function(){
		$('#selectable').toggle();
	});

	//set up the selectable jquery ui
	if (!$('#selectable').hasClass('ui-selectable')){
		  	$('#selectable').selectable({
				selected: function(event, ui){
					var URL, data;

					$(this).hide();

					window.my_config.page_id = $($(ui)[0].selected).attr('data-page_id');
					window.my_config.access_token = $($(ui)[0].selected).attr('data-access_token')
					window.my_config.name = $($(ui)[0].selected).html();

					// $('#selected_page').html(window.my_config.name + '<span> > </span>');

					//post the selected page_id to the db
					URL = '<?php echo site_url("teesheets/update_facebook_page_id");?>';
					data = {page_id: window.my_config.page_id, page_name : window.my_config.name};

					$.post(URL, data, function(response) {
				      console.log(response);
				    });

				    post_facebook_message();
				}
			});
		}

	/*Load Facebook SDK*/
	  window.fbAsyncInit = function() {
	    FB.init({
	      appId      : window.my_config.app_id,
	      channelUrl : '<?php echo site_url();?>', // Channel File
	      status     : true, // check login status
	      cookie     : true, // enable cookies to allow the server to access the session
	      xfbml      : true  // parse XFBML
	    });
	  };

	  (function(d){
	     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement('script'); js.id = id; js.async = true;
	     js.src = "http://connect.facebook.net/en_US/all.js";
	     ref.parentNode.insertBefore(js, ref);
	   }(document));

	 //EVERYTHING STARTS HERE
	$('.post').live('click', function(){
		if (window.my_config.can_post_with_extended_access_token)
		{
			post_facebook_message();
		}
		else
		{
			login();
		}
	});

	function login(){
		FB.login(function(response) {
	        if (response.authResponse) {
	            console.log("accepted connection");
	            console.log(response);

	            fetch_long_lived_access_token(response.authResponse.accessToken);
	        } else {
	            console.log("canceled");
	        }
		},{scope: 'manage_pages publish_stream'});//http://stackoverflow.com/questions/8717388/the-user-hasnt-authorized-the-application-to-perform-this-action
	}

	function logout(remove_logged_out_class){
		FB.logout(function(response) {
		});
	}

	function fetch_long_lived_access_token(short_lived_access_token)
	{
		var URL;
		URL = "https://graph.facebook.com/oauth/access_token?client_id=" + window.my_config.app_id + "&client_secret=" + window.my_config.app_secret + "&grant_type=fb_exchange_token&fb_exchange_token=" + short_lived_access_token;

		$.post(URL, function(response) {

		    var token_elements = response.split('=');
		    extended_access_token = token_elements[1].split('&');
		    window.my_config.extended_access_token = extended_access_token[0];
		    window.my_config.can_post_with_extended_access_token = true;

			//post the facebook extended access token to server
			URL = '<?php echo site_url("teesheets/update_facebook_access_token");?>';
			data = {extended_access_token: window.my_config.extended_access_token};

			$.post(URL, data, function(response) {

		    });

		    set_facebook_page();
	    });

	    return;
	}

	function set_facebook_page()
	{
		if (window.my_config.page_id === '')
		{
			FB.getLoginStatus(function(response) {
				  if (response.status === 'connected') {
				    FB.api('/me/accounts', function(response) {
				    	window.my_config.user_accounts = response.data;
				    	//delete current li items and then rebuild the list
				    	$('#selectable').children().remove();
			  			$.each(response.data, function(index, value){
			  				$('#selectable').append('<li class="ui-widget-content" data-page_id="' +value.id  + '" data-access_token="' + value.access_token + '">' + value.name + '</li>');
			  			});

			  			$('#selectable').show();
			  			logout();
					});
				  }
			 }, true);
		}
		else
		{
			post_facebook_message();
			logout();
		}

		return;
	}

	function post_facebook_message(options)
	{
		var message, URL, success_message;
		message = $('#message').val();
		URL = "https://graph.facebook.com/" + window.my_config.page_id + "/feed?access_token=" + window.my_config.extended_access_token + "&message=" + message;

		$('.facebook_messages').mask("<?php echo lang('common_wait'); ?>");

		$.post(URL, function(response) {

	    })
	    .success(function(){
	    	$('.facebook_messages').unmask();
	    	$('#message').val('');

	    	success_message = "Successfully Posted to " + window.my_config.name + " Facebook Page";

	    	//functioned defined in other script
	    	set_feedback(success_message, 'success_message', false);
	    })
	    .error(function() {
   			 $('.facebook_messages').unmask();
   			 login();
		});
	}
});
</script>
