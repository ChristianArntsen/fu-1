<?php
/*
 *	Hosted checkout class: Integration with Mercury Payment Systems (MPS)
 * 
 * 	Includes hosted checkout POS and hosted checkout eCOMM, both OneTime and Recurring transaction
 * 
 * 	Transactions currently supported by ForeUP:
 * 	
 * 
 * 
 *  
 */
class Hosted_checkout_2
{
	private $soap_request = '';
	private $transaction_type = '';
	private $window = 'POS';
	private $foreup_transaction = false;
	
	public $partial_auth_testing = true;
	
	//Required
	public $merchant_id = '494691720';
	public $password = 'KRD%8rw#+p9C13,T';//'1SF1hSzfA#D58wOM';
	public $tran_type = 'Sale';//'PreAuth', 'Sale', 'VoiceAuth', also MToken tran_types:  
	public $total_amount = '1.00';//decimal to two places 1.00
	public $frequency = 'OneTime';//'OneTime', 'Recurring' 
	public $invoice = '0';
	public $memo = 'ForeUP v.1.0';
	public $tax_amount = '0.00';//decimal to two places 1.00
	public $process_complete_url = '';//Where the screen goes after processing is complete
	public $return_url = '';//Where the screen goes when the user cancels
	public $partial_auth = 'On';
	
	//Not required
	public $avs_fields = 'Off';
	public $keypad = '';
	public $default_swipe = 'Swipe';//'Swipe', 'Manual'
	public $display_style = 'Custom';//'Mercury', 'Custom'
	public $background_color = 'White';
	public $font_color = 'Black';
	public $font_family = 'FontFamily1';
	public $font_size = 'Medium';
	public $logo_url = '';
	public $page_title = 'Test Page Title';
	public $security_logo = 'on';
	public $order_total = 'on';
	public $submit_button_text = 'Submit';
	public $cancel_button_text = 'Cancel';
	
	// MToken Relevant Parameters
	public $cardholder_name = '';
	public $avs_address = '';//billing address
	public $avs_zip = '';//billing zip
	public $customer_code = ''; 
	public $ref_no = '';
	public $operator_id = 'test';
	public $terminal_name = '';
	public $auth_code = '';
	public $acq_ref_data = '';
	public $process_data = '';
	public $auth_amount = '';
	public $gratuity_amount = '';
	public $token = '';
	
	//Returned values
	public $payment_id = '';//Value returned by initiate_payment
	public $response_code = '';
	public $message = '';

	// Sale, PreAuth, VoiceAuth transactions
	function initialize_payment($total_amount = '', $tax_amount = '', $tran_type = '', $window = '', $frequency = '', $guid = '') {
		$CI = get_instance();
		$CI->load->model('sale');
		if ($this->merchant_id == '494691720' && $guid == '')
			echo 'This is a test transaction.';
		$this->total_amount = $total_amount;
		$this->tax_amount = $tax_amount;
		$this->tran_type = $tran_type;
		$this->window = $window;
		$this->frequency = $frequency;
		if ($window == 'eCOM') {
			$this->partial_auth = 'Off';
			$this->memo = 'ForeUP v.1.1';
		}
		//REMOVING SAVE GUID UNTIL mobile_guid IS ADDED TO DB TABLE
		$this->invoice = $CI->sale->add_credit_card_payment(array('tran_type'=>$tran_type,'frequency'=>$frequency));//,'mobile_guid'=>$guid));
		$CI->session->set_userdata('invoice_id', $this->invoice);
		
		$this->transaction_type = 'InitializePayment';
	    $this->set_xml();
		//echo 'InitializePayment Invoice:'.$this->invoice;
		//print "<pre>".htmlentities($this->soap_request)."</pre>";
		//echo "<br/>Before we execute soap";
		$result = $this->execute_soap();
		//echo "<br/><br/>After we execute soap";
		return $this->parse_response($result);
	}
	function verify_payment() {

		$this->transaction_type = 'VerifyPayment';
		$this->set_xml();
		$result = $this->execute_soap();
		return $this->parse_response($result);
	}
	
	function complete_payment() {
		//$this->set_return_urls();
		//$this->set_mercury_credentials();
		$this->transaction_type = 'AcknowledgePayment';
		$this->set_xml();
		$result = $this->execute_soap();
		
		//$CI = get_instance();
		
		//Payment id is reset after payment is added
		//$CI->session->unset_userdata('payment_id');
		
		return $this->parse_response($result);
		
	}
	function get_iframe_url($window_type = '', $payment_id = '') {
		$CI = get_instance();
		$window_type = ($window_type != '')?$window_type:$this->window; 
		$payment_id = ($payment_id != '')?$payment_id:$CI->session->userdata('payment_id');
		if ($window_type == 'POS')
			return $this->base_url().'/CheckoutPOSIFrame.aspx?pid='.$payment_id;
		else if ($window_type == 'eCOM')
			return $this->base_url().'/CheckoutIFrame.aspx?pid='.$payment_id;
		else if ($window_type == 'mobile')
			return $this->base_url().'/mobile/mCheckout.aspx?pid='.$payment_id;
	}
	
	function get_url() {
		if ($this->transaction_type == 'InitializePayment' || $this->transaction_type == 'VerifyPayment' || $this->transaction_type == 'AcknowledgePayment')
			return $this->base_url().'/hcws/hcservice.asmx';
		else //For MToken Transactions
			return $this->base_url().'/tws/transactionservice.asmx';
	}
	
	function cancel_payment($invoice) {
			if ((int)$this->token_transaction('Reversal', $invoice) === 0)
				return true;
			else if ((int)$this->token_transaction('VoidSale', $invoice) === 0)
				return true;
			else if ((int)$this->token_transaction('Return', $invoice) === 0)
				return true;
			else 
				return false;
	}
	function issue_refund($invoice, $amount){
		//If refund is on same day, then we'll run an adjust token
		
		//Otherwise, we need to run a return token
		return $this->token_transaction('Return', $amount);
	}
	
	// MToken transactions
	function token_transaction($type, $purchase_amount = '0.00', $gratuity_amount = '0.00', $tax_amount = '0.00') {
		$this->amount = $purchase_amount;
		$this->auth_amount = $purchase_amount;
		$this->gratuity_amount = $gratuity_amount;
		$this->tax_amount = $tax_amount;
				
		//$CI = get_instance();
		//$CI->load->model('sale');
		//$this->invoice = $CI->sale->add_credit_card_payment(array('tran_type'=>$tran_type));
		//echo 'type '.$type.' invoice '.$invoice.' amount '.$purchase_amount.'<br/>';
		//$payment_details = $CI->sale->get_credit_card_payment($invoice);
		//if (count($payment_details) > 0) {
			/*$this->invoice = $invoice;
			$this->auth_amount = ($purchase_amount == '0.00')?$payment_details[0]['auth_amount']:$purchase_amount;
			$this->cardholder_name = $payment_details[0]['cardholder_name'];
			$this->ref_no = $payment_details[0]['ref_no'];
			$this->operator_id = $payment_details[0]['operator_id'];
			$this->terminal_name = $payment_details[0]['terminal_name'];
			$this->auth_code = $payment_details[0]['auth_code'];
			$this->acq_ref_data = $payment_details[0]['acq_ref_data'];
			$this->process_data = $payment_details[0]['process_data'];
			$this->token = $payment_details[0]['token'];
			$this->avs_address = $payment_details[0]['avs_address'];
			$this->avs_zip = $payment_details[0]['avs_zip'];
			$this->customer_code = $payment_details[0]['customer_code'];
			$this->memo = ($payment_details[0]['memo'] != '')?$payment_details[0]['memo']:$this->memo;
			$this->amount = ($purchase_amount == '0.00')?$payment_details[0]['amount']:$purchase_amount;
			$this->gratuity_amount = ($gratuity_amount == '0.00')?$payment_details[0]['gratuity_amount']:$gratuity_amount;
			$this->tax_amount = ($tax_amount == '0.00')?$payment_details[0]['tax_amount']:$tax_amount;
			$this->frequency = ($payment_details[0]['frequency'] != '')?$payment_details[0]['frequency']:'OneTime';*/
			
			//rdHolderName>$this->cardholder_name</CardHolderName> ";
			//  $soap_request .= "<Frequency>$this->frequency</Frequency> ";
			//  $soap_request .= "<Invoice>$this->invoice</Invoice> ";
			//  $soap_request .= "<Memo>$this->memo</Memo> ";
			//  $soap_request .= "<PurchaseAmount>$this->amount</PurchaseAmount> ";
			//  $soap_request .= "<TaxAmount>$this->tax_amount</TaxAmount> ";
			//  $soap_request .= "<Token>$this->token</Token> ";
		//}
		//else 
		//	return 100;
		
		// PreAuthCapture and Adjust need some additional information
		// Adjusted Amount, etc.
		
		// Void Sale, Reversal, and Return should all just require the info straight from the original transaction
		
		switch ($type) {
			case 'PreAuth'://For recurring billing
				$this->transaction_type = 'CreditPreAuthToken';
				break;
			case 'Sale'://For recurring billing
				$this->transaction_type = 'CreditSaleToken';
				break;
			case 'PreAuthCapture'://Finalizing PreAuth
				$this->transaction_type = 'CreditPreAuthCaptureToken';
				break;
			case 'Adjust'://Adjust the amount of original Sale or PreAuthCapture
				$this->transaction_type = 'CreditAdjustToken';
				break;
			case 'VoidSale'://void Sale or PreAuthCapture
				$this->transaction_type = 'CreditVoidSaleToken';
				break;
			case 'Reversal'://reverse PartialAuth transactions
				$this->transaction_type = 'CreditReversalToken';
				break;
			case 'Return'://return previously ran transaction
				$this->transaction_type = 'CreditReturnToken';
				break;
			case 'VoidReturn'://void Return transaction in current batch
				$this->transaction_type = 'CreditVoidReturnToken';
				break;
		}
		//$this->set_mercury_credentials();
		$this->set_xml();
		$result = $this->execute_soap();
		//echo $this->transaction_type.' Invoice:'.$this->invoice;
		//print "<pre>".htmlentities($this->soap_request)."</pre>";
		return $this->parse_response($result);
	}
	
	
	function display_checkout_page() {
		$CI = get_instance();
		$user_message = $CI->session->userdata('user_message');
		$return_code = $CI->session->userdata('return_code');
		$data = array('user_message'=>$user_message, 'return_code'=>$return_code, 'url'=>$this->get_iframe_url());
		$CI->session->set_userdata('user_message', '');
		$CI->session->set_userdata('return_code', '');
		//echo 'displaying checkout';
		if ($this->window == 'POS')
			$CI->load->view('sales/hc_pos_iframe.php', $data);
		else if ($this->window == 'eCOM')
			$CI->load->view('sales/hc_iframe.php', $data);
	}
	
	/*
	function payment_made($data = array()) {
		$CI = get_instance();
		$this->foreup_transaction = $CI->session->userdata('foreup_transaction');
				
		$payment_id = $CI->input->post('PaymentID');
		$return_code = $CI->input->post('ReturnCode');
		$return_message = $CI->input->post('ReturnMessage');
		$details = $this->verify_payment();
		$this->complete_payment();
		$data['success'] = false;
		//$details['return_code'] = 301;
		//print_r($details);
		switch((int)$details['return_code']) {
			case 0:
				if ($details['status'] == 'Declined')
				{
					$CI->session->set_userdata('user_message', 'Card Declined, please try a different card');
					$CI->session->set_userdata('return_code', (int)$details['return_code']);
					$this->initialize_payment();
					return $data;
				}
				//Success
				//$this->verify_payment();
				
				$data['url'] = $this->get_iframe_url();
				$data['payment_amount'] = (string)$details['auth_amount'];
				$data['card_type'] = (string)$details['card_type'];
				$data['cardholder_name'] = (string)$details['cardholder_name'];
				$data['auth_code'] = (string)$details['auth_code'];
				$data['masked_account'] = (string)$details['masked_account'];
				$data['payment_type'] = (string)$details['card_type'].' '.(string)$details['masked_account'];
				$data['invoice_id'] = (string)$CI->session->userdata('invoice_id');
				$data['success'] = true;
				
				//Unset session data
				$CI->session->unset_userdata('user_message');
				$CI->session->unset_userdata('total_amount');
				$CI->session->unset_userdata('tax_amount');
				$CI->session->unset_userdata('tran_type');
				$CI->session->unset_userdata('window_type');
				$CI->session->unset_userdata('foreup_transaction');
				//This is unset in the Sale_lib
				//$CI->session->unset_userdata('invoice_id');
				
				return $data;
				//return;
				break;
			case 101:
				//Card Declined
				//reload payment window saying card was declined
				$CI->session->set_userdata('user_message', 'Card Declined, please try a different card');
				$CI->session->set_userdata('return_code', (int)$details['return_code']);
				$this->initialize_payment();
//				$this->display_checkout_page('Card Declined, please try new card', (int)$details['return_code']);
				break;
			case 102:
				//user canceled
				//just close popup
			case 103:
				//timed out
				//just close popup
			case 104:
				//maintenance mode
				//just close popup
				$CI->load->view('sales/close_payment_window.php');
				break;
			case 201:
				//Post items missing
			case 202:
				// Init missing 
			case 203:
				// Process payment failed
			case 204:
				// db error
			case 205:
				// db error
				//Reading error, please swipe again.
				$CI->session->set_userdata('user_message', 'Internal error, please swipe card again');
				$CI->session->set_userdata('return_code', (int)$details['return_code']);
				$this->initialize_payment();
				break;
			case 301:
				// CC validation fail
				// Card declined?
			case 302:
				// tampering suspected
				// Card declined?
				$CI->session->set_userdata('user_message', 'Card Error, please try a different card');
				$CI->session->set_userdata('return_code', (int)$details['return_code']);
				$this->initialize_payment();
				break;
		}
		return $data;
		//echo $payment_id.' - '.$return_code.' - '.$return_message;
		//$CI->load->view('sales/payment_made.php', array('url'=>$this->get_iframe_url()));
	
	//	$this->display_checkout_page();
	}*/
	
	 function parse_response($response) {
			
		$CI = get_instance();
		$CI->load->model('sale');
		
		//echo 'response';
		$xml = simplexml_load_string($response, null, null, 'http://schemas.xmlsoap.org/soap/envelope/');
		//print "<pre>".htmlentities($response)."</pre>";
			
		$xml->registerXPathNamespace('soap', 'http://schemas.xmlsoap.org/soap/envelope/');
		$xml->registerXPathNamespace('', 'http://www.mercury.com/');
		
		if ($this->transaction_type == 'InitializePayment') {
			$parsed_response = $xml->Body->children()->InitializePaymentResponse->InitializePaymentResult;
			return $parsed_response;
			/* Possible response code values
			 * 
			 * 0 – Success 
			 * 100 – AuthFail (bad password or MerchantId) 
			 * 200 –Mercury Internal Error 
			 * 300 –ValidationFail: General Validation Error
			 */
		}
		else if ($this->transaction_type == 'VerifyPayment') {
		    $parsed_response = $xml->Body->children()->VerifyPaymentResponse->VerifyPaymentResult;
			return $parsed_response;
		}
		else if ($this->transaction_type == 'CompletePayment') {
		    $parsed_response = $xml->Body->children()->CompletePaymentResponse->CompletePaymentResult;
			return $parsed_response;
		}
		else if ($this->transaction_type == 'CreditPreAuthToken' ||
				$this->transaction_type == 'CreditSaleToken' ||
				$this->transaction_type == 'CreditPreAuthCaptureToken' ||
				$this->transaction_type == 'CreditAdjustToken' ||
				$this->transaction_type == 'CreditVoidSaleToken' ||
				$this->transaction_type == 'CreditReversalToken' ||
				$this->transaction_type == 'CreditReturnToken' ||
				$this->transaction_type == 'CreditVoidReturnToken') {
				
			$response_val = $this->transaction_type.'Response';
			$result_val = $this->transaction_type.'Result';
			$parsed_response = $xml->Body->children()->$response_val->$result_val;
			
			//$invoice = (string)$parsed_response->Invoice;
			return $parsed_response;	
			$payment_data = array(
				'tran_type'=>(string)$this->transaction_type,
				'acq_ref_data'=>(string)$parsed_response->AcqRefData,
				'auth_code'=>(string)$parsed_response->AuthCode,
				'auth_amount'=>(string)$parsed_response->AuthorizeAmount,
				'avs_result'=>(string)$parsed_response->AVSResult,
				'batch_no'=>(string)$parsed_response->BatchNo,
				'card_type'=>(string)$parsed_response->CardType,
				'cvv_result'=>(string)$parsed_response->CVVResult,
				'gratuity_amount'=>(string)$parsed_response->GratuityAmount,
				'masked_account'=>(string)$parsed_response->Account,
				'status_message'=>(string)$parsed_response->Message,
				'amount'=>(string)$parsed_response->PurchaseAmount,
				'ref_no'=>(string)$parsed_response->RefNo,
				'status'=>(string)$parsed_response->Status,
				'token'=>(string)$parsed_response->Token,
				'process_data'=>(string)$parsed_response->ProcessData
			);
			//$CI->sale->update_credit_card_payment($invoice, $payment_data);
			$CI->sale->add_credit_card_payment($payment_data);
			//echo $CI->db->last_query();
			
		}

		return $this->response_code;
	}
	
	function execute_soap(){
		set_time_limit(0);
	      $header = array("POST /ws/ws.asmx HTTP/1.1",
		                "Host: w1.mercurypay.com",
						"Content-Type: text/xml; charset=utf-8",
						"SOAPAction: \"http://www.mercurypay.com/$this->transaction_type\"");

		  $url = $this->get_url();
		  //echo "<br/>Url: $url <br/>";
		  //echo "Headers<br/>";
		  //print_r($header);
		  $ch = curl_init();
		  curl_setopt($ch, CURLOPT_URL,$url);
		  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);
		  curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		  curl_setopt($ch, CURLOPT_TIMEOUT, 15);
		  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
		  curl_setopt($ch, CURLOPT_POSTFIELDS, $this->soap_request);
		  $result = curl_exec($ch);
		//  log_message('error', 'Soap Request');
		 // log_message('error', "$this->soap_request");
		 // log_message('error', 'Result/Response');
		 // log_message('error', "$result");
	//echo "Soap Request<br/>";
	//echo "<pre>$this->soap_request</pre>";
	//echo "<br/><br/><br/>";
	//echo "Result/Response<br/>";
	//echo "<pre>$result</pre>";
	     return $result;
	}
	
	function set_xml()
	{
		$CI = get_instance();
		
		$soap_request  = "<?xml version='1.0' encoding='ISO-8859-1'?>";
	    $soap_request .= "<SOAP-ENV:Envelope SOAP-ENV:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/' xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:SOAP-ENC='http://schemas.xmlsoap.org/soap/encoding/'>";
	    $soap_request .= "<SOAP-ENV:Body>";
	    $soap_request .= "<$this->transaction_type  xmlns='http://www.mercurypay.com/'>";
	  	if ($this->transaction_type == "InitializePayment" || $this->transaction_type == 'VerifyPayment' || $this->transaction_type == 'AcknowledgePayment') {
			$soap_request .= "<request> ";
		  	$soap_request .= "<Password>$this->password</Password> ";
		}
		else {
		  	$soap_request .= "<password>$this->password</password> ";
			$soap_request .= "<request> ";
		}
		$soap_request .= "<MerchantID>$this->merchant_id</MerchantID> ";
		
		if ($this->transaction_type == "InitializePayment") {
			  $soap_request .= "<Invoice>$this->invoice</Invoice> ";
			  $soap_request .= "<TotalAmount>$this->total_amount</TotalAmount>";
			  $soap_request .= "<TaxAmount>$this->tax_amount</TaxAmount>";
			  $soap_request .= "<TranType>$this->tran_type</TranType> ";
			  $soap_request .= "<Frequency>$this->frequency</Frequency>";
			  $soap_request .= "<AVSFields>$this->avs_fields</AVSFields>";
			  $soap_request .= "<PartialAuth>$this->partial_auth</PartialAuth>";
			  $soap_request .= "<Memo>$this->memo</Memo> ";
			  $soap_request .= "<OperatorID>$this->operator_id</OperatorID> ";
			  $soap_request .= "<TerminalName>$this->terminal_name</TerminalName> ";
			  $soap_request .= "<ProcessCompleteUrl>$this->process_complete_url</ProcessCompleteUrl> ";
			  $soap_request .= "<ReturnUrl>$this->return_url</ReturnUrl> ";
			  $soap_request .= "<DefaultSwipe>$this->default_swipe</DefaultSwipe>";
			  //$soap_request .= "<SecurityLogo>$this->security_logo</SecurityLogo>";
			  $soap_request .= "<OrderTotal>$this->order_total</OrderTotal>";
			  //$soap_request .= "<SubmitButtonText>$this->submit_button_text</SubmitButtonText>";
			  //$soap_request .= "<CancelButtonText>$this->cancel_button_text</CancelButtonText>";
		}
		else if ($this->transaction_type == 'VerifyPayment' || $this->transaction_type == 'AcknowledgePayment'){
			  $soap_request .= "<PaymentID>".$this->payment_id."</PaymentID> ";
		}
		else if ($this->transaction_type == 'CreditPreAuthCaptureToken'){
			  $soap_request .= "<Invoice>$this->invoice</Invoice> ";
			  $soap_request .= "<Token>$this->token</Token> ";
			  $soap_request .= "<CardHolderName>$this->cardholder_name</CardHolderName> ";
			  $soap_request .= "<CustomerCode>$this->customer_code</CustomerCode> ";
			  $soap_request .= "<AuthorizeAmount>$this->auth_amount</AuthorizeAmount> ";
			  $soap_request .= "<PurchaseAmount>$this->amount</PurchaseAmount> ";
			  $soap_request .= "<GratuityAmount>$this->gratuity_amount</GratuityAmount> ";
			  $soap_request .= "<TaxAmount>$this->tax_amount</TaxAmount> ";
			  $soap_request .= "<Frequency>$this->frequency</Frequency> ";
			  $soap_request .= "<Memo>$this->memo</Memo> ";
			  $soap_request .= "<OperatorID>$this->operator_id</OperatorID> ";
			  $soap_request .= "<TerminalName>$this->terminal_name</TerminalName> ";
			  $soap_request .= "<AcqRefData>$this->acq_ref_data</AcqRefData> ";
			  $soap_request .= "<AuthCode>$this->auth_code</AuthCode> ";
		}
		else if ($this->transaction_type == 'CreditAdjustToken'){
			  $soap_request .= "<Invoice>$this->invoice</Invoice> ";
			  $soap_request .= "<CardHolderName>$this->cardholder_name</CardHolderName> ";
			  $soap_request .= "<CustomerCode>$this->customer_code</CustomerCode> ";
			  $soap_request .= "<Token>$this->token</Token> ";
			  $soap_request .= "<RefNo>$this->ref_no</RefNo> ";
			  $soap_request .= "<PurchaseAmount>$this->amount</PurchaseAmount> ";
			  $soap_request .= "<GratuityAmount>$this->gratuity_amount</GratuityAmount> ";
			  $soap_request .= "<TaxAmount>$this->tax_amount</TaxAmount> ";
			  $soap_request .= "<Frequency>$this->frequency</Frequency> ";
			  $soap_request .= "<Memo>$this->memo</Memo> ";
			  $soap_request .= "<OperatorID>$this->operator_id</OperatorID> ";
			  $soap_request .= "<TerminalName>$this->terminal_name</TerminalName> ";
			  $soap_request .= "<AuthCode>$this->auth_code</AuthCode> ";
		}
		else if ($this->transaction_type == 'CreditPreAuthToken'){
			  $soap_request .= "<CardHolderName>$this->cardholder_name</CardHolderName> ";
			  $soap_request .= "<Frequency>$this->frequency</Frequency> ";
			  $soap_request .= "<Invoice>$this->invoice</Invoice> ";
			  $soap_request .= "<Memo>$this->memo</Memo> ";
			  $soap_request .= "<OperatorID>$this->operator_id</OperatorID> ";
			  $soap_request .= "<Amount>$this->amount</Amount> ";
			  $soap_request .= "<TerminalName>$this->terminal_name</TerminalName> ";
			  $soap_request .= "<Token>$this->token</Token> ";
		}
		else if ($this->transaction_type == 'CreditSaleToken'){
			  $soap_request .= "<CardHolderName>$this->cardholder_name</CardHolderName> ";
			  $soap_request .= "<Frequency>$this->frequency</Frequency> ";
			  $soap_request .= "<Invoice>$this->invoice</Invoice> ";
			  $soap_request .= "<Memo>$this->memo</Memo> ";
			  $soap_request .= "<OperatorID>$this->operator_id</OperatorID> ";
			  $soap_request .= "<PurchaseAmount>$this->amount</PurchaseAmount> ";
			  $soap_request .= "<TaxAmount>$this->tax_amount</TaxAmount> ";
			  $soap_request .= "<TerminalName>$this->terminal_name</TerminalName> ";
			  $soap_request .= "<Token>$this->token</Token> ";
		}
		else if ($this->transaction_type == 'CreditVoidSaleToken'){
			  $soap_request .= "<Invoice>$this->invoice</Invoice> ";
			  $soap_request .= "<CardHolderName>$this->cardholder_name</CardHolderName> ";
			  $soap_request .= "<Token>$this->token</Token> ";
			  $soap_request .= "<RefNo>$this->ref_no</RefNo> ";
			  $soap_request .= "<PurchaseAmount>$this->auth_amount</PurchaseAmount> ";
			  $soap_request .= "<Frequency>$this->frequency</Frequency> ";
			  $soap_request .= "<Memo>$this->memo</Memo> ";
			  $soap_request .= "<OperatorID>$this->operator_id</OperatorID> ";
			  $soap_request .= "<TerminalName>$this->terminal_name</TerminalName> ";
			  $soap_request .= "<AuthCode>$this->auth_code</AuthCode> ";
		}
		else if ($this->transaction_type == 'CreditReversalToken'){
			  $soap_request .= "<Invoice>$this->invoice</Invoice> ";
			  $soap_request .= "<Token>$this->token</Token> ";
			  $soap_request .= "<CardHolderName>$this->cardholder_name</CardHolderName> ";
			  $soap_request .= "<RefNo>$this->ref_no</RefNo> ";
			  $soap_request .= "<PurchaseAmount>$this->auth_amount</PurchaseAmount> ";
			  $soap_request .= "<Frequency>$this->frequency</Frequency> ";
			  $soap_request .= "<Memo>$this->memo</Memo> ";
			  $soap_request .= "<OperatorID>$this->operator_id</OperatorID> ";
			  $soap_request .= "<TerminalName>$this->terminal_name</TerminalName> ";
			  $soap_request .= "<AuthCode>$this->auth_code</AuthCode> ";
			  $soap_request .= "<AcqRefData>$this->acq_ref_data</AcqRefData> ";
			  $soap_request .= "<ProcessData>$this->process_data</ProcessData> ";
		}
		else if ($this->transaction_type == 'CreditReturnToken'){
			  $soap_request .= "<Invoice>$this->invoice</Invoice> ";
			  $soap_request .= "<CardHolderName>$this->cardholder_name</CardHolderName> ";
			  $soap_request .= "<Token>$this->token</Token> ";
			  $soap_request .= "<PurchaseAmount>$this->auth_amount</PurchaseAmount> ";
			  $soap_request .= "<Frequency>$this->frequency</Frequency> ";
			  $soap_request .= "<Memo>$this->memo</Memo> ";
			  $soap_request .= "<OperatorID>$this->operator_id</OperatorID> ";
			  $soap_request .= "<TerminalName>$this->terminal_name</TerminalName> ";
		}
	    else if ($this->transaction_type == 'CreditVoidReturnToken'){
			  $soap_request .= "<AuthCode>$this->auth_code</AuthCode> ";
			  $soap_request .= "<CardHolderName>$this->cardholder_name</CardHolderName> ";
			  $soap_request .= "<RefNo>$this->ref_no</RefNo> ";
			  $soap_request .= "<Invoice>$this->invoice</Invoice> ";
			  $soap_request .= "<Token>$this->token</Token> ";
			  $soap_request .= "<PurchaseAmount>$this->auth_amount</PurchaseAmount> ";
			  $soap_request .= "<Frequency>$this->frequency</Frequency> ";
			  $soap_request .= "<Memo>$this->memo</Memo> ";
			  $soap_request .= "<OperatorID>$this->operator_id</OperatorID> ";
			  $soap_request .= "<TerminalName>$this->terminal_name</TerminalName> ";
		}
	    
	    $soap_request .= "</request> ";
	    $soap_request .= "</$this->transaction_type>";
		$soap_request .= "</SOAP-ENV:Body>";
	    $soap_request .= "</SOAP-ENV:Envelope>";
	    //echo '<br/>Soap Request<br/>';
		//echo $soap_request;		  
	    //echo '<br/>End Soap Request<br/>';
		$this->soap_request = $soap_request;
	}
	function base_url() {
		if ($this->merchant_id == '494691720')
			return 'https://hc.mercurydev.net';
		else 
			return 'https://hc.mercurypay.com';
	}
	function set_merchant_credentials($merchant_id = '', $password = '') {
		$this->merchant_id = ($merchant_id != '')?$merchant_id:'494691720';
		$this->password = ($password != '')?$password:'KRD%8rw#+p9C13,T';
	}
	function set_operator_id($operator_id) {
		$this->operator_id = $operator_id;
	}
	function set_terminal_name($terminal_name) {
		$this->terminal_name = $terminal_name;
	}
	function set_tran_type($tran_type) {
		$this->tran_type = $tran_type;
	}	
	function set_response_urls($process_complete_url = '', $return_url = '') {
		$this->process_complete_url = base_url().'index.php/'.$process_complete_url;
		$this->return_url = base_url().'index.php/'.$return_url;
	}
	function set_avs_fields($avs_fields = 'Off') {
		$this->avs_fields = $avs_fields;
	}
	function set_partial_auth($partial_auth = 'On') {
		$this->partial_auth = $partial_auth;
	}
	function set_payment_id($payment_id) {
		$this->payment_id = $payment_id;
	}
	function set_token($token) {
		$this->token = $token;
	}	
	function set_invoice($invoice) {
		$this->invoice = $invoice;
	}
	function set_memo($memo) {
		$this->memo = $memo;
	}
	function set_cardholder_name($cardholder_name){
		$this->cardholder_name = $cardholder_name;
	}
	function set_customer_code($customer_code){
		$this->customer_code = $customer_code;
	}
	function set_ref_no($ref_no) {
		$this->ref_no = $ref_no;
	}
	function set_auth_amount($auth_amount) {
		$this->auth_amount = $auth_amount;
	}
	function set_amount($amount) {
		$this->amount = $amount;
	}
	function set_gratuity_amount($gratuity_amount) {
		$this->gratuity_amount = $gratuity_amount;
	}
	function set_tax_amount($tax_amount) {
		$this->tax_amount = $tax_amount;
	}
	function set_auth_code($auth_code) {
		$this->auth_code = $auth_code;
	}
	function set_acq_ref_data($acq_ref_data) {
		$this->acq_ref_data = $acq_ref_data;
	}
	function set_process_data($process_data) {
		$this->process_data = $process_data;
	}
	function set_frequency($frequency) {
		$this->frequency = $frequency;
	}
	function set_default_swipe($default_swipe = 'Swipe') {
		$this->default_swipe = $default_swipe;
	}
	/*function set_return_urls(){
		if ($this->foreup_transaction)
		{
			$this->process_complete_url = base_url().'index.php/be/payment_made';//Where the screen goes after processing is complete
			$this->return_url = base_url().'index.php/be/payment_made';//Where the screen goes when the user cancels
		}
		//else if ($this->course_)
		else 
		{
			$this->process_complete_url = base_url().'index.php/sales/payment_made';//Where the screen goes after processing is complete
			$this->return_url = base_url().'index.php/sales/payment_made';//Where the screen goes when the user cancels
		}
	}*/
		
}
?>
