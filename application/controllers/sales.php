<?php
require_once ("secure_area.php");
class Sales extends Secure_area
{
	function __construct()
	{
		parent::__construct('sales');
        $this->load->library('sale_lib');
		$this->load->model('schedule');
		$this->load->model('teesheet');
		$this->load->model('green_fee');
		$this->load->model('fee');
		$this->load->model('Image');
		$this->load->model('Customer_loyalty');
		$this->load->model('Customer_credit_card');
		$this->load->model('Pricing');
	}
	function db2_test()
	{
		$this->db2->from('deals');
		$results = $this->db2->get()->result_array();
		print_r($results);
		echo '<br/><br/>';
		$this->db->from('teetime');
		$this->db->limit('30');
		$s_results = $this->db->get()->result_array();
		print_r($s_results);
	}

	function index()
	{
		if ($this->config->item('track_cash')) {
			$data = $this->Sale->getUnfinishedRegisterLog()->row_array();
			//print_r($data);
			//$this->input->post('skip');
			if ($this->input->post('skip') || $this->input->get('skip') || $this->session->userdata('skipped_register_log') || $this->session->userdata('unfinished_register_log')) {
				$this->session->set_userdata('skipped_register_log', true);
				//echo 'reloading skip or '
				$this->_reload();
			}
			else if ($this->input->post('opening_amount') != '' || $this->input->get('opening_amount') != '') {
				$now = date('Y-m-d H:i:s');
				$register_log_id = $this->input->post('register_log_id');
				$cash_register = new stdClass();

				$cash_register->employee_id = $this->session->userdata('person_id');
				$cash_register->terminal_id = $this->session->userdata('terminal_id');
				$cash_register->open_amount = $this->input->post('opening_amount') ? $this->input->post('opening_amount') : $this->input->get('opening_amount');
				$cash_register->close_amount = 0;

				// SAVE IN THE DATABASE
				$cash_register->shift_end = '0000-00-00 00:00:00';
				$cash_register->course_id = $this->session->userdata('course_id');

	//echo $this->session->userdata('unfinished_register_log')?'unfinished_register_log':'not_unfinished';
				if ($this->session->userdata('unfinished_register_log') || $register_log_id){
					$this->Sale->updateRegisterLog($cash_register, false, false, $register_log_id);
				}
				else
				{
					$cash_register->shift_start = $now;
					$this->Sale->insertRegisterLog($cash_register);
				}
				$this->session->set_userdata('unfinished_register_log', 1);
				//$this->session->set_userdata('cash_register', $cash_register);
		//			echo '<br/>about to redirect';
				$this->_reload();//redirect(site_url('sales'));
			}
			else if ($this->input->post('close_and_open'))
			{
				$this->session->set_userdata('unfinished_register_log', 1);
				$this->session->set_userdata('reopen_sales', 1);
				redirect(site_url('sales/closeregister'));
			}
			else if ($this->input->post('continue_log'))
			{
				$this->session->set_userdata('unfinished_register_log', 1);
				$this->_reload();
			}
			else {
			//	echo 'opening_amount';
				// GET UNFINISHED SHIFT FROM THE DATABASE
				//$data = $this->Sale->getUnfinishedRegisterLog()->row_array();
				//print_r($data);
				//if (isset($data['shift_start']))
				if ($data['open_amount'] && $this->input->post('submit'))
					$this->session->set_userdata('unfinished_register_log', 1);
				if ($this->session->userdata('mobile'))
				{
					echo json_encode(array('message'=>'cash register log active, opening amount needed', 'status'=>'hold', 'required'=>array('opening_amount','skip')));
				}
				else
					$this->load->view('sales/opening_amount', $data);
			}

		} else {
			$this->_reload();
		}
	}
	function openregister()
	{
		if ($this->config->item('track_cash')) {
			$data = $this->Sale->getUnfinishedRegisterLog()->row_array();

			if ($this->input->post('skip') || $this->input->get('skip') || $this->session->userdata('skipped_register_log') || $this->session->userdata('unfinished_register_log')) {
				$this->session->set_userdata('skipped_register_log', true);

				echo json_encode(array('message'=>'register log successfully skipped', 'status'=>'success'));
				return;
			}
			else if ($this->input->post('opening_amount') != '' || $this->input->get('opening_amount') != '') {
				$now = date('Y-m-d H:i:s');
				$cash_register = new stdClass();

				$cash_register->employee_id = $this->session->userdata('person_id');
				$cash_register->terminal_id = $this->session->userdata('terminal_id');
				$cash_register->open_amount = $this->input->post('opening_amount') ? $this->input->post('opening_amount') : $this->input->get('opening_amount');
				$cash_register->close_amount = 0;

				// SAVE IN THE DATABASE
				$cash_register->shift_end = '0000-00-00 00:00:00';
				$cash_register->course_id = $this->session->userdata('course_id');

				if ($this->session->userdata('unfinished_register_log'))
					$this->Sale->updateRegisterLog($cash_register);
				else
				{
					$cash_register->shift_start = $now;
					$this->Sale->insertRegisterLog($cash_register);
				}
				$this->session->set_userdata('unfinished_register_log', 1);

				echo json_encode(array('message'=>'successfully opened register log at '.$cash_register->open_amount, 'status'=>'success'));
				return;
			} else {
				if ($data['open_amount'])
					$this->session->set_userdata('unfinished_register_log', 1);

				echo json_encode(array('message'=>'cash register log active, opening amount needed', 'status'=>'hold', 'required'=>array('opening_amount','skip')));
				return;
			}
		} else {
			echo json_encode(array('message'=>'cash register log inactive', 'status'=>'success'));
		}
	}
	function view_override() {
		$this->load->model('employee');
		$data = array (
			'admins_managers'=>$this->employee->get_all(10000,0,array(2,3))->result_array()
		);
		$this->load->view('sales/override', $data);
	}
	function override(){
		$this->load->model('employee');
		$person_id = $this->input->post('employee_id');
		$password = $this->input->post('password');
		if ($this->employee->can_override($person_id, $password))
		{
			$this->session->set_userdata('purchase_override', $person_id);
			$this->session->set_userdata('purchase_override_time', date('Y-m-d h:i:sa'));
			echo json_encode(array('success'=>true, 'person_id'=>$person_id));
		}
		else
			echo json_encode(array('success'=>false));
	}
	function swiper() {
		$this->load->view('sales/swiper');
	}
	function closeregister() {
		//echo 'close register';
		//if ($this->session->userdata('unfinished_register_log')) {
		//	redirect(site_url('home'));
		//	return;
		//}
		$employee_id = $this->input->post('employee_id');
		$terminal_id = $this->input->post('terminal_id');
		$result = $this->Sale->getUnfinishedRegisterLog($employee_id, $terminal_id);
		//echo $this->db->last_query();
		//print_r( $cash_register);
		//print_r($result->result_array());
		//return;
		$continueUrl = $this->input->get('continue');
		// echo '<br/>';
		// echo $this->input->post('closing_amount') != '' ? 'closing amount' : 'no closing amount';
		// echo '<br/>';
		// echo $result->num_rows() > 0 ? 'unfinished register log' : 'no register log';
		// echo '<br/>';
		// echo $this->input->post('skip') ? 'skip' : 'no skip';
		// echo '<br/>';
		// echo $this->session->userdata('unfinished_register_log') ? 'session' : 'no session';
		// echo '<br/>';
		if ($this->input->post('closing_amount') != '' && $result->num_rows() > 0) {
			//echo 'closing amount';
			// CLOSING REGISTER LOG
			$cash_register = $result->row();
			//echo 'spot 1';
			$now = date('Y-m-d H:i:s');
			//$cash_register = $this->Sale->getUnfinishedRegisterLog()->row();
			$cash_register->shift_end = $now;
			$cash_register->close_amount = $this->input->post('closing_amount');
			$counts = array(
				'register_log_id'=>$cash_register->register_log_id,
				'pennies'=>$this->input->post('pennies'),
				'nickels'=>$this->input->post('nickels'),
				'dimes'=>$this->input->post('dimes'),
				'quarters'=>$this->input->post('quarters'),
				'ones'=>$this->input->post('ones'),
				'fives'=>$this->input->post('fives'),
				'tens'=>$this->input->post('tens'),
				'twenties'=>$this->input->post('twenties'),
				'fifties'=>$this->input->post('fifties'),
				'hundreds'=>$this->input->post('hundreds')
			);
			$cash_register->cash_sales_amount = $this->Sale->get_cash_sales_total_for_shift($cash_register->shift_start, $cash_register->shift_end, $employee_id, $terminal_id, $cash_register->persist);
			$cash_register->course_id = $this->session->userdata('course_id');
			$cash_register->closing_employee_id = $this->session->userdata('person_id');
			$this->Sale->updateRegisterLog($cash_register, $employee_id, $terminal_id);
			// echo $this->db->last_query();
			// return;
			$this->Sale->saveRegisterLogCounts($counts);
			$this->session->unset_userdata('skipped_register_log');
			$this->session->unset_userdata('cash_register');
			$this->session->unset_userdata('unfinished_register_log');
			if ($this->session->userdata('mobile'))
			{
				echo json_encode(array('message'=>'successfully closed register log', 'status'=>'success'));
			}
			else if ($this->session->userdata('reopen_sales')) {
				redirect(site_url('sales'));
			}
			else if ($continueUrl == 'logout') {
				redirect(site_url('home/logout'));
			} else {
				redirect(site_url('home'));
			}
		} else if(!$this->input->post('skip') && $this->session->userdata('unfinished_register_log') && $result->num_rows() > 0) {
			//echo 'skip';
			// LOADING WINDOW TO CLOSE REGISTER LOG
			$cash_register = $result->row();
			//echo 'spot 2';
			//echo '<br/>'.$cash_register->shift_start;
			$sales_total = $this->Sale->get_cash_sales_total_for_shift($cash_register->shift_start, date("Y-m-d H:i:s"), $employee_id, $terminal_id, $cash_register->persist);
			//echo '<br/>'.$this->db->last_query();
			if ($this->session->userdata('mobile'))
			{
				$data = array('message'=>'cash register log active, closing amount needed', 'status'=>'hold', 'required'=>array('closing_amount'), 'estimated_closing_amount'=>to_currency_no_symbol($cash_register->open_amount + $sale_total));
				echo json_encode($data);
			}
			else
				$this->load->view('sales/closing_amount', array(
					'continue'=>$continueUrl ? "?continue=$continueUrl" : '',
					'closeout'=>to_currency($cash_register->open_amount + $sales_total)
				));
		}
		else {
			//echo 'skipped register log';
			// MOST LIKELY SKIPPED THE REGISTER LOG
			//echo 'spot 3';
			$this->session->unset_userdata('skipped_register_log');
			$this->session->unset_userdata('cash_register');
			$this->session->unset_userdata('unfinished_register_log');
			if ($this->session->userdata('mobile'))
			{
				echo json_encode(array('message'=>'successfully closed register log', 'status'=>'success'));
			}
			else if ($continueUrl == 'logout') {
				redirect(site_url('home/logout'));
			} else {
				redirect(site_url('home'));
			}

		}
	}
	function logout() {
		// LOGOUT CURRENT USER JUST LIKE F & B
		$this->session->unset_userdata('sales_logged_in');
		//$this->session->set_userdata('person_id', '-1');
		$this->session->unset_userdata('user_level');
		$this->session->unset_userdata('auto_mailers');
        $this->session->unset_userdata('config');
        $this->session->unset_userdata('courses');
        $this->session->unset_userdata('customers');
        $this->session->unset_userdata('dashboards');
        $this->session->unset_userdata('employees');
        $this->session->unset_userdata('events');
        $this->session->unset_userdata('food_and_beverage');
        $this->session->unset_userdata('giftcards');
        $this->session->unset_userdata('item_kits');
        $this->session->unset_userdata('items');
        $this->session->unset_userdata('invoices');
        $this->session->unset_userdata('marketing_campaigns');
        $this->session->unset_userdata('promotions');
        $this->session->unset_userdata('receivings');
        $this->session->unset_userdata('reports');
        $this->session->unset_userdata('reservations');
        $this->session->unset_userdata('schedules');
        $this->session->unset_userdata('suppliers');
        $this->session->unset_userdata('teesheets');
		$this->session->unset_userdata('tournaments');

		$this->session->unset_userdata('table_id');
		$this->session->unset_userdata('table_number');
		$this->session->unset_userdata('suspended_sale_id');

		$this->session->unset_userdata('skipped_register_log');
		$this->session->unset_userdata('cash_register');
		//$this->session->unset_userdata('unfinished_register_log');
		// Force the session to update database immediately
		$this->session->sess_write();

		$register_log = $this->Sale->getUnfinishedRegisterLog()->row();
		$data = array('persist'=> 1);
		$this->Sale->save_register_log($data, $register_log->register_log_id);
		// OPEN LOGIN WINDOW
		$this->load->model('employee');
		$data = array (
			'employees'=>$this->employee->get_all(10000,0)->result_array()
		);
		$this->load->view('sales/login', $data);
	}
	function login_window()
	{
		// OPEN LOGIN WINDOW
		$data = array('controller'=>'sales');
		return $this->load->view('food_and_beverage/login', $data, true);
	}
	function login()
	{
		// $pin_or_card = $this->input->post('pin_or_card');
		// $employee_info = $this->Employee->get_info_by_pin_or_card($pin_or_card);
		$person_id = $this->input->post('employee_id');
		$password = $this->input->post('password');
		if ($person_id != '')
		{
			$has_sales_permission = false;
			$allowed_modules = $this->Module->get_allowed_modules($person_id);
			foreach ($allowed_modules->result_array() as $allowed_module)
			{
				$has_sales_permission = ($has_sales_permission ? $has_sales_permission : $allowed_module['module_id'] == 'sales');
			}

			// If user doesn't have F&B permission, return error
			if (!$has_sales_permission)
			{
				echo json_encode(array('success'=>false, 'message'=>lang('food_and_beverage_no_permission')));
				return;
			}
			$employee_info = $this->Employee->get_info($person_id);
			$logged_in = $this->Employee->login($employee_info->username, $password);

			if ($logged_in)
			{
				$this->session->set_userdata('sales_logged_in', true);

				// Force the session to update database immediately
				// instead of waiting for page refresh as usual
				$this->session->sess_write();

				$register_log = $this->Sale->getUnfinishedRegisterLog();

				echo json_encode(
					array(
						'employee' => $employee_info,
						//'recent_transactions' => $this->Table->get_recent_transactions($employee_info->person_id, 500),
						'success' => true,
						'register_log' => $register_log->row_array()
					)
				);

			}else{
				echo json_encode(array('success'=>false, 'message'=>lang('food_and_beverage_error_logging_in')));
			}
			return;
		}

		echo json_encode(array('success'=>false, 'message'=>lang('food_and_beverage_invalid_pin_or_card')));
	}

	function create_credit_card_payment()
    {
        $amount = $this->input->get('amount') ? $this->input->get('amount') : $this->input->post('amount');
        $guid = $this->input->get('guid') ? $this->input->get('guid') : $this->input->post('guid');
        $course_id = $this->input->get('course_id') ? $this->input->get('course_id') : $this->session->userdata('course_id');

		if ($amount == '' || $amount <= 0)
		{
			echo json_encode(array('status'=>'failed', 'message'=>'Invalid payment amount'));
			return;
		}
		if ($guid == '')
		{
			echo json_encode(array('status'=>'failed', 'message'=>'GUID required'));
			return;
		}

        if ($course_id)
        {
            if ($course_id == 'test')
            {
                $mercury_id = config_item('test_mercury_id');
                $mercury_password = config_item('test_mercury_password');
	        }
			else if ($course_id == 'test_ets')
            {
                $ets_key = config_item('test_ets_key');
	        }
            else
            {
                $mercury_id = $this->config->item('mercury_id');
                $mercury_password = $this->config->item('mercury_password');
				$ets_key = $this->config->item('ets_key');
            }
        }
        else
        {
            $mercury_id = config_item('foreup_mercury_id');
            $mercury_password = config_item('foreup_mercury_password');
        }
		if ($ets_key)
		{
			$this->load->library('Hosted_payments');
			$payment = new Hosted_payments();
			$payment->initialize($ets_key);
			$store_Primary = $this->Sale->add_credit_card_payment(array('tran_type'=>'Sale','frequency'=>'OneTime'));;
			$session = $payment->set('action', 'session')
			  		   ->set('amount', $amount)
					   ->set('store.Primary', $store_Primary)
					   //->set("accountID", "902072cf-eada-45b2-96c3-a9e059ef84d7")
					   //->set('isSave', 'true')
					   //->set('successRedirectUrl', 'sales/ets_payment_made')
					   ->send();

			if ((string)$session->id)
	        {
	            $this->session->set_userdata('ets_session_id', (string)$session->id);
				$this->session->set_userdata('ets_session', $session);
				$person_id = $this->session->userdata('person_id');
				echo json_encode(
	                array(
	                    'status'=>'success',
	                    'payment_id'=>1,
	                    'url'=>base_url("index.php/api/payment/mobile_ets_pay_window?amount={$amount}&id={$session->id}&status={$session->status}&message={$session->message}&customers={$session->customers}&course_id={$course_id}&ets_key={$ets_key}&person_id={$person_id}"),
	                    'invoice_id'=>$store_Primary,
	                    'test_transaction'=>$ets_key == config_item('test_ets_key')?true:false
	                )
	            );
				return;
	        }
	        else {
	            echo json_encode(
	                array(
	                    'status'=>'failed',
	                    'payment_id'=>false
	                )
	            );
				return;
	        }
			//$data = array('session'=>$session);
			//$this->load->view('sales/ets', $data);
		}
		// USING MERCURY FOR PAYMENT PROCESSING
		else if ($mercury_id)
		{
	        $this->load->library('Hosted_checkout_2');

	        $HC = new Hosted_checkout_2();
	        $HC->set_merchant_credentials($mercury_id,$mercury_password);
	        $HC->set_response_urls('api/payment/success', 'api/payment/fail');
	        $initialize_results = $HC->initialize_payment($amount,'0.00','Sale','POS','OneTime', $guid);
	        if ((int)$initialize_results->ResponseCode == 0)
	        {
	            $invoice = $this->session->userdata('invoice_id');
	            echo json_encode(
	                array(
	                    'status'=>'success',
	                    'payment_id'=>(string)$initialize_results->PaymentID,
	                    'url'=>$HC->get_iframe_url('POS', (string)$initialize_results->PaymentID),
	                    'invoice_id'=>$invoice     ,
	                    'test_transaction'=>$mercury_id == config_item('test_mercury_id')?true:false
	                )
	            );
				return;
	        }
	        else {
	            echo json_encode(
	                array(
	                    'status'=>'failed',
	                    'payment_id'=>false
	                )
	            );
				return;
	        }
	    }
    }


    function confirm_payment()
    {
        $payment_id = $this->input->get('payment_id') ? $this->input->get('payment_id') : $this->input->post('payment_id');
        $invoice_id = $this->input->get('invoice_id') ? $this->input->get('invoice_id') : $this->input->post('invoice_id');//$this->session->userdata('invoice_id');
		$this->session->unset_userdata('invoice_id');
        $course_id = $this->input->get('course_id') ? $this->input->get('course_id') : $this->session->userdata('course_id');

		if (!$payment_id)
		{
			echo json_encode(array('status'=>'failed', 'message'=>'Payment_id required'));
			return;
		}
		if (!$invoice_id)
		{
			echo json_encode(array('status'=>'failed', 'message'=>'Invoice_id required, but not found in session, please call sales/pay and run a credit card'));
			return;
		}

        if ($course_id)
        {
            if ($course_id == 'test')
            {
                $mercury_id = config_item('test_mercury_id');
                $mercury_password = config_item('test_mercury_password');
	        }
            else if ($course_id == 'test_ets')
            {
                $ets_key = config_item('test_ets_key');
	        }
            else
            {
                $mercury_id = $this->config->item('mercury_id');
                $mercury_password = $this->config->item('mercury_password');
				$ets_key = $this->config->item('ets_key');
            }
        }
        else
        {
            $mercury_id = config_item('foreup_mercury_id');
            $mercury_password = config_item('foreup_mercury_password');
	    }

		if ($ets_key)
		{
			// GET PAYMENT INFO FROM INVOICE ID
			$payment_data = $this->Sale->get_credit_card_payment($invoice_id);
			if ($payment_data[0]['status'] == 'success')
	        {
	            echo json_encode(
	                array(
	                    'status'=>'success',
	                    'card'=>$payment_data[0]['card_type'].' '.str_replace('x', '', $payment_data[0]['masked_account'])
	                )
	            );
	        }
	        else
	        {
	            echo json_encode(
	                array(
	                    'status'=>'success',
	                    'payment_approved'=>false,
	                    'payment_data'=>$payment_data

	                )
	            );
	        }
		}
		else if ($mercury_id)
		{
	        $this->load->library('Hosted_checkout_2');
	        $HC = new Hosted_checkout_2();
	        $HC->set_merchant_credentials($mercury_id,$mercury_password);
	        $HC->set_payment_id($payment_id);
	        $verify_results = $HC->verify_payment();
	        $HC->complete_payment();

	        $payment_data = array (
	            'course_id'=>$course_id,
	            'mercury_id'=>$mercury_id,
	            'tran_type'=>(string)$verify_results->TranType,
	            'amount'=>(string)$verify_results->Amount,
	            'auth_amount'=>(string)$verify_results->AuthAmount,
	            'card_type'=>(string)$verify_results->CardType,
	            'frequency'=>'OneTime',
	            'masked_account'=>(string)$verify_results->MaskedAccount,
	            'cardholder_name'=>(string)$verify_results->CardholderName,
	            'ref_no'=>(string)$verify_results->RefNo,
	            'operator_id'=>(string)$verify_results->OperatorID,
	            'terminal_name'=>(string)$verify_results->TerminalName,
	            'trans_post_time'=>(string)$verify_results->TransPostTime,
	            'auth_code'=>(string)$verify_results->AuthCode,
	            'voice_auth_code'=>(string)$verify_results->VoiceAuthCode,
	            'payment_id'=>$payment_id,
	            'acq_ref_data'=>(string)$verify_results->AcqRefData,
	            'process_data'=>(string)$verify_results->ProcessData,
	            'token'=>(string)$verify_results->Token,
	            'response_code'=>(int)$verify_results->ResponseCode,
	            'status'=>(string)$verify_results->Status,
	            'status_message'=>(string)$verify_results->StatusMessage,
	            'display_message'=>(string)$verify_results->DisplayMessage,
	            'avs_result'=>(string)$verify_results->AvsResult,
	            'cvv_result'=>(string)$verify_results->CvvResult,
	            'tax_amount'=>(string)$verify_results->TaxAmount,
	            'avs_address'=>(string)$verify_results->AVSAddress,
	            'avs_zip'=>(string)$verify_results->AVSZip,
	            'payment_id_expired'=>(string)$verify_results->PaymendIDExpired,
	            'customer_code'=>(string)$verify_results->CustomerCode,
	            'memo'=>(string)$verify_results->Memo
	        );
	        $this->sale->update_credit_card_payment($invoice_id, $payment_data);

	        log_message('error', $this->db->last_query());
	        if ($payment_data['status'] == 'Approved')
	        {
	            echo json_encode(
	                array(
	                    'status'=>'success',
	                    'card'=>$payment_data['card_type'].' '.str_replace('x', '', $payment_data['masked_account'])
	                )
	            );
	        }
	        else
	        {
	            echo json_encode(
	                array(
	                    'status'=>'success',
	                    'payment_approved'=>false
	                )
	            );
	        }
	    }
    }
	function item_search($limit = 100)
	{
		$suggestions = $this->Item->get_item_search_suggestions($this->input->get('term'),$limit);
		$suggestions = array_merge($suggestions, $this->Item_kit->get_item_kit_search_suggestions($this->input->get('term'),$limit));
		$suggestions = array_merge($suggestions, $this->Tournament->get_search_suggestions($this->input->get('term'),$limit));
		echo json_encode($suggestions);
	}
	function employee_search()
	{
		$suggestions = $this->Employee->get_search_suggestions($this->input->get('term'),100, true);
		echo json_encode($suggestions);
	}
	function customer_search($type='')
	{
		$suggestions = $this->Customer->get_customer_search_suggestions($this->input->get('term'),100,$type);
		echo json_encode($suggestions);
	}

	function select_customer($customer_id = -1)
	{
		$message = false;
		if ($customer_id == -1)
			$customer_id = $this->input->post("customer");
		// Get customer id
		$cid = $this->Customer->get_id_from_account_number($customer_id);
		$customer_id = ($cid) ? $cid : $customer_id;
		$data = array();
		if ($this->Customer->exists($customer_id))
		{
			$this->sale_lib->set_customer($customer_id);
		}
		else
		{
			//$data['error']=lang('sales_unable_to_add_customer');
			$message = array('text'=>lang('sales_unable_to_add_customer'),'type'=>'error_message','persist'=>false);
		}
		$this->_reload($data, 'select_customer', $message);
	}
		/*
	Loads the customer edit form
	*/
	function view_customer($customer_id=-1)
	{
		$this->load->model('Image');
		$this->load->model('Customer_credit_card');
		$this->load->model('Household');
		$data['person_info']=$this->Customer->get_info($customer_id, $this->session->userdata('course_id'));
		$groups = $this->Customer->get_group_info($customer_id);
		$data['groups']=$groups;
		$passes = $this->Customer->get_pass_info($customer_id);
		$data['passes']=$passes;
		$data['open_billing'] = $open_billing;
		$data['image_thumb_url'] = $this->Image->get_thumb_url($data['person_info']->image_id, 'person');
		$data['price_classes']=($this->permissions->course_has_module('reservations')?$this->Fee->get_types():$this->Green_fee->get_types());
		$data['credit_cards']=$this->Customer_credit_card->get($customer_id);
		// LOAD HOUSEHOLD DATA
		$data['household_head'] = '';
		$data['household_members'] = '';
		if ($this->Household->is_head($customer_id))
			$data['household_members'] = $this->Household->get_members($customer_id);
		else if ($this->Household->is_member($customer_id))
			$data['household_head'] = $this->Household->get_head($customer_id);
		$data['controller'] = strtolower(get_class());
		//echo $this->db->last_query();
		$this->load->view("customers/form",$data);
	}
	/*
	Inserts/updates a customer
	*/
	function save_customer($customer_id=-1)
	{
		$person_data = array(
		'first_name'=>trim($this->input->post('first_name')),
		'last_name'=>trim($this->input->post('last_name')),
		'email'=>$this->input->post('email'),
		'phone_number'=>$this->input->post('phone_number'),
		'cell_phone_number'=>$this->input->post('cell_phone_number'),
		'birthday'=>$this->input->post('birthday'),
		'address_1'=>$this->input->post('address_1'),
		'address_2'=>$this->input->post('address_2'),
		'city'=>$this->input->post('city'),
		'state'=>$this->input->post('state'),
		'zip'=>$this->input->post('zip'),
		'country'=>$this->input->post('country'),
		'comments'=>$this->input->post('comments')
		);
		$customer_data=array(
		'company_name' => $this->input->post('company_name'),
		'account_number'=>$this->input->post('account_number')=='' ? null:$this->input->post('account_number'),
		'account_balance'=>$this->input->post('account_balance'),
		'member_account_balance'=>$this->input->post('member_account_balance'),
		'account_balance_allow_negative'=>$this->input->post('account_balance_allow_negative')==''?0:1,
		'member_account_balance_allow_negative'=>$this->input->post('member_account_balance_allow_negative')==''?0:1,
		'taxable'=>$this->input->post('taxable')=='' ? 1:0,
		'member'=>$this->input->post('member')==''?0:1,
		'use_loyalty'=>$this->input->post('use_loyalty')==''?0:1,
		'loyalty_points'=>$this->input->post('loyalty_points'),
		'price_class'=>$this->input->post('price_class')?$this->input->post('price_class'):'price_category_1',
		'opt_out_email'=>$this->input->post('email_unsubscribed')==''?0:1,
		'opt_out_text'=>$this->input->post('text_unsubscribed')==''?0:1
		);
		if (!$this->permissions->is_employee() && $this->input->post('username'))
		{
			$customer_data['username'] = $this->input->post('username');
		}

		//Password has been changed OR first time password set
		if($this->input->post('password')!='' && (!$this->permissions->is_employee())){
			$password = $this->input->post('password');
			$customer_data['password'] = md5($password);
		}


		if ($customer_id == -1)
			$customer_data['course_id']=$this->session->userdata('course_id');
		$groups_data = $this->input->post("groups")!=false ? $this->input->post("groups"):'delete';
		$passes_data = $this->input->post("passes")!=false ? $this->input->post("passes"):'delete';
		$employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
		$cur_customer_info = $this->Customer->get_info($customer_id);
		$giftcard_data = array();
		if($this->Customer->save($person_data,$customer_data,$customer_id, $giftcard_data, $groups_data, $passes_data))
		{
			//send username and password via email
			if (!empty($customer_data['password'])) {
				$this->Customer->send_username_password_email($person_data, $customer_data, $password);
			}

			if ($this->config->item('mailchimp_api_key'))
			{
				$this->Person->update_mailchimp_subscriptions($this->input->post('email'), $this->input->post('first_name'), $this->input->post('last_name'), $this->input->post('mailing_lists'));
			}
			//New customer
			if($customer_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>lang('customers_successful_adding').' '.
				$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>$customer_data['person_id']));
				$customer_id = $customer_data['person_id'];
			}
			else //previous customer
			{
				echo json_encode(array('success'=>true,'message'=>lang('customers_successful_updating').' '.
				$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>$customer_id));
			}
			// SAVE HOUSEHOLD INFORMATION IF PRESENT
			$household_members = $this->input->post('household_member_id');
			if ($household_members && count($household_members) > 0)
			{
				$this->load->model("Household");
				$this->Household->save($household_members, $customer_id);
			}

			// UPDATE ACCOUNT BALANCE IF A DIFFERENT AMOUNT IS ENTERED IN
			if ($cur_customer_info->account_balance != $customer_data['account_balance'])
			{
				$account_transaction_data=array(
					'course_id' =>$this->session->userdata('course_id'),
					'trans_customer'=>$customer_id,
					'trans_date'=>date('Y-m-d H:i:s'),
					'trans_user'=>$employee_id,
					'trans_comment'=>lang('items_manually_editing_of_quantity'),
					'trans_description'=>'Customer Edit',
					'trans_amount'=>$cur_customer_info ? (float)$this->input->post('account_balance') - (float)$cur_customer_info->account_balance : $this->input->post('account_balance')
				);
				$this->Account_transactions->insert('customer', $account_transaction_data);
			}
			// UPDATE MEMBER ACCOUNT BALANCE IF A DIFFERENT AMOUNT IS ENTERED IN
			if ($cur_customer_info->member_account_balance != $customer_data['member_account_balance'])
			{
				$m_account_transaction_data=array(
					'course_id' =>$this->session->userdata('course_id'),
					'trans_customer'=>$customer_id,
					'trans_date'=>date('Y-m-d H:i:s'),
					'trans_user'=>$employee_id,
					'trans_comment'=>lang('items_manually_editing_of_quantity'),
					'trans_description'=>'Customer Edit',
					'trans_amount'=>$cur_customer_info ? (float)$this->input->post('member_account_balance') - (float)$cur_customer_info->member_account_balance : $this->input->post('member_account_balance')
				);
				$this->Account_transactions->insert('member', $m_account_transaction_data);
			}
			// UPDATE LOYALTY BALANCE IF A DIFFERENT AMOUNT IS ENTERED IN
			if ($cur_customer_info->loyalty_points != $customer_data['loyalty_points'])
			{
				$loyalty_transaction_data=array(
					'trans_customer'=>$customer_id,
					'trans_date'=>date('Y-m-d H:i:s'),
					'trans_user'=>$employee_id,
					'trans_comment'=>lang('items_manually_editing_of_quantity'),
					'trans_description'=>'Customer Edit',
					'trans_amount'=>$cur_customer_info ? (float)$this->input->post('loyalty_points') - (float)$cur_customer_info->loyalty_points : $this->input->post('loyalty_points')
				);
				$this->Customer_loyalty->insert($loyalty_transaction_data);
			}
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('customers_error_adding_updating').' '.
			$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>-1));
		}
	}
	function remove_customer($customer_id = -1)
	{
		$this->sale_lib->delete_customer();
		$this->sale_lib->delete_customer_quickbutton($customer_id);
		$this->_reload(null, 'remove_customer');
	}
	function change_taxable()
	{
		$taxable = $this->input->post("taxable");
		$this->sale_lib->set_taxable($taxable);

        echo json_encode($this->sale_lib->get_basket_info());
	}
	function change_mode($mode = 'sale')
	{
		if ($mode == 'sale') {
			$mode = 'return';
		} else {
			$mode = 'sale';
		}
		$this->sale_lib->set_mode($mode);
		$this->_reload($data, "change_mode");
	}

	function set_comment()
	{
 	  $this->sale_lib->set_comment($this->input->post('comment'));
	}

	function set_email_receipt()
	{
 	  $this->sale_lib->set_email_receipt($this->input->post('email_receipt'));
	}
	function deduct_coupon_amount($message = '', $payment_amount, $coupon_number)
    {
        $ajax = 'add_item';            
        $data = array();
        $valid_amount = true;
        $payment_type = 'Coupon';
        $this->form_validation->set_rules('amount_tendered', 'lang:sales_amount_tendered', 'required');
        $current_payments = $this->sale_lib->get_payments();
        
        if (isset($current_payments['Coupon']))
        {                
            unset($current_payments['Coupon']);
            $this->sale_lib->set_payments($current_payments);
        }
            if( !$this->sale_lib->add_payment( $payment_type, $payment_amount, $valid_amount, $coupon_number))
            {                   
                $message = array('text'=>lang('sales_unable_to_add_payment'),'type'=>'error_message','persist'=>false);	
            }
	
		$this->_reload($data, $ajax, $message);
    }
	
	//Alain Multiple Payments
	function add_payment($type = false, $amount = false, $full_transaction = false, $invoice_id = false)
	{
		log_debug_message($this->config->item('course_id'), 'add payment, type: '.$this->input->post('payment_type').', amount: '.$this->input->post('amount_tendered'), $this->session->userdata('tracker_sale_id'));
			
		$message = false;
		$valid_amount = false;
		$ajax = 'add_payment';
		$data=array();
        $punch_card_used = array();
		$this->form_validation->set_rules('amount_tendered', 'lang:sales_amount_tendered', 'required');

		$tendered_amt = $this->input->post('amount_tendered');
		//echo "'".$tendered_amt."'";
		if (strlen($tendered_amt) > 9)
		{
			//echo 'trying to add credit card';
			//echo 'failed';
			$message = array('text'=>lang('sales_press_credit_card_button'),'type'=>'error_message','persist'=>false);
			$this->_reload($data, $ajax, $message);
			return;
		}
		if (!$full_transaction && $this->form_validation->run() == FALSE)
		{
			if ( $this->input->post('payment_type') == lang('sales_gift_card') )
				//$data['error']=lang('sales_must_enter_numeric_giftcard');
				$message = array('text'=>lang('sales_must_enter_numberic_giftcard'),'type'=>'error_message','persist'=>false);
			else
				//$data['error']=lang('sales_must_enter_numeric');
				$message = array('text'=>lang('sales_must_enter_numeric'),'type'=>'error_message','persist'=>false);
			if ($full_transaction)
			{
				return;
			}
			else
				$this->_reload($data, $ajax, $message);
 			return;
		}

		$payment_type=$type?$type:$this->input->post('payment_type');
		if ( $payment_type == lang('sales_giftcard') )
		{
			$giftcard_number = $this->input->post('giftcard_number');
			$payment_gc_number = $this->input->post('payment_gc_number');
			$amount_tendered = $this->input->post('amount_tendered');
			$giftcard_id = $this->Giftcard->get_giftcard_id($giftcard_number);
			if($payment_gc_number != '----------------' && (!$this->Giftcard->exists($giftcard_id)  || $this->Giftcard->is_expired($giftcard_id)))
			{
				$message = array('text'=>lang('sales_giftcard_does_not_exist'),'type'=>'error_message','persist'=>false);
 				//$data['error']=lang('sales_giftcard_does_not_exist');
				if ($full_transaction)
				{
					return;
				}
				else
					$this->_reload($data, $ajax, $message);
				return;
			}
			if ($payment_gc_number != '----------------')
			{
				$giftcard_info = $this->Giftcard->get_info($giftcard_id);
				$valid_amount = $this->sale_lib->get_valid_amount($giftcard_info->department, $giftcard_info->category);
			}
			else {
				$valid_amount = array('valid_amount'=>$amount_tendered);
			}
			$payments = $this->sale_lib->get_payments(0);
			$payment_type = $payment_type.':'.$giftcard_number;
			$current_payments_with_giftcard = isset($payments[$payment_type]) ? $payments[$payment_type]['payment_amount'] : 0;
			if ($payment_gc_number != '----------------')
			{
				$giftcard_value = $this->Giftcard->get_giftcard_value( $giftcard_number );
				$cur_giftcard_value =  $giftcard_value - $current_payments_with_giftcard;
			}
			else {
				$giftcard_value = $amount_tendered;
				$cur_giftcard_value = $amount_tendered;
			}
			if ( $cur_giftcard_value <= 0 && $amount_tendered > 0)
			{
				//$data['error']=lang('sales_giftcard_balance_is').' '.to_currency( $giftcard_value ).' !';
				$message = array('text'=>lang('sales_giftcard_balance_is').' '.to_currency($giftcard_value),'type'=>'error_message','persist'=>false);
				if ($full_transaction)
				{
					return;
				}
				else
					$this->_reload($data, $ajax, $message);
				return;
			}
			elseif ( ( $cur_giftcard_value - $amount_tendered ) >= 0 )
			{
				//$data['warning']=lang('sales_giftcard_balance_is').' '.to_currency( $cur_giftcard_value - $amount_tendered ).' !';
				$message = array('text'=>lang('sales_giftcard_balance_is').' '.to_currency($cur_giftcard_value),'type'=>'warning_message','persist'=>false);
 			}
			$payment_amount=min(min( $amount_tendered, $giftcard_value ), $valid_amount['valid_amount']);
		}
		else if ( $payment_type == lang('sales_punch_card') )
		{
			$punch_card_number = $this->input->post('punch_card_number');
			//$amount_tendered = $this->input->post('amount_tendered');
			$punch_card_id = $this->Punch_card->get_punch_card_id($punch_card_number);
			if($this->Punch_card->is_expired($punch_card_id))
			{
				$message = array('text'=>lang('sales_punch_card_does_not_exist'),'type'=>'error_message','persist'=>false);
 				//$data['error']=lang('sales_giftcard_does_not_exist');
				if ($full_transaction)
				{
					return;
				}
				else
					$this->_reload($data, $ajax, $message);
				return;
			}
			$punch_card_info = $this->Punch_card->get_info($punch_card_id);

			//$valid_amount = $this->sale_lib->get_valid_amount();
			$payments = $this->sale_lib->get_payments(0);
			$payment_type=$payment_type.':'.$punch_card_number;
			$current_payments_with_punch_card = isset($payments[$payment_type]) ? $payments[$payment_type]['payment_amount'] : 0;
			$punch_card_value = $this->Punch_card->get_punch_card_value($punch_card_id, true);
			if(!$punch_card_value['success'])
			{
				$message = array('text'=>$punch_card_value['message'],'type'=>'error_message','persist'=>false);
 				//$data['error']=lang('sales_giftcard_does_not_exist');
				if ($full_transaction)
				{
					return;
				}
				else
					$this->_reload($data, $ajax, $message);
				return;
			}
			$payment_amount = $punch_card_value['value'];
            $punch_card_used = $this->sale_lib->get_punch_card_items();
            $punch_card_used[$punch_card_number] = $punch_card_value['item_id'];
            $this->sale_lib->set_punch_card_items($punch_card_used);
		}
		else if ( $payment_type == lang('sales_raincheck') )
		{
			$raincheck_number = $this->input->post('raincheck_number');
			if (strpos($raincheck_number, 'RID') === false)
				$raincheck_number = "RID ".$raincheck_number;
			$message = '';
			if ($this->sale_lib->raincheck_is_used_or_expired($raincheck_number))
				//$data['error'] = 'Raincheck expired or used';
				$message = array('text'=>'Raincheck expired or used','type'=>'error_message','persist'=>false);
 			else
				$this->sale_lib->apply_raincheck($raincheck_number);
			$this->_reload($data, $ajax, $message);
			return;
		}
		else
		{
			$payment_amount=$amount?$amount:$this->input->post('amount_tendered');
		}
		if( !$this->sale_lib->add_payment( $payment_type, $payment_amount, $valid_amount, false, $invoice_id) )
		{
			//$data['error']=lang('sales_unable_to_add_payment');
			$message = array('text'=>lang('sales_unable_to_add_payment'),'type'=>'error_message','persist'=>false);
 		}
		else
        {
            $current_payments = $this->sale_lib->get_payments();
    
            if (isset($current_payments['Coupon']))
            {
                $this->sale_lib->void_coupon($current_payments['Coupon']['coupon_number']);
            }
        }
		
		if ($full_transaction)
		{
			return;
		}
		else
		{
			$this->_reload($data, $ajax, $message);
		}
	}

	function load_return ($sale_id)
	{
		$this->sale_lib->set_mode('return');
		//echo 'trying to redirect';
		redirect("sales/add/POS {$sale_id}", 'location');
		//header("Location:sales/add/POS {$sale_id}");
		//die('should have redirected by now');
	}
	function confirm_delete_payment($payment_id)
	{
		$payment_id = urldecode(urldecode($payment_id));
		$payments = $this->sale_lib->get_payments(0);
		$data = array(
			'payment_id'=>$payment_id,
			'payment'=>$payments[$payment_id]
		);

		$this->load->view('sales/confirm_delete_payment', $data);
	}
    //Alain Multiple Payments
	function delete_payment($payment_id)
	{
		$payment_id = ($this->input->post('payment_id')) ? $this->input->post('payment_id') : $payment_id;
		//echo urldecode($payment_id);
		//return;
		$this->sale_lib->delete_payment((($payment_id)));
		$this->_reload(array(), 'delete_payment');
	}
	function test_ets($total_amount)
	{
		$this->load->library('Hosted_payments');
		$payment = new Hosted_payments();
		$payment->initialize($this->config->item('ets_key'));
		$store_Primary = $this->Sale->add_credit_card_payment(array('tran_type'=>'Sale','frequency'=>'OneTime'));;

		// $session = $payment->set('action', 'payment')
			// ->set('amount', 21)
			// ->set('store.Primary', $store_Primary)
			// ->set("accountID", "902072cf-eada-45b2-96c3-a9e059ef84d7") ->send();
			// $session = $payment->set('action', 'void')
				// ->set('sessionID', "f4931e3b-ce6c-4d32-be01-bc9c1acd0a4d")
				// ->set("transactionID", "247d9474-9008-4a32-aafc-ec9fa957aadc")
				// ->send();
				// $session = $payment->set('action', 'refund')
					// ->set('transactionID', 'ecff32db-b117-4f00-b57c-5ce1a2e6ef16')
					// ->send();
				$session = $payment->set('action', 'addTip')
					->set('transactionID', '3427332e-c339-45e8-aef9-8f6efd03aa3a')
					->set('amount', '6.00') ->send();
		// $session = $payment->set('action', 'session') ->set('isSave', "true")
			// ->send();
//		$session = $payment->set('action', 'void') ->set('sessionID', "45cd9edc-8412-4fa2-9466-1fe49ffd69ab")
	//		//->set('store.Primary', 1003)
		//	->set("transactionID", "1c71f66a-599d-4182-a578-b31966adda92") ->send();
		print_r($session);
		return;
		if ($session->id)
		{
			$user_message = $previous_card_declined!='false'?'Card declined, please try another.':'';
			$return_code = '';
			$this->session->set_userdata('ets_session_id', (string)$session->id);
			//$url = $HC->get_iframe_url('POS', (string)$initialize_results->PaymentID);
			$data = array('user_message'=>$user_message, 'return_code'=>$return_code, 'session'=>$session, 'url'=>site_url('sales/ets_payment_made'));
			$this->load->view('sales/ets', $data);
		}
		else
		{
			$data = array('processor' => 'ETS');
			$this->load->view('sales/cant_load', $data);
		}
	}

	function credit_card_refund_window(){
		$amount = $this->input->post('amount');
		$sale_id = $this->input->post('sale_id');
		$customer_id = $this->input->post('customer_id');

		$this->session->set_userdata('partial_return_sale_id', (int) $sale_id);
		$this->session->set_userdata('partial_return_customer_id', (int) $customer_id);
		$response_url = site_url('sales/partial_return/creditcard');

		// If course is using ETS
		if ($this->config->item('ets_key'))
		{
			$this->load->library('Hosted_payments');
			$payment = new Hosted_payments();
			$payment->initialize($this->config->item('ets_key'));
			$store_Primary = $this->Sale->add_credit_card_payment(array('tran_type'=>'Return','frequency'=>'OneTime'));

			$session = $payment->set('action', 'session')
			  		   ->set('amount', $amount)
					   ->set('store.Primary', $store_Primary)
					   ->set('posAction', 'refund')
					   ->send();

			if ($session->id)
			{
				$user_message = $previous_card_declined != 'false' ? 'Card declined, please try another.':'';
				$return_code = '';
				$this->session->set_userdata('ets_session_id', (string)$session->id);
				$data = array('user_message'=>$user_message, 'return_code'=>$return_code, 'session'=>$session, 'url'=>$response_url, 'amount'=>$amount);
				$this->load->view('sales/ets', $data);
			}
			else
			{
				$data = array('processor' => 'ETS');
				$this->load->view('sales/cant_load', $data);
			}
		}

		// If course is using Mercury
		else if ($this->config->item('mercury_id'))
		{
			$this->load->library('Hosted_checkout_2');
			$HC = new Hosted_checkout_2();

			$HC->set_merchant_credentials($this->config->item('mercury_id'), $this->config->item('mercury_password'));
			$HC->set_response_urls('sales/partial_return/creditcard', 'sales/process_cancelled');
			$initialize_results = $HC->initialize_payment($amount, 0, 'Return', '', 'OneTime');

			if ((int)$initialize_results->ResponseCode == 0)
			{
				$return_code = (int) $initialize_results->ResponseCode;
				$this->session->set_userdata('partial_return_payment_id', (string) $initialize_results->PaymentID);

				$url = $HC->get_iframe_url('POS', (string) $initialize_results->PaymentID);
				$data = array('user_message'=>$user_message, 'return_code'=>$return_code, 'url'=>$url);
				$this->load->view('sales/hc_pos_iframe.php', $data);
			}
		}
	}

    function open_payment_window($window = 'POS', $tran_type = 'Sale', $frequency = 'OneTime', $total_amount='1.00',$tax_amount='0.00', $previous_card_declined = 'false', $type = 'credit_card') {
		$cancelled = false;

		// USING ETS FOR PAYMENT PROCESSING
		if ($this->config->item('ets_key'))
		{
			$this->load->library('Hosted_payments');
			$payment = new Hosted_payments();
			$payment->initialize($this->config->item('ets_key'));
			$store_Primary = $this->Sale->add_credit_card_payment(array('tran_type'=>'Sale','frequency'=>'OneTime'));
			if ($type == 'credit_card')
			{
				if($tran_type == 'Return'){
					$session = $payment->set('action', 'session')
							   ->set('amount', abs($total_amount))
							   ->set('posAction', 'refund')
							   ->set('store.Primary', $store_Primary)
							   ->set('PaymentMethods', "ACH,CreditCard")
							   ->send();
				}else{
					$session = $payment->set('action', 'session')
							   ->set('amount', $total_amount)
							   ->set('store.Primary', $store_Primary)
							   ->set('PaymentMethods', "ACH,CreditCard")
							   ->send();
				}
			}
			else if ($type == 'giftcard')
			{
				if ($tran_type == 'Return' || $tran_type == 'Cancel')
				{
					if($tran_type == 'Cancel'){
						$cancelled = true;
					}
					$session = $payment->set('action', 'session')
				  		   ->set('posAction', 'refund')
						   ->set('amount',$total_amount)
						   ->set('store.Primary', $store_Primary)
						   ->set('PaymentMethods', "GiftCard")
						   ->send();
				}
				else {
					$session = $payment->set('action', 'session')
				  		   ->set('amount', $total_amount)
						   ->set('store.Primary', $store_Primary)
						   ->set('PaymentMethods', "GiftCard")
						   ->send();
				}
			}

			if ($session->id)
			{
				$user_message = $previous_card_declined!='false'?'Card declined, please try another.':'';
				$return_code = '';
				$this->session->set_userdata('ets_session_id', (string)$session->id);
				$data = array('user_message'=>$user_message, 'tran_type'=>$tran_type, 'return_code'=>$return_code, 'session'=>$session, 'url'=>site_url('sales/ets_payment_made').'/0/'.(int) $cancelled, 'amount'=>$total_amount);
				$data['type'] = $type;
				$this->load->view('sales/ets', $data);
			}
			else
			{
				$data = array('processor' => 'ETS');
				$this->load->view('sales/cant_load', $data);
			}
		}
		// USING MERCURY FOR PAYMENT PROCESSING
		else if ($this->config->item('mercury_id'))
		{
			$this->load->library('Hosted_checkout_2');
			$HC = new Hosted_checkout_2();

			$HC->set_merchant_credentials($this->config->item('mercury_id'),$this->config->item('mercury_password'));//ForeUP's Credentials
			$HC->set_response_urls('sales/payment_made', 'sales/process_cancelled');//Response URLs
			$initialize_results = $HC->initialize_payment($total_amount, $tax_amount, $tran_type, $window, $frequency);

			if ((int)$initialize_results->ResponseCode == 0)
			{
				//Set invoice number to save in the database
				//$invoice = $this->sale->add_credit_card_payment(array('tran_type'=>'Sale','frequency'=>$frequency));
				//$this->session->set_userdata('invoice', $invoice);

				$user_message = $previous_card_declined!='false'?'Card declined, please try another.':'';
				$return_code = (int)$initialize_results->ResponseCode;
				$this->session->set_userdata('payment_id', (string)$initialize_results->PaymentID);
				$url = $HC->get_iframe_url('POS', (string)$initialize_results->PaymentID);
				$data = array('user_message'=>$user_message, 'return_code'=>$return_code, 'url'=>$url);
				$this->load->view('sales/hc_pos_iframe.php', $data);
			}
		}

	}

	function charge_ets_giftcard_window($line) {
		$this->load->view('sales/ets_giftcard');
	}

	// Opens the window to add credit to an ETS giftcard
	function credit_ets_giftcard_window($amount = 1, $line_number = false, $load_ets_window = false) {

		if(!$load_ets_window){
			$data['amount'] = $amount;
			$data['line'] = $line_number;
			$this->load->view('giftcards/ets_giftcard_amount', $data);
			return true;
		}

		$this->load->library('Hosted_payments');
		$payment = new Hosted_payments();
		$payment->initialize($this->config->item('ets_key'));

		// Open ETS session
		$session = $payment->set("action", "session")
			->set("posAction", "refund")
			->set("amount", $amount)
			->set("paymentMethods", "giftcard")
			->send();

		// If session opened successfully
		if($session->id)
		{
			$this->session->set_userdata('ets_session_id', (string) $session->id);
			$data = array(
				'session' => $session,
				'url' => site_url('sales/ets_giftcard_credit'),
				'amount' => $amount
			);

			$this->load->view('sales/ets_giftcard.php', $data);
		}
		// If session failed to open
		else
		{
			$data = array('processor' => 'ETS');
			$this->load->view('sales/cant_load', $data);
		}
	}

	// Receives response from ETS when creating or adding credit
	// to an ETS giftcard
	function ets_giftcard_credit(){
		$ets_response = json_decode($this->input->post('response'), true);

		if($ets_response['status'] == 'success'){
			echo "<script>
				window.parent.set_feedback('ETS giftcard credited successfully', 'success_message');
				window.parent.$.colorbox.close();
			</script>";
		}
	}

	function complete_sale_window() {
		$data['payments'] = $this->sale_lib->get_payments(0);
		$data['cart'] = $this->sale_lib->get_basket();
		$data['taxes']=$this->sale_lib->get_basket_taxes();
		$data['total']=$this->sale_lib->get_basket_total();
		$data['amount_change']=to_currency($this->sale_lib->get_basket_amount_due() * -1);

		$this->load->view('sales/complete_sale.php', $data);
	}
	function payment_made() {
		$this->load->library('Hosted_checkout_2');
		$HC = new Hosted_checkout_2();
		$HC->set_merchant_credentials($this->config->item('mercury_id'),$this->config->item('mercury_password'));//ForeUP's Credentials
		$payment_id = $this->session->userdata('payment_id');
		$this->session->unset_userdata('payment_id');
		$HC->set_payment_id($payment_id);
		$verify_results = $HC->verify_payment();
		$HC->complete_payment();
		$invoice = $this->session->userdata('invoice_id');
		log_debug_message($this->config->item('course_id'), 'mercury payment made: '.$invoice, $this->session->userdata('tracker_sale_id'));
		
		
		$credit_card_data = array(
			'course_id'=>$this->session->userdata('selected_course'),
			'token'=>(string)$verify_results->Token,
			'token_expiration'=>date('Y-m-d', strtotime('+2 years')),
			'card_type'=>(string)$verify_results->CardType,
			'masked_account'=>(string)$verify_results->MaskedAccount,
			'cardholder_name'=>(string)$verify_results->CardholderName
		);

		//Update credit card payment data
		$payment_data = array (
			'course_id'=>$this->session->userdata('course_id'),
			'mercury_id'=>$this->config->item('mercury_id'),
			'tran_type'=>(string)$verify_results->TranType,
			'amount'=>(string)$verify_results->Amount,
			'auth_amount'=>(string)$verify_results->AuthAmount,
			'card_type'=>(string)$verify_results->CardType,
			'frequency'=>'OneTime',
			'masked_account'=>(string)$verify_results->MaskedAccount,
			'cardholder_name'=>(string)$verify_results->CardholderName,
			'ref_no'=>(string)$verify_results->RefNo,
			'operator_id'=>(string)$verify_results->OperatorID,
			'terminal_name'=>(string)$verify_results->TerminalName,
			'trans_post_time'=>(string)$verify_results->TransPostTime,
			'auth_code'=>(string)$verify_results->AuthCode,
			'voice_auth_code'=>(string)$verify_results->VoiceAuthCode,
			'payment_id'=>$payment_id,
			'acq_ref_data'=>(string)$verify_results->AcqRefData,
			'process_data'=>(string)$verify_results->ProcessData,
			'token'=>(string)$verify_results->Token,
			'response_code'=>(int)$verify_results->ResponseCode,
			'status'=>(string)$verify_results->Status,
			'status_message'=>(string)$verify_results->StatusMessage,
			'display_message'=>(string)$verify_results->DisplayMessage,
			'avs_result'=>(string)$verify_results->AvsResult,
			'cvv_result'=>(string)$verify_results->CvvResult,
			'tax_amount'=>(string)$verify_results->TaxAmount,
			'avs_address'=>(string)$verify_results->AVSAddress,
			'avs_zip'=>(string)$verify_results->AVSZip,
			'payment_id_expired'=>(string)$verify_results->PaymendIDExpired,
			'customer_code'=>(string)$verify_results->CustomerCode,
			'memo'=>(string)$verify_results->Memo
		);
		$this->sale->update_credit_card_payment($invoice, $payment_data);
		$payment_data['payment_type'] = $payment_data['card_type'].' '.$payment_data['masked_account'];

		//Save receipt data for when page reloads
		$receipt_data .= "Card Type: {$payment_data['card_type']}\\n";
        $receipt_data .= "Card No.: {$payment_data['masked_account']}\\n";
        $receipt_data .= "Auth: {$payment_data['auth_code']}\\n\\n";
        $receipt_data .= "Total: $".number_format($payment_data['auth_amount'], 2)." \\n\\n\\n";
        $receipt_data .= 'I agree to pay the above amount according to the card issuer agreement.\n\n\n';
        $receipt_data .= 'X_____________________________________________\n';
        $receipt_data .= $payment_data['cardholder_name'];
		$this->session->set_userdata('receipt_data', $receipt_data);

		$approved = ($payment_data['response_code'] === 0 && $payment_data['status'] == "Approved") ? 1 : 0;
		log_debug_message($this->config->item('course_id'), $invoice.' charge approved '.($approved ? 'yes' : 'no'), $this->session->userdata('tracker_sale_id'));
		
		if ($payment_data['response_code'] === 0 && $payment_data['status'] == "Approved")
			$this->load->view('sales/payment_made.php', $payment_data);
		else
			$this->load->view('sales/payment_made.php', array('status'=>'declined'));
	}

	function ets_payment_made($mobile = false, $cancelled_giftcard_payment = false) {
		$this->load->model('sale');
		$response = $this->input->post('response');
		$ets_response = json_decode($response);

		$primary_Store = (string) $ets_response->store->primary;
		log_debug_message($this->config->item('course_id'), 'ets payment made '.$primary_Store, $this->session->userdata('tracker_sale_id'));
		
		// VERIFYING A POSTed TRANSACTION
		$this->load->library('Hosted_payments');
		$payment = new Hosted_payments();
		$payment->initialize($this->config->item('ets_key'));
		$session_id = $payment->get("session_id");

		$transaction_id = $ets_response->transactions->id;

		$payment->set("action", "verify")
			->set("sessionID", $ets_response->id)
			->set("transactionID", $transaction_id);

		$account_id = $ets_response->customers->id;
		if ($account_id)
			$payment->set('accountID', $account_id);

		$verify = $payment->send();
		$transaction_time = date('Y-m-d H:i:s', strtotime($ets_response->created));

		// Convert card type to match mercury card types
		if ((string)$ets_response->transactions->type == 'credit card') {
			$ets_card_type = $ets_response->transactions->cardType;
			$card_type = '';
			switch($ets_card_type){
				case 'MasterCard':
					$card_type = 'M/C';
				break;
				case 'Visa':
					$card_type = 'VISA';
				break;
				case 'Discover':
					$card_type = 'DCVR';
				break;
				case 'AmericanExpress':
					$card_type = 'AMEX';
				break;
				case 'Diners':
					$card_type = 'DINERS';
				break;
				case 'JCB':
					$card_type = 'JCB';
				break;
			}
			$masked_account = (string) $ets_response->transactions->cardNumber;
			$auth_code = (string) $ets_response->transactions->approvalCode;
		}
		else if ((string) $ets_response->transactions->type == 'gift card')
		{
			$card_type = 'Giftcard';
			$masked_account = (string) $verify->transactions->cardNumber;
			$auth_code = '';
		}
		else
		{
			$card_type = 'Bank Acct';
			$masked_account = (string) $verify->transactions->accountNumber;
			$auth_code = '';
		}

		$credit_card_data = array(
			'course_id' => $this->session->userdata('selected_course'),
			'card_type' => $card_type,
			'masked_account' => $masked_account,
			'cardholder_name' => ''
		);

		$amount = (float) $ets_response->transactions->amount;
		$tran_type = 'Sale';
		if((string) $ets_response->transactions->message == '00:CREDIT'){
			$amount = -(float) $ets_response->transactions->amount;
			$tran_type = 'Return';
		}

		//Update credit card payment data
		$trans_message = (string) $ets_response->transactions->message;
		$payment_data = array (
			'course_id' 		=> $this->session->userdata('course_id'),
			'masked_account' 	=> $masked_account,
			'trans_post_time' 	=> (string) $transaction_time,
			'ets_id' 			=> $this->config->item('ets_key'),
			'tran_type' 		=> $tran_type,
			'amount' 			=> $amount,
			'auth_amount'		=> $amount,
			'auth_code'			=> $auth_code,
			'card_type' 		=> $card_type,
			'frequency' 		=> 'OneTime',
			'payment_id' 		=> (string) $ets_response->transactions->id,
			'process_data'		=> (string) $ets_response->id,
			'status'			=> (string) $ets_response->transactions->status,
			'status_message'	=> (string) $ets_response->transactions->message,
			'display_message'	=> (string) $ets_response->message,
			'operator_id'		=> (string) $this->session->userdata('person_id')
		);

		//$invoice_id = $this->sale->add_credit_card_payment($payment_data);
		$primary_Store = (string) $ets_response->store->primary;
		
		$approved = $ets_response->status == "success" ? 1 : 0;
		log_debug_message($this->config->item('course_id'), $primary_Store.' ets approved: '.($approved ? 'yes' : 'no'), $this->session->userdata('tracker_sale_id'));

		$this->sale->update_credit_card_payment($primary_Store, $payment_data);
		$this->session->set_userdata('invoice_id', $primary_Store);
		$payment_data['payment_type'] = $payment_data['card_type'].' '.$payment_data['masked_account'];

		//Save receipt data for when page reloads
		$receipt_data .= "Card Type: {$credit_card_data['card_type']}\\n";
        $receipt_data .= "Card No.: {$credit_card_data['masked_account']}\\n";
        $receipt_data .= "Auth: {$payment_data['auth_code']}\\n\\n";
        $receipt_data .= "Total: $".number_format($amount, 2)." \\n\\n\\n";
        $receipt_data .= 'I agree to pay the above amount according to the card issuer agreement.\n\n\n';
        $receipt_data .= 'X_____________________________________________\n';
        $receipt_data .= $credit_card_data['cardholder_name'];
		$this->session->set_userdata('receipt_data', $receipt_data);
		$payment_data['cancelled_giftcard_payment'] = (bool) $cancelled_giftcard_payment;

		if($ets_response->status == "success"){
			$payment_data['mobile'] = $mobile;
			$this->load->view('sales/ets_payment_made', $payment_data);
		}else{
			$this->load->view('sales/ets_payment_made', array('status'=>'declined', 'mobile'=>$mobile));
		}
	}
	function process_cancelled() {
		$this->load->view('sales/payment_made.php', array('status'=>'cancelled'));
	}

	function token_transaction($type, $invoice, $purchase_amount = '0.00', $gratuity_amount = '0.00', $tax_amount = '0.00') {
		$this->load->library('Hosted_checkout');
		$HC = new Hosted_checkout();
		$HC->token_transaction($type, $invoice, $purchase_amount, $gratuity_amount, $tax_amount);
	}
//when an error occurs, a string is returned in place of a currency amount
    function get_numeric_discount($basket_items, $type, $total_price_in_basket, $coupon_data)
    {
        $numeric_discount = 0;
        $item_count = 0;
        
        $coupon_is_valid = false;
        $highest_bogo_price = 0;
        $bogo_prices = array();
        $quantity = array();
        foreach($basket_items as $basket_item)
        { 
            //log_message('error', 'CHECKING: ' . $basket_item[$type] . ' ' . $coupon_data[$type.'_type']);
            if ($basket_item[$type] === $coupon_data[$type.'_type'])
            {
                $item_count += $basket_item['quantity'];
                //log_message('error', 'ITEM ID: ' . intval($basket_item['item_id']) . ' ' . intval($coupon_data['item_id']));
                if ($coupon_data['bogo'] === 'discount')
                {
                    
                    if ($coupon_data['amount_type'] === '$' && $basket_item['base_price'] >= $coupon_data['amount'])
                    {
                        $numeric_discount = $coupon_data['amount'];
                        $coupon_is_valid = true;
                    }
                    else if ($coupon_data['amount_type'] === '%' && $numeric_discount < bcmul($$basket_item['base_price'], $coupon_data['amount'] / 100, 2))
                    {                                                   
                        $numeric_discount = bcmul($basket_item['base_price'], $coupon_data['amount'] / 100, 2);
                        $coupon_is_valid = true;
                    }
                    else
                    {
                        $error_message = 'There is an internal error';
                    }
                    if ($coupon_data['min_purchase'] == 0)
                    {
                        $coupon_is_valid = true;                                           
                    }
                    else if ($total_price_in_basket < $coupon_data['min_purchase'])
                    {
                        $coupon_is_valid = false;                                           
                        return $error_message = "This coupon requires a minimum purchase amount of {$coupon_data['min_purchase']}";                                           
                    }
                    else
                    {
                        $coupon_is_valid = true;                                             
                    }
                }
                else if ($coupon_data['bogo'] === 'bogo')
                {
                   $bogo_prices[] = $basket_item['base_price'];
                   $quantity[] = $basket_item['quantity']; 
                }
            }
        }
        
        if ($coupon_data['bogo'] === 'bogo' && $item_count >= (intval($coupon_data['get_quantity']) + intval($coupon_data['buy_quantity'])))
        {
            //log_message('error', 'PRICES' . $bogo_prices[1]);
            array_multisort($bogo_prices, $quantity);
            $total_items = 0;
            
            foreach ($quantity as $number_of_item)
            {
                $total_items += $number_of_item;
            }
                
            $count = 0;
            //log_message('error', "TOTAL ITEMS $total_items");
            //log_message('error', 'hi ' . $coupon_data['buy_quantity']);
            for ($i = count($bogo_prices) - 1; $i >= 0; $i--)
            {
                for ($j = $quantity[$i] - 1; $j >= 0; $j--)
                {
                    if ($count < $coupon_data['get_quantity'] + $coupon_data['buy_quantity'] && $count > $coupon_data['buy_quantity'] - 1)
                    {    
                        //log_message('error', "IN IF: COUNT: $count J: $j Quantity:" . $quantity[$i] . $bogo_prices[$i]);
                        $numeric_discount = bcadd($numeric_discount, $bogo_prices[$i], 2);
                    }
                    
                    $count++;
                }
            }
            
            if ($coupon_data['amount_type'] !== 'FREE') 
            {                                                                           
                $numeric_discount = bcmul(strval($numeric_discount), $coupon_data['amount'] / 100, 2);                                                
            }  
            $coupon_is_valid = true;
        }
        
        
        if (!$coupon_is_valid && $error_message === '')
        {                                      
            return $error_message = "This coupon requires the purchase of {$coupon_data['valid_for']}";                                  
        }
        
        if ($coupon_data['bogo'] === 'bogo' && $item_count < (intval($coupon_data['get_quantity']) + intval($coupon_data['buy_quantity'])))
        {
            
            return $error_message = "More {$coupon_data['valid_for']} items need to be placed in the basket for this coupon to take effect";
        }
        else if (!$coupon_is_valid)
        {
            return $error_message;
        }
        
        
        return $numeric_discount;
    }
    
	function add($item_number = '', $mode = '', $full_transaction = false)
	{
        if (!$this->session->userdata('tracker_sale_id'))
		{
			$guid = sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
		        // 32 bits for "time_low"
		        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
		        // 16 bits for "time_mid"
		        mt_rand( 0, 0xffff ),
		        // 16 bits for "time_hi_and_version",
		        // four most significant bits holds version number 4
		        mt_rand( 0, 0x0fff ) | 0x4000,
		        // 16 bits, 8 bits for "clk_seq_hi_res",
		        // 8 bits for "clk_seq_low",
		        // two most significant bits holds zero and one for variant DCE1.1
		        mt_rand( 0, 0x3fff ) | 0x8000,
		        // 48 bits for "node"
		        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
		    );
			$this->session->set_userdata('tracker_sale_id', $guid);
		}
		$coupon_is_valid = false;
        $numeric_discount = 0;
        $error_message = '';
        $coupon_array = array();
        $item_type = 4;
		$message = false;				
		$data=array();
        $coupon_id = '';
		log_debug_message($this->config->item('course_id'), 'add: '.$item_number, $this->session->userdata('tracker_sale_id'));
		$serial_number = null;
		
		if ($mode != '')
			$this->sale_lib->set_mode($mode);
        if ($item_number[0] === 'C' && $item_number[1] === 'P')
        {
            
           $item_type = $item_number[2];
           $coupon_id = substr($item_number, 3);
            
            $basket_items = $this->sale_lib->get_basket();
            
            
            $coupon_type_id = substr($coupon_id, 0, 10);
            $coupon_number_id = substr($coupon_id, 10);
            
            $coupon_type_id = intval($coupon_type_id);
            
            $this->load->model('promotion');
            $coupon_data = $this->promotion->get_coupon_data($coupon_type_id);
            
            
            if ($coupon_data['coupon_limit'] === 'one' && $this->promotion->is_coupon_redeemed($coupon_number_id, $coupon_type_id))
            {
                
                $error_message = 'This Coupon Has Already Been Redeemed';
                $item_type = 5;
                $coupon_is_valid = false;
            }
            if (!$this->promotion->is_valid_day_of_week($coupon_type_id))
            {
                $error_message = 'This Coupon Is Not Available Today. Try again on one of the listed days on the coupon.';
                $item_type = 5;
                $coupon_is_valid = false;
            }
            $total_price_in_basket = 0;
            foreach ($basket_items as $basket_item)
            {
                $total_price_in_basket += intval($basket_item['base_price']);
                
            }  
            
            switch ($item_type)
            {
                case (0):
                    $numeric_discount = $this->get_numeric_discount($basket_items, 'category', $total_price_in_basket, $coupon_data);
                    
                    if (!is_numeric($numeric_discount))
                    {
                        
                        $coupon_is_valid = false;
                        $error_message = $numeric_discount;
                    }
                    else
                    {
                        $coupon_is_valid = true;
                    }
                    //look for category
                    break;
                case (1):
                    $numeric_discount = $this->get_numeric_discount($basket_items, 'subcategory', $total_price_in_basket, $coupon_data);
                    if (!is_numeric($numeric_discount))
                    {
                        $coupon_is_valid = false;
                        $error_message = $numeric_discount;
                    }
                    else
                    {
                        $coupon_is_valid = true;
                    }
                    //look for subcategory
                    break;
                case (2):
                    $item_count = 0;
                    foreach($basket_items as $basket_item)
                    {                          
                        if (intval($basket_item['item_id']) == intval($coupon_data['item_id']))
                        {
                            
                            $item_count += $basket_item['quantity'];
                            log_message('error', 'ITEM ID: ' . intval($basket_item['item_id']) . ' ' . intval($coupon_data['item_id']));
                            if ($coupon_data['bogo'] === 'discount')
                            {
                                if ($coupon_data['amount_type'] === '$' && $basket_item['base_price'] >= $coupon_data['amount'])
                                {
                                    $numeric_discount = $coupon_data['amount'];
                                }
                                else if ($numeric_discount < bcmul(strval($basket_item['base_price']), $coupon_data['amount'] / 100, 2))
                                {                                                      
                                    $numeric_discount = bcmul(strval($basket_item['base_price']), $coupon_data['amount'] / 100, 2);
                                }
                                if ($coupon_data['min_purchase'] == 0)
                                {
                                    $coupon_is_valid = true;                                        
                                    
                                }
                                else if ($total_price_in_basket < $coupon_data['min_purchase'])
                                {
                                    $coupon_is_valid = false;
                                    $numeric_discount = 0;
                                    $error_message = "This coupon requires a minimum purchase amount of {$coupon_data['min_purchase']}";
                                    break;
                                }
                                else
                                {
                                    $coupon_is_valid = true;
                                    
                                }
                            }
                            else
                            {
                                if ($item_count == (intval($coupon_data['get_quantity']) + intval($coupon_data['buy_quantity'])))
                                {
                                    $coupon_is_valid = true;
                                    if ($coupon_data['amount_type'] === 'FREE')
                                    {                                               
                                        $numeric_discount = bcmul($basket_item['base_price'], $coupon_data['get_quantity'], 2);                                       
                                    }
                                    else 
                                    {
                                        $numeric_discount = bcmul($basket_item['base_price'], $coupon_data['get_quantity'], 2);                            
                                        $numeric_discount = bcmul(strval($numeric_discount), $coupon_data['amount'] / 100, 2);                                               
                                    }
                                    $coupon_is_valid = true;
                                    break;
                                }
                            }
                        }                          
                    }
                    if (!$coupon_is_valid && $error_message === '')
                    {                                      
                        $error_message = "This coupon requires the purchase of {$coupon_data['valid_for']}";                                  
                    }
                    if ($coupon_data['bogo'] === 'bogo' && $item_count != (intval($coupon_data['get_quantity']) + intval($coupon_data['buy_quantity'])))
                    {
                        $error_message = "More {$coupon_data['valid_for']} items need to be placed in the basket for this coupon to take effect";
                    }
                    break;
                case (3):
                    
                    $numeric_discount = $this->get_numeric_discount($basket_items, 'department', $total_price_in_basket, $coupon_data);
                    if (!is_numeric($numeric_discount))
                    {
                        $coupon_is_valid = false;
                        $error_message = $numeric_discount;
                    }
                    else
                    {
                        $coupon_is_valid = true;
                    }
                    
                    //if ($basket_items[])
                    //coupon is for a department
                    break;
                case (4):
                    $error_message = "This is not a valid coupon";
                    break;
            }
            
            if (!$coupon_is_valid)
            {               
                
                $message = array('text'=>"Sorry, $error_message",'type'=>'error_message','persist'=>false);
                
                $this->_reload($data, 'remove_item', $message);
            }
            else
            {
                 $this->deduct_coupon_amount($message, $numeric_discount, $coupon_number_id);
            }
                
                //log_message('error', 'NUMERIC AMOUNT: ' . $numeric_discount);
        }
        else
        {
			if ($item_number != 'account_balance' && $item_number != 'member_balance' && $item_number != 'invoice_balance')
				$item_number_array = explode('_', $item_number);
			if (count($item_number_array) > 1)
			{
				if($this->config->item('seasonal_pricing') == 1){
					$item_number = $item_number_array[0];
					$price_index = $item_number_array[1];
					$timeframe_id = $item_number_array[2];
					$teetime_type = $item_number_array[3];
					$serial_number = $timeframe_id;
				}else{
					$item_number = $item_number_array[0];
					$price_index = $item_number_array[1];
					$teetime_type = $item_number_array[2];
				}
			}
			else
			{
				$price_index = $this->input->post('price_index');
				$teetime_type = $this->input->post('teetime_type');
				$timeframe_id = $this->input->post('timeframe_id');
	
				$serial_number = $timeframe_id;
			}
			$mode = $this->sale_lib->get_mode();
			$invoice_info = array();
	
			if ($item_number != '')
				$item_id_or_number_or_item_kit_or_receipt = urldecode($item_number);
			else
				$item_id_or_number_or_item_kit_or_receipt = $this->input->post("item");
			$quantity = $mode=="sale" ? 1:-1;
	
			$price = (float) $this->input->post('price');
			if(empty($price)){
				$price = null;
			}
	
			if($item_id_or_number_or_item_kit_or_receipt == 'account_balance' || $item_id_or_number_or_item_kit_or_receipt == 'member_balance' || $item_id_or_number_or_item_kit_or_receipt == 'invoice_balance')
			{
				$iinikr = $item_id_or_number_or_item_kit_or_receipt;
				// GET CUSTOMER BALANCE
				$customer_id = $this->sale_lib->get_customer();
				$customer_info = $this->Customer->get_info($customer_id);
				// PULL  UP (ADD) "ACCOUNT BALANCE" ITEM OR CREATE IT
				$item_id = $this->Sale->get_balance_item($iinikr);
	
				$iinikr = $iinikr == 'member_balance' ? 'member_account_balance' : $iinikr;
				$balance = ($customer_info->$iinikr < 0 ? -$customer_info->$iinikr : 0);
				// ADD TO CART/BASKET
				$this->sale_lib->add_item($item_id,1,0,$balance);
				$this->sale_lib->add_item_to_basket($item_id,1,0,$balance);
			}
			else if($this->sale_lib->is_valid_receipt($item_id_or_number_or_item_kit_or_receipt))
			{
				$this->sale_lib->set_mode('return');
				$this->sale_lib->return_entire_sale($item_id_or_number_or_item_kit_or_receipt);
			}
			elseif ($this->sale_lib->is_valid_invoice($item_id_or_number_or_item_kit_or_receipt, $invoice_info))
			{
				if ($this->sale_lib->invoice_in_cart($item_id_or_number_or_item_kit_or_receipt, $invoice_info)){
					//$data['error'] = 'Invoice already added to cart';
					$message = array('text'=>'Invoice already added to cart','type'=>'error_message','persist'=>false);
	 			}else {
					$this->sale_lib->add_invoice($invoice_info, $quantity);
					$this->sale_lib->set_customer($invoice_info['person_id']);
				}
			}
			elseif($mode == 'sale' && $this->sale_lib->is_valid_raincheck($item_id_or_number_or_item_kit_or_receipt))
			{
				if ($this->sale_lib->raincheck_is_used_or_expired($item_id_or_number_or_item_kit_or_receipt))
					//$data['error'] = 'Raincheck expired or used';
					$message = array('text'=>'Raincheck expired or used','type'=>'error_message','persist'=>false);
	 			else
					$this->sale_lib->apply_raincheck($item_id_or_number_or_item_kit_or_receipt);
			}
			elseif($this->sale_lib->is_valid_tournament($item_id_or_number_or_item_kit_or_receipt))
			{
				if ($this->sale_lib->tournament_without_customer())
				{
					//$data['error'] = 'A customer must be selected before adding a tournament';
					$message = array('text'=>'A customer must be selected before adding a tournament','type'=>'error_message','persist'=>false);
	 			}
				else
				{
					$this->sale_lib->add_tournament($item_id_or_number_or_item_kit_or_receipt, $quantity);
					$this->sale_lib->add_tournament_to_basket($item_id_or_number_or_item_kit_or_receipt,$quantity);
				}
			}
			elseif($this->sale_lib->is_valid_item_kit($item_id_or_number_or_item_kit_or_receipt))
			{
				$this->sale_lib->add_item_kit($item_id_or_number_or_item_kit_or_receipt, $quantity);
				$this->sale_lib->add_item_kit_to_basket($item_id_or_number_or_item_kit_or_receipt,$quantity);
			}
			elseif(!$this->sale_lib->add_item($item_id_or_number_or_item_kit_or_receipt,$quantity,0,$price,null,$serial_number,$price_index,$teetime_type) ||
					!$this->sale_lib->add_item_to_basket($item_id_or_number_or_item_kit_or_receipt,$quantity,0,$price,null,$serial_number,$price_index,$teetime_type))
			{
				//$data['error']=lang('sales_unable_to_add_item');
				$message = array('text'=>lang('sales_item_does_not_exist_or_is_deleted'),'type'=>'error_message','persist'=>false);
	 		}
	
			if($this->sale_lib->is_unlimited($item_id_or_number_or_item_kit_or_receipt) != 1 && $this->sale_lib->out_of_stock($item_id_or_number_or_item_kit_or_receipt))
			{
				//$data['warning'] = lang('sales_quantity_less_than_zero');
				$message = array('text'=>lang('sales_quantity_less_than_zero'),'type'=>'error_message','persist'=>false);
	 		}
			if ($full_transaction)
			{
				return;
			}
			else
				$this->_reload($data, 'add_item', $message);
		}
	}

	function edit_item($line,$description = false,$serialnumber = false,$quantity=false,$discount=false,$price=false, $item_id=false, $full_transaction = false)
	{
		$message = false;
		$data= array();

		$this->form_validation->set_rules('price', 'lang:items_price', 'required|numeric');
		$this->form_validation->set_rules('quantity', 'lang:items_quantity', 'required|numeric');

        $description = $description !== false ? $description : $this->input->post("description");
        $serialnumber = $serialnumber !== false ? $serialnumber : $this->input->post("serialnumber");
		$price = $price !== false ? $price : $this->input->post("price");
        $item_id = $item_id !== false ? $item_id : $this->input->post('item_id');
		$quantity = $quantity !== false ? $quantity : $this->input->post("quantity");
		$discount = $discount !== false  ? $discount : $this->input->post("discount");


		if ($full_transaction || $this->form_validation->run() != FALSE)
		{
			$this->sale_lib->edit_item($line,$description,$serialnumber,$quantity,$discount,$price, $item_id);
			$this->sale_lib->edit_basket_item($line,$description,$serialnumber,$quantity,$discount,$price, $item_id);
		}
		else
		{
			//$data['error']=lang('sales_error_editing_item');
			$message = array('text'=>lang('sales_error_editing_item'),'type'=>'error_message','persist'=>false);
 		}

		if($this->sale_lib->is_unlimited($item_id) != 1 && $this->sale_lib->out_of_stock($this->sale_lib->get_item_id($line)))
		{
			//$data['warning'] = lang('sales_quantity_less_than_zero');
			$message = array('text'=>lang('sales_quantity_less_than_zero'),'type'=>'error_message','persist'=>false);
 		}


		if ($full_transaction)
		{
			return ;
		}
		else
			$this->_reload($data, false, $message);
	}

	function delete_item($item_number)
	{
		$this->sale_lib->delete_item($item_number);
		$this->sale_lib->delete_item_from_basket($item_number);
		$this->_reload(null, "remove_item");
	}

	function delete_customer()
	{
		$this->sale_lib->set_taxable('true');
		$this->sale_lib->delete_customer();
		$this->_reload();
	}

	function complete($guid = NULL, $sale_date = false, $employee_id = false, $terminal_id = false)
	{
		log_debug_message($this->config->item('course_id'), 'complete sale', $this->session->userdata('tracker_sale_id'));

		$this->benchmark->mark('start_gathering_data');
		// THIS PREVENTS SALES FROM HAPPENING TOO CLOSE TOGETHER
		// $last_sale_time = $this->session->userdata('last_sale_time');
		// if (time() - $last_sale_time < 5)
		// {
			// // WE'RE GOING TO RETURN THE DATA FOR THE LAST SALE OR JUST AN ERROR
			// $message = array('error'=>true,'text'=>lang('sales_second_sale_record_attempt'),'type'=>'error_message','persist'=>false);
			// echo json_encode($message);
// 			
			// return;
		// }
// 		
		// $this->session->set_userdata('last_sale_time', time());
		
		$message = false;
		$mode = $this->sale_lib->get_mode();
		$no_receipt = $this->input->post('no_receipt');
		$data['cart']=$this->sale_lib->get_basket();
		$data['subtotal']=$this->sale_lib->get_basket_subtotal();
		$data['taxes']=$this->sale_lib->get_basket_taxes();
		$data['total']=$this->sale_lib->get_basket_total();
		$data['receipt_title']=lang('sales_receipt');
		$data['transaction_time']= date(get_date_format().' '.get_time_format());
		$data['has_online_booking'] = $this->permissions->course_has_module('reservations') ? $this->schedule->has_online_booking($this->session->userdata('course_id')) : $this->teesheet->has_online_booking($this->session->userdata('course_id'));
		$data['website'] = $this->config->item('website');
		$customer_id=$this->sale_lib->get_customer();
		$teetime_id=$this->sale_lib->get_teetime();
		$employee_id= $employee_id ? $employee_id : $this->Employee->get_logged_in_employee_info()->person_id;
		$comment = $this->sale_lib->get_comment();
		$emp_info=$this->Employee->get_info($employee_id);
		$amount_change = $this->sale_lib->get_basket_amount_due();
		$data['amount_change']=to_currency($amount_change * -1);
		if ($amount_change != 0)
			$this->sale_lib->add_payment( 'Change issued', $amount_change);
		$data['payments']=$this->sale_lib->get_payments(0);
		$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;
		if($customer_id!=-1)
		{
			$cust_info=$this->Customer->get_info($customer_id);
			$data['customer']=$cust_info->last_name.', '.$cust_info->first_name;
		}
		$this->benchmark->mark('end_gathering_data');

		$missing_or_repeat = $this->sale_lib->missing_or_repeat_giftcard_number();
		$missing_or_repeat_punch_card = $this->sale_lib->missing_or_repeat_punch_card_number();
		$location = ($this->config->item('after_sale_load') == 1)?'sales':($this->permissions->course_has_module('reservations') ? 'reservations' : 'teesheets');

		if ($missing_or_repeat && $this->config->item('use_ets_giftcards') == 0)
		{
			$message = array('error'=>true,'text'=>lang('sales_missing_or_repeat_giftcard_number'),'type'=>'error_message','persist'=>false);
			echo json_encode($message);
			return;
		}
		else if ($missing_or_repeat_punch_card)
		{
			$message = array('error'=>true,'text'=>lang('sales_missing_or_repeat_punch_card_number'),'type'=>'error_message','persist'=>false);
			echo json_encode($message);
			return;
		}

		else
		{
			//Record purchased teetimes
			if($teetime_id!=-1){
				log_debug_message($this->config->item('course_id'), 'recording purchased tee time: '.$teetime_id, $this->session->userdata('tracker_sale_id'));
				$teetime_id = $this->permissions->course_has_module('reservations') ? $this->schedule->record_teetime_purchases($data['cart']) : $this->teesheet->record_teetime_purchases($data['cart']);
				log_debug_message($this->config->item('course_id'), '* done * recording purchased tee time '.$teetime_id, $this->session->userdata('tracker_sale_id'));

				$TTID = $this->sale_lib->get_teetime();
				$TTID = substr($TTID, 0, 20);
				$this->load->model('event');
				$this->event->person_pay($TTID, $customer_id, true);
				log_debug_message($this->config->item('course_id'), 'after person_pay '.$customer_id, $this->session->userdata('tracker_sale_id'));
			}

			//Record raincheck used
			if ($this->session->userdata('raincheck_id')){
				if ($this->Sale->raincheck_redeemed())
					$this->session->unset_userdata('raincheck_id');
			}

			//SAVE sale to database
			$sale_id_number = $this->Sale->save($data['cart'], $customer_id, $employee_id, $comment, $data['payments'], false, $teetime_id, false, $guid, $sale_date, '', $terminal_id);
			log_debug_message($this->config->item('course_id'), 'finished Sale->save. Sale ID '.$sale_id_number, $this->session->userdata('tracker_sale_id'));
			$this->session->unset_userdata('tracker_sale_id');
			
			$this->sale_lib->set_taxable('true');
			$data['sale_id']='POS '.$sale_id_number;

			if ($data['sale_id'] == 'POS -1')
			{
				$message = array('text'=>lang('sales_transaction_failed'),'type'=>'error_message','persist'=>false);
			}
			else
			{
				if ($this->sale_lib->get_email_receipt() && !empty($cust_info->email))
				{
					send_sendgrid($cust_info->email, lang('sales_receipt'), $this->load->view("sales/receipt_email",$data, true), $this->config->item('email'), $this->config->item('name'));
				}
			}

			if (!$this->config->item('print_after_sale'))
			{

				$sale_id = $this->sale_lib->get_suspended_sale_id();
				$this->Sale_suspended->delete($sale_id, true);

				if (!$no_receipt)
		        {   //redirect('sales/receipt/'.$sale_id_number);
		            echo json_encode(array('no_auto_receipt'=>true, 'location'=>'sales/receipt/'.$sale_id_number));
					return;
				}
		        $this->sale_lib->clear_all_minus_cart();

				if (count($this->sale_lib->get_cart())==0)
		        {
		            $this->sale_lib->set_teetime(-1);
					$this->session->unset_userdata('purchase_override');
		        	$this->session->unset_userdata('purchase_override_time');
		        }

		        if ($no_receipt){
		            if (count($this->sale_lib->get_cart())>0)
		            {
		                //redirect('sales', 'location');//    $this->load->view("sales/register");
			            echo json_encode(array('no_auto_receipt'=>true, 'location'=>'sales'));
						return;
					}
					else
		            {
		                //redirect($location, 'location');
					    echo json_encode(array('no_auto_receipt'=>true, 'location'=>$location));
						return;
					}

		        }
				return;
			}

		}

		$sale_id = $this->sale_lib->get_suspended_sale_id();
		$this->Sale_suspended->delete($sale_id);
		$this->sale_lib->clear_all_minus_cart();
        if (count($this->sale_lib->get_cart())==0)
		{
			$this->sale_lib->set_teetime(-1);
			$this->session->unset_userdata('purchase_override');
	        $this->session->unset_userdata('purchase_override_time');
		}
		if ($mode == 'return')
			$this->sale_lib->clear_all();


		$additional_receipt_data = $this->session->userdata('additional_receipt_data');
		if (isset($additional_receipt_data[$data['sale_id']]))
			$data = array_merge_recursive($data, $additional_receipt_data[$data['sale_id']]);

		$this->session->unset_userdata('punch_card_items');
		if ($guid !== NULL)
		{
			return array('sale_id' => $data['sale_id']);
		}
		else if (count($this->sale_lib->get_cart())>0){
			$this->_reload(array('receipt_data' => $data, 'sale_id' => $data['sale_id']), "completed_sale", $message);
		}else {
			$this->sale_lib->delete_customer_quickbuttons();
		    if ($location == 'sales')
	        	$this->_reload(array('receipt_data' => $data, 'sale_id' => $data['sale_id']), "completed_sale", $message);
	        else {
	        	echo json_encode(array('location'=>$location, 'receipt_data'=>$data, 'sale_id'=>$data['sale_id']));//redirect("teesheets", 'location');
	        }
        }

        //}
	}
	function set_quickbutton_tab($tab)
	{
		$this->session->set_userdata('quickbutton_tab', $tab);
	}
	function email_receipt($sale_id, $email = false)
	{
		$sent = false;
		//$sale_id = $this->Sale->get_sale_id($sale_number);
		if(!$email) {
			$email = $this->input->post('email_id');
		}
		$sale_info = $this->Sale->get_info($sale_id)->row_array();
		$this->sale_lib->copy_entire_sale($sale_id);
		$data['cart']=$this->sale_lib->get_cart();
		$data['payments']=$this->sale_lib->get_payments(0);
		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['taxes']=$this->sale_lib->get_taxes($sale_id);
		$data['total']=$this->sale_lib->get_total($sale_id);
		$data['receipt_title']=lang('sales_receipt');
		$data['transaction_time']= date(get_date_format().' '.get_time_format(), strtotime($sale_info['sale_time']));
		$data['replace_newline'] = true;
		$customer_id=$this->sale_lib->get_customer();
		$emp_info=$this->Employee->get_info($sale_info['employee_id']);
		$data['payment_type']=$sale_info['payment_type'];
		//$data['amount_change']=to_currency($this->sale_lib->get_amount_due($sale_id) * -1);
		$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;

		if($customer_id!=-1)
		{
			$cust_info=$this->Customer->get_info($customer_id);
			$data['customer']=$cust_info->first_name.' '.$cust_info->last_name.($cust_info->company_name==''  ? '' :' ('.$cust_info->company_name.')');
		}
		//$data['sale_number'] = 'POS '.$sale_info['sale_number'];
		$data['sale_id'] = 'POS '.$sale_id;
		if (!empty($cust_info->email) || !empty($email))
		{
			/*$this->load->library('email');
			//$config['mailtype'] = 'html';
			//$this->email->initialize($config);
			$this->email->from($this->config->item('email'), $this->config->item('name'));
			$this->email->to($cust_info->email);

			$this->email->subject(lang('sales_receipt'));
			$this->email->message($this->load->view("sales/receipt_email",$data, true));
			$this->email->send();*/
			$email = $email ? $email : $cust_info->email;
			send_sendgrid($email, lang('sales_receipt'), $this->load->view("sales/receipt_email",$data, true), $this->config->item('email'), $this->config->item('name'));
			$sent = true;
		}
		$this->sale_lib->clear_all();
		if ($sent) {
			echo json_encode(array('success' => $sent, 'message' => 'Email sent successfully'));
		} else {
			echo json_encode(array('success' => $sent, 'message' => 'Error while sending email'));
		}
	}

	function receipt($sale_id, $json=false)
	{
		$this->sale_lib->clear_all();
		//$sale_id = $this->Sale->get_sale_id($sale_number);
		$sale_info = $this->Sale->get_info($sale_id)->row_array();
		$this->sale_lib->copy_entire_sale($sale_id);
		$data['cart']=$this->sale_lib->get_cart();
		$data['payments']=$this->sale_lib->get_payments(0);
		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['taxes']=$this->sale_lib->get_taxes($sale_id);
		$data['total']=$this->sale_lib->get_total($sale_id);
		$data['receipt_title']=lang('sales_receipt');
		$data['transaction_time']= date(get_date_format().' '.get_time_format(), strtotime($sale_info['sale_time']));
		$customer_id=$this->sale_lib->get_customer();
		$emp_info=$this->Employee->get_info($sale_info['employee_id']);
		$data['payment_type']=$sale_info['payment_type'];
		$data['amount_change']=to_currency($this->sale_lib->get_amount_due($sale_id) * -1);
		$data['replace_newline'] = true;
		$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;

		// Add an extra tax line for invoice items that aren't part of inventory
		$total_tax = 0;
		foreach($data['taxes'] as $tax_name => $tax){
			$total_tax += $tax;
		}
		$leftover_tax = ($data['total'] - ($data['subtotal'] + $total_tax));

		if($leftover_tax > 0){
			$data['taxes']['Tax'] += $leftover_tax;
		}

		if($customer_id!=-1)
		{
			$cust_info=$this->Customer->get_info($customer_id);
			$data['customer']=$cust_info->first_name.' '.$cust_info->last_name.($cust_info->company_name==''  ? '' :' ('.$cust_info->company_name.')');
		}
		$data['sale_id']='POS '.$sale_id;
		//$data['sale_number']='POS '.$sale_info['sale_number'];
		if ($json){
			echo json_encode($data);
		}
		else{
			$this->load->view("sales/receipt",$data);
		}
		$this->sale_lib->clear_all();
		if (count($this->sale_lib->get_cart())==0)
        {
            $this->sale_lib->set_teetime(-1);
			$this->session->unset_userdata('purchase_override');
        	$this->session->unset_userdata('purchase_override_time');
        }
	}
	function raincheck($raincheck_number)
	{
		$raincheck_id = $this->Sale->get_raincheck_id($raincheck_number);
		$data = $this->Sale->raincheck_info($raincheck_id)->row_array();
//		echo $this->db->last_query();
		$data['receipt_title']='Raincheck';//lang('sales_raincheck');
		$data['transaction_time']= date(get_date_format().' '.get_time_format(), strtotime($data['date_issued']));
		$emp_info=$this->Employee->get_info($data['employee_id']);
		$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;
		$cust_info=$this->Customer->get_info($data['customer_id']);
		$data['customer']=$cust_info->first_name.' '.$cust_info->last_name;
		$tee_info=$this->teesheet->get_info($data['teesheet_id']);
		$data['teesheet']=$tee_info->title;

//print_r($data);

		$this->load->view("sales/raincheck",$data);
	}

	function edit($sale_id, $type = '',  $sales_page = false)
	{
		$data = array();

		$data['customers'] = array('' => 'No Customer');
		$data['employees'] = array();
		foreach ($this->Employee->get_all()->result() as $employee)
		{
			$data['employees'][$employee->person_id] = $employee->first_name . ' '. $employee->last_name;
		}

		$data['sale_info'] = $this->Sale->get_info($sale_id)->row_array();
		$totals = $this->Sale->get_total($sale_id);

		$data['sale_info']['total'] = $totals['total'];
		$data['sale_info']['paid'] = $totals['paid'];
		$data['sale_info']['refunded'] = $totals['refunded'];

		$payments = $this->Sale->get_sale_payments($sale_id);
		$data['invoice_id'] = false;
		foreach ($payments->result_array() as $payment){
			if ($payment['invoice_id']){
				$data['invoice_id'] = $payment['invoice_id'];
				$data['credit_card_info'] = $this->Sale->get_credit_card_payment($payment['invoice_id']);
			}
		}

		$customer = $this->Customer->get_info($data['sale_info']['customer_id']);
		$data['customer_name'] = $customer->first_name . ' '. $customer->last_name;
		$data['cab_name']=($this->config->item('customer_credit_nickname') == '') ? lang('customers_account_balance') : $this->config->item('customer_credit_nickname');
		$data['cmb_name']=($this->config->item('member_balance_nickname') == '') ? lang('customers_member_account_balance') : $this->config->item('member_balance_nickname');
		$data['type'] = $type;
		$data['sales_page'] = $sales_page;
		$this->load->view('sales/edit', $data);
	}
	function confirm_refund($invoice_id, $amount = 0)
	{
		$amount = ($amount < 0)?-$amount:$amount;
		$cc_payment = $this->Sale->get_credit_card_payment($invoice_id);
		$data['cc_payment'] = $cc_payment[0];
		//print_r($cc_payment[0]);
		//$data['cc_payment']['amount'] = ($cc_payment[0]['amount'] < $amount || $amount == 0)?$cc_payment[0]['amount']:$amount;
		//$data['cc_payment']['amount'] += $cc_payment[0]['gratuity_amount'];
		$this->load->view('sales/confirm_refund', $data);
	}
	function issue_refund($invoice)
	{
		//echo 'issue_refund<br/>';
		$amount = $this->input->post('amount') ? $this->input->post('amount') : $this->input->get('amount');
		$gratuity_amount = $this->input->post('gratuity_amount') ? $this->input->post('gratuity_amount') : $this->input->get('gratuity_amount');
		$cc_payment = $this->Sale->get_credit_card_payment($invoice);
		$cc_payment = $cc_payment[0];
		if ($cc_payment['amount_refunded'] > 0)
		{
			echo json_encode(array('success'=>false, 'message'=>lang('sales_return_already_issued').to_currency($cc_payment['amount_refunded'])));
			return;
		}
		$success = false;
		$amount = $amount + $gratuity_amount;

		$ets_key = $this->input->get('ets_key') ?  $this->input->get('ets_key') : $this->config->item('ets_key');
		$mercury_id = $this->input->get('mercury_id') ? $this->input->get('mercury_id') : $this->config->item('mercury_id');
		$mercury_password = $this->input->get('mercury_password') ? $this->input->get('mercury_password') : $this->config->item('mercury_password');

		if($ets_key){

			$this->load->library('Hosted_payments');
			$payment = new Hosted_payments();
			$payment->initialize($ets_key);

			$ets_response = $payment->set('action', 'refund')
						->set('transactionID', $cc_payment['payment_id'])
						->set('amount', $amount)
						->send();
//echo 'payment id '.$cc_payment['payment_id'];
//echo 'amount '.$amount;
//print_r($ets_response);
			if($ets_response->status == 'success'){
				$success = true;
				$sql = $this->Sale->add_ets_refund_payment($invoice, $amount, $payment_data, $ets_response);
			}

		}else if($mercury_id){

			$this->load->library('Hosted_checkout_2');
			$HC = new Hosted_checkout_2();
			$HC->set_merchant_credentials($mercury_id,$mercury_password);//Golf Course's Credentials
			$HC->set_invoice($invoice);
			$HC->set_cardholder_name($cc_payment['cardholder_name']);
			$HC->set_token($cc_payment['token']);
			$response = $HC->issue_refund($invoice, $amount);

			if ((int)$response->ResponseCode === 0 && $response->Status == 'Approved'){
				$success = true;
				$sql = $this->Sale->add_refund_payment($invoice, $amount, $response);
			}
		}

		if($success){
			$this->sale_lib->add_payment('CC Refund', -$amount);
			if ($gratuity_amount > 0)
				$this->sale_lib->add_payment('Tip Refund', -$gratuity_amount);
			echo json_encode(array('success'=>true));
		}else{
			echo json_encode(array('success'=>false));
		}
	}
	function delete($sale_id, $type = '')
	{
		if ($this->permissions->is_employee())
		{
			echo json_encode(array('success'=>false));
			return;
		}

		$data = array();

		if ($this->Sale->delete($sale_id))
		{
			// RETURN ANY CREDIT CARD PAYMENTS - OR LOYALTY - OR ACCOUNT PAYMENTS
			$payments = $this->Sale->get_sale_payments($sale_id);

			foreach ($payments->result_array() as $payment)
			{
				if ($payment['invoice_id'] != 0)
				{
					$return_response = $this->return_payment($payment);
				}
			}
			$data['success'] = true;

           // No longer storing recent transactions in the session
           $recent_transactions = $this->session->userdata('recent_transactions');
           foreach($recent_transactions as $index => $rt)
           {
                   if ($rt['sale_id'] == $sale_id)
                   {
                           unset($recent_transactions[$index]);
                   }
           }
           $this->session->set_userdata('recent_transactions', $recent_transactions);
		}
		else
		{
			$data['success'] = false;
		}

		if ($type == 'popup')
            echo json_encode($data);
        else
            $this->load->view('sales/delete', $data);
	}
	function view_raincheck($teetime_id = '')
	{
		$this->load->model('Teetime');
		$this->load->model('Price_class');
		$data = $this->Sale->get_raincheck_info();

		$data['pricing']['time'] = 0;
		$data['pricing']['date'] = date('m/d/Y');
		$data['pricing']['teesheet_id'] = 0;
		$data['pricing']['green_fee_dropdown'] = 0;
		$data['pricing']['cart_fee_dropdown'] = 0;

		if ($teetime_id != '')
		{
			$data['teetime_info'] = $this->Teetime->get_info($teetime_id);

			$price_class_id = (int) $data['teetime_info']->price_class_1;
			if(empty($price_class_id) && !empty($data['teetime_info']->person_id)){
				$price_class_id = $this->Price_class->get_customer_price_class($data['teetime_info']->person_id);
			}

			$holes = $data['teetime_info']->holes;
			$carts = $data['teetime_info']->carts;

			$date = DateTime::createFromFormat('YmdHi', $data['teetime_info']->start);

			$hours = substr($data['teetime_info']->start, -4, 2);
			$mins = substr($data['teetime_info']->start, -2, 2);
			$year = substr($data['teetime_info']->start, 0, 4);
			$month = substr($data['teetime_info']->start, 4, 2) + 1;
			$day = substr($data['teetime_info']->start, 6, 2);

			if($mins < 30){
				$mins = '00';
			}else{
				$mins = '30';
			}
			$data['pricing']['time'] = $hours.$mins;
			$data['pricing']['date'] = $month.'/'.$day.'/'.$year;
			$data['pricing']['teesheet_id'] = $data['teetime_info']->teesheet_id;

			if($holes == 9){
				$data['pricing']['green_fee_dropdown'] = $price_class_id.'_2';
			}else if($holes == 18){
				$data['pricing']['green_fee_dropdown'] = $price_class_id.'_1';
			}

			if($holes == 9 && $carts > 0){
				$data['pricing']['cart_fee_dropdown'] = $price_class_id.'_4';
			}else if($holes == 18 && $carts > 0){
				$data['pricing']['cart_fee_dropdown'] = $price_class_id.'_3';
			}
		}
		$data['taxes'] = array('gf'=>$this->Item->get_teetime_tax_rate(), 'c'=>$this->Item->get_cart_tax_rate());

		$this->load->view('teetimes/raincheck', $data);
	}
	function save_raincheck()
	{
		$customer_id=$this->input->post('raincheck_customer_id');//$this->sale_lib->get_customer();
		$raincheck_number = $this->Sale->get_next_raincheck_number();
		$teetime_id = $this->input->post('raincheck_teetime_id');
		$split_raincheck = $this->input->post('split_raincheck');
		$players = $this->input->post('players');
		$raincheck_data = array(
			'teesheet_id' 				=> $this->input->post('teesheet_id'),
			'raincheck_number'			=> $raincheck_number,
			'date_issued'				=> date('Y-m-d H:i:s'),
			'players'					=> $players,
			'holes_completed'			=> $this->input->post('holes'),
			'employee_id'				=> $this->session->userdata('person_id'),
			'customer_id'				=> $customer_id,
			'green_fee'					=> $this->input->post('green_fee'),
			'cart_fee'					=> $this->input->post('cart_fee'),
			'tax'						=> $this->input->post('tax'),
			'total'						=> $this->input->post('total_credit'),
			'green_fee_price_category'	=> $this->input->post('green_fee_dropdown'),
			'cart_price_category'		=> $this->input->post('cart_fee_dropdown'),
			'teetime_id'				=> $teetime_id,
			'expiry_date'				=> $this->input->post('expiry_date')	
		);

		if ($teetime_id && $teetime_id != '')
		{
			// RECORD RAINCHECK ON TEE TIME
			$this->Sale->record_teetime_raincheck($teetime_id, $raincheck_data['players']);
		}
		$all_rainchecks = array();
		if ($split_raincheck)
		{
			$raincheck_data['players'] = 1;
			$raincheck_data['tax'] = $raincheck_data['tax'] / $players;
			$raincheck_data['total'] = $raincheck_data['total'] / $players;
			while ($players > 0)
			{
				unset($raincheck_data['id']);
				unset($raincheck_data['customer']);
				$raincheck_data['raincheck_number'] = $this->Sale->get_next_raincheck_number();
				$raincheck_id = $this->Sale->save_raincheck($raincheck_data);
				$raincheck_data['id'] = $raincheck_id;
				$customer = $this->Customer->get_info($customer_id);
				$raincheck_data['customer'] = $customer->first_name.' '.$customer->last_name;
				$all_rainchecks[] = $raincheck_data;
				$players--;
			}
		}
		else
		{
			$raincheck_id = $this->Sale->save_raincheck($raincheck_data);
			$raincheck_data['id'] = $raincheck_id;
			$customer = $this->Customer->get_info($customer_id);
			$raincheck_data['customer'] = $customer->first_name.' '.$customer->last_name;
			$all_rainchecks[] = $raincheck_data;
		}

		echo json_encode(array('success'=>$raincheck_id?true:false, 'data'=>$all_rainchecks));
	}
	function remove_raincheck()
	{
		$this->sale_lib->remove_raincheck();
		$this->_reload();
	}
	function get_raincheck_info($teesheet_id = ''){
		echo json_encode($this->Sale->get_raincheck_info($teesheet_id));
	}

	function undelete($sale_id)
	{
		if ($this->permissions->is_employee())
		{
			echo json_encode(array('success'=>false));
			return;
		}

		$data = array();

		if ($this->Sale->undelete($sale_id))
		{
			$data['success'] = true;
		}
		else
		{
			$data['success'] = false;
		}

		$this->load->view('sales/undelete', $data);

	}

	function save($sale_id)
	{
		$sale_data = array(
			'sale_time' => date('Y-m-d H:i:s', strtotime($this->input->post('date'))),
			'customer_id' => $this->input->post('person_id') ? $this->input->post('person_id') : null,
			'employee_id' => $this->input->post('sale_employee_id'),
			'comment' => $this->input->post('comment')
		);

		if ($this->Sale->update($sale_data, $sale_id))
		{
			echo json_encode(array('success'=>true,'message'=>lang('sales_successfully_updated')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('sales_unsuccessfully_updated')));
		}
	}
	function full_transaction()
	{
		// MAKE SURE THE CART IS COMPLETELY CLEAR BEFORE STARTING
		$this->sale_lib->clear_all();

		$this->session->set_userdata('mobile', true);
		$sale_date = $this->input->post('sale_date');
		$sale_guid = $this->input->post('sale_guid');
		$items = json_decode($this->input->post('items'));//Including price and quantity
		$payments = json_decode($this->input->post('payments'));
		$tips = json_decode($this->input->post('tips'));
		$customer_id = $this->input->post('customer_id');
		$employee_id = $this->input->post('employee_id');
		$terminal_id = $this->input->post('terminal_id');
		if (!$sale_date)
		{
			echo json_encode(array('success'=>false, 'message'=>'sale_date is a required parameter'));
			return;
		}
		if (!$sale_guid)
		{
			echo json_encode(array('success'=>false, 'message'=>'sale_guid is a required parameter'));
			return;
		}
		if (!$items)
		{
			echo json_encode(array('success'=>false, 'message'=>'items is a required parameter'));
			return;
		}
		if (!$payments)
		{
			echo json_encode(array('success'=>false, 'message'=>'payments is a required parameter'));
			return;
		}

		$tax_array = array();
		//return;
		// CHECK IF GUID HAS ALREADY BEEN USED, IF SO, RETURN SALE_ID
		$guid_sale = $this->Sale->get_sale_by_guid($sale_guid);
		if ($guid_sale['sale_id'])
		{
			echo json_encode(array('sale_id'=>'POS '.$guid_sale['sale_id']));
			return;
		}
		$this->session->set_userdata('manual_taxes', 1);
		// SET TAXES
		foreach($items as $index => $item)
		{
			$item = (array) $item;
			$tax_array[$item['item_id']] = $item['taxes'];
		}
		// SAVE TAXES TO SALE_LIB
		$this->sale_lib->set_transaction_taxes($tax_array);

		// ADD ITEMS, ITEM KITS, INVOICES, ETC
		foreach($items as $index => $item)
		{
			$item = (array) $item;
			// ADD ITEM
			$response = $this->add($item['item_id'], '', true);
			// ADJUST ITEM QUANTITY AND PRICE
			$discount = $item['discount'] ? $item['discount'] : 0;
			$serial_number = '';
			$description = '';
			// HANDLE INCOMING TAX RATES (MAY BE DIFFERENT THAN WHAT IS CURRENTLY IN THE SYSTEM)
			$response = $this->edit_item($index + 1,$description,$serialnumber,$item['quantity'],$discount,$item['price'], '', true);
		}
		$response_credit_card_data = array();

		// ADD PAYMENTS
		foreach($payments as $payment)
		{
			$payment = (array) $payment;
			//print_r($payment);
			$invoice_id = '';//isset($payment['invoice_id']) ? $payment['invoice_id'] : false;
			if (isset($payment['auth_code']) && $payment['auth_code'] != '')
			{
				$ref_no	= isset($payment['ref_no']) ? $payment['ref_no'] : '';
				$acq_ref_data = isset($payment['acq_ref_data']) ? $payment['acq_ref_data'] : '';
				$process_data = isset($payment['process_data']) ? $payment['process_data'] : '';
				$token = isset($payment['token']) ? $payment['token'] : '';
				$payment_data = array (
					'tran_type'			=> 'Sale',
					'frequency'			=> 'OneTime',
					'course_id' 		=> $this->session->userdata('course_id'),
					'masked_account' 	=> $payment['masked_account'],
					'trans_post_time' 	=> $payment['trans_post_time'],
					'mercury_id'		=> $this->config->item('mercury_id'),
					'ets_id' 			=> $this->config->item('ets_key'),
					'amount' 			=> $payment['amount'],
					'auth_amount'		=> $payment['amount'],
					'auth_code'			=> $payment['auth_code'],
					'card_type' 		=> $payment['card_type'],
					'payment_id' 		=> $payment['payment_id'],
					'memo'				=> $payment['memo'],
					'status'			=> 'success',
					'status_message'	=> $payment['status_message'],
					'operator_id'		=> (string) $this->session->userdata('person_id'),
					'ref_no'			=> $ref_no,
					'acq_ref_data'		=> $acq_ref_data,
					'process_data'		=> $process_data,
					'token'				=> $token
				);
//		print_r($payment_data);
				$invoice_id = $this->Sale->add_credit_card_payment($payment_data);
				//echo $this->db->last_query();
				$response_credit_card_data[] = array('payment_id'=>$payment_data['payment_id'], 'invoice_id'=>$invoice_id);
	//			echo $this->db->last_query();
			}
			$this->add_payment($payment['type'], $payment['amount'], true, $invoice_id);
		}
		$this->sale_lib->set_customer($customer_id);
		$response = $this->complete($sale_guid, $sale_date, $employee_id, $terminal_id);

		// ADD TIPS
	//echo 'here';
	//print_r($tips);
		foreach($tips as $tip)
		{
			$tip = (array) $tip;
			//print_r($tip);
			$sale_id = substr($response['sale_id'], 4);
			$invoice_id = isset($tip['invoice_id']) ? $tip['invoice_id'] : false;
			$tips_response = $this->add_tip($sale_id, $invoice_id, $tip['amount'], $tip['recipient'], $tip['type'], true);
		}

		$this->session->unset_userdata('manual_taxes');
		//RETURN CUSTOM RESPONSE
		echo json_encode(array('sale_id'=>$response['sale_id'], 'credit_card_data'=>$response_credit_card_data));
	}

	// Accepts a payment type and partial amount to refund on a particular sale
	function partial_return($source = false){

		$type = $this->input->post('payment_type');
		$this->load->model('Account_transactions');

		// If refund payment is made via credit card with ETS
		if($source == 'creditcard' && $this->config->item('ets_key') != ''){
			$response = $this->input->post('response');
			$ets_response = json_decode($response);

			if ((string) $ets_response->transactions->type == 'credit card') {
				$ets_card_type = $ets_response->transactions->cardType;
				$card_type = '';
				switch($ets_card_type){
					case 'MasterCard':
						$card_type = 'M/C';
					break;
					case 'Visa':
						$card_type = 'VISA';
					break;
					case 'Discover':
						$card_type = 'DCVR';
					break;
					case 'AmericanExpress':
						$card_type = 'AMEX';
					break;
					case 'Diners':
						$card_type = 'DINERS';
					break;
					case 'JCB':
						$card_type = 'JCB';
					break;
				}
			}
			$invoice_id = (int) $ets_response->store->primary;
			$sale_id = (int) $this->session->userdata('partial_return_sale_id');
			$type = 'Partial CC Refund';
			$amount = (float) abs($ets_response->transactions->total);

		// If refund payment is made via credit card with Mercury
		}else if($source == 'creditcard' && $this->config->item('mercury_id') != ''){

			// Complete refund payment to credit card
			$this->load->library('Hosted_checkout_2');
			$HC = new Hosted_checkout_2();
			$HC->set_merchant_credentials($this->config->item('mercury_id'),$this->config->item('mercury_password'));

			$payment_id = $this->session->userdata('partial_return_payment_id');
			$sale_id = $this->session->userdata('partial_return_sale_id');

			$this->session->unset_userdata('partial_refund_payment_id');
			$this->session->userdata('partial_refund_sale_id');

			$HC->set_payment_id($payment_id);
			$verify_results = $HC->verify_payment();
			$HC->complete_payment();
			$invoice = $this->session->userdata('invoice_id');

			$credit_card_data = array(
				'course_id'=>$this->session->userdata('selected_course'),
				'token'=>(string)$verify_results->Token,
				'token_expiration'=>date('Y-m-d', strtotime('+2 years')),
				'card_type'=>(string)$verify_results->CardType,
				'masked_account'=>(string)$verify_results->MaskedAccount,
				'cardholder_name'=>(string)$verify_results->CardholderName
			);

			//Update credit card payment data
			$payment_data = array (
				'course_id'=>$this->session->userdata('course_id'),
				'mercury_id'=>$this->config->item('mercury_id'),
				'tran_type'=>(string)$verify_results->TranType,
				'amount'=>(string)$verify_results->Amount,
				'auth_amount'=>(string)$verify_results->AuthAmount,
				'card_type'=>(string)$verify_results->CardType,
				'frequency'=>'OneTime',
				'masked_account'=>(string)$verify_results->MaskedAccount,
				'cardholder_name'=>(string)$verify_results->CardholderName,
				'ref_no'=>(string)$verify_results->RefNo,
				'operator_id'=>(string)$verify_results->OperatorID,
				'terminal_name'=>(string)$verify_results->TerminalName,
				'trans_post_time'=>(string)$verify_results->TransPostTime,
				'auth_code'=>(string)$verify_results->AuthCode,
				'voice_auth_code'=>(string)$verify_results->VoiceAuthCode,
				'payment_id'=>$payment_id,
				'acq_ref_data'=>(string)$verify_results->AcqRefData,
				'process_data'=>(string)$verify_results->ProcessData,
				'token'=>(string)$verify_results->Token,
				'response_code'=>(int)$verify_results->ResponseCode,
				'status'=>(string)$verify_results->Status,
				'status_message'=>(string)$verify_results->StatusMessage,
				'display_message'=>(string)$verify_results->DisplayMessage,
				'avs_result'=>(string)$verify_results->AvsResult,
				'cvv_result'=>(string)$verify_results->CvvResult,
				'tax_amount'=>(string)$verify_results->TaxAmount,
				'avs_address'=>(string)$verify_results->AVSAddress,
				'avs_zip'=>(string)$verify_results->AVSZip,
				'payment_id_expired'=>(string)$verify_results->PaymendIDExpired,
				'customer_code'=>(string)$verify_results->CustomerCode,
				'memo'=>(string)$verify_results->Memo
			);

			$this->sale->update_credit_card_payment($invoice, $payment_data);
			$type = 'Partial CC Refund';
			$amount = (float) abs($payment_data['amount']);
			$invoice_id = (int) $invoice;

		// If refund is to member account
		}else if($type == 'member_account'){

			$customer_id = $this->input->post('customer_id');
			$sale_id = $this->input->post('sale_id');
			$amount = $this->input->post('payment_amount');
			$account_name = ($this->config->item('member_balance_nickname') == '') ? lang('customers_member_account_balance') : $this->config->item('member_balance_nickname');
			$invoice_id = 0;
			$customer_data = $this->Customer->get_info($customer_id);
			$customer_name = $customer_data->last_name.', '.$customer_data->first_name;

			$this->Account_transactions->save('member', $customer_id, 'POS '.$sale_id.' Refund', $amount, 'Partial refund', $sale_id, 0, $this->session->userdata('person_id'));
			$type = $account_name.' Refund- '.$customer_name;

		// If refund is to customer account
		}else if($type == 'customer_account'){

			$customer_id = $this->input->post('customer_id');
			$sale_id = $this->input->post('sale_id');
			$amount = $this->input->post('payment_amount');
			$account_name = ($this->config->item('customer_credit_nickname') == '') ? lang('customers_account_balance') : $this->config->item('customer_credit_nickname');
			$invoice_id = 0;
			$customer_data = $this->Customer->get_info($customer_id);
			$customer_name = $customer_data->last_name.', '.$customer_data->first_name;

			$this->Account_transactions->save('customer', $customer_id, 'POS '.$sale_id.' Refund', $amount, 'Partial refund', $sale_id, 0, $this->session->userdata('person_id'));
			$type = $account_name.' Refund- '.$customer_name;

		// If refund is cash
		}else{
			$sale_id = $this->input->post('sale_id');
			$amount = $this->input->post('payment_amount');
			$invoice_id = 0;
		}
		$this->load->model('Payment');

		// Make sure valid payment data is passed
		if(empty($sale_id)){
			echo json_encode(array('success'=>false, 'message'=>"'sale_id' is required"));
			return;
		}
		if(empty($amount) || $amount < 0){
			echo json_encode(array('success'=>false, 'message'=>"Amount must be greater than 0"));
			return;
		}
		if(empty($type)){
			echo json_encode(array('success'=>false, 'message'=>"'payment_type' is required"));
			return;
		}

		// Make sure payment amount is negative
		$amount = 0 - abs($amount);

		$sale_total = $this->Sale->get_total($sale_id);
		$available_to_refund = $sale_total['total'] + $sale_total['refunded'];

		// Make sure refund amount is not greater than sale (including past refunds)
		if(abs($amount) > $available_to_refund){
			echo json_encode(array('success'=>false, 'message'=>"Refund amount must be no more than ".to_currency($available_to_refund)));
			return;
		}

		// Retrieve any current refunds of the same type
		$existing_payment = $this->Payment->get($sale_id, $type);

		// If existing refunds exist of the same type, add them together
		// as the new refund
		if(!empty($existing_payment)){
			$amount = (float) $existing_payment['amount'] + $amount;
		}

		// Add new refund payment to sale
		$payment = array(
			'sale_id' => (int) $sale_id,
			'payment_amount' => (float) $amount,
			'payment_type' => $type,
			'invoice_id' => $invoice_id
		);
		$payment_response = $this->Payment->add($payment);

		if(!empty($invoice_id)){
			echo "<html><body><script>
			window.parent.set_feedback('Refund successful', 'success_message', true, 2500);
			window.parent.$.colorbox2.close();
			</script></body></html>";

		}else{
			echo json_encode(array('success'=>$payment_response));
		}
		return true;
	}
	function return_full_transaction()
	{
		$this->session->set_userdata('mobile', true);
		$sale_id = $this->input->post('sale_id');
		//$sale_number = $this->input->post('sale_id');
		//$sale_id = $this->Sale->get_sale_id($sale_number);
		$sale_date = $this->input->post('sale_date');
		$sale_guid = $this->input->post('sale_guid');
		$employee_id = $this->input->post('employee_id');
		$terminal_id = $this->input->post('terminal_id');

		if (!$sale_date)
		{
			echo json_encode(array('success'=>false, 'message'=>'sale_date is a required parameter'));
			return;
		}
		if (!$sale_guid)
		{
			echo json_encode(array('success'=>false, 'message'=>'sale_guid is a required parameter'));
			return;
		}
		if (!$sale_id)
		{
			echo json_encode(array('success'=>false, 'message'=>'sale_id is a required parameter'));
			return;
		}

		// CHECK IF GUID HAS ALREADY BEEN USED, IF SO, RETURN SALE_ID
		$guid_sale = $this->Sale->get_sale_by_guid($sale_guid);
		if ($guid_sale['sale_id'])
		{
			echo json_encode(array('sale_id'=>'POS '.$guid_sale['sale_id']));
			return;
		}
		// LOAD SALE ITEMS
		$sale_id = strpos($sale_id, 'POS') !== false ? $sale_id : 'POS '.$sale_id;
		$this->add($sale_id, '', true);
		//$sale_number = strpos($sale_number, 'POS') !== false ? $sale_number : 'POS '.$sale_number;
		//$this->add($sale_number, '', true);
		// LOAD SALES PAYMENT REVERSE
		// GET PAYMENTS
		$return_response = array();
		$payments = $this->Sale->get_sale_payments(substr($sale_id, 4));
		//$payments_sql = $this->db->last_query();
		$tips = array();
		foreach ($payments->result_array() as $payment)
		{
			// IF CREDIT CARD PAYMENT, RETURN IT
			if (strpos($payment['payment_type'], 'Tip') !== false)
			{
				$tips[] = $payment;
				continue;
			}
			if ($payment['invoice_id'] != 0)
			{
				$return_response[] = $this->return_payment($payment);
			}
			// EITHER WAY, ADD IT
			$return_response[] = $this->add_payment($payment['payment_type'], -$payment['payment_amount'], true);
		}
		// COMPLETE THE RETURN
		$response = $this->complete($sale_guid, $sale_date, $employee_id, $terminal_id);
		// GET TIPS
		//$tips = $this->Sale->get_sale_tips($sale_id);
		// RETURN TIPS
		$sale_id_number = substr($response['sale_id'], 4);
//		$sale_id_number = substr($response['sale_number'], 4);
		foreach($tips as $tip)
		{
			$tip = (array) $tip;
			//print_r($tip);
			$tips_response = $this->add_tip($sale_id_number, false, -$tip['payment_amount'], $tip['tip_recipient'], str_replace('Tip', '', $tip['payment_type']), true);
		}

		echo json_encode(array('sale_id'=>$response['sale_id']));
	}
	function return_payment($payment)
	{
		$success = false;
		$cc_payment = $this->Sale->get_credit_card_payment($payment['invoice_id']);
		$cc_payment = $cc_payment[0];
		if ($cc_payment['amount_refunded'] > 0)
			return array('success'=>false, 'response'=>array('message'=>'Already refunded', 'amount_refunded'=>$cc_payment['amount_refunded']));
		//print_r($cc_payment);
		$amount = $cc_payment['amount'] + $cc_payment['gratuity_amount'];
		if($this->config->item('ets_key') && $cc_payment['process_data'] != '')
		{
			$this->load->library('Hosted_payments');
			$hosted_payment = new Hosted_payments();
			$hosted_payment->initialize($this->config->item('ets_key'));

			$ets_response = $hosted_payment->set('action', 'void')
						->set('transactionID', $cc_payment['payment_id'])
						->set('sessionID', $cc_payment['process_data'])
						->send();

			if($ets_response->status == 'success'){
				$success = true;
				$sql = $this->Sale->add_ets_refund_payment($payment['invoice_id'], $amount);
			}

			return array('success'=>$success,'response'=>$ets_response);
		}else if($this->config->item('mercury_id')){

			$this->load->library('Hosted_checkout_2');
			$HC = new Hosted_checkout_2();
			$HC->set_merchant_credentials($this->config->item('mercury_id'),$this->config->item('mercury_password'));//Golf Course's Credentials
			$HC->set_invoice($payment['invoice_id']);
			$HC->set_cardholder_name($cc_payment['cardholder_name']);
			$HC->set_token($cc_payment['token']);
			$response = $HC->issue_refund($payment['invoice_id'], $amount);

			if ((int)$response->ResponseCode === 0 && $response->Status == 'Approved'){
				$success = true;
				$sql = $this->Sale->add_refund_payment($payment['invoice_id'], $amount, $response);
			}

			return array('success'=>$success,'response'=>$response);
		}
	}
	function save_invoice_details($cart_line)
	{
		$invoice_info = $this->sale_lib->get_invoice_details($cart_line);
		$invoice_info['price'] = $this->input->post('invoice_payment_total');
		$this->sale_lib->update_invoice_details($invoice_info, $cart_line);

		echo json_encode(array('cart_line'=>$cart_line,'invoice_info'=>$this->sale_lib->get_invoice_details($cart_line),'basket_info'=>$this->sale_lib->get_basket_info(), 'items'=>$this->sale_lib->get_basket()));
	}
	function save_giftcard_details($cart_line)
	{
		echo json_encode(array("item_info"=>$this->sale_lib->save_giftcard_details($cart_line), 'basket_info'=>$this->sale_lib->get_basket_info()));
		//echo json_encode($this->sale_lib->save_giftcard_details($cart_line));
	}
	function save_punch_card_details($cart_line)
	{
		echo json_encode(array("item_info"=>$this->sale_lib->save_punch_card_details($cart_line), 'basket_info'=>$this->sale_lib->get_basket_info()));
		//echo json_encode($this->sale_lib->save_giftcard_details($cart_line));
	}
	function save_item_modifiers($cart_line)
	{
		echo json_encode(array("item_info"=>$this->sale_lib->save_item_modifiers($cart_line), 'basket_info'=>$this->sale_lib->get_basket_info()));
		//echo json_encode($this->sale_lib->save_giftcard_details($cart_line));
	}
	function tip_window($sale_id, $invoice)
	{
		$data['sale_id'] = $sale_id;
		$data['payment_info'] = $this->Sale->get_sale_payment($sale_id, $invoice)->row_array();
		$data['tip_info'] = $this->Sale->get_sale_tips($sale_id);
		//echo $this->db->last_query();
		$credit_card_info = $this->Sale->get_credit_card_payment($invoice);
		//echo $this->db->last_query();
		$data['employee'] = $this->Employee->get_info($data['payment_info']['tip_recipient'] ? $data['payment_info']['tip_recipient'] : $this->session->userdata('person_id'));
		//print_r($credit_card_info);
		$data['credit_card_info'] = $credit_card_info[0];
		//echo $this->db->last_query();
		$this->load->view('sales/add_tip', $data);
	}
	function add_tip($sale_id, $invoice = false, $tip_amount = false, $tip_recipient = false, $tip_type = false, $full_transaction = false)
	{
		$this->load->model('payment');
		$this->load->model('sale');
		$gratuity = $tip_amount ? $tip_amount : $this->input->post('tip_amount');
		$new_amount = $new_amount ? $new_amount : $this->input->post('new_total');
		$tip_recipient = $tip_recipient ? $tip_recipient : $this->input->post('tip_recipient');
		$tip_type = $tip_type ? $tip_type : $this->input->post('tip_type');
		//$course_info = $this->Course->get_info($this->session->userdata('course_id'));

		if ($full_transaction || $this->session->userdata('mobile'))
		{
			$tip_error = false;
			if (!$tip_type && !$invoice)
				$tip_error = array('success'=>false, 'message'=>'either tip_type or invoice_id is required');
			else if (!$gratuity)
				$tip_error = array('success'=>false, 'message'=>'tip_amount is a required parameter');

			if ($tip_error)
			{
				if ($full_transaction)
					return $tip_error;
				else
				{
					echo json_encode($tip_error);
					return;
				}
			}
		}

		if ($invoice)// && !$full_transaction)
		{
			$payment_info = $this->Sale->get_sale_payment($sale_id, $invoice)->row_array();
			//echo $this->db->last_query();
			$credit_card_info = $this->Sale->get_credit_card_payment($invoice);
			$credit_card_info = $credit_card_info[0];

			// If account is using ETS
			if ($this->config->item('ets_key')){

				$this->load->library('Hosted_payments');
				$payment = new Hosted_payments();
				$payment->initialize($this->config->item('ets_key'));

				$session = $payment->set('action', 'addTip')
							->set('transactionID', $credit_card_info['payment_id'])
							->set('amount', $gratuity)
							->send();

				if($session->status == 'success'){
					$approved = true;
				}

				$auth_amount = $session->transactions->amount - $gratuity;
				$payment_data = array (
					'auth_amount'=>(string)$auth_amount,
					'gratuity_amount'=>(string)$gratuity,
				);

			// If account is using Mercury
			}else if($this->config->item('mercury_id')){

				$this->load->library('Hosted_checkout_2');
				$HC = new Hosted_checkout_2();
				$HC->set_merchant_credentials($this->config->item('mercury_id'), $this->config->item('mercury_password'));//ForeUP's Credentials
				$HC->set_invoice($credit_card_info['invoice']);
				$HC->set_token($credit_card_info['token']);
				$HC->set_ref_no($credit_card_info['ref_no']);
				$HC->set_frequency($credit_card_info['frequency']);
				$HC->set_memo($credit_card_info['memo']);
				$HC->set_auth_code($credit_card_info['auth_code']);
				$HC->set_cardholder_name($credit_card_info['cardholder_name']);
				$HC->set_customer_code($credit_card_info['customer_code']);

				$response = $HC->token_transaction('Adjust', $credit_card_info['amount'], $gratuity, $credit_card_info['tax_amount']);

				if ((string)$response->Status == 'Approved'){
					$approved = true;
				}

				$auth_amount = $response->AuthorizeAmount;
				$process_data = $response->ProcessData;
				$token = $response->Token;
				$payment_data = array (
					'auth_amount'=>(string)$auth_amount,
					'gratuity_amount'=>(string)$gratuity,
					'process_data'=>(string)$process_data,
					'token'=>(string)$token
				);
			}

			if($approved){
				//Update credit card payment info
				$this->Sale->update_credit_card_payment($invoice, $payment_data);

				//Update sale payment info
				$sales_payments_data = array
				(
					'sale_id'=>$sale_id,
					'payment_type'=>"{$credit_card_info['card_type']} ".substr($credit_card_info['masked_account'], -8)." Tip",
					'payment_amount'=>$payment_data['gratuity_amount'],
					'invoice_id'=>$invoice,
					'tip_recipient'=>$tip_recipient
				);
				$this->payment->add($sales_payments_data);
				if ($full_transaction)
				{
					return array('success'=>true, 'message'=>"Tip of \${$payment_data['gratuity_amount']} added successfully. ", 'response'=>$response);
				}
				else
				{
					echo json_encode(array('success'=>true, 'message'=>"Tip of \${$payment_data['gratuity_amount']} added successfully. ", 'response'=>$response));
					return;
				}
			}
		}
		else
		{
			//Update sale payment info
			$sales_payments_data = array
			(
				'sale_id'=>$sale_id,
				'payment_type'=>$tip_type." ".lang('sales_tip'),
				'payment_amount'=>$gratuity,
				'invoice_id'=> $invoice ? $invoice : 0,
				'tip_recipient'=>$tip_recipient
			);
			if ($this->payment->add($sales_payments_data))
			{
				if ($full_transaction)
				{
					return array('success'=>true, 'message'=>"Tip of \${$gratuity} added successfully. ");
				}
				else
				{
					echo json_encode(array('success'=>true, 'message'=>"Tip of \${$gratuity} added successfully. "));
					return;
				}
			}
		}
		if ($full_transaction)
		{
			return array('success'=>false, 'message'=>'We were unable to add a tip to this sale at this time. Please try again later. Note: Tips must be added before end of day.', 'response'=>$response);
		}
		else
		{
			echo json_encode(array('success'=>false, 'message'=>'We were unable to add a tip to this sale at this time. Please try again later. Note: Tips must be added before end of day.', 'response'=>$response));
			return;
		}
	}
	function _payments_cover_total()
	{
		$total_payments = 0;

		foreach($this->sale_lib->get_payments(0) as $payment)
		{
			$total_payments += $payment['payment_amount'];
		}

		/* Changed the conditional to account for floating point rounding (Note: now ignores anything less than half a cent)*/
		if ( ( $this->sale_lib->get_mode() == 'sale' ) && ( ( round( $this->sale_lib->get_basket_total(), 2) - round($total_payments, 2) ) > .0049 ) )
		{
			return false;
		}
		return true;
	}

	function _reload($data=array(), $ajax = false, $message = false)
	{
		//if ($data['error_message'])
		//	$message = array('text'=>$data['error_message'],'type'=>'error_message','persist'=>false);
		// TEMPLATE FOR MESSAGE array('text'=>'i forgot that the message lasts longer depending on how long the message is','type'=>'error_message','persist'=>false);
		$employee_info = $this->Employee->get_logged_in_employee_info();
		$user_level = $employee_info->user_level;
		$data['employee_level'] = $user_level;
		$this->benchmark->mark('start_data_collection');
		$data['raincheck_info'] = ($this->session->userdata('raincheck_id'))?$this->sale_lib->get_raincheck():false;
		$data['mode']=$this->sale_lib->get_mode();
        $data['recent_transactions']=($this->session->userdata('recent_transactions'))?$this->session->userdata('recent_transactions'):array();//$this->Sale->get_recent_transactions();
		$data['taxes']=$this->sale_lib->get_taxes();
		$data['basket_taxes']=$this->sale_lib->get_basket_taxes();
		$data['amount_due']=$this->sale_lib->get_amount_due();
		$data['basket_amount_due']=$this->sale_lib->get_basket_amount_due();
		$data['total']=$this->sale_lib->get_total();
		$data['basket_total']=$this->sale_lib->get_basket_total();
		$data['payments']=$this->sale_lib->get_payments(0);
		$data['items_in_cart'] = $this->sale_lib->get_items_in_cart();
		$data['items_in_basket'] = $this->sale_lib->get_items_in_basket();
		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['basket_subtotal']=$this->sale_lib->get_basket_subtotal();
		$data['taxable_checked']=($this->session->userdata('taxable') === 'false')?false:true;
		$data['taxable_disabled']= '';
        $person_info = $this->Employee->get_logged_in_employee_info();
		$data['cart']=$this->sale_lib->get_cart();
		$data['basket']=$this->sale_lib->get_basket();
		$data['modes']=array('sale'=>lang('sales_sale'),'return'=>lang('sales_return'));
		$data['items_module_allowed'] = $this->Employee->has_permission('items', $person_info->person_id);
		$data['comment'] = $this->sale_lib->get_comment();
		$data['email_receipt'] = $this->sale_lib->get_email_receipt();
		$data['payments_total']=$this->sale_lib->get_payments_total(0);
		$data['payment_options']=array(
			lang('sales_credit') => lang('sales_credit'),
			lang('sales_cash') => lang('sales_cash'),
			lang('sales_giftcard') => lang('sales_giftcard'),
			lang('sales_check') => lang('sales_check'),
                        'Coupon' => 'Coupon'
		);
		$this->load->helper('sale');
		$data['quickbutton_info'] = $this->Quickbutton->get_all();
		$data['quickbutton_tab'] = $this->session->userdata('quickbutton_tab');
		$data['customer_quickbuttons'] = $this->sale_lib->get_customer_quickbuttons();
		$data['suspended_sales'] = $this->Sale_suspended->get_all(true);
		$data['selected_suspended_sale_id'] = $this->sale_lib->get_suspended_sale_id();
		if ($data['selected_suspended_sale_id'])
			$ss_info = $this->Sale_suspended->get_info($data['selected_suspended_sale_id'])->row_array();
		$data['selected_suspended_sale_table'] = $ss_info['table_id'];
		$data['controller_name']=strtolower(get_class());
		$this->benchmark->mark('end_data_collection');

		if ($data['mode'] == 'return')
		{
			//$this->sale_lib->delete_customer();
			if ($this->session->userdata('return_sale_id'))
			{
				$sale_id = $this->session->userdata('return_sale_id');
				$data['sale_id'] = "POS ".$sale_id;
				$data['credit_card_payments'] = $this->Sale->get_credit_card_payments($sale_id);
				$data['sale_payments'] = $this->Sale->get_sale_payments($sale_id)->result_array();
				//print_r($data['credit_card_payments']);
				//print_r($data['sale_payemnts']);

			}
		}
		$this->benchmark->mark('start_get_customer_information');
		$customer_id=$this->sale_lib->get_customer();
		if($customer_id!=-1)
		{
			$info=$this->Customer->get_info($customer_id);
			$data['customer']=$info->first_name.' '.$info->last_name;
			$data['customer_email']=$info->email;
			$data['customer_phone']=$info->phone_number;
			$data['customer_image_id']=$info->image_id;
			$data['is_member']=$info->member;
			$data['customer_id']=$customer_id;
			$data['customer_account_number']=$info->account_number;
			$data['customer_account_balance']=$info->account_balance;
			$data['customer_member_balance']=$info->member_account_balance;
			$data['invoice_balance']=$info->invoice_balance;
			$data['cab_allow_negative']=$info->account_balance_allow_negative;
			$data['cmb_allow_negative']=$info->member_account_balance_allow_negative;
			$data['loyalty_points']=$info->loyalty_points;
			$data['use_loyalty']=$info->use_loyalty;
			$data['taxable_checked']=(!$info->taxable || $this->session->userdata('taxable')=='false')?false:true;
			$data['taxable_disabled']=(!$info->taxable)?'disabled':'';
			$data['customer_groups']=$this->Customer->get_customer_groups($customer_id);
			$data['customer_passes']=$this->Customer->get_customer_passes($customer_id);
            $data['status_flag'] = $info->status_flag;
            if($data['status_flag'] == 1) { 
                $data['status_class'] = 'border_red'; 
            } else if ($data['status_flag'] == 2) { 
                $data['status_class'] = 'border_yellow'; 
            } else { 
                $data['status_class'] = 'border_green'; 
            }
		}
		else
		{
			$data['customer']='';
			$data['customer_email']='';
			$data['customer_phone']='';
			$data['customer_image_id']='';
			$data['is_member']='';
			$data['customer_id']='-1';
			$data['customer_account_number']='';
			$data['customer_account_balance']='';
			$data['customer_member_balance']='';
			$data['cab_allow_negative']='';
			$data['cmb_allow_negative']='';
			$data['loyalty_points']='';
			$data['taxable_disabled']='';
			$data['customer_groups']='';
			$data['customer_passes']='';
		}
		$data['cab_name']=($this->config->item('customer_credit_nickname') == '') ? lang('customers_account_balance') : $this->config->item('customer_credit_nickname');
		$data['cmb_name']=($this->config->item('member_balance_nickname') == '') ? lang('customers_member_account_balance') : $this->config->item('member_balance_nickname');
		$data['payments_cover_total'] = (($data['total'] != 0 || ($data['items_in_basket'] > 0 && count($data['payments']) > 0)) && $this->_payments_cover_total());
		$data['cart_data'] = $this->sale_lib->get_cart();

		$this->benchmark->mark('end_get_customer_info');
		if ($ajax == false) // Reload Whole Page
		{
			if ($this->session->userdata('mobile')){
				unset($data['suspended_sales']);
				echo json_encode($data);
			}else{
				$this->load->view("sales/register",$data);
			}

			if ($this->config->item('display_benchmarks')) {
				echo '<br/>'.$this->benchmark->elapsed_time('start_data_collection','end_data_collection');
				echo '<br/>'.$this->benchmark->elapsed_time('end_data_collection','start_get_customer_information');
				echo '<br/>'.$this->benchmark->elapsed_time('start_get_customer_information','end_get_customer_info');
			}
			return;
		}

		$register_box_info = 	 array("cart"						=> $data['cart'],
									   //"giftcard_error_line"		=> $data['giftcard_error_line'],
									   "items_module_allowed"		=> $data['items_module_allowed'],
									   "basket"						=> $data['basket']);
			$customer_info = 	 array("customer_id"				=> $data['customer_id'],
									   "customer"					=> $data['customer'],
									   "customer_quickbuttons"		=> $data['customer_quickbuttons'],
									   "customer_account_number"	=> $data['customer_account_number'],
									   "customer_email"				=> $data['customer_email'],
									   "customer_phone"				=> $data['customer_phone'],
									   "cab_name"					=> $data['cab_name'],
									   "customer_account_balance"	=> $data['customer_account_balance'],
									   "cab_allow_negative"			=> $data['cab_allow_negative'],
									   "is_member"					=> $data['is_member'],
									   "cmb_name"					=> $data['cmb_name'],
									   "customer_member_balance"	=> $data['customer_member_balance'],
									   "invoice_balance"			=> $data['invoice_balance'],
									   "cmb_allow_negative"			=> $data['cmb_allow_negative'],
									   "mode"						=> $data['mode'],
									   "customer_image_id"			=> $data['customer_image_id'],
									   "use_loyalty"				=> $data['use_loyalty'],
									   'loyalty_points'				=> $data['loyalty_points'],
									   "customer_groups"			=> $data['customer_groups'],
									   "customer_passes"			=> $data['customer_passes'],
									   "status_class"			    => $data['status_class'],
									   "status_flag"			    => $data['status_flag']);
									   //"giftcard"					=> $giftcard,
									   //"giftcard_balance"			=> $giftcard_balance);
		$suspended_sales_info =  array("suspended_sales" 			=> $data['suspended_sales'],
									   "selected_suspended_sale_id" => $data['selected_suspended_sale_id']);

		if ($ajax == 'add_payment' || $ajax == 'delete_payment')
		{
			if ($this->session->userdata('mobile'))
			{
				echo json_encode(array(
				   'payments'				=>$data['payments'],
				   'raincheck_info'			=>$data['raincheck_info'],
				   'mode'				=>$data['mode'],
				   'sale_payments'			=>$data['sale_payments'],
				   'amount_due'				=>$data['basket_amount_due'],
				   'payments_cover_total'	=>$data['payments_cover_total'],
				   'message'				=>$message));
			}
			else
			{
				echo json_encode(array('payments'=>$this->load->view("sales/payments_list", $data, true),
				   'amount_due'=>$data['basket_amount_due'],
				   'payments_cover_total'=>$data['payments_cover_total'],
				   'message'=>$message));
			}
		}
		else if ($ajax == 'remove_item' || $ajax == 'add_item' || $ajax == 'change_mode')
		{
			// echo "<pre>";
			// print_r($register_box_info);
			// echo "</pre>";
			if ($this->session->userdata('mobile'))
			{
				echo json_encode(
					array(
						'register_box_info'	=>	$register_box_info,
				   		'basket_info'		=>	$this->sale_lib->get_basket_info(),
				   		'payments'			=>  $data['payments'],
						'raincheck_info'	=> 	$data['raincheck_info'],
						'sale_payments'		=>  $data['sale_payments'],
						'amount_due'		=>  $data['basket_amount_due'],
						'mode'				=>  $data['mode'],
						'customer_info'		=>	$customer_info,
						'message'			=>	$message
				   )
			   );
			}
			else
			{
				echo json_encode(
					array(
						'register_box'	=>	$this->load->view("sales/register_box", $register_box_info, true),
				   		'basket_info'	=>	$this->sale_lib->get_basket_info(),
				   		'table_top'		=>	$this->load->view("sales/table_top", array("mode" => $data['mode']), true),
				   		'payments'				=>  $this->load->view("sales/payments_list", $data, true),
						'mode'					=>  $data['mode'],
						'customer_info_filled'	=>	$this->load->view("sales/customer_info_filled", $customer_info, true),
				   		'payment_window'		=>	$this->load->view("sales/payment_window", $customer_info, true),
						'message'=>$message
				   )
			   );
			}
		}
		else if ($ajax == 'select_customer' || $ajax == 'remove_customer')
		{

			echo json_encode(array('register_box_info'		=>	$register_box_info,
								   'register_box'			=>	$this->load->view("sales/register_box", $register_box_info, true),
								   'basket_info'			=>	$this->sale_lib->get_basket_info(),
								   'customer_info_filled'	=>	$this->load->view("sales/customer_info_filled", $customer_info, true),
								   'amount_due'				=>  $data['basket_amount_due'],
								   'payment_window'			=>	$this->load->view("sales/payment_window", $customer_info, true),
								   'message'=>$message
								   ));
		}
		else if ($ajax == 'suspend_sale' || $ajax == 'unsuspend' || $ajax == 'completed_sale' || $ajax == 'cancel_sale')
		{
			//set the return_sale_id blank so when they user selects the 'return' tab it won't pull in the return_sale_id from the session and get the connected payments
			if ($ajax == 'cancel_sale') $this->session->set_userdata('return_sale_id', '');

			if ($this->session->userdata('mobile'))
			{
				$suspended_sales_info = null;
				$json_array = array('register_box_info'		=>	$register_box_info,
									'basket_info'			=>	$this->sale_lib->get_basket_info(),
									'mode'					=>  $data['mode'],
									'customer_info'			=>	$customer_info,
									'suspended_sales'		=> 	$suspended_sales_info,
									'payments'				=>  $data['payments'],
								    'amount_due'			=>  $data['basket_amount_due'],
								    'payments_cover_total'	=>  $data['payments_cover_total'],
								    'message'=>$message
								);
				if ($ajax == 'completed_sale') {
					$json_array['location'] = 'sales';
					$json_array['receipt_data'] = $data['receipt_data']; // need to trim this down
					$json_array['is_cart_empty'] = count($data['cart'])==0?'true':'false';
					$json_array['recent_transactions'] = $data['recent_transactions'];
				}
			}
			else
			{
				$json_array = array('register_box'			=>	$this->load->view("sales/register_box", $register_box_info, true),
									'basket_info'			=>	$this->sale_lib->get_basket_info(),
									'table_top'				=>	$this->load->view("sales/table_top", array("mode" => $data['mode']), true),
									'mode'					=>  $data['mode'],
									'customer_info_filled'	=>	$this->load->view("sales/customer_info_filled", $customer_info, true),
									'payment_window'		=>	$this->load->view("sales/payment_window", $customer_info, true),
									'suspended_sales'		=> 	$this->load->view("sales/suspended_sales", $suspended_sales_info, true),
									'suspend_button_title'	=>  lang('sales_suspend_sale'),
									'payments'				=>  $this->load->view("sales/payments_list", $data, true),
								    'amount_due'			=>  $data['basket_amount_due'],
								    'payments_cover_total'	=>  $data['payments_cover_total'],
								    'customer_info_filled'	=>	$this->load->view("sales/customer_info_filled", $customer_info, true),
									'message'=>$message
								);
				if ($ajax == 'completed_sale') {
					$json_array['location'] = 'sales';
					$json_array['receipt_data'] = $data['receipt_data']; // need to trim this down
					$json_array['is_cart_empty'] = count($data['cart'])==0?'true':'false';

					$recent_transactions_info = array("recent_transactions" =>	$data['recent_transactions']);
					$json_array['recent_transactions'] = $this->load->view("sales/recent_transactions", $recent_transactions_info, true);
					$json_array['message']=$message;
				}
			}
			echo json_encode($json_array);
		}
		$this->benchmark->mark('end_page_sections');
		if ($this->config->item('display_benchmarks')) {
			echo '<br/>'.$this->benchmark->elapsed_time('start_data_collection','end_data_collection');
			echo '<br/>'.$this->benchmark->elapsed_time('end_data_collection','start_get_customer_information');
			echo '<br/>'.$this->benchmark->elapsed_time('start_get_customer_information','end_get_customer_info');
			echo '<br/>'.$this->benchmark->elapsed_time('end_get_customer_info','end_page_sections');
		}
	}
	function get_quickbuttons()
	{
		$data = array();
		$data['status'] = 'success';
		$data['quickbutton_info'] = $this->Quickbutton->get_all();
		$data['quickbutton_tab'] = $this->session->userdata('quickbutton_tab');
		echo json_encode($data);
	}
	function get_recent_transactions($limit = 5)
	{
		if ($this->session->userdata('mobile'))
		{
			echo json_encode($this->Sale->get_recent_transactions($limit));
		}
		else
		{
			echo $this->load->view("sales/recent_transactions", array('recent_transactions'=>$this->Sale->get_recent_transactions($limit)), true);
		}
	}

	function invoice_details($invoice_id = -1, $cart_line = 0)
	{
		$data['invoice_info'] = $this->sale_lib->get_invoice_details($cart_line);
		$data['overdue'] = (float) abs($this->Account_transactions->get_total('invoice', $data['invoice_info']['customer_id']));
		$data['cart_line'] = $cart_line;
		$this->load->view('customers/invoice_form', $data);
	}

    function cancel_sale()
    {
       	$this->session->unset_userdata('purchase_override');
      	$this->session->unset_userdata('purchase_override_time');
    	$this->sale_lib->clear_all();
    	$this->_reload(null, "cancel_sale");

    }
	function suspend_menu(){
		$data = array();

		$fss = array();
		$suspended_sales = $this->Sale_suspended->get_all(true)->result_array();
		foreach($suspended_sales as $ss)
			$fss[$ss['table_id']] = 1;
		$data['suspended_sales'] = $fss;
		$this->load->view('sales/suspend_menu', $data);
	}

	function suspend($table_number = false)
	{
		$message = false;
		if ($table_number)
		{
			$data['cart']=$this->sale_lib->get_cart();
			$data['basket']=$this->sale_lib->get_basket();
			$data['subtotal']=$this->sale_lib->get_subtotal();
			$data['taxes']=$this->sale_lib->get_taxes();
			$data['total']=$this->sale_lib->get_total();
			$data['receipt_title']=lang('sales_receipt');
			$data['transaction_time']= date(get_date_format().' '.get_time_format());
			$customer_id=$this->sale_lib->get_customer();
			$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
			$comment = $this->sale_lib->get_comment();
			$emp_info=$this->Employee->get_info($employee_id);
			//Alain Multiple payments
			$data['payments']=$this->sale_lib->get_payments(0);
			//$data['amount_change']=to_currency($this->sale_lib->get_amount_due() * -1);
			$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;

			if($customer_id!=-1)
			{
				$cust_info=$this->Customer->get_info($customer_id);
		//		$data['customer']=$cust_info->first_name.' '.$cust_info->last_name.($cust_info->company_name==''  ? '' :' ('.$cust_info->company_name.')');
			}

			$total_payments = 0;

			foreach($data['payments'] as $payment)
			{
				$total_payments += $payment['payment_amount'];
			}

			$sale_id = $this->sale_lib->get_suspended_sale_id();
			//SAVE sale to database
			$data['sale_id']='POS '.$this->Sale_suspended->save($data['cart'], $customer_id,$employee_id,$comment,$data['payments'], $sale_id, $table_number);
			if ($data['sale_id'] == 'POS -1')
			{
				//$data['error'] = lang('sales_transaction_failed');
				$message = array('text'=>lang('sales_transaction_failed'),'type'=>'error_message','persist'=>false);
			}
			else {
				//$data['success'] = lang('sales_successfully_suspended_sale');
				$message = array('text'=>lang('sales_successfully_suspended_sale'),'type'=>'success_message','persist'=>false);
			}
			$this->sale_lib->clear_all();
		}
		$this->_reload($data, "suspend_sale", $message);
	}
	function giftcard_exists($giftcard_number = '', $line)
	{
		$giftcard_exists = $this->Giftcard->get_giftcard_id($giftcard_number, true);
		// CHECK TO SEE IF THERE IS ANOTHER CARD IN THE CART WITH THE SAME NUMBER ALREADY
		if (!$giftcard_exists)
		{
			$cart = $this->sale_lib->get_basket();
			//print_r($cart);
			foreach ($cart as $item)
			{
				if (isset($item['giftcard_data']) &&
					isset($item['giftcard_data']['giftcard_number']) &&
					$item['giftcard_data']['giftcard_number'] == $giftcard_number &&
					$item['line'] != $line)
				{
					echo json_encode(array('exists'=>true));
					return;
				}
			}
		}
		echo json_encode(array('exists'=>$giftcard_exists));
	}

	function punch_card_exists($punch_card_number = '')
	{
		echo json_encode(array('exists'=>$this->Punch_card->get_punch_card_id($punch_card_number)));
	}

	function view_giftcard($giftcard_id=-1, $cart_line = 0, $view_only = false)
	{
		$this->load->library('Sale_lib');
		$data = array();
		$data['customers'] = array('' => 'No Customer');
		/*foreach ($this->Customer->get_all()->result() as $customer)
		{
			$data['customers'][$customer->person_id] = $customer->first_name . ' '. $customer->last_name;
		}*/
		$data['view_only'] = false;
		$data['cart_line'] = $cart_line;
		$data['giftcard_info']=$this->Giftcard->get_info($giftcard_id);
		if ($cart_line)
		{
			$giftcard_data = $this->sale_lib->get_giftcard_details($cart_line);
			$data['giftcard_info']->customer_id = $giftcard_data['giftcard_data']['customer_id'];
			$data['giftcard_info']->action = $giftcard_data['giftcard_data']['action'];
			$data['giftcard_info']->giftcard_number = $giftcard_data['giftcard_data']['giftcard_number'];
			$data['giftcard_info']->customer_name = $giftcard_data['giftcard_data']['customer_name'];
			$data['giftcard_info']->value = $giftcard_data['value'];
			$data['giftcard_info']->details = $giftcard_data['giftcard_data']['details'];
			$data['giftcard_info']->expiration_date = $giftcard_data['giftcard_data']['expiration_date'];
		}
		$customer_info = $this->Customer->get_info($data['giftcard_info']->customer_id);
		if ($customer_info->last_name.$customer_info->first_name != '')
			$data['customer'] = $customer_info->last_name.', '.$customer_info->first_name;
		else
			$data['customer'] = 'No Customer';

		$this->load->view("giftcards/form",$data);
	}
	function view_punch_card($punch_card_id=-1, $cart_line = 0, $view_only = false)
	{
		$this->load->library('Sale_lib');
		$data = array();
		$data['customers'] = array('' => 'No Customer');
		/*foreach ($this->Customer->get_all()->result() as $customer)
		{
			$data['customers'][$customer->person_id] = $customer->first_name . ' '. $customer->last_name;
		}*/
		$data['view_only'] = false;
		$data['cart_line'] = $cart_line;
		$data['punch_card_info']=$this->Punch_card->get_info($punch_card_id);
		if ($cart_line)
		{
			$punch_card_data = $this->sale_lib->get_punch_card_details($cart_line);
			$data['punch_card_info']->customer_id = $punch_card_data['punch_card_data']['customer_id'];
			$data['punch_card_info']->punch_card_number = $punch_card_data['punch_card_data']['punch_card_number'];
			$data['punch_card_info']->customer_name = $punch_card_data['punch_card_data']['customer_name'];
			$data['punch_card_info']->item_kit_id = $punch_card_data['punch_card_data']['item_kit_id'];
			$data['punch_card_info']->value = $this->Item_kit_items->get_info($punch_card_data['punch_card_data']['item_kit_id'], true);
			$data['punch_card_info']->details = $punch_card_data['punch_card_data']['details'];
			$data['punch_card_info']->expiration_date = $punch_card_data['punch_card_data']['expiration_date'];
		}
		$customer_info = $this->Customer->get_info($data['punch_card_data']->customer_id);
		$data['customer'] = '';
		if ($customer_info->last_name.$customer_info->first_name != '')
			$data['customer'] = $customer_info->last_name.', '.$customer_info->first_name;

		$this->load->view("punch_cards/form",$data);
	}
	function view_modifiers($item_id=-1, $cart_line = 0, $view_only = false)
	{
		$this->load->library('Sale_lib');
		$data = array();
		$data['view_only'] = false;
		$data['cart_line'] = $cart_line;
		$data['item_id'] = $item_id;

		if ($cart_line)
		{
			$data['modifiers'] = $this->sale_lib->get_modifiers($cart_line);
		}

		$this->load->view("modifiers/sales_item_modifiers", $data);
	}

	function suspended()
	{
		$data = array();
		$data['suspended_sales'] = $this->Sale_suspended->get_all()->result_array();
		$this->load->view('sales/suspended', $data);
	}
	function delete_suspended_sale($sale_id)
	{
		$this->Sale_suspended->delete($sale_id);

		echo json_encode(array());
	}
	function print_suspended_sale($sale_id, $json=false)
	{
		$this->sale_lib->clear_all();
		//$sale_id = $this->Sale->get_sale_id($sale_number);
		$sale_info = $this->Sale_suspended->get_info($sale_id)->row_array();
		$this->sale_lib->copy_entire_suspended_sale($sale_id);
		$data['cart']=$this->sale_lib->get_cart();
		$data['payments']=$this->sale_lib->get_payments(0);
		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['taxes']=$this->sale_lib->get_taxes(false, true);
		$data['total']=$this->sale_lib->get_total(false, true);
		$data['receipt_title']=lang('sales_receipt');
		$data['transaction_time']= date(get_date_format().' '.get_time_format(), strtotime($sale_info['sale_time']));
		$customer_id=$this->sale_lib->get_customer();
		$emp_info=$this->Employee->get_info($sale_info['employee_id']);
		$data['payment_type']=$sale_info['payment_type'];
		$data['amount_change']=to_currency($this->sale_lib->get_amount_due($sale_id) * -1);
		$data['amount_due']=to_currency($this->sale_lib->get_amount_due(false, true));
		$data['replace_newline'] = true;
		$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;

		if($customer_id!=-1)
		{
			$cust_info=$this->Customer->get_info($customer_id);
			$data['customer']=$cust_info->first_name.' '.$cust_info->last_name.($cust_info->company_name==''  ? '' :' ('.$cust_info->company_name.')');
		}
		$data['sale_id']='POS '.$sale_id;
		//$data['sale_number']='POS '.$sale_info['sale_number'];
		if ($json){
			$this->sale_lib->clear_all();
			// SECTIONS TO UPDATE
			$data['register_box'] =	$this->load->view("sales/register_box", array(), true);
			//$data['table_top'] = $this->load->view("sales/table_top", array("mode" =>'sales'), true);
			$data['customer_info_filled'] = $this->load->view("sales/customer_info_filled", array(), true);
			$data['payment_window'] = $this->load->view("sales/payment_window", array(), true);
			$data['suspend_button_title'] = lang('sales_suspend_sale');
			$data['basket_info'] = $this->sale_lib->get_basket_info();
			$data['cart_payments'] = $this->load->view("sales/payments_list", array(), true);
			echo json_encode($data);
		}
		else{
			$this->load->view("sales/receipt",$data);
			$this->sale_lib->clear_all();
		}
		if (count($this->sale_lib->get_cart())==0)
        {
            $this->sale_lib->set_teetime(-1);
			$this->session->unset_userdata('purchase_override');
        	$this->session->unset_userdata('purchase_override_time');
        }
	}
	function unsuspend($sale_id = false)
	{
		if ($sale_id)
		{
			//$sale_id = $this->input->post('suspended_sale_id');
			$this->sale_lib->clear_all();
			$this->sale_lib->copy_entire_suspended_sale($sale_id);
			$this->sale_lib->set_suspended_sale_id($sale_id);
			//$this->Sale_suspended->delete($sale_id);
	    	$this->_reload($data, "unsuspend");
		}
	}
	function update_cart() {
        $item_id = '';
        $attribute = '';
        $value = '';
        $course_id = $this->session->userdata('course_id');
        $teesheet_id = $this->session->userdata('teesheet_id');
        $item_number = '';

        $line = $this->input->post('line');
        $checked = $this->input->post('checked');
        if ($this->input->post('price') !== false) {
            $attribute = 'price';
            $value = $this->input->post('price');
        }
        else if ($this->input->post('quantity') !== false) {
            $attribute = 'quantity';
            $value = $this->input->post('quantity');
        }
        else if ($this->input->post('discount') !== false) {
            $attribute = 'discount';
            $value = $this->input->post('discount');
        }
        else if ($this->input->post('item_id')) {
            $item_id = $this->input->post('item_id');
        }
        else if ($this->input->post('item_number')){
            $attribute = 'item_number';

            $teetime_type = substr($this->input->post('item_number'), strrpos($this->input->post('item_number'), '_') + 1);
            if($this->config->item('seasonal_pricing') == 1){
				$item_number = $course_id.'_seasonal_'.$teetime_type;
			}else{
				$item_number = $course_id.'_'.$teetime_type;
			}
			$item_id = $this->Item->get_item_id($item_number);
        }

        if ($checked == 'true')
            $this->sale_lib->edit_basket_item_attribute($line, $attribute, $value, $item_id);
        echo json_encode(array("item_info"=>$this->sale_lib->edit_item_attribute($line, $attribute, $value, $item_id), 'basket_info'=>$this->sale_lib->get_basket_info()));
    }
    function update_basket(){
        $line = $this->input->post('line');
        $checked = $this->input->post('checked');
        if ($line == 'all') {
            if ($checked == 'true')
                $this->sale_lib->put_cart_into_basket();
            else
                $this->sale_lib->empty_basket();
        }
        else {
            if ($checked == 'true')
                $this->sale_lib->copy_item_into_basket($line);
            else
                $this->sale_lib->delete_item_from_basket($line);
        }

        echo json_encode($this->sale_lib->get_basket_info());
    }
    function delete_cart_items() {
        $line_array = $this->input->post('line_array');
        $cart_data = $this->sale_lib->get_cart();
        $basket_data = $this->sale_lib->get_basket();
        //print_r($line_array);
        foreach($line_array as $line) {
            $index = str_replace('select_', '', $line);
            unset($cart_data[$index]);
            unset($basket_data[$index]);
        }
        $this->sale_lib->set_cart($cart_data);
        $this->sale_lib->set_basket($basket_data);
    }
	function generate_stats()
	{
		$results = $this->Dash_data->fetch_pos_data();
		echo json_encode($results);
	}
	function email_reciept_window($sale_id) {
		$data['sale_id'] = $sale_id;
		$cust_info = $this->Sale->get_customer($sale_id);
		if (!empty($cust_info->email)) {
			$data['customer_email'] = $cust_info->email;
		} else {
			$data['customer_email'] = '';
		}
		$this->load->view('sales/email_receipt', $data);
	}
    
    //Update Raincheck data
    function update_raincheck($raincheck_id) {
        $data = $this->Sale->raincheck_info($raincheck_id)->row_array();
        $data['expiry_date'] = $this->input->post('expiry_date');
        $data['customer_id'] = $this->input->post('raincheck_customer_id');
        $data['green_fee'] = $this->input->post('green_fee');
        $data['cart_fee'] = $this->input->post('cart_fee');
        $data['tax'] = $this->input->post('tax_val');
        $data['total'] = $this->input->post('total_value');
        $success = $this->Sale->update_raincheck($data, $raincheck_id);
        if ($success) {
            echo json_encode(array('success' => true, 'message' => lang('raincheck_successfully_edit')));
        } else {
            echo json_encode(array('success' => false, 'message' => lang('raincheck_unsuccessfully_edit')));
        }
    }

    //Raincheck view for edit
    function raincheck_view($raincheck_id = -1) {
        $data = $this->Sale->raincheck_info($raincheck_id)->row_array();
        $data['receipt_title'] = 'Raincheck'; //lang('sales_raincheck');
        $data['transaction_time'] = date(get_date_format() . ' ' . get_time_format(), strtotime($data['date_issued']));
        $emp_info = $this->Employee->get_info($data['employee_id']);
        $data['employee'] = $emp_info->first_name . ' ' . $emp_info->last_name;
        $cust_info = $this->Customer->get_info($data['customer_id']);
        $data['customer'] = $cust_info->first_name . ' ' . $cust_info->last_name;
        $this->load->view("reports/raincheck_edit", $data);
    }

}
?>
