<?php
function get_quickbutton_menu ($type='ul'){
	$CI =& get_instance();
	
	$items = $CI->Item->get_all(10000,0,true,true)->result_array();
	if ($type=='ul')
	{
		$quickbutton_array = array();
		$current_category = '';
		$current_subcategory = '';
		$qb_html = "<ul class='sf-menu'>";
		$counter = 0;
		foreach ($items as $item) {
			if ($current_category != $item['category']) {
				if ($current_subcategory != '')
					$qb_html .= "</ul></li>";
				if ($current_category != '')
					$qb_html .= "</ul></li>";
				$qb_html .= "<li><a href='index.php/sales#'>{$item['category']}</a><ul>";//starting the new category level
				if ($item['subcategory'] != '') 
					$qb_html .= "<li><a href='index.php/sales#'>{$item['subcategory']}</a><ul>";//starting new subcategory if needed
						$counter++;
		
			}
			else if ($current_subcategory != $item['subcategory']) {
				if ($current_subcategory != '')
					$qb_html .= "</ul></li>";
				if ($item['subcategory'] != '') 
					$qb_html .= "<li><a href='index.php/sales#'>{$item['subcategory']}</a><ul>";//starting the new subcategory level
			}
			if ($item['subcategory'] != '') 
				$qb_html .= "<li><a href='index.php/sales/add/{$item['item_id']}'>{$item['name']}</a></li>";//inserting value, whether under category or subcategory
			else 
				$qb_html .= "<li><a href='index.php/sales/add/{$item['item_id']}'>{$item['name']}</a></li>";//inserting value, whether under category or subcategory
			
			$current_category = $item['category'];
			$current_subcategory = $item['subcategory'];
		}
		if ($current_category != '')
			$qb_html .= "</ul></li>";
		if ($current_subcategory != '')
			$qb_html .= "</ul></li>";
		$qb_html .= "</ul>";
		$final_html = $qb_html;
	}
	else if ($type=='tabs')
	{
		$current_category = '';
		$current_subcategory = '';
		$header_html = '';
		$subheader_html = '';
		$subquickbuttons = '';
		$quickbuttons = '';
		$counter = 0;
		$js_html = '';
		$subcat_counter = 0;
		$qb_html = "";
		$sqb_html = "";
		foreach ($items as $item) {
			if ($current_category != $item['category']) {
				if ($current_category != '') {//End of a category
					if ($current_subcategory != '') {//End of a category and therefore subcategory
						$sqb_html .= "<div id='tabs-$counter-$subcat_counter'>$quickbuttons</div>";
						$qb_html .= "<div id='tabs-$counter'><div id='subtabs-$counter'><ul>".$subheader_html."</ul>".$sqb_html."</div></div>";
						$js_html .= "$('#subtabs-$counter').tabs();";
						$quickbuttons = '';
						$subheader_html = "";
						$sqb_html = "";
						$subcat_counter = 0;
					}
					else {
						$qb_html .= "<div id='tabs-$counter'>".$quickbuttons."</div>";
						$quickbuttons = "";
					}
				}
				$counter++;
				$header_html .= "<li><a href='#tabs-$counter'>{$item['category']}</a></li>";//starting the new category level
				if ($item['subcategory'] != '') {
					$subcat_counter++;
					$subheader_html .= "<li><a href='#tabs-$counter-$subcat_counter'>{$item['subcategory']}</a></li>";//starting new subcategory if needed
				}
			}
			else if ($current_subcategory != $item['subcategory']) {
				if ($current_subcategory != '') {//End of a subcategory
					$sqb_html .= "<div id='tabs-$counter-$subcat_counter'>$quickbuttons</div>";
					$quickbuttons = "";
				}
				if ($item['subcategory'] != '') {//Starting new subcategory
					$subcat_counter++;
					$subheader_html .= "<li><a href='#tabs-$counter-$subcat_counter'>{$item['subcategory']}</a></li>";//starting the new subcategory level
				}
			}
			if ($item['subcategory'] != '') 
				$quickbuttons .= "<a href='index.php/sales/add/{$item['item_id']}'>{$item['name']}</a>";//inserting value, whether under category or subcategory
			else 
				$quickbuttons .= "<a href='index.php/sales/add/{$item['item_id']}'>{$item['name']}</a>";//inserting value, whether under category or subcategory
			
			$current_category = $item['category'];
			$current_subcategory = $item['subcategory'];
		}
		//if ($current_category != '')
		if ($current_subcategory != '') {
			$sqb_html .= "<div id='tabs-$counter-$subcat_counter'>$quickbuttons</div>";
			$qb_html .= "<div id='tabs-$counter'><div id='subtabs-$counter'><ul>".$subheader_html."</ul>".$sqb_html."</div></div>";
			$js_html .= "$('#subtabs-$counter').tabs();";
		}
		else {
			$qb_html .= "<div id='tabs-$counter'>$quickbuttons</div>";
		}
		$qb_html .= "</ul>";
	}
	$js_html .= "$('#tabs').tabs();";
	$final_html = "<div id='tabs'><ul>".$header_html."</ul>".$qb_html."</div><script type='text/javascript'>$(document).ready(function() {{$js_html}});</script>";
	return $final_html;
}
