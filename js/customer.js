var customer = {
	delete_household_member: function(person_id){
		$('#member_row_'+person_id).remove();
	},
	add_household_member: function(person_id, name){
		var household_member_html = '<div class="field_row clearfix" id="member_row_'+person_id+'">'+
				'<label for="household_member_notice"><span onclick="customer.delete_household_member('+person_id+')">x</span></label>'+
				'<div class="form_field">'+
				name+
				'<input type="hidden" id="household_member_id[]" name="household_member_id[]" value="'+person_id+'">'+
				'</div>'+
			'</div>';
		$('#household_members').append(household_member_html);
		console.log('added member');
	},
	billing : {
		load_color_box: function(customer_id, customer_name){
			$.colorbox({'href':'index.php/customers/billing/'+customer_id+'/width~1200', 'title':"Member Billing <input id='customer_name' type='text' placeholder='customer' value='"+customer_name+"'/>",
				onComplete:function(){
					customer.billing.initialize_customer_search();
				}
			});
		},
		initialize_customer_search: function () {
			if ($('#customer_name').val() === 'undefined')	$('#customer_name').val('');
			$("#customer_name").click(function(e){e.preventDefault(); $(this).select();})
			$( "#customer_name" ).autocomplete({
				source: 'index.php/customers/customer_search/last_name',
				delay: 200,
				autoFocus: false,
				minLength: 1,
				select: function(event, ui)
				{
					customer.billing.load_color_box(ui.item.value, ui.item.label);
				}
			});
		},
		initialize_invoice_table_controls:function(){
			$('.line_value','.line_item').blur(function(){
				var new_value = 0;

				new_value = parseFloat($(this).val());
				new_value = !isNaN(new_value) ? new_value : 0;
				//make sure the value has two decimal places unless its the quantity
				if (!$(this).hasClass('quantity')){
					$(this).val((new_value).toFixed(2));
				}

				customer.billing.update_invoice_totals();
			});

			$("input.item-search").autocomplete({
				source: 'index.php/sales/item_search',
				delay: 200,
				autoFocus: false,
				minLength: 1,
				select: function(event, ui)
				{
					var item_id = ui.item.value;
					var row = $(this).parents('tr:first');
					var search_field = $(this);

					if(item_id.indexOf('KIT') > -1){
						var type = 'item_kit';
						item_id = item_id.split(' ')[1];
					}else{
						var type = 'item';
					}

					$.get('index.php/customers/get_item_info/' + type +'/'+ item_id, null, function(response){
						row.find('input.price.line_value').val(response.unit_price);
						row.find('input.tax.line_value').val('').attr('disabled', 'disabled');
						row.find('input.item-type').val(type);
						row.find('input.item-id').val(item_id);
						row.attr('data-taxes', JSON.stringify(response.taxes));
						search_field.val(response.name);
						customer.billing.update_invoice_totals();
					},'json');

					event.preventDefault();
				}
			});

			$('.delete_row').click(function(){
				customer.billing.remove_row(this);
				customer.billing.update_invoice_totals();
			});
		},
		initialize_controls:function(){
			$('#add_item').click(function(e){
				customer.billing.add_row();
				e.preventDefault();
			});

			$('#add_customer_email').click(function(){
				var href = $(this).attr('data-href');

				$.colorbox2({'href':href, 'title':'Add Email',
					onComplete:function(){
						$('#new_email').select();
					}
				});
			});

			customer.billing.initialize_customer_search();

			// $('#add_credit_card_link').unbind('click').click(function(){
				// $('#invoice_form').submit();
			// });
			$('#add_credit_card_link').colorbox2({'width':712,'height':610});

			$('#frequency_selector').unbind('change').change(function(){
				$('#frequency').val($(this).val());
				$('#month_range').toggle();
				$('#bill_month').toggle();
			});

			//disable second row options when necessary so that tabbing over doesn't take you to the second row
			if ($('#invoice_time_options', '#billing_info').hasClass('advanced')){
					$(':input', '#second_row_options').prop('disabled',false);
			} else {
				$(':input', '#second_row_options').prop('disabled',true);
			}

			var submitting = false;
			$('#invoice_form').validate({
				submitHandler:function(form)
				{
					var is_visible = true,
						billing_title = $('#billing_title').val();
						is_visible = $('#billing_title').is(':visible');
					if (!billing_title && is_visible){
						set_feedback('Billing Title is required','error_message',false);
						$('#billing_title').select();
						return;
					}
					frequency_period = $('#frequency_period').val();
					recur_days = $('#frequency').val();
					if (frequency_period == 'day' && recur_days > 30){	
						set_feedback('Days must be less than 30','error_message',false);
						return;
					}
					var missing_description;
					$('input[name=description[]]').each(function(){

						if (!$(this).val()){
							set_feedback('A description for each line item is requred','error_message',false);
							$(this).select();
							missing_description = true;
						}
					});
					if (submitting || missing_description) return;
					submitting = true;
					$(form).mask('Please wait');
					$(form).ajaxSubmit({
						success:function(response)
						{
							var customer_id = $('#customer_id').val();
							var customer_name = $('#customer_name').val();
							customer.billing.load_color_box(customer_id, customer_name);
			                submitting = false;
						},
						dataType:'json'
					});
				},
				errorLabelContainer: "#error_message_box",
		 		wrapper: "li",
				rules:
				{
				},
				messages:
				{
		    	}
			});
			$("#save_invoice").unbind('click').click(function(){

				if ($(this).text() === 'Save'){
					//re enable all controls before saving or their values will be lost
					$(':input', '#second_row_options').prop('disabled',false);
					$('#invoice_form').submit();
				} else
				{
					$('#billing_info').hide();
					$('#invoice_creation_options').show();
					$('.loaded_invoice').removeClass('loaded_invoice');
					$.colorbox.resize();
				}
			});

			$('#pay_member_balance').click(function(){
				var type = 'member';

				if ($(this).attr('checked')){
					if (!$(this).hasClass('is_auto')){
						customer.invoice.add_account_balance_row(type);
					}
					else{
						customer.billing.add_account_balance_row(type);
					}
				} else {
					customer.billing.remove_balance_row(type);
				}
			});

			$('#pay_account_balance').click(function(){
				var type = 'customer';

				if ($(this).attr('checked')){
					if (!$(this).hasClass('is_auto')){
						customer.invoice.add_account_balance_row(type);
					}
					else{
						customer.billing.add_account_balance_row(type);
					}
				} else {
					customer.billing.remove_balance_row(type);
				}
			});

			$('#advanced_options').unbind('click').click(function(){
				console.log('clicking advanced_options');
				$('#invoice_time_options', '#billing_info').toggleClass('advanced');
				if ($('#invoice_time_options', '#billing_info').hasClass('advanced')){
					$(':input', '#second_row_options').prop('disabled',false);
				} else {
					$(':input', '#second_row_options').prop('disabled',true);
				}

				$.colorbox.resize();
			});
			customer.billing.initialize_invoice_table_controls();
		},
		calculate_tax: function(subtotal, taxes){
			var total = 0.0;

			$.each(taxes, function(index, val){
				if(val.percent){
					total += subtotal * (val.percent / 100);
				}
			});

			return total;
		},
		update_invoice_totals:function()
		{
			var row = 0,
				tax_total = 0,
				quantity = 0,
				price = 0,
				row_total = 0,
				subtotal = 0,
				total_with_tax = 0,
				new_sub = 0,
				new_total_with_tax = 0;

			//loop through line items to update the line total and add to subtotal and total with tax
			$('.line_item').each(function(){

				row = $(this);

				var line_quantity = $('.quantity',row).val();
				var line_price = $('.price',row).val();
				line_price = !isNaN(line_price) ? line_price: 0;
				if (! line_price > 0) line_price = 0;
				var line_subtotal = line_quantity * line_price;

				console.debug( $('.price', row) );

				var line_taxes = eval(row.attr('data-taxes'));
				if(!line_taxes){
					line_taxes = [];
					line_taxes[0] = {'percent': $('.tax',row).val(), 'cumulative':false };
				}

				var line_tax_amount = customer.billing.calculate_tax(line_subtotal, line_taxes);

				$('.row_total_col', row).html('$' + line_subtotal.toFixed(2));

				new_sub = parseFloat(line_subtotal);
				new_tax = parseFloat(line_tax_amount);

				subtotal += new_sub;
				tax_total += new_tax;
			});

			total = subtotal + tax_total;

			$('#current_subtotal').html('$' + subtotal.toFixed(2));
			$('#current_tax').html('$' + tax_total.toFixed(2));
			$('#current_total_due').html('$' + total.toFixed(2));

			$('#customer_current_charges').html('$' + total.toFixed(2));
			$('#customer_total_due').html('$' + total.toFixed(2));
		},
		create:function(type, person_id) {
			//Show invoice
			$('#invoice_creation_options').hide();
			$('#billing_info').show();

			//Show correct options
			if (type == 'recurring_billing')
			{
				var form_url = SITE_URL + '/customers/save_recurring_invoice';
				this.load(-1, person_id);
				$('#frequency').val('monthly');
			}
			else if (type == 'invoice')
			{
				var form_url = SITE_URL + '/customers/save_invoice';
				customer.invoice.load(-1, person_id);
				$('#frequency').val('one_time');
			}

			this.update_invoice_time_options();
			$.colorbox.resize();

			$('#invoice_form').attr('action', form_url);
			$('#billing_base_url').val(form_url);
		},
		load:function(billing_id, person_id) {
			$('#invoice_creation_box').mask();
			$.ajax({
	            type: "POST",
	            url: "index.php/customers/load_billing/"+billing_id+"/"+person_id,
	            data: '',
	            success: function(response){

	           		$('#invoice_creation_box').unmask();
					if (typeof response.start_date === 'undefined')	response.start_date = '';
	           		$('#start_date').val(response.start_date);

					if(response.end_date){
						$('#end_date').val(response.end_date);
					}else{
						$('#end_on_never').trigger('click');
					}

					$('#billing_title').val(response.title);
					$('#due_days').val(response.due_days);
	           		$('#frequency').val(response.frequency);
	           		$("#receipt_wrapper").replaceWith(response.html);

	           		change_frequency_period(response.frequency_period);
					change_frequency_on(response.frequency_on);
					$("#frequency_period").val(response.frequency_period);
					$('select.frequency_on.' + frequency_period).val(response.frequency_on);

					if(response.frequency_on_date){
						$("#frequency_on_date").val(response.frequency_on_date);
					}

					if(response.end_date){
						$("#end_date").val(response.end_date);
						select_end_on('date');
					}else{
						$("#end_date").val('');
						select_end_on('never');
					}

	           		customer.billing.update_invoice_time_options();
	           		$('#invoice_creation_options').hide();

					$('#billing_info').show();

					var url = $('#billing_base_url').val();
					$('#invoice_form').attr('action', url+'/'+billing_id);
					customer.billing.initialize_controls();
					$('#save_invoice').html('Save');
					$('#recurring_settings').show();
					$('#recurring_title').show();
					$.colorbox.resize();
			    },
	            dataType:'json'
	        });
		},
		save:function() {

		},

		remove:function(type, billing_id){
			$.ajax({
	            type: "POST",
	            url: "index.php/customers/delete_billing/"+billing_id,
	            data: '',
	            success: function(response){
	            	var count = $(".recurring_billing_box_holder > div").length;
	            	if ((count === 0)){
	            		customer.billing.add_no_billing_or_invoice_placeholder('billing');
	            	}
	        		$('#billing_info').hide();
	        		$('#invoice_creation_options').show();
	        		$.colorbox.resize();
			    },
	            dataType:'json'
	        });
		},

		update_invoice_time_options:function () {
			var frequency = $('#frequency').val();
			var time_option_box = $('#time_components');
			var month_range = $('#month_range');
			var bill_month = $('#bill_month');
			if (frequency == 'one_time')
			{
				time_option_box.hide();
			}
			else if (frequency == 'monthly')
			{
				month_range.show();
				bill_month.hide();
				time_option_box.show();
			}
			else if (frequency == 'yearly')
			{
				month_range.hide();
				bill_month.show();
				time_option_box.show();
			}
		},

		add_account_balance_row:function(type){
			var description, id, item_type;

			if (type === 'member'){
				description = window.billing_config.member_balance_nickname;
				id = 'member_balance_row';
				item_type = 'member_balance';
			}else{
				description = window.billing_config.customer_credit_nickname;
				id = 'customer_balance_row';
				item_type = 'customer_balance';
			}
			var index = $('tr.line_item').length + 1;
			var row_html = "<tr class='line_item' id="+id+"><td class='name_col' style='padding: 4px 8px; border:1px solid #060606;'>"+description+
					"<input type='hidden' name='items["+index+"][description]' value='"+description+"' /><input type='hidden' name='items["+index+"][item_type]' value='"+item_type+"' /></td>" +
				"<td class='qty_col' style='padding: 4px 8px; border:1px solid #060606;'>1</td>" +
				"<td class='price_col' style='padding: 4px 8px; border:1px solid #060606; text-align:right;'>$--.--</td>" +
				"<td class='tax_col' style='padding: 4px 8px; border:1px solid #060606; text-align:right;'>$--.--</td>" +
				"<td class='row_total_col' style='padding: 4px 8px; border:1px solid #060606; text-align:right;'>$--.--</td></tr>";

			$('#item_header').after(row_html);
			$.colorbox.resize();
			customer.billing.initialize_invoice_table_controls();
		},
		remove_balance_row:function(type){
			var id;

			if (type === 'member'){
				id = '#member_balance_row';
			}else{
				id = '#customer_balance_row';
			}
			$(id).remove();
			customer.billing.update_invoice_totals();
		},
		add_row:function(cc_id){
			var index = $('tr.line_item').length + 1;
			var row_html = "<tr class='line_item'>" +
				"<td class='name_col'>" +
					"<span class='symbol delete_row'>x</span><input class='item-search' name='items["+ index +"][description]' placeholder='Description' value='' />" +
					"<input class='item-id' type='hidden' value='' name='items["+ index +"][item_id]' />" +
					"<input class='item-type' type='hidden' value='' name='items["+ index +"][item_type]' />" +
				"</td>" +
				"<td class='qty_col'>" +
					"<input class='quantity line_value' name='items["+ index +"][quantity]' placeholder='Qty' value='1'  />" +
				"</td>" +
				"<td class='price_col' style='border:1px solid #606060;'>" +
					"<span class='symbol'>$</span>" +
					"<input class='price line_value' name='items["+ index +"][price]' placeholder='0.00' value='' />" +
				"</td>" +
				"<td class='tax_col' style='border:1px solid #606060;'>" +
					"<input class='tax line_value' name='items["+ index +"][tax_percentage]' placeholder='0.0' value=''/>" +
					"<span class='symbol'>%</span>" +
				"</td>" +
				"<td class='row_total_col'>$0.00</td>"+
			"</tr>";

			$('#current_subtotal_row').before(row_html);
			customer.billing.initialize_invoice_table_controls();
		},

		remove_row: function(link, type)
		{
			$(link).parent().parent().remove();
		},

		add_no_billing_or_invoice_placeholder:function(type){
			var html, container;
			if (type === 'billing'){
				html = "<div class='recurring_billing_box no_results'>No Recurring Billings</div>";
				container = ".recurring_billing_box_holder";
			} else if (type === 'invoice') {
				html = "<div class='invoice_box no_results'>No Invoices</div>";
				container = ".invoice_box_holder";
			}

			$(container).append(html);
		}
	},
	invoice:{
		load:function (invoice_id, person_id) {
			$('#invoice_creation_box').mask();
			var URL = "index.php/customers/load_invoice/"+invoice_id+"/"+person_id;
			$.ajax({
	            type: "POST",
	            url: URL,
	            data: '',
	            success: function(response){
	            	$('#invoice_creation_box').unmask();
					var button_text = 'Save';
	           		$('#frequency').val('one_time');
	           		$("#receipt_wrapper").replaceWith(response.html);
	           		customer.billing.update_invoice_time_options();
	           		$('#invoice_creation_options').hide();
					$('#billing_info').show();
					customer.billing.initialize_controls();
					if (invoice_id > 0 ){
						button_text = 'Close';
					}
					var url = $('#billing_base_url').val();
					$('#invoice_form').attr('action', url+'/'+invoice_id+"/"+person_id);
					$('#save_invoice').html(button_text);
					$('#recurring_settings').hide();
					$('#recurring_title').hide();
					$.colorbox.resize();
			    },
			    error: function(result){
			    },
	            dataType:'json'
	        });
		},
		remove:function(type, invoice_id){
			$.ajax({
	            type: "POST",
	            url: "index.php/customers/delete_invoice/"+invoice_id,
	            data: '',
	            success: function(response){
	        		$('#billing_info').hide();
	        		$('#invoice_creation_options').show();
	        		var count = $(".invoice_box_holder > div").length;
	            	if ((count === 0)){
	            		customer.billing.add_no_billing_or_invoice_placeholder('invoice');
	            	}
	        		$.colorbox.resize();
			    },
	            dataType:'json'
	        });
		},
		add_account_balance_row:function(type){

			var description, id, amount, item_type,
				cp = window.billing_config.cp,
				bc = window.billing_config.bc;

			if (type === 'member'){
				description = window.billing_config.member_balance_nickname;
				id = 'member_balance_row';
				item_type = 'member_balance';
				amount = -parseFloat(window.billing_config.member_balance).toFixed(2);
			}else{
				description = window.billing_config.customer_credit_nickname;
				id = 'customer_balance_row';
				item_type = 'customer_balance';
				amount = -parseFloat(window.billing_config.customer_credit).toFixed(2);
			}
			amount = (amount < 0 ? 0 : amount);
			var index = $('tr.line_item').length + 1;

			var row_html = "<tr class='line_item' id="+id+"><td class='name_col' style='padding: 4px 8px; border:1px solid #060606;'>"+ description +
				"<input type='hidden' name='items["+index+"][description]' value='"+description+"' /><input type='hidden' name='items["+index+"][item_type]' value='"+item_type+"' /></td>" +
				"<td class='qty_col' style='padding: 4px 8px; border:1px solid #060606;'>1<input type='hidden' class='quantity' value='1' /></td>" +
				"<td class='price_col' style='padding: 4px 8px; border:1px solid #060606; text-align:right;'>$"+amount+" <input type='hidden' name='items["+index+"][price]' class='price' value='"+amount+"' /></td>" +
				"<td class='tax_col' style='padding: 4px 8px; border:1px solid #060606; text-align:right;'>-- <input type='hidden' name='items["+index+"][tax]' class='tax' value='0' /></td>" +
				"<td class='row_total_col' style='padding: 4px 8px; border:1px solid #060606; text-align:right;'>$"+amount+"</td></tr>";
			$('#item_header').after(row_html);

			$.colorbox.resize();
			customer.billing.initialize_invoice_table_controls();
			customer.billing.update_invoice_totals();
		}
	}
};
