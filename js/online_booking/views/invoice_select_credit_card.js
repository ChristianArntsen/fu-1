var InvoiceSelectCreditCardView = Backbone.View.extend({
	
	id: 'select-credit-card',
	className: 'modal-dialog',
	template: _.template( $('#template_invoice_select_credit_card').html() ),
	
	events: {
		'click .btn.pay': 'pay',
		'click .add-card': 'addCard'
	},
	
	initialize: function(options){
		var view = this;
		$(document).on('hidden.bs.modal', function(){
			view.close();
		});
		this.creditCards =  App.data.user.get('credit_cards');
		
		this.listenTo(App.vent, 'invoice:error', this.showError);
		
		if(options && options.selected_card){
			if(this.creditCards.get(options.selected_card)){
				this.creditCards.get(options.selected_card).set({'selected':true});
			}
		}
	},
	
    close: function() {
		this.remove();
    },

	showError: function(model, response, options){
		this.$el.find('#invoice-payment-error').text(response.responseJSON.msg).show();
	},

    render: function() {
		var attributes = this.model.attributes;
		attributes.credit_cards = this.creditCards;

		var html = this.template(attributes);
		this.$el.html(html);
		$('#modal').html(this.el);
		return this;
    },
    
    show: function(){
		this.render();
		$('#modal').modal();
	},
	
	// Display credit card authorization iframe
	addCard: function(event){
		var addCardView = new AddCreditCardView({model: this.model});
		addCardView.show();
		return false;
	},
	
	// Pay invoice using selected credit card
	pay: function(event){
		
		$(event.currentTarget).button('loading');
		
		var model = this.model;
		var data = {};
		data.credit_card_id = this.$el.find('#invoice_credit_card').val();
		data.amount = _.round(this.model.get('total') - this.model.get('paid'));
		
		$.ajax({
			url: this.model.url() + '/payments',
			type: 'POST',
			data: JSON.stringify(data),
			contentType: 'application/json',
			success: function(response){
				model.set(response);
				var details = new InvoiceDetailsView({model: model});
				details.show();
			},
			error: function(response){
				App.vent.trigger('invoice:error', model, response);
			},
			dataType: 'json'
		});
		
		return false;
	}
});
