var TeeTimeLoginView = Backbone.View.extend({
	id: 'teetime-login',
	className: '',
	template: _.template( $('#template_tee_time_log_in').html() ),
	
	events: {
		'click .btn.login': 'login',
		'click .btn.register': 'register',
		'click a.change-class': 'changeClass'
	},

	render: function(){
		var attributes = {};
		var schedule = App.data.schedules.findWhere({selected:true});
		attributes.booking_class_name = schedule.get('booking_classes').get( App.data.filters.get('booking_class') ).get('name');
		
		this.$el.html( this.template(attributes) );
	},
	
	login: function(event){	
		event.preventDefault();
		var view = new LoginView();
		view.show();
	},
	
	register: function(event){
		event.preventDefault();		
		var view = new LoginView();
		view.show('signup');
	},
	
	changeClass: function(event){
		event.preventDefault();	
		App.data.filters.set('booking_class', false);
		App.router.teetimes();		
	}
});
