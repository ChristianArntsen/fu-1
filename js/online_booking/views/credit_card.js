var CreditCardView = Backbone.View.extend({
	tagName: 'li',
	className: 'credit-card',
	template: _.template( $('#template_credit_card').html() ),
	
	events: {
		'click .delete': 'deleteCreditCard'
	},
	
	render: function(){
		this.$el.html(this.template(this.model.attributes));
		return this;
	},		
	
	deleteCreditCard: function(){
		if(confirm("Are you sure you want to delete this card?")){
			this.$el.find('button').button('loading');
			this.model.destroy({wait: true});
		}
	}
});

var CreditCardListView = Backbone.View.extend({
	tagName: 'ul',
	className: 'credit-cards',
	
	initialize: function(){
		this.listenTo(this.collection, 'add remove reset', this.render);
	},
	
	render: function(){
		var self = this;
		if(!this.$el.html()){
			this.$el.html('');
		}
		this.$el.html('');
		
		if(this.collection.length == 0){
			this.renderEmpty();
		}else{
			_.each(this.collection.models, this.renderCard, this);
		}

		return this;
	},
	
	renderEmpty: function(){
		this.$el.append('<li class="credit-card empty muted">No credit cards available</li>');
	},
	
	renderCard: function(card){
		this.$el.append( new CreditCardView({model: card}).render().el );
	}
});
