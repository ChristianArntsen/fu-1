var TimeView = Backbone.View.extend({
	tagName: 'li',
	className: 'time col-md-4 col-sm-6 col-xs-12',
	template: _.template( $('#template_time').html() ),
	
	events: {
		'click': 'viewTime'
	},
	
	render: function(){
		this.$el.html(this.template(this.model.attributes));
		return this;
	},
	
	// Adds the selected teetime to cart
	viewTime: function(){
		
		var attributes = _.clone(this.model.attributes);
		attributes.players = App.data.filters.get('players');
		attributes.holes = App.data.filters.get('holes');
		
		// Create new reservation defaulted to filter settings
		var reservation = new Reservation(attributes);

		// Show modal window with new time
		var bookTimeView = new BookTimeView({model: reservation});
		bookTimeView.show();
	}
});

var TimeListView = Backbone.View.extend({
	tagName: 'ul',
	className: 'col-md-12',
	id: 'times',
	
	initialize: function(){
		this.listenTo(this.collection, "reset", this.render);
		this.listenTo(App.data.status, "change:loading", this.toggleLoading);
	},
	
	toggleLoading: function(model){
		if(model.get('loading')){
			this.$el.prepend('<li class="loading"><h1>Loading Tee times...</h1><img src="'+URL+'/images/loading_golfball2.gif" /></li>');
		}else{
			this.$el.find('li.loading').remove();
		}
	},
	
	render: function() {
		var self = this;
		if(!this.$el.html()){
			this.$el.html('');
		}
		this.$el.html('');
		
		if(this.collection.models.length == 0){
			this.renderEmpty();
		}else{
			_.each(this.collection.models, this.renderTime, this);
		}
		this.toggleLoading( App.data.status );
		return this;
	},
	
	renderEmpty: function(){
		this.$el.append('<li class="time empty muted"><h1>No tee times available</h1><p>Try a different time or day</p></li>');
	},
	
	renderTime: function(time){
		this.$el.append( new TimeView({model: time}).render().el );
	}
});

var BookTimeView = Backbone.View.extend({
	
	id: 'book_time',
	className: 'modal-dialog',
	template: _.template( $('#template_book_time').html() ),
	
	events: {
		'click .book': 'completeBooking',
		'click .players': 'setPlayers',
		'click .carts': 'setCarts'			
	},
	
	initialize: function(){
		var view = this;
		$(document).on('hidden.bs.modal', function(){
			view.close();
		});
		
		this.listenTo(this.model, 'change:total', this.renderPrice);
		this.listenTo(this.model, 'invalid', this.validationError);
		this.listenTo(App.vent, 'error', this.showError);			
	},
	
    close: function() {
		this.remove();
    },

    render: function() {
		var attributes = this.model.attributes;	
		var schedule = App.data.schedules.findWhere({selected:true});
		
		attributes.reservation_time = moment(attributes.time).format('h:mma');
		attributes.reservation_day = moment(attributes.time).format('MMM D, YYYY');	
		attributes.booking_carts = App.settings.booking_carts;
		attributes.minimum_players = parseInt(App.settings.minimum_players);
		
		var html = this.template(attributes);
		this.$el.html(html);
		$('#modal').html(this.el);
		ga('send', 'event', 'Online Booking', 'View Tee Time', App.data.course.getCurrentSchedule());
		return this;
    },
    
    renderPrice: function(){
		var total = this.model.get('total');
		this.$el.find('span.total').text( accounting.formatMoney(total) );
	},

    show: function(){
		this.render();
		$('#modal').modal();
	},
	
	showError: function(model, response, options){
		this.$el.find('#booking-error').text(response.responseJSON.msg).show();
	},	
	
	setPlayers: function(event){
		var btn = $(event.target);
		btn.addClass('active').siblings().removeClass('active');
		var value = btn.data('value');
		this.model.set('players', value);	
	},
	
	setCarts: function(event){
		var btn = $(event.target);
		btn.addClass('active').siblings().removeClass('active');
		var value = btn.data('value');
		
		var carts = false;
		if(value == 'yes'){
			carts = true;
		}
		
		this.model.set('carts', carts);		
	},
	
	setHoles: function(event){
		var btn = $(event.target);
		btn.addClass('active').siblings().removeClass('active');
		var value = btn.data('value');
		this.model.set('holes', value);	
	},	
	
	validationError: function(model){
		
		// If user is not logged in yet, show log in window
		if(model.validationError == 'log_in_required'){
			App.vent.trigger('unauthenticated', this.model);
		}
		
		// If teetime requires credit card, show credit card selection window	
		if(model.validationError == 'credit_card_required'){
			var creditCardView = new SelectCreditCardView({model: this.model});
			creditCardView.show();
		}
	},
	
	// Book the teetime
	completeBooking: function(){

		// If user is logged in, book tee time
		if(App.data.user.get('logged_in')){
			
			if(this.model && this.model.isValid()){
				
				// If the selected tee time is available for purchase,
				// show payment method selection
				if(this.model.canPurchase()){
					var paymentMethod = new PaymentSelectionView({model: this.model});
					paymentMethod.show();

				}else{
					this.$el.find('button.book').button('loading');
					App.data.user.get('reservations').create(this.model.attributes, {wait: true});
				}
			}

		// If user not logged in, show log in window
		}else{
			var loginView = new LoginView({reservation: this.model});
			loginView.show();
		}
	}
});
