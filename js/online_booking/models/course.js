var Course = Backbone.Model.extend({
	idAttribute: 'course_id',
	defaults: {
		name: '',
		address: ''
	},
	
	getCurrentSchedule: function(){
		return this.get('name') +':'+ App.data.schedules.findWhere({selected: true}).get('title');
	}
});
