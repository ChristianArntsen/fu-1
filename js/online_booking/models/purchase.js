var Purchase = Backbone.Model.extend({
	idAttribute: 'sale_id',
	
	defaults: {
		total: 0.00
	}
});

var PurchaseCollection = Backbone.Collection.extend({
	url: function(){
		return BASE_API_URL + 'courses/' + App.data.filters.get('course_id') + '/users/purchases';
	},
	model: Purchase
});	
